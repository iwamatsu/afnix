// ---------------------------------------------------------------------------
// - Predwax.cpp                                                             -
// - afnix:wax service - predicates implementation                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "Predwax.hpp"
#include "Boolean.hpp"
#include "XsmHtml.hpp"
#include "XhtmlHr.hpp"
#include "XhtmlBr.hpp"
#include "XhtmlUl.hpp"
#include "XhtmlTh.hpp"
#include "XhtmlTd.hpp"
#include "XhtmlTr.hpp"
#include "XhtmlCol.hpp"
#include "XhtmlCgr.hpp"
#include "XhtmlImg.hpp"
#include "XhtmlDiv.hpp"
#include "XhtmlPre.hpp"
#include "XhtmlRef.hpp"
#include "JsonMime.hpp"
#include "XhtmlMime.hpp"
#include "XhtmlMeta.hpp"
#include "XhtmlLink.hpp"
#include "XhtmlRoot.hpp"
#include "XhtmlPara.hpp"
#include "XhtmlEmph.hpp"
#include "XhtmlTxti.hpp"
#include "XhtmlTxtb.hpp"
#include "XhtmlTxtt.hpp"
#include "Exception.hpp"
#include "XhtmlForm.hpp"
#include "XhtmlText.hpp"
#include "XhtmlThead.hpp"
#include "XhtmlTbody.hpp"
#include "XhtmlTfoot.hpp"
#include "XhtmlTable.hpp"
#include "XhtmlTitle.hpp"
#include "XhtmlEquiv.hpp"
#include "XhtmlStyle.hpp"
#include "XhtmlCanvas.hpp"
#include "XhtmlSubmit.hpp"
#include "XhtmlScript.hpp"
#include "XhtmlOption.hpp"
#include "XhtmlHidden.hpp"
#include "XhtmlCaption.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // xsmhp: xsm html object predicate

  Object* wax_xsmhp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xsm-html-p");
    bool result = (dynamic_cast <XsmHtml*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // jsonp: json mime object predicate
  
  Object* wax_jsonp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "json-mime-p");
    bool result = (dynamic_cast <JsonMime*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xmlmp: xml mime object predicate

  Object* wax_xmlmp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xml-mime-p");
    bool result = (dynamic_cast <XmlMime*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xhtmp: xhtml mime object predicate

  Object* wax_xhtmp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-mime-p");
    bool result = (dynamic_cast <XhtmlMime*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rootp: xhtml root object predicate

  Object* wax_rootp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-root-p");
    bool result = (dynamic_cast <XhtmlRoot*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // htmlp: xhtml html object predicate

  Object* wax_htmlp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-html-p");
    bool result = (dynamic_cast <XhtmlHtml*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // headp: xhtml head object predicate

  Object* wax_headp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-head-p");
    bool result = (dynamic_cast <XhtmlHead*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // bodyp: xhtml body object predicate

  Object* wax_bodyp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-body-p");
    bool result = (dynamic_cast <XhtmlBody*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // thp: xhtml th object predicate

  Object* wax_thp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-th-p");
    bool result = (dynamic_cast <XhtmlTh*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // tdp: xhtml td object predicate

  Object* wax_tdp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-td-p");
    bool result = (dynamic_cast <XhtmlTd*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // trp: xhtml tr object predicate

  Object* wax_trp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-tr-p");
    bool result = (dynamic_cast <XhtmlTr*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // colp: xhtml col object predicate

  Object* wax_colp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-col-p");
    bool result = (dynamic_cast <XhtmlCol*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // cgrp: xhtml cgr object predicate

  Object* wax_cgrp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-cgr-p");
    bool result = (dynamic_cast <XhtmlCgr*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // captp: xhtml caption object predicate
  
  Object* wax_captp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-caption-p");
    bool result = (dynamic_cast <XhtmlCaption*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // telemp: xhtml table element object predicate

  Object* wax_telemp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-telem-p");
    bool result = (dynamic_cast <XhtmlTelem*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // theadp: xhtml table head object predicate

  Object* wax_theadp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-thead-p");
    bool result = (dynamic_cast <XhtmlThead*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // tbodyp: xhtml table body object predicate

  Object* wax_tbodyp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-tbody-p");
    bool result = (dynamic_cast <XhtmlTbody*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // tfootp: xhtml table foot object predicate

  Object* wax_tfootp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-ttoot-p");
    bool result = (dynamic_cast <XhtmlTfoot*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // tablep: xhtml table object predicate

  Object* wax_tablep (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-table-p");
    bool result = (dynamic_cast <XhtmlTable*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // titlep: xhtml title object predicate

  Object* wax_titlep (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-title-p");
    bool result = (dynamic_cast <XhtmlTitle*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // metap: xhtml meta object predicate

  Object* wax_metap (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-meta-p");
    bool result = (dynamic_cast <XhtmlMeta*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // linkp: xhtml link object predicate

  Object* wax_linkp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-link-p");
    bool result = (dynamic_cast <XhtmlLink*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // equivp: xhtml equiv object predicate

  Object* wax_equivp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-equiv-p");
    bool result = (dynamic_cast <XhtmlEquiv*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // stylep: xhtml style object predicate

  Object* wax_stylep (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-style-p");
    bool result = (dynamic_cast <XhtmlStyle*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // canvasp: xhtml canvas object predicate

  Object* wax_canvasp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-canvas-p");
    bool result = (dynamic_cast <XhtmlCanvas*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // scriptp: xhtml script object predicate

  Object* wax_scriptp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-script-p");
    bool result = (dynamic_cast <XhtmlScript*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // imgp: xhtml img object predicate

  Object* wax_imgp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-img-p");
    bool result = (dynamic_cast <XhtmlImg*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // divp: xhtml div object predicate

  Object* wax_divp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-div-p");
    bool result = (dynamic_cast <XhtmlDiv*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // prep: xhtml pre object predicate

  Object* wax_prep (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-pre-p");
    bool result = (dynamic_cast <XhtmlPre*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // refp: xhtml ref object predicate

  Object* wax_refp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-ref-p");
    bool result = (dynamic_cast <XhtmlRef*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // parap: xhtml paragraph object predicate

  Object* wax_parap (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-para-p");
    bool result = (dynamic_cast <XhtmlPara*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // emphp: xhtml emphasize object predicate

  Object* wax_emphp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-emph-p");
    bool result = (dynamic_cast <XhtmlEmph*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // txtip: xhtml text italic object predicate

  Object* wax_txtip (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-i-p");
    bool result = (dynamic_cast <XhtmlTxti*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // txtbp: xhtml text bold object predicate

  Object* wax_txtbp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-b-p");
    bool result = (dynamic_cast <XhtmlTxtb*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // txttp: xhtml text teletype object predicate

  Object* wax_txttp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-tt-p");
    bool result = (dynamic_cast <XhtmlTxtt*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // hrp: xhtml hr object predicate

  Object* wax_hrp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-hr-p");
    bool result = (dynamic_cast <XhtmlHr*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // brp: xhtml r object predicate

  Object* wax_brp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-br-p");
    bool result = (dynamic_cast <XhtmlBr*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // ulp: xhtml ul object predicate

  Object* wax_ulp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-ul-p");
    bool result = (dynamic_cast <XhtmlUl*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // lip: xhtml list item object predicate

  Object* wax_lip (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-li-p");
    bool result = (dynamic_cast <XhtmlLi*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // formp: xhtml form object predicate

  Object* wax_formp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-form-p");
    bool result = (dynamic_cast <XhtmlForm*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // formp: xhtml input text object predicate

  Object* wax_textp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-text-p");
    bool result = (dynamic_cast <XhtmlText*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // subtp: xhtml input submit object predicate

  Object* wax_subtp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-submit-p");
    bool result = (dynamic_cast <XhtmlSubmit*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // optnp: xhtml select option object predicate

  Object* wax_optnp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-option-p");
    bool result = (dynamic_cast <XhtmlOption*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // hidep: xhtml input hidden object predicate

  Object* wax_hidep (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xhtml-hide-p");
    bool result = (dynamic_cast <XhtmlHidden*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
}
