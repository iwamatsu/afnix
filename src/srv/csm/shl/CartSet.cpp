// ---------------------------------------------------------------------------
// - CartSet.cpp                                                             -
// - afnix:csm service - cart set class implementation                       -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Boolean.hpp"
#include "Integer.hpp"
#include "CartSet.hpp"
#include "Runnable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create an empty cart set

  CartSet::CartSet (void) {
    d_acf = false;
    reset ();
  }

  // create an anonymous cart set

  CartSet::CartSet (const bool acf) {
    d_acf = acf;
    reset ();
  }

  // return the object class name

  String CartSet::repr (void) const {
    return "CartSet";
  }

  // reset this cart
  
  void CartSet::reset (void) {
    wrlock ();
    try {
      d_lht.reset ();
      d_aco.reset ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the number of cart in the cart set

  long CartSet::length (void) const {
    rdlock ();
    try {
      long result = d_lht.length ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
	
  // return true if the cart set is empty

  bool CartSet::empty (void) const {
    rdlock ();
    try {
      bool result = d_lht.empty ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // check if a cart exists by rid

  bool CartSet::exists (const String& rid) const {
    rdlock ();
    try {
      bool result = d_lht.exists (rid);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a cart by index

  Cart* CartSet::get (const long idx) const {
    rdlock ();
    try {
      Cart* lco = dynamic_cast <Cart*> (d_lht.getobj (idx));
      unlock ();
      return lco;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a cart by rid

  Cart* CartSet::lookup (const String& rid) const {
    rdlock ();
    try {
      Cart* lco = dynamic_cast <Cart*> (d_lht.lookup (rid));
      unlock ();
      return lco;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add a agent in the cart set

  t_long CartSet::addagt (Agent* agt) {
    wrlock ();
    try {
      if (agt == nilp) {
	throw Exception ("cartset-error", "cannot add nil agent");
      }
      // get the agent rid
      String rid = agt->getrid ();
      if (rid.isnil () == true) {
	// check for valid anonymous
	if (d_acf == false) {
	  throw Exception ("cartset-error", "invalid anonymous agent to add");
	}
	// add the agent and return
	t_long result = d_aco.addagt (agt);
	unlock ();
	return result;
      }
      // check if the cart exists in the hashtable
      Cart* lco = dynamic_cast <Cart*> (d_lht.get (rid));
      if (lco == nilp) {
	lco = new Cart (rid);
	d_lht.add (rid, lco);
      }
      // add the agent in the cart
      t_long result = lco->addagt (agt);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a agent by rid and index

  Agent* CartSet::getagt (const String& rid, const t_long kid) const {
    rdlock ();
    try {
      // check for anonymous
      if (rid.isnil () == true) {
	// check for valid anonymous
	if (d_acf == false) {
	  throw Exception ("cart set-error", "invalid anonymous agent query");
	}
	Agent* agt = d_aco.getagt (kid);
	unlock ();
	return agt;
      }
      // find the cart by rid
      Cart* lco = dynamic_cast <Cart*> (d_lht.lookup (rid));
      if (lco == nilp) {
	throw Exception ("internal-error", "invalid nil cart in lookup");
      }
      // get the rco by index
      Agent* agt = lco->getagt (kid);
      unlock ();
      return agt;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 7;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_GET     = zone.intern ("get");
  static const long QUARK_LOOKUP  = zone.intern ("lookup");
  static const long QUARK_LENGTH  = zone.intern ("length");
  static const long QUARK_EMPTYP  = zone.intern ("empty-p");
  static const long QUARK_EXISTSP = zone.intern ("exists-p");
  static const long QUARK_ADDAGT  = zone.intern ("add-agent");
  static const long QUARK_GETAGT  = zone.intern ("get-agent");

  // create a new object in a generic way

  Object* CartSet::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new CartSet;
    // check for 1 argument
    if (argc == 1) {
      bool acf = argv->getbool (0);
      return new CartSet (acf);
    }
    throw Exception ("argument-error",
                     "too many argument with cart set constructor");
  }

  // return true if the given quark is defined

  bool CartSet::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* CartSet::apply (Runnable* robj, Nameset* nset, const long quark,
			  Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (quark == QUARK_EMPTYP) return new Boolean (empty  ());
    if (quark == QUARK_LENGTH) return new Integer (length ());
    // check for 1 argument
    if (argc == 1) {
      if (quark == QUARK_ADDAGT) {
	Object* obj = argv->get (0);
	Agent*  agt = dynamic_cast <Agent*> (obj);
	if (agt == nilp) {
	  throw Exception ("type-error", "invalid object with cart set add",
			   Object::repr (obj));
	}
	t_long result = addagt (agt);
	return new Integer (result);
      }
      if (quark == QUARK_GET) {
	long idx = argv->getlong (0);
	rdlock ();
	try {
	  Cart* lco = get (idx);
	  robj->post (lco);
	  unlock ();
	  return lco;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_LOOKUP) {
	String rid = argv->getstring (0);
	rdlock ();
	try {
	  Cart* lco = lookup (rid);
	  robj->post (lco);
	  unlock ();
	  return lco;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_EXISTSP) {
	String rid = argv->getstring (0);
	return new Boolean (exists (rid));
      }
    }
    // check for 2 arguments
    if (argc == 2) {
      if (quark == QUARK_GETAGT) {
	String rid = argv->getstring (0);
	t_long kid = argv->getlong (1);
	try {
	  Object* result = getagt (rid, kid);
	  robj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock();
	  throw;
	}
      }
    }
    // call the object method
    return Object::apply (robj, nset, quark, argv);
  }
}
