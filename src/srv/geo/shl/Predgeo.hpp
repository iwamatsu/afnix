// ---------------------------------------------------------------------------
// - Predgeo.hpp                                                             -
// - afnix:geo service - predicates declaration                              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDGEO_HPP
#define  AFNIX_PREDGEO_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// This file contains the predicates associated with the afnix geometry
  /// service.
  /// @author amaury darsch

  /// the Point1 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_pnt1p (Runnable* robj, Nameset* nset, Cons* args);

  /// the Point2 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_pnt2p (Runnable* robj, Nameset* nset, Cons* args);

  /// the Point3 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_pnt3p (Runnable* robj, Nameset* nset, Cons* args);

  /// the Point4 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_pnt4p (Runnable* robj, Nameset* nset, Cons* args);
  
  /// the Vector1 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_vec1p (Runnable* robj, Nameset* nset, Cons* args);

  /// the Vector2 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_vec2p (Runnable* robj, Nameset* nset, Cons* args);

  /// the Vector3 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_vec3p (Runnable* robj, Nameset* nset, Cons* args);

  /// the Vector4 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_vec4p (Runnable* robj, Nameset* nset, Cons* args);

  /// the Quaternion object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_qtrnp (Runnable* robj, Nameset* nset, Cons* args);
  
  /// the solid object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_sldp (Runnable* robj, Nameset* nset, Cons* args);

  /// the cuboid object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_cubp (Runnable* robj, Nameset* nset, Cons* args);

  /// the plane object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* geo_plnp (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
