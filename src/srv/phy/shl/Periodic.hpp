// ---------------------------------------------------------------------------
// - Periodic.hpp                                                            -
// - afnix:phy service - periodic elements class definition                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PERIODIC_HPP
#define  AFNIX_PERIODIC_HPP

#ifndef  AFNIX_PLIST_HPP
#include "Plist.hpp"
#endif

namespace afnix {

  /// The Periodic class is a simple class that holds the fundamental 
  /// properties of the elements. Ths classificaton is based on the
  /// standard periodic element table.
  /// @author amaury darsch

  class Periodic : public Object {
  public:
    /// the element list
    enum t_elem {
      ELEM_H,   // hydrogen
      ELEM_HE,  // helium
      ELEM_LI,  // lithium
      ELEM_BE,  // beryllium
      ELEM_B,   // boron
      ELEM_C,   // carbon
      ELEM_N,   // nitrogen
      ELEM_O,   // oxygen
      ELEM_F,   // fluorine
      ELEM_NE,  // neon
      ELEM_NA,  // sodium
      ELEM_MG,  // magnesium
      ELEM_AL,  // aluminium
      ELEM_SI,  // silicon
      ELEM_P,   // phosphorus
      ELEM_S,   // sulfur
      ELEM_CL,  // chlorine
      ELEM_AR,  // argon
      ELEM_K,   // potassium
      ELEM_CA,  // calcium
      ELEM_SC,  // scandium
      ELEM_TI,  // titanium
      ELEM_V,   // vanadium
      ELEM_CR,  // chromium
      ELEM_MN,  // manganese
      ELEM_FE,  // iron
      ELEM_CO,  // cobalt
      ELEM_NI,  // nickel
      ELEM_CU,  // copper
      ELEM_ZN,  // zinc
      ELEM_GA,  // gallium
      ELEM_GE,  // germanium
      ELEM_AS,  // arsenic
      ELEM_SE,  // selenium
      ELEM_BR,  // bromine
      ELEM_KR,  // krypton
      ELEM_RB,  // rubidium
      ELEM_SR,  // strontium
      ELEM_Y,   // yttrium
      ELEM_ZR,  // zirconium
      ELEM_NB,  // niobium
      ELEM_MO,  // molybdenum
      ELEM_TC,  // technetium
      ELEM_RU,  // ruthenium
      ELEM_RH,  // rhodium
      ELEM_PD,  // palladium
      ELEM_AG,  // silver
      ELEM_CD,  // cadmium
      ELEM_IN,  // indium
      ELEM_SN,  // tin
      ELEM_SB,  // antimony
      ELEM_TE,  // tellurium
      ELEM_I,   // iodine
      ELEM_XE,  // xenon
      ELEM_CS,  // cesium
      ELEM_BA,  // barium
      ELEM_LA,  // lanthanum
      ELEM_CE,  // cerium
      ELEM_PR,  // praseodymium
      ELEM_ND,  // neodymium
      ELEM_PM,  // promethium
      ELEM_SM,  // samarium
      ELEM_EU,  // europium
      ELEM_GD,  // gadolinium
      ELEM_TB,  // terbium
      ELEM_DY,  // dysprosium
      ELEM_HO,  // holmium
      ELEM_ER,  // erbium
      ELEM_TM,  // thulium
      ELEM_YB,  // ytterbium
      ELEM_LU,  // lutetium
      ELEM_HF,  // hafnium
      ELEM_TA,  // tantalum
      ELEM_W,   // tungsten
      ELEM_RE,  // rhenium
      ELEM_OS,  // osmium
      ELEM_IR,  // iridium
      ELEM_PT,  // platinum
      ELEM_AU,  // gold
      ELEM_HG,  // mercury
      ELEM_TL,  // thallium
      ELEM_PB,  // lead
      ELEM_BI,  // bismuth
      ELEM_PO,  // polonium
      ELEM_AT,  // astatine
      ELEM_RN,  // radon
      ELEM_FR,  // francium
      ELEM_RA,  // radium
      ELEM_AC,  // actinium
      ELEM_TH,  // thorium
      ELEM_PA,  // protactinium
      ELEM_U,   // uranium
      ELEM_NP,  // neptunium
      ELEM_PU,  // plutonium
      ELEM_AM,  // americium
      ELEM_CM,  // curium
      ELEM_BK,  // berkelium
      ELEM_CF,  // californium
      ELEM_ES,  // einsteinium
      ELEM_FM,  // fermium
      ELEM_MD,  // mendelevium
      ELEM_NO,  // nobelium
      ELEM_LR,  // lawrencium
      ELEM_RF,  // rutherfordium
      ELEM_DB,  // dubnium
      ELEM_SG,  // seaborgium
      ELEM_BH,  // bohrium
      ELEM_HS,  // hassium 
      ELEM_MT,  // meitnerium
      ELEM_DS,  // darmstadtium
      ELEM_RG,  // roentgenium
      ELEM_CN,  // copernicium
      ELEM_UUT, // ununtrium
      ELEM_UUQ, // ununquadium
      ELEM_UUP, // ununpentium
      ELEM_UUH, // ununhexium
      ELEM_UUS, // ununseptium
      ELEM_UUO  // ununoctium
    };

    /// the atomic properties
    struct s_atom {
      /// the element type
      t_elem d_elem;
      /// the element name
      String d_name;
      /// the symbol name
      String d_symn;
      /// the atomic number
      long   d_anum;
      /// the atomic weight
      t_real d_awht;
      /// the ionization energy
      t_real d_eion;
    };

    /// the band gap properties
    struct s_bgap {
      /// the energy band gap
      t_real d_eg;
      /// the varshni correction factor (alpha)
      t_real d_ega;
      /// the varshni correction factor (beta)
      t_real d_egb;
    };

    /// check if an element exists by name
    /// @param name the element name to check
    static bool exists (const String& name);

  private:
    /// the atomic properties
    s_atom d_atom;
    /// the band gap properties
    s_bgap d_bgap;

  public:
    /// create an element by name
    /// @param name the symbol name
    Periodic (const String& name);

    /// create an element by symbol type
    /// @param elem the element type
    Periodic (const t_elem elem);

    /// copy construct this object
    /// @param that the object to copy
    Periodic (const Periodic& that);

    /// assign an object to this one
    /// @param that the object to assign
    Periodic& operator = (const Periodic& that);

    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// get the atomic properties
    /// @param elem the element type
    virtual s_atom getatom (void) const;
    
    /// get the band gap properties
    /// @param elem the element type
    virtual s_bgap getbgap (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
