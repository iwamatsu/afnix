// ---------------------------------------------------------------------------
// - Rpi.cpp                                                                 -
// - afnix:mth module - real point interface implementation                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Rpi.hpp"
#include "Real.hpp"
#include "Mthsid.hxx"
#include "Vector.hpp"
#include "Integer.hpp"
#include "Runnable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "OutputStream.hpp"
 
namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default point

  Rpi::Rpi (void) {
    d_size = 0;
  }

  // create a point by size

  Rpi::Rpi (const t_long size) {
    // check the size
    if (size < 0) {
      throw Exception ("size-error", "invalid real point size");
    }
    d_size = size;
  }

  // return the anonymous rpi serial code

  t_byte Rpi::serialid (void) const {
    return SERIAL_ARPI_ID;
  }

  // serialize this object
  
  void Rpi::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      // write the point size
      mth_wrlong (d_size, os);
      // write the point data
      for (long k = 0; k < d_size; k++) {
	// get the point value
	t_real val = nlget (k);
	if (val == 0.0) continue;
	// write by position
	mth_wrlong (k,   os);
	mth_wrreal (val, os);
      }
      // write marker for end
      mth_wrlong (-1, os);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this object

  void Rpi::rdstream (InputStream& is) {
    wrlock ();
    try {
      // get the point size
      long size = mth_rdlong (is);
      // resize the point
      resize (size);
      // get the point data by position
      for (long k = 0; k < size; k++) {
	long idx = mth_rdlong (is);
	// check for marker
	if (idx == -1) {
	  unlock ();
	  return;
	}
	t_real val = mth_rdreal (is);
	nlset (idx, val);
      }
      // get the remaining marker
      if (mth_rdlong (is) != -1) {
	throw Exception ("rpi-error", "inconsistent serialized point");
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // compare two points

  bool Rpi::operator == (const Rpi& x) const {
    rdlock ();
    x.rdlock ();
    try {
      // check size first
      if (d_size != x.d_size) {
	throw Exception ("point-error",
			 "incompatible point size with compare");
      }
      // initialize result
      bool result = true;
      // loop in point
      for (t_long i = 0; i < d_size; i++) {
	if (nlget (i) != x.nlget (i)) {
	  result = false;
	  break;
	}
      }
      // unlock and return
      unlock ();
      x.unlock ();
      return result;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // compare two points

  bool Rpi::operator != (const Rpi& x) const {
    return !(*this == x);
  }

  // return the point size

  t_long Rpi::getsize (void) const {
    rdlock ();
    try {
      t_long result = d_size;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // reset this point

  void Rpi::reset (void) {
    wrlock ();
    try {
      d_size = 0LL;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear this point

  void Rpi::clear (void) {
    wrlock ();
    try {
      for (t_long i = 0; i < d_size; i++) nlset (i, 0.0);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set a point by value

  void Rpi::set (const t_real val) {
    wrlock ();
    try {
      for (t_long i = 0; i < d_size; i++) nlset (i, val);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set a point by position

  void Rpi::set (const t_long pos, const t_real val) {
    wrlock ();
    try {
      if ((pos < 0) || (pos >= d_size)) {
	throw Exception ("index-error", "invalid point position");
      }
      nlset (pos, val);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a point by position

  t_real Rpi::get (const t_long pos) const {
    rdlock ();
    try {
      if ((pos < 0) || (pos >= d_size)) {
	throw Exception ("index-error", "invalid point position");
      }
      t_real result = nlget (pos);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 5;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_SET       = zone.intern ("set");
  static const long QUARK_GET       = zone.intern ("get");
  static const long QUARK_CLEAR     = zone.intern ("clear");
  static const long QUARK_RESET     = zone.intern ("reset");
  static const long QUARK_GETSIZE   = zone.intern ("get-size");

  // return true if the given quark is defined

  bool Rpi::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true){
	unlock ();
	return true;
      }
      bool result = hflg ? Serial::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Rpi::apply (Runnable* robj, Nameset* nset, const long quark,
                      Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();

    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETSIZE) return new Integer (getsize ());
      if (quark == QUARK_CLEAR) {
	clear ();
	return nilp;
      }
      if (quark == QUARK_RESET) {
	reset ();
	return nilp;
      }
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_GET) {
        t_long pos = argv->getlong (0);
        return new Real (get (pos));
      }
    }
    // dispatch 2 arguments
    if (argc == 2) {
      if (quark == QUARK_SET) {
        t_long pos = argv->getlong (0);
        t_real val = argv->getreal (1);
	set (pos, val);
        return nilp;
      }
    }
    // call the serial object
    return Serial::apply (robj, nset, quark, argv);
  }
}
