// ---------------------------------------------------------------------------
// - TlsCerts.cpp                                                            -
// - afnix:tls service - tls certificate list class implementation           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Pkcs.hpp"
#include "Vector.hpp"
#include "Strvec.hpp"
#include "Integer.hpp"
#include "TlsCerts.hpp"
#include "TlsFormat.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "InputMapped.hpp"

namespace afnix {
  
  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // this procedure create a chunk from a buffer
  static TlsChunk tls_tochunk (const Buffer& buf) {
    TlsChunk result;
    // add the buffer length
    t_quad blen = buf.length ();
    result.add ((t_byte) ((blen & 0x00FF0000) >> 16));
    result.add ((t_byte) ((blen & 0x0000FF00) >> 8));
    result.add ((t_byte)  (blen & 0x000000FF));
    // add the buffer
    for (long k = 0L; k < blen; k++) result.add (buf.get (k));
    return result;
  }
  
  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create an empty certificate list 

  TlsCerts::TlsCerts (void) {
    reset ();
  }

  // create a certificate list by names

  TlsCerts::TlsCerts (const String& cert) {
    reset ();
    addcert (cert);
  }

  // create a certificate list by names and key

  TlsCerts::TlsCerts (const String& cert, const String& ckey) {
    reset ();
    addcert (cert);
    setckey (ckey);
  }

  // create a certificate list by names and key information

  TlsCerts::TlsCerts (const String& cert,
		      const String& ckey, const String& kfmt) {
    reset ();
    addcert (cert);
    setckey (ckey, kfmt);
  }
  // return the class name
  
  String TlsCerts::repr (void) const {
    return "TlsCerts";
  }

  // reset this class

  void TlsCerts::reset (void) {
    wrlock ();
    try {
      d_cert = "";
      d_ckey = "";
      d_kfmt = "";
      d_vbuf.reset ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the certificate list info as a plist

  Plist TlsCerts::getinfo (void) const {
    rdlock ();
    try {
      // create a result plist
      Plist plst;
      plst.add ("TLS-HSK-CERT", "TLS SERVER CERTIFICATE LIST", d_cert);
      // here it is
      unlock ();
      return plst;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the number of certificates

  long TlsCerts::length (void) const {
    rdlock ();
    try {
      long result = d_vbuf.length ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // add certificates

  void TlsCerts::addcert (const String& cert) {
    wrlock ();
    try {
      // split the list name
      Strvec clst = Strvec::split (cert, ":");
      long   clen = clst.length ();
      // check for null or multiple add
      if (clen == 0) {
	unlock ();
	return;
      }
      if (clen > 1L) {
	for (long k = 0L; k < clen; k++) addcert (clst.get (k));
	unlock ();
	return;
      }
      // single add starts here
      Buffer* buf = TlsFormat::tobuffer (cert);
      if (buf != nilp) {
	d_vbuf.add (buf);
	if (d_cert.isnil () == true) {
	  d_cert = cert;
	} else {
	  d_cert = d_cert + ':' + cert;
	}
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the certificate key

  void TlsCerts::setckey (const String& ckey) {
    wrlock ();
    try {
      d_ckey = ckey;
      d_kfmt = TlsFormat::tostring (TlsFormat::getkdef ());
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the certificate key information

  void TlsCerts::setckey (const String& ckey, const String& kfmt) {
    wrlock ();
    try {
      d_ckey = ckey;
      d_kfmt = kfmt;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // map a certificate list to a chunk block

  TlsChunk TlsCerts::tochunk (void) const {
    rdlock ();
    try {
      // the certificate list chunk
      TlsChunk clc;
      // loop in the certificate list
      long vlen = d_vbuf.length ();
      for (long k = 0; k < vlen; k++) {
	// get the buffer by index
	Buffer* buf = dynamic_cast <Buffer*> (d_vbuf.get (k));
	if (buf == nilp) {
	  throw Exception ("tls-error", "cannot map certificate buffer");
	}
	// map the certificate chunk
	TlsChunk cc = tls_tochunk (*buf);
	// merge it in the list chunk
	clc.add (cc);
      }
      // create the result chunk
      TlsChunk result;
      t_quad clen = clc.length ();
      result.add ((t_byte) ((clen & 0x00FF0000) >> 16));
      result.add ((t_byte) ((clen & 0x0000FF00) >> 8));
      result.add ((t_byte)  (clen & 0x000000FF));
      result.add (clc);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // open the certificate key
  
  Key* TlsCerts::openkey (void) const {
    rdlock ();
    Key* result = nilp;
    try {
      // get the key format
      TlsFormat::t_kfmt kfmt = TlsFormat::tokfmt (d_kfmt);
      // PKCS 1 key format
      if (kfmt == TlsFormat::KFMT_PK1) {
	Pkcs1 pkcs (d_ckey);
	result = new Key (*pkcs.getkey ());
      }
      unlock ();
      return result;
    } catch (...) {
      delete result;
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 4;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_ADD     = zone.intern ("add");
  static const long QUARK_LENGTH  = zone.intern ("length");
  static const long QUARK_SETKEY  = zone.intern ("set-key");
  static const long QUARK_OPENKEY = zone.intern ("open-key");

  // create a new object in a generic way

  Object* TlsCerts::mknew (Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) return new TlsCerts;
    // check for 1 argument
    if (argc == 1) {
      String cert = argv->getstring (0);
      return new TlsCerts (cert);
    }
    // check for 2 arguments
    if (argc == 2) {
      String cert = argv->getstring (0);
      String ckey = argv->getstring (1);
      return new TlsCerts (cert, ckey);
    }
    // check for 3 arguments
    if (argc == 2) {
      String cert = argv->getstring (0);
      String ckey = argv->getstring (1);
      String kfmt = argv->getstring (2);
      return new TlsCerts (cert, ckey, kfmt);
    }
    // too many arguments
    throw Exception ("argument-error", 
                     "too many argument with tls certificate list");
  }

  // return true if the given quark is defined

  bool TlsCerts::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? TlsInfos::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark

  Object* TlsCerts::apply (Runnable* robj, Nameset* nset, const long quark,
			   Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_LENGTH)   return new Integer (length ());
      if (quark == QUARK_OPENKEY)  return openkey ();
    }
    // check for 1 argument
    if (argc == 1) {
      if (quark == QUARK_ADD) {
	String cert = argv->getstring (0);
	addcert (cert);
	return nilp;
      }
      if (quark == QUARK_SETKEY) {
	String ckey = argv->getstring (0);
	setckey (ckey);
	return nilp;
      }
    }
    // check for 2 arguments
    if (argc == 2) {
      if (quark == QUARK_SETKEY) {
	String ckey = argv->getstring (0);
	String kfmt = argv->getstring (1);
	setckey (ckey, kfmt);
	return nilp;
      }
    }
    // call the tls infos method
    return TlsInfos::apply (robj, nset, quark, argv);
  }
}
