// ---------------------------------------------------------------------------
// - Geometry.hpp                                                            -
// - afnix:geo service - base  class definition                         -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_GEOMETRY_HPP
#define  AFNIX_GEOMETRY_HPP

#ifndef  AFNIX_PLIST_HPP
#include "Plist.hpp"
#endif

#ifndef  AFNIX_NAMEABLE_HPP
#include "Nameable.hpp"
#endif

namespace afnix {

  /// The Geometry class is a base class for the afnix geometry service. A
  /// geometry is an abstract class which is used to model shapes and solid.
  /// Other form of geometry might include curved space or multi-dimensional
  /// representation which can be embedded into a 2 or 3 dimensional space.
  /// @author amaury darsch

  class Geometry : public Nameable {
    /// the geometry uuid
    String d_uuid;
    
  public:
    /// reset this geometry
    virtual void reset (void) =0;

    /// @return the geometry uuid
    virtual String getuuid (void) const;

    /// set the geometry by plist
    /// @param plst the geometry plist
    virtual void setplst (const Plist& plst) =0;

    /// @return the geometry plist
    virtual Plist getplst (void) const =0;
    
  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
