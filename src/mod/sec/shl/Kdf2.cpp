// ---------------------------------------------------------------------------
// - Kdf2.cpp                                                                -
// - afnix:sec module - key derivation function (KDF2) class implementation  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Kdf2.hpp"
#include "Vector.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // KDF2 algorithm constants
  static const String KDF_ALGO_NAME = "KDF-2";

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a kdf by hasher and size

  Kdf2::Kdf2 (Hasher* hobj,const long kbsz) : Hkdf (hobj,KDF_ALGO_NAME,kbsz) {
    reset ();
  }
  
  // return the class name

  String Kdf2::repr (void) const {
    return "Kdf2";
  }

  // derive the key from an octet string

  void Kdf2::derive (const t_byte* ostr, const long size) {
    wrlock ();
    try {
      // do nothing but reset without hash
      if (p_hobj == nilp) {
	reset  ();
	unlock ();
	return;
      }
      // get the hasher result length
      long rlen = p_hobj->getrlen ();
      // get the max counter and index
      long cmax = d_kbsz / rlen;
      if ((d_kbsz % rlen) != 0) cmax++;
      long kidx = 0;
      // key generation loop
      for (long i = 1; i <= cmax; i++) {
	// reset the hasher
	p_hobj->reset ();
	// generate counter string
	t_byte bcnt[4];
	bcnt[0] = (t_byte) ((i >> 24) & 0x000000FF);
	bcnt[1] = (t_byte) ((i >> 16) & 0x000000FF);
	bcnt[2] = (t_byte) ((i >> 8)  & 0x000000FF);
	bcnt[3] = (t_byte) (i & 0x000000FF);
	// hash the data
	p_hobj->process (ostr, size);
	p_hobj->process (bcnt, 4);
	p_hobj->finish  ();
	// copy the hasher result
	for (long j = 0; j < rlen; j++) {
	  if (kidx >= d_kbsz) break;
	  p_kbuf[kidx++] = p_hobj->getbyte (j);
	}
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // create a new object in a generic way
  
  Object* Kdf2::mknew (Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 2 argument
    if (argc == 2) {
      // get the hasher object
      Object*  obj = argv->get (0);
      Hasher* hobj = dynamic_cast <Hasher*> (obj);
      if (hobj == nilp) {
	throw Exception ("type-error", "invalid object with KDF2 constructor",
			 Object::repr (obj));
      }
      // get the key size
      long kbsz = argv->getlong (1);
      return new Kdf2 (hobj, kbsz);
    }
    // invalid arguments
    throw Exception ("argument-error", "too many arguments for KDF2");
  }
}
