<?xml version="1.0" encoding="UTF-8"?>

<!-- ====================================================================== -->
<!-- = client.xml                                                         = -->
<!-- = afnix cross librarian manual                                       = -->
<!-- ====================================================================== -->
<!-- = This  program  is  free  software; you  can redistribute it and/or = -->
<!-- = modify it provided that this copyright notice is kept intact.      = -->
<!-- = This program is distributed in the hope that it will be useful but = -->
<!-- = without  any  warranty;  without  even  the  implied  warranty  of = -->
<!-- = merchantability or fitness for  a  particular purpose. In no event = -->
<!-- = shall  the  copyright  holder be liable for any  direct, indirect, = -->
<!-- = incidental  or special  damages arising  in any way out of the use = -->
<!-- = of this software.                                                  = -->
<!-- ====================================================================== -->
<!-- = copyright (c) 1999-2017 - amaury darsch                            = -->
<!-- ====================================================================== -->

<client>
  <!-- client name -->
  <name>axl</name>
  <!-- synopsis -->
  <synopsis>axl [options] file...</synopsis>
  <!-- options -->
  <options>
    <optn>
      <name>h</name>
      <p>
	prints the help message
      </p>
    </optn>

    <optn>
      <name>v</name>
      <p>
	prints the program version
      </p>
    </optn>

    <optn>
      <name>c</name>
      <p>
	create a new librarian
      </p>
    </optn>

    <optn>
      <name>x</name>
      <p>
	extract from a librarian
      </p>
    </optn>

    <optn>
      <name>t</name>
      <p>
	write a detailed list of the files in the librarian
      </p>
    </optn>

    <optn>
      <name>s</name>
      <p>
	write a short list of the files in the librarian
      </p>
    </optn>

    <optn>
      <name>f</name>
      <args>file</args>
      <p>
	set the librarian file name
      </p>
    </optn>

    <optn>
      <name>m</name>
      <args>name</args>
      <p>
	set the the start module
      </p>
    </optn>
  </options>

  <!-- description -->
  <remark>
    <title>Description</title>
    
    <p>
      <product>axl</product> invokes the cross librarian utility. A
      librarian is a collection of files that are read by the
      interpreter. A librarian is similar to an archive. The idea is to
      store a complete project into a single file and let the
      interpreter to find them. A librarian is created with the
      <option>c</option> option. Extracting a file or all files is
      performed with the <option>x</option> option. Both options
      <option>c</option> and <option>x</option> are mutually
      exclusive. The start module can be set with the <option>m</option>.
    </p>
  </remark>

  <!-- version -->
  <remark>
    <title>Version</title>
    
    <p>
      The current version is the <major/>.<minor/>.<patch/> release.
    </p>
  </remark>

  <!-- see also -->
  <remark>
    <title>See also</title>

    <p>
      <link url="axc.xht">axc</link>,
      <link url="axd.xht">axd</link>,
      <link url="axi.xht">axi</link>,
    </p>
  </remark>

  <!-- notes -->
  <remark>
    <title>Notes</title>

    <p>
      The distribution comes with an extensive documentation. The
      documentation is available <link
      url="http://www.afnix.org">online</link> 
      or in the <path>doc</path> directory in the form of formatted
      xhtml documents. 
    </p>
  </remark>

  <!-- author -->
  <remark>
    <title>Author</title>
    
    <p>
      <product>axl</product> has been written by
      <mail address="amaury@afnix.org">Amaury Darsch</mail>.
    </p>
  </remark>
</client>
