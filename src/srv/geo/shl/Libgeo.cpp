// ---------------------------------------------------------------------------
// - Libgeo.cpp                                                              -
// - afnix:geo service - declaration & implementation                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Meta.hpp"
#include "Plane.hpp"
#include "Cuboid.hpp"
#include "Libgeo.hpp"
#include "Predgeo.hpp"
#include "Vector1.hpp"
#include "Vector2.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"
#include "Function.hpp"
#include "Quaternion.hpp"

namespace afnix {

  // initialize the afnix:geo service

  Object* init_afnix_geo (Interp* interp, Vector* argv) {
    // make sure we are not called from something crazy
    if (interp == nilp) return nilp;

    // create the afnix:geo nameset
    Nameset* aset = interp->mknset ("afnix");
    Nameset* gset = aset->mknset   ("geo");

    // bind all symbols in the afnix:geo nameset
    gset->symcst ("Point1",          new Meta (Point1::mknew));
    gset->symcst ("Point2",          new Meta (Point2::mknew));
    gset->symcst ("Point3",          new Meta (Point3::mknew));
    gset->symcst ("Point4",          new Meta (Point4::mknew));
    gset->symcst ("Vector1",         new Meta (Vector1::mknew));
    gset->symcst ("Vector2",         new Meta (Vector2::mknew));
    gset->symcst ("Vector3",         new Meta (Vector3::mknew));
    gset->symcst ("Vector4",         new Meta (Vector4::mknew));
    gset->symcst ("Quaternion",      new Meta (Quaternion::mknew));

    gset->symcst ("Plane",           new Meta (Plane::mknew));
    gset->symcst ("Cuboid",          new Meta (Cuboid::mknew));

    // bind the predicates
    gset->symcst ("point1-p",        new Function (geo_pnt1p));
    gset->symcst ("point2-p",        new Function (geo_pnt2p));
    gset->symcst ("point3-p",        new Function (geo_pnt3p));
    gset->symcst ("point4-p",        new Function (geo_pnt4p));
    gset->symcst ("vector1-p",       new Function (geo_vec1p));
    gset->symcst ("vector2-p",       new Function (geo_vec2p));
    gset->symcst ("vector3-p",       new Function (geo_vec3p));
    gset->symcst ("vector4-p",       new Function (geo_vec4p));
    gset->symcst ("quaternion-p",    new Function (geo_qtrnp));

    gset->symcst ("solid-p",         new Function (geo_sldp));
    gset->symcst ("plane-p",         new Function (geo_plnp));
    gset->symcst ("cuboid-p",        new Function (geo_cubp));

    // not used but needed
    return nilp;
  }
}

extern "C" {
  afnix::Object* dli_afnix_geo (afnix::Interp* interp, afnix::Vector* argv) {
    return init_afnix_geo (interp, argv);
  }
}
