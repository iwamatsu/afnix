// ---------------------------------------------------------------------------
// - t_cdir.cpp                                                              -
// - standard platform library - simple directory tester module              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "cdir.hpp"
#include "cstr.hpp"

int main (int argc, char**) {
  using namespace afnix;

  // check simple path
  char buf[512];
  c_strcpy (buf, "hello");
  char* fname = c_xname (buf);
  if (c_strcmp (fname, "hello") == false) return 1;
  delete [] fname; fname = nilp;

  // check root name
  buf[0] = c_getdsep ();
  buf[1] = nilc;
  c_strcat (buf, "hello");
  fname  = c_xname (buf);
  if (c_strcmp (fname, "hello") == false) return 1;
  delete [] fname;

  // check multiple name
  buf[6] = c_getdsep ();
  buf[7] = nilc;
  c_strcat (buf, "world");
  fname  = c_xname (buf);
  if (c_strcmp (fname, "world") == false) return 1;
  delete [] fname;

  // check for extension removal
  char* name = c_rmext ("t_csio.cpp");
  if (c_strcmp (name, "t_csio") == false) return 1;
  delete [] name;
  char* noxt = c_rmext ("t_csio");
  if (c_strcmp (noxt, "t_csio") == false) return 1;
  delete [] noxt;

  // check for extension extraction
  char* ext = c_xext ("./t_csio.cpp");
  if (c_strcmp (ext, "cpp") == false) return 1;
  if (c_xext   ("t_csio")   != nilp)  return 1;
  delete [] ext;

  // ok this is enough
  return 0;
}
