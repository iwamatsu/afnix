// ---------------------------------------------------------------------------
// - cgen.cxx                                                                -
// - standard platform library - C generator functions implementation        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "cthr.hpp"
#include "csys.hpp"
#include "cclk.hpp"
#include "cgen.hpp"
#include "cgen.hxx"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the twister internal constants
  static const long   MT_NN = 312L;
  static const long   MT_NM = MT_NN - 1L;
  static const long   MT_MM = 156L;
  static const t_octa MT_MA = 0xB5026F5AA96619E9ULL;
  static const t_octa MT_UM = 0xFFFFFFFF80000000ULL;
  static const t_octa MT_LM = 0x000000007FFFFFFFULL;
  static const t_octa MT_MG[2] = {0ULL, MT_MA};
  
  // static mutex creation function
  static void* mtx_create (void);
  // mutex or network services
  static void* mtx = mtx_create ();

  // this function destroy the mutex at exit
  static void mtx_destroy (void) {
    c_mtxdestroy (mtx);
  }

  // this function initialize a mutex statically and register its
  // destruction to be done at exit
  static void* mtx_create (void) {
    void* mtx = c_mtxcreate ();
    c_atexit (mtx_destroy);
    return mtx;
  }

  // the mersenne-twister [MT19937-64] pseudo-random number generator  
  struct s_mtprng {
    // the mt states
    t_octa d_state[MT_NN];
    // the mt index
    long   d_index;
    // create a default mt with seed 5489
    s_mtprng (void) {
      ldseed (5489ULL);
    }
    // create a mt with a seed
    s_mtprng (const t_octa seed) {
      ldseed (seed);
    }
    // load the generator with a seed
    void ldseed (const t_octa seed) {
      // set the states
      d_state[0] = seed;
      for (long k = 1L; k < MT_NN; k++) {
	t_octa   x = d_state[k-1] ^ (d_state[k-1] >> 62);
	d_state[k] = 0x5851F42D4C957F2DULL * x + (t_octa) k;
      }
      // reset the index
      d_index = 0;
    }
    // generate the random state
    void setrnd (void) {
      // do nothing if the index is not null
      if (d_index != 0) return;
      // make a round
      for (long k = 0L; k < MT_MM; k++) {
	t_octa x = (d_state[k] & MT_UM) | (d_state[k+1] & MT_LM);
	d_state[k] = d_state[k + MT_MM]^(x >> 1)^ MT_MG[(long)(x & 1ULL)];
      }
      for (long k = MT_MM; k < MT_NM; k++) {
	t_octa   x = (d_state[k] & MT_UM) | (d_state[k+1] & MT_LM);
	d_state[k] = d_state[k+MT_MM-MT_NN]^(x >> 1)^MT_MG[(long)(x & 1ULL)];
      }
      t_octa x = (d_state[MT_NM] & MT_UM) | (d_state[0] & MT_LM);
      d_state[MT_NM] = d_state[MT_MM-1] ^ (x >> 1)^MT_MG[(long)(x & 1ULL)];
    }
    // get a random value
    t_octa getrnd (void) {
      // generate a state if the index is null
      if (d_index == 0) setrnd ();
      // extract the number
      t_octa y = d_state[d_index];
      y ^= (y >> 29) & 0x5555555555555555ULL;
      y ^= (y << 17) & 0x71D67FFFEDA60000ULL;
      y ^= (y << 37) & 0xFFF7EEE000000000ULL;
      y ^= (y >> 43);
      // adjust the index
      d_index = (d_index + 1L) % MT_NN;
      // here it is
      return y;
    }
  };

  // the maximum values for MT19937-64
  static const t_real CGEN_PRNG_RMAX = 1.0 / 9007199254740991.0;
  static const t_real CGEN_PRNG_NMAX = 1.0 / 9007199254740992.0;

  // the random engine seeding flag
  static bool cgen_sflg = false;
  // the mersenne-twister (64) prng
  static s_mtprng cgen_prng;

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // initialize the random generator

  void c_initrnd (void) {
    // compute a seed
    t_octa seed = (t_octa) (c_time () * c_getpid ());
    // seed the random generator
    c_mtxlock (mtx);
    cgen_prng.ldseed (seed);
    cgen_sflg = true;
    c_mtxunlock (mtx);
  }

  // return a real random number between 0.0 and 1.0

  t_real c_realrnd (const bool iflg) {
    c_mtxlock (mtx);
    t_octa rval = cgen_prng.getrnd ();
    t_real result = 0.0;
    if (iflg == true) {
      result = (rval >> 11) * CGEN_PRNG_RMAX;
    } else {
      result = (rval >> 11) * CGEN_PRNG_NMAX;
    }
    c_mtxunlock (mtx);
    return result;
  }

  // return a random byte

  t_byte c_byternd (void) {
    return (t_byte) (0xFF * c_realrnd (true));
  }

  // return a random word

  t_word c_wordrnd (void) {
    return (t_word) (0xFFFF * c_realrnd (true));
  }

  // return a quad random number

  t_quad c_quadrnd (void) {
    return (t_quad) (0xFFFFFFFF * c_realrnd (true));
  }

  // return an octa random number

  t_octa c_octarnd (void) {
    c_mtxlock (mtx);
    t_octa result = cgen_prng.getrnd ();
    c_mtxunlock (mtx);
    return result;
  }
}
