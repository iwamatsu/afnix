// ---------------------------------------------------------------------------
// - Saas.hpp                                                                -
// - afnix:csm service - software as a service class definition              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SAAS_HPP
#define  AFNIX_SAAS_HPP

#ifndef  AFNIX_RAAS_HPP
#include "Raas.hpp"
#endif

namespace afnix {

  /// The Saas class is the class registering a server that acts as a
  /// 'software as a service'. The saas is identified by its name, host, port
  /// and supported protocol. When a server register itself, the saas
  /// is validated afetr a succesfull capability query.
  /// @author amaury darsch

  class Saas : public Raas {
  public:
    /// create a service by name
    /// @param name he service name
    Saas (const String& name);

    /// create a service by name and info
    /// @param name he service name
    /// @param info he service info
    Saas (const String& name, const String& info);

    /// create a service by plist
    /// @param plst the service plist
    Saas (const Plist& plst);

    /// copy construct this service
    /// @param that the object to copy
    Saas (const Saas& that);

    /// assign a service to this one
    /// @param that the object to assign
    Saas& operator = (const Saas& that);
    
    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// set the service host
    /// @param host the host name
    virtual void sethost (const String& host);

    /// @return the service host name
    virtual String gethost (void) const;

    /// set the service port
    /// @param port the host port number
    virtual void setport (const long port);

    /// @return the service port number
    virtual long getport (void) const;

    /// set the service protocol
    /// @param prto the service protocol
    virtual void setprto (const String& prto);

    /// @return the service protocol
    virtual String getprto (void) const;

    /// check of a service as the same authority
    /// @param xas the service to check
    virtual bool isauth (const Saas& xas) const;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
