// ---------------------------------------------------------------------------
// - Vector.hpp                                                              -
// - standard object library - dynamic vector class definition               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_VECTOR_HPP
#define  AFNIX_VECTOR_HPP

#ifndef  AFNIX_SERIAL_HPP
#include "Serial.hpp"
#endif

#ifndef  AFNIX_ITERABLE_HPP
#include "Iterable.hpp"
#endif

#ifndef  AFNIX_COLLECTABLE_HPP
#include "Collectable.hpp"
#endif

namespace afnix {

  /// The Vector class implementes a dynamic array. It is by far less efficent
  /// than an array where the size of the array is known in advance. This
  /// class should be used when an indexed container is required.
  /// @author amaury darsch

  class Vector : public Iterable, public Serial, public Collectable {
  private:
    /// the vector size
    long d_size;
    /// the vector length
    long d_vlen;
    /// the allocated vector
    Object** p_vobj;

  public:
    /// create an empty vector.
    Vector (void);

    /// create a vector with a predefined allocated size
    /// @param size the requested size
    Vector (const long size);

    /// copy construct this vector
    /// @param that the vector to copy 
    Vector (const Vector& that);

    /// copy move this vector
    /// @param that the vector to move 
    Vector (Vector&& that);

    /// destroy this vector
    ~Vector (void);

    /// assign a vector to this one
    /// @param that the vector to assign
    Vector& operator = (const Vector& that);

    /// move a vector to this one
    /// @param that the vector to move
    Vector& operator = (Vector&& that);

    /// @return the class name
    String repr (void) const;

    /// @return the vector serial code
    t_byte serialid (void) const;

    /// serialize this vector to an output stream
    /// @param os the output stream to write
    void wrstream (class OutputStream& os) const;

    /// deserialize a vector from an input stream
    /// @param is the input steam to read in
    void rdstream (class InputStream& is);

    /// @return a clone of this object
    Object* clone (void) const;

    /// release this object links
    void release (void);
    
    /// reset this vector
    void reset (void);

    /// @return true if the vector is empty
    bool empty (void) const;

    /// @return true if the object exists
    bool exists (Object* object) const;

    /// @return the number of objects in this vector
    long length (void) const;

    /// add an element in this vector
    /// @param object the object to add
    void add (Object* object);

    /// add an element in this vector at a certain index
    /// @param index  the object index 
    /// @param object the object to add
    void add (const long index, Object* object);

    /// set an object at a given position
    /// @param index the vector index
    /// @param object the object to set
    void set (const long index, Object* object);

    /// get an object at a certain index
    /// @param index the vector index
    /// @return the object at this position
    Object* get (const long index) const;

    /// @return the first object in this vector
    Object* first (void) const;

    /// @return the last object in this vector
    Object* last (void) const;

    /// @return the first object in the vector
    Object* pop (void);

    /// pop the last object from this vector
    Object* rml (void);

    /// find and object in this vector
    /// @param object the object to find
    long find (Object* object);

    /// remove and object by index and repack the vector
    /// @param index the object index to remove
    void remove (const long index);

    /// remove and object and repack the vector
    /// @param object the object to remove
    void remove (Object* object);

    /// shift the vector by one element and return a new one
    Vector* shift (void) const;

    /// merge a vector into this one
    /// @param v the vector to merge
    void merge (const Vector& v);

    /// @return a long integer value by object index
    t_long getlong (const long index) const;

    /// @return an octa value by object index
    t_octa getocta (const long index) const;

    /// @return a real value by object index
    t_real getreal (const long index) const;

    /// @return a real value by object index, even from an integer
    t_real getrint (const long index) const;

    /// @return a boolean value by object index
    bool getbool (const long index) const;

    /// @return a byte value by object index
    t_byte getbyte (const long index) const;

    /// @return a word value by object index
    t_word getword (const long index) const;

    /// @return a character value by object index
    t_quad getchar (const long index) const;

    /// @return a string value by object index
    String getstring (const long index) const;

  private:
    // make the vector iterator a friend
    friend class Vectorit;

  public:
    /// @return a new iterator for this vector
    Iterator* makeit (void);

    /// create an iterator at the begining
    class Vectorit begin (void);

    /// create an iterator at the end
    class Vectorit end (void);

  public:
    /// generate an object of evaluated arguments
    /// @param robj the current runnable
    /// @param nset the current nameset
    /// @param args the arguments to evaluate
    /// @return a vector of evaluated argument
    static Vector* eval (Runnable*robj, Nameset* nset, Cons* args);
    
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
  
  /// The Vectorit class is the iterator for the vector class. Such 
  /// iterator is constructed with the "makeit" vector method. The iterator
  /// is reset to the beginning of the vector.
  /// @author amaury darsch

  class Vectorit : public Iterator {
  private:
    /// the vector to iterate
    Vector* p_vobj;
    /// the iterator vector index
    long    d_vidx;

  public:
    /// create a new iterator from a vector
    /// @param vobj the vector to iterate
    Vectorit (Vector* vobj);

    /// copy construct this iterator
    /// @param that the object to copy
    Vectorit (const Vectorit& that);
    
    /// destroy this vector iterator
    ~Vectorit (void);

    /// assign an iterator to this one
    /// @param that the object to assign
    Vectorit& operator = (const Vectorit& that);

    /// compare two iterators
    /// @param it the iteraror to compare
    bool operator == (const Vectorit& it) const;

    /// compare two iterators
    /// @param it the iteraror to compare
    bool operator != (const Vectorit& it) const;

    /// move the iterator to the next position
    Vectorit& operator ++ (void);
    
    /// move the iterator to the previous position
    Vectorit& operator -- (void);
    
    /// @return the iterator object
    Object* operator * (void) const;
    
    /// @return the class name
    String repr (void) const;

    /// reset the iterator to the begining
    void begin (void);

    /// reset the iterator to the end
    void end (void);

    /// move the vector iterator to the next position
    void next (void);

    /// move the vector iterator to the previous position
    void prev (void);

    /// @return the object at the current position
    Object* getobj (void) const;

    /// @return true if the iterator is at the end
    bool isend (void) const;
  };
}

#endif
