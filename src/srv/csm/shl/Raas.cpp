// ---------------------------------------------------------------------------
// - Raas.cpp                                                                -
// - afnix:csm service - registration as a service class implementation      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Raas.hpp"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {
 
  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the default rid
  static const String RAAS_XRID_XDEF = "default@localdomain";
  // the default active mode
  static const bool   RAAS_MODE_XDEF = true;

  // the rid property name
  static const String RAAS_XRID_NAME = "RAAS-RID";
  static const String RAAS_XRID_INFO = "RAAS REGISTRATION ID";

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default service

  Raas::Raas (void) {
    d_mode = RAAS_MODE_XDEF;
    setrid (RAAS_XRID_XDEF);
  }

  // create a service by name

  Raas::Raas (const String& name) : Plist (name) {
    d_mode = RAAS_MODE_XDEF;
    setrid (RAAS_XRID_XDEF);
  }

  // create a service by name and info

  Raas::Raas (const String& name, const String& info) : Plist (name, info) {
    d_mode = RAAS_MODE_XDEF;
    setrid (RAAS_XRID_XDEF);
  }

  // create a service by plist

  Raas::Raas (const Plist& plst) : Plist (plst) {
    d_mode = RAAS_MODE_XDEF;
    if (plst.exists (RAAS_XRID_NAME) == false) setrid (RAAS_XRID_XDEF);
  }

  // copy construct this account

  Raas::Raas (const Raas& that) {
    that.rdlock ();
    try {
      // copy the base table
      Plist::operator = (that);
      // copy locally
      d_mode = that.d_mode;
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }

  // assign a service to this one

  Raas& Raas::operator = (const Raas& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      // copy the base table
      Plist::operator = (that);
      // copy locally
      d_mode = that.d_mode;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }
  
  // get the class name

  String Raas::repr (void) const {
    return "Raas";
  }

  // return a clone of this object

  Object* Raas::clone (void) const {
    return new Raas (*this);
  }
  
  // set the registration id

  void Raas::setrid (const String& rid) {
    wrlock ();
    try {
      if (Plist::exists (RAAS_XRID_NAME) == true) {
	Plist::set (RAAS_XRID_NAME, rid);
      } else {
	Plist::add (RAAS_XRID_NAME, RAAS_XRID_INFO, rid);
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the registration id

  String Raas::getrid (void) const {
    rdlock ();
    try {
      String result = Plist::getpval (RAAS_XRID_NAME);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the service mode

  void Raas::setmode (const bool mode) {
    wrlock ();
    try {
      d_mode = mode;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the service port

  bool Raas::getmode (void) const {
    rdlock ();
    try {
      bool result = d_mode;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------
  
  // the quark zone
  static const long QUARK_ZONE_LENGTH = 4;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_SETRID  = zone.intern ("set-rid");
  static const long QUARK_GETRID  = zone.intern ("get-rid");
  static const long QUARK_SETMODE = zone.intern ("set-mode");
  static const long QUARK_GETMODE = zone.intern ("get-mode");

  // create a new object in a generic way

  Object* Raas::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) return new Raas;
    // check for 1 argument
    if (argc == 1) {
      Object* obj = argv->get (0);
      // check for a string
      String* sobj = dynamic_cast <String*> (obj);
      if (sobj != nilp) return new Raas (*sobj);
      // check for a plist
      Plist* pobj = dynamic_cast <Plist*> (obj);
      if (pobj != nilp) return new Raas (*pobj);
      // invalid argument
      throw Exception ("type-error", "invalid object with raas constructor",
		       Object::repr (obj));
    }
    // check for 2 arguments
    if (argc == 2) {
      String name = argv->getstring (0);
      String info = argv->getstring (1);
      return new Raas (name, info);
    }
    // invalid arguments
    throw Exception ("argument-error", 
                     "invalid arguments with raas constructor");
  }
  
  // return true if the given quark is defined

  bool Raas::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? Plist::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Raas::apply (Runnable* robj, Nameset* nset, const long quark,
		       Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETRID)  return new String  (getrid  ());
      if (quark == QUARK_GETMODE) return new Boolean (getmode ());
    }
    if (argc == 1) {
      if (quark == QUARK_SETRID) {
	String rid = argv->getstring (0);
	setrid (rid);
	return nilp;
      }
      if (quark == QUARK_SETMODE) {
	long mode = argv->getbool (0);
	setmode (mode);
	return nilp;
      }
    }
    // call the table methods
    return Plist::apply (robj, nset, quark, argv);
  }
}
