// ---------------------------------------------------------------------------
// - Predmth.cpp                                                             -
// - afnix:mth module - predicates implementation                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Qr.hpp"
#include "Mgs.hpp"
#include "Cgs.hpp"
#include "Bcs.hpp"
#include "Cov.hpp"
#include "Mean.hpp"
#include "Tqmr.hpp"
#include "Cons.hpp"
#include "Ippd.hpp"
#include "Lufit.hpp"
#include "Linear.hpp"
#include "Newton.hpp"
#include "Rblock.hpp"
#include "Rpoint.hpp"
#include "Predmth.hpp"
#include "Boolean.hpp"
#include "Permute.hpp"
#include "Rvector.hpp"
#include "Rgivens.hpp"
#include "Rmatrix.hpp"
#include "Analytic.hpp"
#include "Rsamples.hpp"
#include "Rpolynom.hpp"
#include "Parallel.hpp"
#include "Rfunction.hpp"
#include "Exception.hpp"
#include "LinearFactory.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // rsap: real samples array object predicate

  Object* mth_rsap  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "r-samples-p");
    bool result = (dynamic_cast <Rsamples*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rmdp: rmd object predicate

  Object* mth_rmdp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "rmd-p");
    bool result = (dynamic_cast <Rmd*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // cpip: cpi object predicate

  Object* mth_cpip  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "cpi-p");
    bool result = (dynamic_cast <Cpi*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rvip: rvi object predicate

  Object* mth_rvip  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "rvi-p");
    bool result = (dynamic_cast <Rvi*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rmip: rmi object predicate

  Object* mth_rmip  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "rmi-p");
    bool result = (dynamic_cast <Rmi*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // slvp: solver object predicate

  Object* mth_slvp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "solver-p");
    bool result = (dynamic_cast <Solver*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // dlsp: direct object predicate

  Object* mth_dlsp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "direct-p");
    bool result = (dynamic_cast <Direct*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // parp: parallel object predicate

  Object* mth_parp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "parallel-p");
    bool result = (dynamic_cast <Parallel*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // qrdp: qr object predicate

  Object* mth_qrdp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "qr-p");
    bool result = (dynamic_cast <Qr*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // mgsp: mgs direct object predicate

  Object* mth_mgsp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "mgs-p");
    bool result = (dynamic_cast <Mgs*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // ilsp: iterative object predicate

  Object* mth_ilsp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "iterative-p");
    bool result = (dynamic_cast <Iterative*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // cgsp: cgs solver object predicate

  Object* mth_cgsp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "cgs-p");
    bool result = (dynamic_cast <Cgs*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // bcsp: bcs solver object predicate

  Object* mth_bcsp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "bcs-p");
    bool result = (dynamic_cast <Bcs*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
 
  // tqmrp: tqmr solver object predicate

  Object* mth_tqmrp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "tqmr-p");
    bool result = (dynamic_cast <Tqmr*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // lnrp: linear object predicate

  Object* mth_lnrp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "linear-p");
    bool result = (dynamic_cast <Linear*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // sfcp: solver factory object predicate

  Object* mth_sfcp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "solver-factory-p");
    bool result = (dynamic_cast <SolverFactory*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
 
  // lfcp: linear factory object predicate

  Object* mth_lfcp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "linear-factory-p");
    bool result = (dynamic_cast <LinearFactory*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // ntwp: newton object predicate

  Object* mth_ntwp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "netwon-p");
    bool result = (dynamic_cast <Newton*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rfip: fsi object predicate
  
  Object* mth_rfip  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "rfi-p");
    bool result = (dynamic_cast <Rfi*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // ippdp: integer plane point object predicate

  Object* mth_ippdp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "ippd-p");
    bool result = (dynamic_cast <Ippd*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // prmtp: permute object predicate

  Object* mth_prmtp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "permute-p");
    bool result = (dynamic_cast <Permute*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rvectp: rvector object predicate

  Object* mth_rvectp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "r-vector-p");
    bool result = (dynamic_cast <Rvector*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rblokp: rblock object predicate

  Object* mth_rblokp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "r-block-p");
    bool result = (dynamic_cast <Rblock*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rmtrxp: rmatrix object predicate

  Object* mth_rmtrxp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "r-matrix-p");
    bool result = (dynamic_cast <Rmatrix*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rfuncp: rfunction object predicate

  Object* mth_rfuncp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "r-function-p");
    bool result = (dynamic_cast <Rfunction*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rpolyp: rpolynom object predicate

  Object* mth_rpolyp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "r-polynom-p");
    bool result = (dynamic_cast <Rpolynom*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // rgvnsp: rgivens object predicate

  Object* mth_rgvnsp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "r-givens-p");
    bool result = (dynamic_cast <Rgivens*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // anap: analytic object predicate

  Object* mth_anap  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "analytic-p");
    bool result = (dynamic_cast <Analytic*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rpip: rpi object predicate

  Object* mth_rpip  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "rpi-p");
    bool result = (dynamic_cast <Rpi*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rptp: rpoint object predicate

  Object* mth_rptp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "r-point-p");
    bool result = (dynamic_cast <Rpoint*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // meanp: mean object predicate

  Object* mth_meanp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "mean-p");
    bool result = (dynamic_cast <Mean*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // covp: covariance object predicate

  Object* mth_covp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "cov-p");
    bool result = (dynamic_cast <Cov*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // fitp: fitter object predicate

  Object* mth_fitp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "fit-p");
    bool result = (dynamic_cast <Fit*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // luf: lufit object predicate
  
  Object* mth_lufp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "lufit-p");
    bool result = (dynamic_cast <Lufit*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
}
