// ---------------------------------------------------------------------------
// - Stdsid.hpp                                                              -
// - afnix:std - serial id definition                                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_STDSID_HXX
#define  AFNIX_STDSID_HXX

#ifndef  AFNIX_CCNF_HPP
#include "ccnf.hpp"
#endif

namespace afnix {

  // All codes between 0x00 and 0x7F are reserved by the engine and the 
  // core modules and services. From 0x80 to 0xEF, the codes are reserved
  // for future use. The codes from 0xF0 to 0xFE are open for private use only. 
  // The 0xFF code is the chain code.

  static const t_byte SERIAL_NILP_ID = 0x00; // nil pointer
  static const t_byte SERIAL_BOOL_ID = 0x01; // boolean
  static const t_byte SERIAL_BYTE_ID = 0x02; // byte
  static const t_byte SERIAL_RSV1_ID = 0x03; // reserved
  static const t_byte SERIAL_EOSC_ID = 0x04; // eos character
  static const t_byte SERIAL_RSV2_ID = 0x05; // reserved
  static const t_byte SERIAL_INTG_ID = 0x06; // integer
  static const t_byte SERIAL_REAL_ID = 0x07; // real
  static const t_byte SERIAL_CHAR_ID = 0x08; // character
  static const t_byte SERIAL_STRG_ID = 0x09; // string
  static const t_byte SERIAL_RELT_ID = 0x0A; // relative
  static const t_byte SERIAL_REGX_ID = 0x0B; // regex
  static const t_byte SERIAL_BUFR_ID = 0x0C; // buffer
  static const t_byte SERIAL_CONS_ID = 0x0D; // cons cell
  static const t_byte SERIAL_VECT_ID = 0x0E; // vector
  static const t_byte SERIAL_OSET_ID = 0x0F; // set
  static const t_byte SERIAL_NTBL_ID = 0x10; // name table
  static const t_byte SERIAL_STRV_ID = 0x11; // string vector
  static const t_byte SERIAL_PROP_ID = 0x12; // property
  static const t_byte SERIAL_PLST_ID = 0x13; // plist
  static const t_byte SERIAL_LIST_ID = 0x14; // list
  static const t_byte SERIAL_STRF_ID = 0x15; // string fifo
  static const t_byte SERIAL_PTBL_ID = 0x16; // print table
  static const t_byte SERIAL_MESG_ID = 0x17; // message
  static const t_byte SERIAL_STYL_ID = 0x18; // style
  static const t_byte SERIAL_OBLK_ID = 0x1B; // internal block byte
  static const t_byte SERIAL_BBLK_ID = 0x1C; // internal block bool
  static const t_byte SERIAL_LBLK_ID = 0x1D; // internal block long
  static const t_byte SERIAL_RBLK_ID = 0x1E; // internal block real
  static const t_byte SERIAL_RPT2_ID = 0x1F; // internal block point
}

#endif
