// ---------------------------------------------------------------------------
// - Plane.hpp                                                              -
// - afnix:geo service - solid plane with texture class definition           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PLANE_HPP
#define  AFNIX_PLANE_HPP

#ifndef  AFNIX_SOLID_HPP
#include "Solid.hpp"
#endif

namespace afnix {

  /// The plane class is a rectangular plane with a 3 dimensional origin
  /// which can be associated with a texture. The z coordinate is the layer
  /// coordinate of the plane.
  /// @author amaury darsch

  class Plane : public Solid {
  protected:
    /// the plane origin
    Point3 d_o;
    /// the plane width
    t_real d_w;
    /// the plane height
    t_real d_h;
    
  public:
    /// create a unity plane
    Plane (void);

    /// create a plane by origin and size
    /// @param o the plane origin
    /// @param w the plane width
    /// @param h the plane height
    Plane (const Point3& o, const t_real w, const t_real h);

    /// copy construct this plane
    /// @param that the object to copy
    Plane (const Plane& that);
    
    /// assign a plane to this one
    /// @param that the object to copy
    Plane& operator = (const Plane& that);

    /// @return the class name
    String repr (void) const;
    
    /// @return a clone of this object
    Object* clone (void) const;
    
    /// reset this plane
    void reset (void);

    /// @return the geometry name
    String getname (void) const;

    /// set the geometry by plist
    /// @param plst the geometry plist
    void setplst (const Plist& plst);

    /// @return the geometry plist
    Plist getplst (void) const;
    
    /// @return the plane origin
    Point3 getorigin (void) const;

    /// @return the plane width
    virtual t_real getw (void) const;

    /// @return the plane height
    virtual t_real geth (void) const;

    /// set a plane by origin and size
    /// @param o the plane origin
    /// @param w the plane width
    /// @param h the plane height
    virtual void set (const Point3& o, const t_real w, const t_real h);

    /// normalize the plane coordinates
    virtual void normalize (void);
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
