// ---------------------------------------------------------------------------
// - Predtxt.cpp                                                             -
// - afnix:txt module - predicates implementation                            - 
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "Trie.hpp"
#include "Worder.hpp"
#include "Predtxt.hpp"
#include "Scanner.hpp"
#include "Boolean.hpp"
#include "Lexicon.hpp"
#include "Literate.hpp"
#include "Exception.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // patp: pattern object predicate

  Object* txt_patp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "pattern-p");
    bool result =  (dynamic_cast <Pattern*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // lexp: lexeme object predicate

  Object* txt_lexp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "lexeme-p");
    bool result =  (dynamic_cast <Lexeme*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // tlitp: (trans)literate object predicate

  Object* txt_tlitp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "literate-p");
    bool result =  (dynamic_cast <Literate*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // scanp: scanner object predicate

  Object* txt_scanp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "scanner-p");
    bool result =  (dynamic_cast <Scanner*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // triep: trie object predicate

  Object* txt_triep (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "trie-p");
    bool result =  (dynamic_cast <Trie*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // tlexp: lexicon object predicate

  Object* txt_tlexp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "lexicon-p");
    bool result =  (dynamic_cast <Lexicon*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // wordp: worder object predicate

  Object* txt_wordp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "worder-p");
    bool result =  (dynamic_cast <Worder*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
}
