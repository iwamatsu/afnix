// ---------------------------------------------------------------------------
// - Cart.hpp                                                                -
// - afnix:csm service - cloud object cart class definition                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CART_HPP
#define  AFNIX_CART_HPP

#ifndef  AFNIX_SET_HPP
#include "Set.hpp"
#endif

#ifndef  AFNIX_AGENT_HPP
#include "Agent.hpp"
#endif

namespace afnix {

  /// The Cart class is the local cloud agent set. The class is designed
  /// to store registered agent objects. The implementation is primarily
  /// designed to store a unique form of objects and provides the necessary
  /// mechanism to retrieve an object by key id. A cart can store
  /// any kind of agent object as long as its registration id is null. If
  /// the registration id is defined, the agent id must be the same.
  /// @author amaury darsch

  class Cart : public virtual Object {
  protected:
    /// the cart agent set
    Set d_cas;
    /// the registration id
    String d_rid;

  public:
    /// create an empty set
    Cart (void);

    /// create a registered cart
    /// @param rid the registration id
    Cart (const String& rid);

    /// @return the class name
    String repr (void) const;

    /// reset this cart
    virtual void reset (void);

    /// validate a cart registration id
    /// @param rid the rid to validate
    virtual bool isrid (const String& rid) const;

    /// set the registration id
    /// @param rid the rid to set
    virtual void setrid (const String& rid);

    /// @return the registration id
    virtual String getrid (void) const;

    /// @return the number of agent in the cart
    virtual long length (void) const;

    /// @return true if the cart is empty
    virtual bool empty (void) const;

    /// @return true if an agent exists in the set
    virtual bool exists (const t_long kid) const;

    /// get a agent at position
    /// @param idx the agent position index
    virtual Agent* getat (const long idx) const;

    /// add a agent in this cart
    /// @param agent the agent to add
    virtual t_long addagt (Agent* agt);

    /// get a agent by key id
    /// @param kid the key id
    virtual Agent* getagt (const t_long kid) const;

  private:
    // make the copy constructor private
    Cart (const Cart&);
    // make the assignment operator private
    Cart& operator = (const Cart&);

  public:
    /// create a object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
