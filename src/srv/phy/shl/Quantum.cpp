// ---------------------------------------------------------------------------
// - Quantum.cpp                                                             -
// - afnix:phy service - base quantum function class implementation          -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Quantum.hpp"
#include "Physics.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // compute the boltzmann energy by temperature

  t_real Quantum::getkt (const t_real temp) {
    if (temp < 0.0) {
      throw Exception ("quantum-error", 
		       "invalid absolute negative temperature");
    }
    return (Physics::PCH_K * temp);
  }

  // compute the thermal voltage by temperature

  t_real Quantum::getvth (const t_real temp) {
    if (temp < 0.0) {
      throw Exception ("quantum-error", 
		       "invalid absolute negative temperature");
    }
    return (Physics::PCH_KV * temp);
  }

  // derate the energy gap with Varshni's expression

  t_real Quantum::geteg (const t_real temp,
			 const t_real eg, const t_real ega, const t_real egb) {
    return (eg - ((ega * (temp * temp)) / (temp + egb)));
  }
}
