// ---------------------------------------------------------------------------
// - Predsps.hpp                                                             -
// - afnix:sps module - predicates declaration                               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDSPS_HPP
#define  AFNIX_PREDSPS_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix:sys
  /// standard module.
  /// @author amaury darsch

  /// the bundle object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_bndp (Runnable* robj, Nameset* nset, Cons* args);

  /// the cell object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_celp (Runnable* robj, Nameset* nset, Cons* args);

  /// the record object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_rcdp (Runnable* robj, Nameset* nset, Cons* args);

  /// the sheet object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_shtp (Runnable* robj, Nameset* nset, Cons* args);

  /// the folio object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_folp (Runnable* robj, Nameset* nset, Cons* args);

  /// the index object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_idxp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xref object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_xrfp (Runnable* robj, Nameset* nset, Cons* args);

  /// the csv reader object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_csvp (Runnable* robj, Nameset* nset, Cons* args);

  /// the transfer object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_xfer (Runnable* robj, Nameset* nset, Cons* args);
  
  /// the record transfer object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_rfer (Runnable* robj, Nameset* nset, Cons* args);

  /// the sheet transfer object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sps_sfer (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
