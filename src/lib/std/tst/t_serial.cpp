// ---------------------------------------------------------------------------
// - t_serial.cpp                                                            -
// - standard object library - serial class tester                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Serial.hpp"
#include "String.hpp"

// ---------------------------------------------------------------------------
// - private section                                                         -
// ---------------------------------------------------------------------------

namespace afnix {
  static long TST_XDIM_SIZE = 64;
  static long TST_YDIM_SIZE = 64;
  static long TST_BLOK_SIZE = TST_XDIM_SIZE * TST_YDIM_SIZE;

  // test the byte serial block
  static bool tst_block_byte (void) {
    // create a byte serial block
    Serial::Block blok (TST_BLOK_SIZE, Serial::Block::BYTE);
    // add a byte in the block
    for (long k = 0; k < TST_BLOK_SIZE; k++) blok.add ((t_byte) (k % 256));
    // test the block
    for (long k = 0; k < TST_BLOK_SIZE; k++) {
      if (blok.getbyte (k) != (t_byte) (k % 256)) return false;
    }
    return true;
  }

  // test the long serial block
  static bool tst_block_long (void) {
    // create a long serial block
    Serial::Block blok (TST_BLOK_SIZE, Serial::Block::LONG);
    // add a long in the block
    for (long k = 0; k < TST_BLOK_SIZE; k++) blok.add ((t_long) k);
    // test the block
    for (long k = 0; k < TST_BLOK_SIZE; k++) {
      if (blok.getlong (k) != (t_long) k) return false;
    }
    return true;
  }

  // test the real serial block
  static bool tst_block_real (void) {
    // create a real serial block
    Serial::Block blok (TST_BLOK_SIZE, Serial::Block::REAL);
    // add a real in the block
    for (long k = 0; k < TST_BLOK_SIZE; k++) blok.add ((t_real) k);
    // test the block
    for (long k = 0; k < TST_BLOK_SIZE; k++) {
      if (blok.getreal (k) != (t_real) k) return false;
    }
    return true;
  }

  // test the real point serial block
  static bool tst_block_rpt2 (void) {
    // create a real point serial block
    Serial::Block blok (TST_BLOK_SIZE, Serial::Block::RPT2);
    // add a point in the block
    for (long x = 0; x < TST_XDIM_SIZE; x++) {
      for (long y = 0; y < TST_YDIM_SIZE; y++) {
	blok.add (x, y, (t_real) x * y);
      }
    }
    // test the block
    for (long x = 0; x < TST_XDIM_SIZE; x++) {
      for (long y = 0; y < TST_YDIM_SIZE; y++) {
	long index = x * TST_XDIM_SIZE + y;
	if (blok.getreal (index) != (t_real) x * y) return false;
      }
    }
    return true;
  }
}

int main (int, char**) {
  using namespace afnix;

  // test a byte block
  if (tst_block_byte () == false) return 1;
  // test a long block
  if (tst_block_long () == false) return 1;
  // test a real block
  if (tst_block_real () == false) return 1;
  // test a real point block
  if (tst_block_rpt2 () == false) return 1;

  // ok - everything is fine
  return 0;
}
