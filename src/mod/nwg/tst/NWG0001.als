# ---------------------------------------------------------------------------
# - NWG0001.als                                                             -
# - afnix:nwg module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   uri test unit
# @author amaury darsch

# get the module
interp:library "afnix-nwg"

# check the normalized uri
trans  unm "http://www.afnix.org"
assert unm (afnix:nwg:normalize-uri-name unm)
assert unm (afnix:nwg:normalize-uri-name "www.afnix.org")
assert unm (afnix:nwg:normalize-uri-name "//www.afnix.org")

# check the path normalization
trans  unm "/"
assert unm (afnix:nwg:path-uri-name "http://www.afnix.org")
assert unm (afnix:nwg:path-uri-name "/")
trans  unm "../afnix"
assert unm (afnix:nwg:path-uri-name "../afnix")
assert unm (afnix:nwg:path-uri-name "../afnix?lang=als")
assert unm (afnix:nwg:path-uri-name "../afnix?lang=als#tester")

# check for a simple uri
trans  unm "http://www.afnix.org"
trans  uri (afnix:nwg:Uri unm)
assert true                (afnix:nwg:uri-p uri)
assert unm                 (uri:get-name)

# check components
assert "http"              (uri:get-scheme)
assert "www.afnix.org"     (uri:get-authority)
assert ""                  (uri:get-path)
assert ""                  (uri:get-path-target)
assert ""                  (uri:get-query)
assert ""                  (uri:get-fragment)

# check for a simple uri
trans  unm "http://www.afnix.org/"
trans  uri (afnix:nwg:Uri unm)
assert "http"              (uri:get-scheme)
assert "www.afnix.org"     (uri:get-authority)
assert "/"                 (uri:get-path)
assert unm                 (uri:get-hname)

# check for a simple uri with a port
trans  uri (afnix:nwg:Uri "http" "www.afnix.org" 888)
assert "http"              (uri:get-scheme)
assert "www.afnix.org:888" (uri:get-authority)

# check for a simple uri
trans  unm "http://www.afnix.org/fr/âllô"
trans  rnm "http://www.afnix.org/fr/a%CC%82llo%CC%82"
trans  uri (afnix:nwg:Uri (unm:to-normal-form))
assert "http"              (uri:get-scheme)
assert "www.afnix.org"     (uri:get-authority)
assert rnm                 (uri:get-rname)

trans  unm "http://www.afnix.org/fr/"
trans  uri (afnix:nwg:Uri unm)
assert "http"              (uri:get-scheme)
assert "www.afnix.org"     (uri:get-authority)
assert "/fr/"              (uri:get-path)
assert unm                 (uri:get-hname)

# check add-path
trans  uri (uri:add-path "/hello/")
assert "/hello/"           (uri:get-path)
assert "hello"             (uri:get-path-target)

# check for a uri with a path
trans  unm "http://www.afnix.org/example"
trans  bnm "http://www.afnix.org"
trans  hnm "http://www.afnix.org/example"
trans  uri (afnix:nwg:Uri unm)
assert unm                 (uri:get-name)
assert bnm                 (uri:get-base)
assert hnm                 (uri:get-hname)
assert "http"              (uri:get-scheme)
assert "www.afnix.org"     (uri:get-authority)
assert "/example"          (uri:get-path)
assert "example"           (uri:get-path-target)
assert ""                  (uri:get-query)
assert ""                  (uri:get-fragment)

# check add-path
trans  uri (uri:add-path "/hello")
assert "/hello"            (uri:get-path)
trans  uri (uri:add-path "../")
assert "/"                 (uri:get-path)
trans  uri (uri:add-path "../")
assert "/"                 (uri:get-path)
trans  uri (uri:add-path "/hello/./world/./example/..")
assert "/hello/world"      (uri:get-path)

# check for a uri with a path and a query
trans  unm "http://www.afnix.org/example?val=hello+world" 
trans  uri (afnix:nwg:Uri unm)
assert unm                 (uri:get-name)
assert "http"              (uri:get-scheme)
assert "www.afnix.org"     (uri:get-authority)
assert "/example"          (uri:get-path)
assert "val=hello+world"   (uri:get-query)
assert ""                  (uri:get-fragment)
assert "www.afnix.org"     (uri:get-host)
assert 80                  (uri:get-port)

# check for a complete uri
trans  unm "http://a.b.org:2000/example?val=hello+world#good"
trans  anm "http://a.b.org:2000/example?val=hello+world"
trans  uri (afnix:nwg:Uri unm)
assert unm                 (uri:get-name)
assert anm                 (uri:get-aname)
assert "http"              (uri:get-scheme)
assert "a.b.org:2000"      (uri:get-authority)
assert "/example"          (uri:get-path)
assert "val=hello+world"   (uri:get-query)
assert "good"              (uri:get-fragment)
assert "a.b.org"           (uri:get-host)
assert 2000                (uri:get-port)

# check the normalized uri
trans  unm "file:///home/afnix"
assert unm (afnix:nwg:normalize-uri-name unm)
assert unm (afnix:nwg:normalize-uri-name "/home/afnix")

# check for a file uri
trans  unm "file:///home/afnix"
trans  uri (afnix:nwg:Uri unm)
assert unm                 (uri:get-name)
assert "file"              (uri:get-scheme)
assert ""                  (uri:get-authority)
assert "/home/afnix"       (uri:get-path)
assert "afnix"             (uri:get-path-target)

# check for a mail uri
trans  unm "mailto:support@afnix.org"
trans  uri (afnix:nwg:Uri unm)
assert unm                 (uri:get-name)
assert "mailto"            (uri:get-scheme)
assert ""                  (uri:get-authority)
assert "support@afnix.org" (uri:get-path)
assert "support@afnix.org" (uri:get-path-target)
assert "afnix.org"         (uri:get-host)
assert 25                  (uri:get-port)

# check for a urn
trans  unm "urn:example:afnix"
trans  uri (afnix:nwg:Uri unm)
assert unm                 (uri:get-name)
assert "urn"               (uri:get-scheme)
assert ""                  (uri:get-authority)
assert "example:afnix"     (uri:get-path)
assert "example:afnix"     (uri:get-path-target)

# test uri with encoded path
trans  uri (afnix:nwg:Uri "file:///%c3%a7")
assert "/%c3%a7" (uri:get-path-encoded)

# test uri with special characters
trans  uri (afnix:nwg:Uri "http://www.afnix.org/programming_&_language")
assert "www.afnix.org" (uri:get-authority)
trans  uri (afnix:nwg:Uri "http://www.afnix.org/data^")
assert "/data^" (uri:get-path)
trans  uri (afnix:nwg:Uri "http://www.afnix.org/data_[A-Z]/")
assert "/data_[A-Z]/" (uri:get-path)
trans  uri (afnix:nwg:Uri "http://www.afnix.org/data_<A-Z>;")
assert "/data_<A-Z>;" (uri:get-path)
trans  uri (afnix:nwg:Uri "http://www.afnix.org/data_A=Z")
assert "/data_A=Z" (uri:get-path)
trans  uri (afnix:nwg:Uri "http://www.afnix.org/`data")
assert "/`data" (uri:get-path)
trans  uri (afnix:nwg:Uri "http://www.afnix.org/{data}")
assert "/{data}" (uri:get-path)
trans  uri (afnix:nwg:Uri "http://www.afnix.org/data\\")
assert "/data\\" (uri:get-path)
trans  uri (afnix:nwg:Uri "http://www.afnix.org/data!")
assert "/data!" (uri:get-path)
trans  uri (afnix:nwg:Uri "http://www.afnix.org/data*")
assert "/data*" (uri:get-path)

