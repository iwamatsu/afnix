// ---------------------------------------------------------------------------
// - Session.cpp                                                             -
// - afnix:csm service - session class implementation                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Time.hpp"
#include "Csmsid.hxx"
#include "Vector.hpp"
#include "System.hpp"
#include "Sha256.hpp"
#include "Session.hpp"
#include "Boolean.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "InputStream.hpp"
#include "OutputStream.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the random session key size
  static const long SESS_KEY_SIZE = 1024;

  // generate a random session key
  static String sess_new_skey (void) {
    Relatif rn = Relatif::random (SESS_KEY_SIZE);
    return rn.tohstr ();
  }

  // this procedure returns a new session for deserialization
  static Serial* mksob (void) {
    return new Session;
  }
  // register this session serial id
  static const t_byte SERIAL_ID = Serial::setsid (SERIAL_SESS_ID, mksob);

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create an empty session

  Session::Session (void) {
    d_skey = sess_new_skey ();
    d_path = System::tempnam ();
    d_ctim = Time::gettclk ();
    d_mtim = d_ctim;
    d_mage = 0LL;
  }

  // create a session by name

  Session::Session (const String& name) {
    d_name = name;
    d_user = name;
    d_urid = name;
    d_krid = name;
    d_skey = sess_new_skey ();
    d_path = System::tempnam ();
    d_ctim = Time::gettclk ();
    d_mtim = d_ctim;
    d_mage = 0LL;
  }

  // create a session by name and user

  Session::Session (const String& name, const String& user) {
    d_name = name;
    d_user = user;
    d_urid = user;
    d_krid = user;
    d_skey = sess_new_skey ();
    d_path = System::tempnam ();
    d_ctim = Time::gettclk ();
    d_mtim = d_ctim;
    d_mage = 0LL;
  }

  // create a session by name, user and maximum age

  Session::Session (const String& name, const String& user, const t_long mage) {
    d_name = name;
    d_user = user;
    d_urid = user;
    d_krid = user;
    d_skey = sess_new_skey ();
    d_ctim = Time::gettclk ();
    d_mtim = d_ctim;
    d_mage = (mage < 0LL) ? 0LL : mage;
  }

  // copy construct this session object

  Session::Session (const Session& that) {
    that.rdlock ();
    try {
      d_name = that.d_name;
      d_user = that.d_user;
      d_urid = that.d_urid;
      d_krid = that.d_krid;
      d_skey = that.d_skey;
      d_shid = that.d_shid;
      d_path = that.d_path;
      d_ctim = that.d_ctim;
      d_mtim = that.d_mtim;
      d_mage = that.d_mage;
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }

  // return the class name

  String Session::repr (void) const {
    return "Session";
  }

  // return a clone of this object

  Object* Session::clone (void) const {
    return new Session (*this);
  }
  
  // return the session serial code

  t_byte Session::serialid (void) const {
    return SERIAL_ID;
  }

  // serialize this session

  void Session::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      d_name.wrstream    (os);
      d_user.wrstream    (os);
      d_urid.wrstream    (os);
      d_krid.wrstream    (os);
      d_skey.wrstream    (os);
      d_shid.wrstream    (os);
      csm_wrlong (d_ctim, os);
      csm_wrlong (d_mtim, os);
      csm_wrlong (d_mage, os);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this session

  void Session::rdstream (InputStream& is) {
    wrlock ();
    try {
      d_name.rdstream     (is);
      d_user.rdstream     (is);
      d_urid.rdstream     (is);
      d_krid.rdstream     (is);
      d_skey.rdstream     (is);
      d_shid.rdstream     (is);
      d_ctim = csm_rdlong (is);
      d_mtim = csm_rdlong (is);
      d_mage = csm_rdlong (is);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the session name

  String Session::getname (void) const {
    rdlock ();
    try {
      String result = d_name;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // assign a session object to this one

  Session& Session::operator = (const Session& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      d_name = that.d_name;
      d_user = that.d_user;
      d_urid = that.d_urid;
      d_krid = that.d_krid;
      d_skey = that.d_skey;
      d_shid = that.d_shid;
      d_path = that.d_path;
      d_ctim = that.d_ctim;
      d_mtim = that.d_mtim;
      d_mage = that.d_mage;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }

  // set the session user
  
  void Session::setuser (const String& user) {
    wrlock ();
    try {
      d_user = user;
      d_mtim = Time::gettclk ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the session user

  String Session::getuser (void) const {
    rdlock ();
    try {
      String result = d_user;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the session user rid
  
  void Session::seturid (const String& urid) {
    wrlock ();
    try {
      d_urid = urid;
      d_mtim = Time::gettclk ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the session user rid

  String Session::geturid (void) const {
    rdlock ();
    try {
      String result = d_urid;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the session key rid
  
  void Session::setkrid (const String& krid) {
    wrlock ();
    try {
      d_krid = krid;
      d_mtim = Time::gettclk ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the session key rid

  String Session::getkrid (void) const {
    rdlock ();
    try {
      String result = d_krid;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the session hash id
  
  void Session::setshid (const String& shid) {
    wrlock ();
    try {
      d_shid = shid;
      d_mtim = Time::gettclk ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the session hash id

  String Session::getshid (void) const {
    rdlock ();
    try {
      // prepare result
      String result = d_shid;
      // check for nil and rehash
      if (result.isnil () == true) {
	// prepare the hash value
	String hs = d_name + d_user + d_skey;
	// hash the string
	result = Sha256().compute (hs);
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the session path
  
  void Session::setpath (const String& path) {
    wrlock ();
    try {
      d_path = path;
      d_mtim = Time::gettclk ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the session path

  String Session::getpath (void) const {
    rdlock ();
    try {
      String result = d_path;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the session creation time

  t_long Session::getctim (void) const {
    rdlock ();
    try {
      t_long result = d_ctim;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the session modification time 

  t_long Session::getmtim (void) const {
    rdlock ();
    try {
      t_long result = d_mtim;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return true if the session has expired

  bool Session::isetim (void) const {
    rdlock ();
    try {
      // get the current time
      t_long time = Time::gettclk ();
      // check for expiration
      bool result = (d_mage == 0LL) ? false : (d_ctim + d_mage <= time);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }


  // set the session expiration time
  void Session::setetim (const t_long etim) {
    wrlock ();
    try {
      d_mtim = Time::gettclk ();
      d_mage = etim - d_ctim;
      if (d_mage < 0LL) d_mage = 0LL;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the session expiration time

  t_long Session::getetim (void) const {
    rdlock ();
    try {
      t_long result = d_ctim + d_mage;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the session maximum age
  
  void Session::setmage (const t_long mage) {
    wrlock ();
    try {
      d_mtim = Time::gettclk ();
      d_mage = mage;
      if (d_mage < 0LL) d_mage = 0LL;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the session maximum age

  t_long Session::getmage (void) const {
    rdlock ();
    try {
      t_long result = d_mage;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the session valid remaining time

  t_long Session::getrtim (void) const {
    rdlock ();
    try {
      // get the current time
      t_long time = Time::gettclk ();
      // compute remaining time
      t_long vldt = (d_mage == 0) ? 0 : (d_ctim + d_mage - time);
      if (vldt < 0) vldt = 0;
      // unlock and return
      unlock ();
      return vldt;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // bake a new cookie from the session information

  Cookie* Session::getcookie (const String& name) {
    rdlock ();
    try {
      // save the session hash name
      d_shnm = name;
      d_mtim = Time::gettclk ();
      // create a new cookie
      Cookie* result = new Cookie (d_shnm, getshid (), d_mage);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // close a session by reseting the maximum age

  Cookie* Session::close (void) {
    wrlock ();
    try {
      // close the session
      setmage (0LL);
      // get a closing cookie
      Cookie* result = new Cookie (d_shnm, getshid (), d_mage);
      // done
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
      
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 21;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_CLOSE   = zone.intern ("close");
  static const long QUARK_ETIMP   = zone.intern ("expire-p");
  static const long QUARK_GETNAME = zone.intern ("get-name");
  static const long QUARK_SETUSER = zone.intern ("set-user");
  static const long QUARK_GETUSER = zone.intern ("get-user");
  static const long QUARK_SETSHID = zone.intern ("set-hash-id");
  static const long QUARK_GETSHID = zone.intern ("get-hash-id");
  static const long QUARK_SETURID = zone.intern ("set-user-rid");
  static const long QUARK_GETURID = zone.intern ("get-user-rid");
  static const long QUARK_SETKRID = zone.intern ("set-key-rid");
  static const long QUARK_GETKRID = zone.intern ("get-key-rid");
  static const long QUARK_GETPATH = zone.intern ("get-path");
  static const long QUARK_SETPATH = zone.intern ("set-path");
  static const long QUARK_GETCOOK = zone.intern ("get-cookie");
  static const long QUARK_SETMAGE = zone.intern ("set-max-age");
  static const long QUARK_GETMAGE = zone.intern ("get-max-age");
  static const long QUARK_SETETIM = zone.intern ("set-expire-time");
  static const long QUARK_GETETIM = zone.intern ("get-expire-time");
  static const long QUARK_GETCTIM = zone.intern ("get-creation-time");
  static const long QUARK_GETRTIM = zone.intern ("get-remaining-time");
  static const long QUARK_GETMTIM = zone.intern ("get-modification-time");

  // create a new object in a generic way

  Object* Session::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();

    // check for 1 argument
    if (argc == 1) {
      String name  = argv->getstring (0);
      return new Session (name);
    }
    // check for 2 arguments
    if (argc == 2) {
      String name = argv->getstring (0);
      String user = argv->getstring (1);
      return new Session (name, user);
    }
    // check for 3 arguments
    if (argc == 3) {
      String name = argv->getstring (0);
      String user = argv->getstring (1);
      t_long mage = argv->getlong   (2);
      return new Session (name, user, mage);
    }
    // invalid arguments
    throw Exception ("argument-error", "invalid arguments with session");
  }

  // return true if the given quark is defined

  bool Session::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? Nameable::isquark (quark, hflg) : false;
    if (result == false) {
      result = hflg ? Serial::isquark (quark, hflg) : false;
    }
    unlock ();
    return result;
  }

  // apply this object with a set of arguments and a quark

  Object* Session::apply (Runnable* robj, Nameset* nset, const long quark,
			 Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_ETIMP)   return new Boolean (isetim  ());
      if (quark == QUARK_GETNAME) return new String  (getname ());
      if (quark == QUARK_GETSHID) return new String  (getshid ());
      if (quark == QUARK_GETUSER) return new String  (getuser ());
      if (quark == QUARK_GETURID) return new String  (geturid ());
      if (quark == QUARK_GETKRID) return new String  (getkrid ());
      if (quark == QUARK_GETPATH) return new String  (getpath ());
      if (quark == QUARK_GETETIM) return new Integer (getetim ());
      if (quark == QUARK_GETCTIM) return new Integer (getctim ());
      if (quark == QUARK_GETMTIM) return new Integer (getmtim ());
      if (quark == QUARK_GETMAGE) return new Integer (getmage ());
      if (quark == QUARK_GETRTIM) return new Integer (getrtim ());
      if (quark == QUARK_CLOSE)   return close ();
    }

    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_SETUSER) {
	String user = argv->getstring (0);
	setuser (user);
	return nilp;
      }   
      if (quark == QUARK_SETURID) {
	String urid = argv->getstring (0);
	seturid (urid);
	return nilp;
      }
      if (quark == QUARK_SETKRID) {
	String krid = argv->getstring (0);
	setkrid (krid);
	return nilp;
      }
      if (quark == QUARK_SETSHID) {
	String shid = argv->getstring (0);
	setshid (shid);
	return nilp;
      }
      if (quark == QUARK_SETPATH) {
	String path = argv->getstring (0);
	setpath (path);
	return nilp;
      }
      if (quark == QUARK_SETETIM) {
	t_long time = argv->getlong (0);
	setetim (time);
	return nilp;
      }
      if (quark == QUARK_SETMAGE) {
	t_long mage = argv->getlong (0);
	setmage (mage);
	return nilp;
      }
      if (quark == QUARK_GETCOOK) {
	String name = argv->getstring (0);
	return getcookie (name);
      }
    }
    // check the nameable method
    if (Nameable::isquark (quark, true) == true) {
      return Nameable::apply (robj, nset, quark, argv);
    }
    // call the serial method
    return Serial::apply (robj, nset, quark, argv);
  }
}
