// ---------------------------------------------------------------------------
// - Predxml.hpp                                                             -
// - afnix:xml module - predicates declaration                               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDXML_HPP
#define  AFNIX_PREDXML_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix:xml
  /// standard module.
  /// @author amaury darsch

  /// the xml document object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_docp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml tag node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_tagp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_nodep (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml root node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_rootp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml processing instruction node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_pindp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml declaration node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_declp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml document type node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_doctp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml comment node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_comtp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml text node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_textp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml cdata node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_datap (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml reference node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_xrefp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml cref node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_crefp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml eref node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_erefp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml ent node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_entp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml gent node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_gentp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml pent node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_pentp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml element node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_elemp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml attribute node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_attlp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml section node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_sectp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml reader object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_readp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml texter object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_txtrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xne condition object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_condp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xne tree object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_treep (Runnable* robj, Nameset* nset, Cons* args);

  /// the xsm node object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_xsmnp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xsm reader object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_xsmrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xsm document object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_xsmdp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xso info object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* xml_xsoip (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
