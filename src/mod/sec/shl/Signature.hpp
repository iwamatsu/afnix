// ---------------------------------------------------------------------------
// - Signature.hpp                                                           -
// - afnix:sec module - message signature class definition                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SIGNATURE_HPP
#define  AFNIX_SIGNATURE_HPP

#ifndef  AFNIX_RELATIF_HPP
#include "Relatif.hpp"
#endif

namespace afnix {

  /// The Signature class is a container class designed to store a message
  /// signature. The signature object is produced by a signing process,
  /// implemented in the form of a digital signature algorithm such like
  /// RSA or DSA.
  /// @author amaury darsch

  class Signature : public virtual Object {
  public:
    /// the signature type
    enum t_sign {
      SNIL, // null signature
      SDSA  // DSA signature
    };

    /// the signature index accessor
    enum t_isgn {
      SDSA_SCMP, // dsa s component
      SDSA_RCMP  // dsa r component
    };

  private:
    /// the signature type
    t_sign d_type;
    /// the signature structure
    union {
      void*          p_snil;
      struct s_sdsa* p_sdsa;
    };

  public:
    /// create a default signature
    Signature (void);

    /// create a signature by type and value
    /// @param type the signature type
    /// @param r    the signature first value
    /// @param s    the signature second value
    Signature (const t_sign type, const Relatif& r, const Relatif& s);

    /// copy construct this signature
    /// @param that the signature to copy
    Signature (const Signature& that);

    /// destroy the signature
    ~Signature (void);

    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// assign a signature to this one
    /// @param that the signature to assign
    Signature& operator = (const Signature& that);

    /// reset this signature
    void reset (void);

    /// @return a formatted component by type
    String format (const t_isgn type) const;

    /// @return a relatif component by type
    Relatif getrcmp (const t_isgn type) const;

  public:
    /// evaluate an object data member
    /// @param robj  the current runnable
    /// @param nset  the current nameset
    /// @param quark the quark to evaluate
    static Object* meval (Runnable* robj, Nameset* nset, const long quark);

    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
