// ---------------------------------------------------------------------------
// - Material.cpp                                                            -
// - afnix:geo service - material class implementation                       -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Real.hpp"
#include "Vector.hpp"
#include "Material.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the default material
  static const t_real XML_ACHN_DEF  = 1.0;

  // the material xml name
  static const String XML_TAG_NAME  = "MATERIAL";
  static const String XML_TAG_INFO  = "GEOMETRY MATERIAL";
  // the material name attribute
  static const String XML_ACHN_ATTR = "alpha";

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default material

  Material::Material (void) {
    reset ();
  }

  // create a material by name

  Material::Material (const String& name) : Texture (name) {
    d_achn = XML_ACHN_DEF;
  }

  // copy construct this material

  Material::Material (const Material& that) {
    that.rdlock ();
    try {
      Texture::operator = (that);
      d_achn = that.d_achn;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }  
  }

  // assign a material to this one

  Material& Material::operator = (const Material& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      Texture::operator = (that);
      d_achn = that.d_achn;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return the class name
  
  String Material::repr (void) const {
    return "Material";
  }

  // return a clone of this object

  Object* Material::clone (void) const {
    return new Material (*this);
  }

  // get the material alpha channel

  t_real Material::getachn (void) const {
    rdlock ();
    try {
      t_real result = d_achn;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // reset this material

  void Material::reset (void) {
    wrlock ();
    try {
      d_achn = XML_ACHN_DEF;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the material by plist

  void Material::setplst (const Plist& plst) {
    wrlock ();
    try {
      reset ();
      Texture::setplst (plst);
      if (plst.exists (XML_ACHN_ATTR) == true)
	d_achn = plst.toreal (XML_ACHN_ATTR);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the material plist

  Plist Material::getplst (void) const {
    rdlock ();
    try {
      // the generic plist
      Plist result = Texture::getplst ();
      result.setname (XML_TAG_NAME); result.setinfo (XML_TAG_INFO);
      // add the material properties
      result.add (XML_ACHN_ATTR, d_achn);
      // here it is
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 1;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_GETACHN = zone.intern ("get-alpha-channel");

  // create a new object in a generic way

  Object* Material::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Material;
    // check for 1 argument
    if (argc == 1) {
      String name = argv->getstring (0);
      return new Material (name);
    }
    throw Exception ("argument-error", 
                     "invalid arguments with with material"); 
  }
  
  // return true if the given quark is defined
  
  bool Material::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Texture::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark

  Object* Material::apply (Runnable* robj, Nameset* nset, const long quark,
			  Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETACHN) return new Real  (getachn ());
    }
    // call the texture method
    return Texture::apply (robj, nset, quark, argv);
  }
}
