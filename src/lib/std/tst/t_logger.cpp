// ---------------------------------------------------------------------------
// - t_logger.cpp                                                            -
// - standard object library - message logger class tester                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Logger.hpp"

int main (int, char**) {
  using namespace afnix;
  
  // create a message logger
  Logger* log = new Logger (5);

  // check default
  if (log->length  () != 0) return 1;
  if (log->getdlvl () != 0) return 1;

  // add a message and check
  log->add ("hello 0", 0);
  if (log->length  () != 1) return 1;
  log->add ("hello 1", 1);
  if (log->length () != 2) return 1;
  log->add ("hello 2", 2);
  if (log->length () != 3) return 1;
  log->add ("hello 3", 3);
  if (log->length () != 4) return 1;
  log->add ("hello 4", 4);
  if (log->length () != 5) return 1;
  // check the log level
  if (log->getmlvl (0) != 0) return 1;
  if (log->getmlvl (1) != 1) return 1;
  if (log->getmlvl (2) != 2) return 1;
  if (log->getmlvl (3) != 3) return 1;
  if (log->getmlvl (4) != 4) return 1;
  // check the messages
  if (log->getmesg (0) != "hello 0") return 1;
  if (log->getmesg (1) != "hello 1") return 1;
  if (log->getmesg (2) != "hello 2") return 1;
  if (log->getmesg (3) != "hello 3") return 1;
  if (log->getmesg (4) != "hello 4") return 1;

  // set and check default level
  log->setdlvl (-1);
  if (log->getdlvl () != -1) return 1;

  // add new message for circulation
  log->add ("hello 5");
  if (log->length () != 5) return 1;

  // check at index 0
  if (log->getmlvl (0) != 1)         return 1;
  if (log->getmesg (0) != "hello 1") return 1;
  // check at index 4
  if (log->getmlvl (4) != -1)        return 1;
  if (log->getmesg (4) != "hello 5") return 1;
    
  // delete everything
  delete log;
  return 0;
}
