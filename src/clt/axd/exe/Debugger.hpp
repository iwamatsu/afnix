// ---------------------------------------------------------------------------
// - Debugger.hpp                                                            -
// - afnix cross debugger - debugger class definitions                       -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef AFNIX_DEBUGGER_HPP
#define AFNIX_DEBUGGER_HPP

#ifndef  AFNIX_INTERP_HPP
#include "Interp.hpp"
#endif

#ifndef  AFNIX_READER_HPP
#include "Reader.hpp"
#endif

#ifndef  AFNIX_IFILE_HPP
#include "Ifile.hpp"
#endif

namespace afnix {

  /// The Debugger class is a derived class from the afnix interpreter that
  /// provide facility to debug an afnix program. The class works with the
  /// standard i/o streams and provides a special nameset called axd. Such
  /// nameset incorporates all commands needed to perform successfuly a
  /// debug or program tracing.
  /// @author amaury darsch

  class Debugger : public Interp {
  private:
    /// emacs mode flag
    bool d_emacs;
    /// the start flag
    bool d_start;
    /// th finish flag
    bool d_finish;
    /// the exit flag
    bool d_exit;
    /// the verbose flag
    bool d_vflag;
    /// the max number of lines
    long d_lmax;
    /// the initial file name
    String  d_fname;
    /// the breakpoint vector
    Vector  d_bpoint;
    /// the debugger reader
    Reader* p_reader;
    /// the current form
    Form* p_form;
    /// the axd nameset
    Nameset* p_aset;
    /// the i-files nameset
    Nameset* p_iset;

  public:
    /// create a new debugger
    Debugger (void);

    /// copy construct this debugger
    /// @param that the object to copy
    Debugger (const Debugger& that);

    /// destroy this debugger
    ~Debugger (void);

    /// @return the class name
    String repr (void) const;

    /// break the runnable in a nameset with an object
    /// @param nset   the nameset to loop
    /// @param object the object to break on
    bool bpt (Nameset* nset, Object* object);

    /// the debugger top level loop
    bool loop (void);

    /// loop on the standard input in a nameset context
    /// @param nset the nameset context
    bool loop (Nameset* nset);

    /// load a file by doing a read-eval loop
    void load (const String& fname);

    /// clone this interpreter and set the runnable form
    /// @param form the runnable form to set
    Debugger* dup (Object* form) const;

  public:
    /// set the emacs info mode
    /// @param mode the mode to set
    void setemacs (const bool mode);

    /// @return the start flag
    bool getstart (void) const;

    /// set the exit flag
    void setexit (const bool flag);

    /// set the initial file name
    /// @param fname the file name to set
    void setinitial (const String& fname);

    /// run the initial file name
    void runinitial (void);

    /// instrument a file
    /// @param fname the file name to instrument
    Ifile* instrument (const String& fname);

    /// install a breakpoint at the current position
    void setbpt (void);

    /// install a breakpoint by line number
    /// @param lnum  the line number for the form
    void setbpt (const long lnum);

    /// install a breakpoint by file name and line number
    /// @param fname the file name to use
    /// @param lnum  the line number for the form
    void setbpt (const String& fname, const long lnum);

    /// print a listing at the current form
    void flist (void) const;

    /// print a listing at current form  line number
    /// @param lnum  the line number to start
    void flist (const long lnum) const;

    /// print a listing upto to max lines
    /// @param fname the file name
    /// @param lnum  the line number to start
    void flist (const String& fname, const long lnum) const;

    /// write an info message to the output stream
    /// @param name the file name to info
    /// @param lnum the line number in the file
    void info (const String& name, const long lnum) const;

    /// print a message if the verbose flag is on
    /// @param msg   the message to print
    /// @param data  the info data
    /// @param fname the optional file name
    /// @param lnum  the line number
    void pmsg (const String& msg, const long data, const String& fname, 
	       const long lnum) const;

    /// write some debugger information
    void dbginfo (void) const;

    /// write some breakpoint information
    void brkinfo (void) const;

  private:
    // make the assignment operator private
    Debugger& operator = (const Debugger&);
  };
}

#endif
