// ---------------------------------------------------------------------------
// - WorkSpace.hpp                                                           -
// - afnix:csm module - abstract workspace class definition                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_WORKSPACE_HPP
#define  AFNIX_WORKSPACE_HPP

#ifndef  AFNIX_URI_HPP
#include "Uri.hpp"
#endif

#ifndef  AFNIX_STRVEC_HPP
#include "Strvec.hpp"
#endif

#ifndef  AFNIX_PRINTTABLE_HPP
#include "PrintTable.hpp"
#endif

#ifndef  AFNIX_INPUTSTREAM_HPP
#include "InputStream.hpp"
#endif

#ifndef  AFNIX_OUTPUTSTREAM_HPP
#include "OutputStream.hpp"
#endif

namespace afnix {

  /// The WorkSpace class is an abstract class designed to manipulate
  /// a file user space.  A workspace is defined as a set of user spaces.
  /// For each user, defined by name, a file system is made available with
  /// the standard operations, like creating directories, reading and writing
  /// files. Each user file system associate also, on demand a temporary space
  /// which is deleted when the object is destroyed. A workspace can be made
  /// persistent or not, depending on the underlying implementation.
  /// @author amaury darsch

  class WorkSpace : public virtual Nameable {
  public:
    /// the workspace uri scheme
    static const String URI_SCHM_DEF;

    /// map a name to a zone uri
    /// @param name the name to map
    static String tozuri (const String& name);

  protected:
    /// the workspace name
    String d_name;
    /// the workspace info
    String d_info;
    /// the public zone
    String d_zpub;
    /// global temporary flag
    bool   d_gtmp;

  public:
    /// create a default workspace
    WorkSpace (void);

    /// create a workspace by name and info
    /// @param name the workspace name
    /// @param info the workspace info
    WorkSpace (const String& name, const String& info);

    /// @return the workspace name
    String getname (void) const;

    /// set the workspace name
    /// @param name the name to set
    virtual void setname (const String& name);

    /// @return the workspace info
    String getinfo (void) const;

    /// set the workspace info
    /// @param info the info to set
    virtual void setinfo (const String& info);

    /// @return the workspace public zone
    String getzpub (void) const;

    /// set the workspace public zone
    /// @param zone the public zone to set
    virtual void setzpub (const String& zone);

    /// set the global temporary flag
    /// @param gtmp the temporary flag
    virtual void setgtmp (const bool gtmp);

    /// @return the global temporary flag
    virtual bool getgtmp (void) const;

    /// check if a zone exists by name
    /// @param zone the zone to check
    virtual bool iszone (const String& zone) const =0;

    /// add a new working zone by name
    /// @param zone the zone name
    virtual bool addzone (const String& zone) =0;

    /// check if an entity exists by zone and uri
    /// @param zone the working zone
    /// @param uri  the uri to check
    virtual bool exists (const String& zone, const Uri& uri) const =0;

    /// check if an entity exists by zone and string uri
    /// @param zone the working zone
    /// @param nuri the normalized uri
    virtual bool exists (const String& zone, const String& nuri) const;

    /// map a file path to a workspace uri if possible - no public mapping
    /// @param zone the working zone
    /// @param name the file name to map
    virtual String towuri (const String& zone, const String& name) const =0;

    /// map a file name to a normalized uri if possible
    /// @param zone the working zone
    /// @param name the file name to map
    virtual String tonuri (const String& zone, const String& name) const =0;

    /// get a workspace zone file list
    /// @param zone the working zone
    virtual Strvec* getfiles (const String& zone) const =0;

    /// get a workspace zone file table
    /// @param zone the working zone
    virtual PrintTable* tofptbl (const String& zone) const =0;

    /// get an input stream by zone and uri
    /// @param zone the working zone
    /// @param uri  the uri to open
    virtual InputStream* getis (const String& zone, const Uri& uri) const =0;
    
    /// get an input stream by zone and normalized uri
    /// @param zone the working zone
    /// @param nuri the normalized uri
    virtual InputStream* getis (const String& zone, const String& nuri) const;

    /// get an ouput stream by zone and uri
    /// @param zone the working zone
    /// @param uri  the uri to open
    virtual OutputStream* getos (const String& zone, const Uri& uri) const =0;
    
    /// get an output stream by zone and normalized uri
    /// @param zone the working zone
    /// @param nuri the normalized uri
    virtual OutputStream* getos (const String& zone, const String& nuri) const;
    
  private:
    // make the copy constructor private
    WorkSpace (const WorkSpace&);
    // make the assignment operator private
    WorkSpace& operator = (const WorkSpace&);

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
