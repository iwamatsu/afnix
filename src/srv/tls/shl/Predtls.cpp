// ---------------------------------------------------------------------------
// - Predtls.cpp                                                             -
// - afnix:tls service - predicates implementation                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "X509.hpp"
#include "Cons.hpp"
#include "Boolean.hpp"
#include "Predtls.hpp"
#include "TlsShake.hpp"
#include "TlsCerts.hpp"
#include "TlsSuite.hpp"
#include "TlsRecord.hpp"
#include "TlsServer.hpp"
#include "TlsShello.hpp"
#include "TlsChello.hpp"
#include "Exception.hpp"
#include "TlsConnect.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // -------------------------------------------------------------------------
  // - tls section                                                           -
  // -------------------------------------------------------------------------

  // blkp: tls block object predicate
  
  Object* tls_blkp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "block-p");
    bool result = (dynamic_cast <TlsBlock*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // hdrp: tls header object predicate
  
  Object* tls_hdrp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "header-p");
    bool result = (dynamic_cast <TlsHeader*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // recp: tls record object predicate
  
  Object* tls_recp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "record-p");
    bool result = (dynamic_cast <TlsRecord*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // msgp: tls message object predicate
  
  Object* tls_msgp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "message-p");
    bool result = (dynamic_cast <TlsMessage*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // hskp: tls handshake object predicate
  
  Object* tls_shkp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "handshake-p");
    bool result = (dynamic_cast <TlsShake*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // stap: tls state object predicate
  
  Object* tls_stap  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "state-p");
    bool result = (dynamic_cast <TlsState*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // prmp: tls parameter object predicate
  
  Object* tls_prmp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "params-p");
    bool result = (dynamic_cast <TlsParams*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // conp: tls connect object predicate
  
  Object* tls_conp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "connect-p");
    bool result = (dynamic_cast <TlsConnect*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // sckp: tls dupleix object predicate
  
  Object* tls_dpxp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "dupleix-p");
    bool result = (dynamic_cast <TlsDupleix*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // srvp: tls server object predicate
  
  Object* tls_srvp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "server-p");
    bool result = (dynamic_cast <TlsServer*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // shlop: tls server hello object predicate
  
  Object* tls_shlop  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "server-hello-p");
    bool result = (dynamic_cast <TlsShello*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // chlop: tls client hello object predicate
  
  Object* tls_chlop  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "client-hello-p");
    bool result = (dynamic_cast <TlsChello*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // suitep: tls suite object predicate
  
  Object* tls_suitep  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "suite-p");
    bool result = (dynamic_cast <TlsSuite*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // certsp: tls certificate list object predicate
  
  Object* tls_certsp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "certificate-list-p");
    bool result = (dynamic_cast <TlsCerts*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // -------------------------------------------------------------------------
  // - itu section                                                           -
  // -------------------------------------------------------------------------


  // xalgop: X509 algorithm object predicate

  Object* tls_xalgop  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xalgo-p");
    bool result = (dynamic_cast <Xalgo*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // pkcs: pkcs object predicate

  Object* tls_pkcsp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "pkcs-p");
    bool result = (dynamic_cast <Pkcs*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // pkcs1: pkcs1 object predicate

  Object* tls_pkcs1p (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "pkcs-1-p");
    bool result = (dynamic_cast <Pkcs1*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // x509p: X509 ceritifcate object predicate

  Object* tls_x509p (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "x509-p");
    bool result = (dynamic_cast <X509*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
}
