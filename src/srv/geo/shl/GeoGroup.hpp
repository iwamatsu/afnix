// ---------------------------------------------------------------------------
// - GeoGroup.hpp                                                            -
// - afnix:geo service - geometry group class definition                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_GEOGROUP_HPP
#define  AFNIX_GEOGROUP_HPP

#ifndef  AFNIX_GEOMETRY_HPP
#include "Geometry.hpp"
#endif

namespace afnix {

  /// The GeoGroup class is a grouping class of geometry objects. Grouping
  /// is simply implemented as an object vector specialization. Things become
  /// interesting when someone decides to mix shapes and solid in a group.
  /// @author amaury darsch

  class GeoGroup : public virtual Object {
  protected:
    /// the geometry group
    Vector d_geom;
    
  public:
    /// create an empty group
    GeoGroup (void);
    
    /// copy construct this group
    /// @param that the group to copy
    GeoGroup (const GeoGroup& that);

    /// assign a group to this one
    /// @param that the group to assign
    GeoGroup& operator = (const GeoGroup& that);

    /// @return the class name
    String repr (void) const;
    
    /// @return a clone of this object
    Object* clone (void) const;
    
    /// reset this group
    virtual void reset (void);

    /// @return the group length
    virtual long length (void) const;
    
    /// add a geometry to this group
    /// @param geo the geometry to add
    virtual void add (Geometry* geo);

    /// get a geometry by index
    /// @param idx the geometry index
    virtual Geometry* get (const long idx) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
