// ---------------------------------------------------------------------------
// - Physics.cpp                                                             -
// - afnix:phy service - fundamental physical constants class implementation -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Real.hpp"
#include "Boolean.hpp"
#include "Physics.hpp"
#include "Runnable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // speed of light in vacuum
  static const String UNV_NAME_C  = "C";
  static const String UNV_INFO_C  = "speed of light in vacuum";
  /// magnetic constant
  static const String UNV_NAME_U0 = "U0";
  static const String UNV_INFO_U0 = "magnetic constant";
  // electric constant
  static const String UNV_NAME_E0 = "E0";
  static const String UNV_INFO_E0 = "electric constant";
  // constant of gravitation
  static const String UNV_NAME_G  = "G";
  static const String UNV_INFO_G  = "constant of gravitation";
  // planck constant
  static const String UNV_NAME_H  = "H";
  static const String UNV_INFO_H  = "Planck's constant";
  // planck constant
  static const String UNV_NAME_HB = "HB";
  static const String UNV_INFO_HB = "Planck's constant bar";

  // boltzmann constant
  static const String PCH_NAME_K  = "K";
  static const String PCH_INFO_K  = "Boltzmann's constant";
  // boltzmann constant in eV
  static const String PCH_NAME_KV = "KV";
  static const String PCH_INFO_KV = "Boltzmann's constant in eV";
  // avogadro constant
  static const String PCH_NAME_NA = "NA";
  static const String PCH_INFO_NA = "Avogadro constant";
  // absolute temperature offset
  static const String PCH_NAME_TK = "TK";
  static const String PCH_INFO_TK = "Absolute temperature offset";

  // elementary charge
  static const String EMT_NAME_Q  = "Q";
  static const String EMT_INFO_Q  = "elementary charge";

  // electron mass
  static const String NUC_NAME_EM = "EM";
  static const String NUC_INFO_EM = "electron mass";
  // proton mass
  static const String NUC_NAME_PM = "PM";
  static const String NUC_INFO_PM = "proton mass";

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // speed of light in vacuum
  const t_real Physics::UNV_C  = 299792458.0;
  // magnetic constant
  const t_real Physics::UNV_U0 = 12.566370614E-7;
  // electric constant
  const t_real Physics::UNV_E0 = 8.854187817E-12;
  // constant of gravitation
  const t_real Physics::UNV_G  = 6.67428E-11;
  // planck constant
  const t_real Physics::UNV_H  = 6.62606896E-34;
  // planck constant h/2*PI
  const t_real Physics::UNV_HB = 1.054571628E-34;

  // boltzmann constant
  const t_real Physics::PCH_K  = 1.3806504E-23;
  // boltzmann constant in ev
  const t_real Physics::PCH_KV = 8.617343E-5;
  // avogadro constant
  const t_real Physics::PCH_NA = 6.02214179E23;
  // absolute temperature offset
  const t_real Physics::PCH_TK = 273.15;
  // default celcius nominal temperature
  const t_real Physics::PCH_NC = 27.0;
  // default kelvin nominal temperature
  const t_real Physics::PCH_NK = 300.15;

  // elementary charge
  const t_real Physics::EMT_Q  = 1.602176487E-19;

  // electron mass
  const t_real Physics::NUC_EM = 9.10938291E-31;
  // proton mass
  const t_real Physics::NUC_PM = 1.672621777E-27;

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default physical constants object

  Physics::Physics (void) {
    // universal constants
    d_plst.add (UNV_NAME_C,  UNV_INFO_C,  UNV_C);
    d_plst.add (UNV_NAME_U0, UNV_INFO_U0, UNV_U0);
    d_plst.add (UNV_NAME_E0, UNV_INFO_E0, UNV_E0);
    d_plst.add (UNV_NAME_G,  UNV_INFO_G,  UNV_G);
    d_plst.add (UNV_NAME_H,  UNV_INFO_H,  UNV_H);
    d_plst.add (UNV_NAME_HB, UNV_INFO_HB, UNV_HB);

    // physico chemical constants
    d_plst.add (PCH_NAME_K,  PCH_INFO_K,  PCH_K);
    d_plst.add (PCH_NAME_KV, PCH_INFO_KV, PCH_KV);
    d_plst.add (PCH_NAME_TK, PCH_INFO_TK, PCH_TK);
   
    // electromagnetic constant
    d_plst.add (EMT_NAME_Q,  EMT_INFO_Q,  EMT_Q);

    // nuclear constants
    d_plst.add (NUC_NAME_EM, NUC_INFO_EM, NUC_EM);
    d_plst.add (NUC_NAME_PM, NUC_INFO_PM, NUC_PM);
  }

  // copy constucts this object

  Physics::Physics (const Physics& that) {
    that.rdlock ();
    try {
      d_plst = that.d_plst;
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }

  // assign an object to this one

  Physics& Physics::operator = (const Physics& that) {
    // check for self assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      d_plst = that.d_plst;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }

  // return the class name

  String Physics::repr (void) const {
    return "Physics";
  }

  // return a clone of this object

  Object* Physics::clone (void) const {
    return new Physics (*this);
  }

  // check if a name exists

  bool Physics::exists (const String& name) const {
    rdlock ();
    try {
      bool result = d_plst.exists (name);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a value by name

  t_real Physics::getval (const String& name) const {
    rdlock ();
    try {
      t_real result = d_plst.toreal (name);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a copy of the physical constant table

  Plist Physics::gettbl (void) const {
    rdlock ();
    try {
      Plist result = d_plst;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 3;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_EXISTP = zone.intern ("exists-p");
  static const long QUARK_GETVAL = zone.intern ("get-value");
  static const long QUARK_GETTBL = zone.intern ("get-table");

  // create a new object in a generic way

  Object* Physics::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Physics;
    // wrong arguments
    throw Exception ("argument-error", 
		     "too many arguments with physics constructor");
  }

  // return true if the given quark is defined

  bool Physics::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? Object::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }

  // apply this object with a set of arguments and a quark

  Object* Physics::apply (Runnable* robj, Nameset* nset, const long quark,
			  Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETTBL) return new Plist (gettbl ());
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_EXISTP) {
	String name = argv->getstring (0);
	return new Boolean (exists (name));
      }
      if (quark == QUARK_GETVAL) {
	String name = argv->getstring (0);
	return new Real (getval (name));
      }
    }
    // call the object method
    return Object::apply (robj, nset, quark, argv);
  }
}
