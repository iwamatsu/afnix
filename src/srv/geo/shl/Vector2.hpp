// ---------------------------------------------------------------------------
// - Vector2.hpp                                                             -
// - afnix:geo service - vector 2 class definition                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_VECTOR2_HPP
#define  AFNIX_VECTOR2_HPP

#ifndef  AFNIX_POINT2_HPP
#include "Point2.hpp"
#endif

#ifndef  AFNIX_RVECTOR_HPP
#include "Rvector.hpp"
#endif

namespace afnix {
  
  /// The Vector2 class is a 2 dimensional vector class for the afnix geometry
  /// service. A 2 dimensional vector is defined by its (x,y) components in
  /// a base choosen by the system.
  /// @author amaury darsch

  class Vector2 : public Rvector {
  public:
    /// create a default vector
    Vector2 (void);

    /// create a vector by components
    /// @param x the x component
    /// @param y the y component
    Vector2 (const t_real x, const t_real y);

    /// copy construct this vector
    /// @param that the vector to copy
    Vector2 (const Vector2& that);

    /// assign a vector to this one
    /// @param that the vector to assign
    Vector2& operator = (const Vector2& that);
    
    /// @return the class name
    String repr (void) const;
    
    /// @return a clone of this object
    Object* clone (void) const;

    /// @return the vector x component
    virtual t_real getx (void) const;

    /// @return the vector y component
    virtual t_real gety (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
