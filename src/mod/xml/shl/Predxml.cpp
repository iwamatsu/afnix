// ---------------------------------------------------------------------------
// - Predxml.cpp                                                             -
// - afnix:xml module - predicates implementation                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "XmlGe.hpp"
#include "XmlPe.hpp"
#include "XmlTag.hpp"
#include "XmlRoot.hpp"
#include "XmlText.hpp"
#include "XmlData.hpp"
#include "XmlCref.hpp"
#include "XmlEref.hpp"
#include "Predxml.hpp"
#include "Boolean.hpp"
#include "XneTree.hpp"
#include "XneCond.hpp"
#include "XsmReader.hpp"
#include "XmlReader.hpp"
#include "XmlTexter.hpp"
#include "Exception.hpp"
#include "XmlComment.hpp"
#include "XmlDoctype.hpp"
#include "XmlElement.hpp"
#include "XmlAttlist.hpp"
#include "XmlSection.hpp"
#include "XmlDocument.hpp"
#include "XsmDocument.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // docp: xml document object predicate

  Object* xml_docp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "document-p");
    bool result = (dynamic_cast <XmlDocument*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // tagp: xml tag node object predicate

  Object* xml_tagp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "tag-p");
    bool result = (dynamic_cast <XmlTag*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // nodep: xml node object predicate

  Object* xml_nodep  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "node-p");
    bool result = (dynamic_cast <XmlNode*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rootp: xml root node object predicate

  Object* xml_rootp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "root-p");
    bool result = (dynamic_cast <XmlRoot*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // pindp: xml processing instruction node object predicate

  Object* xml_pindp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "pi-p");
    bool result = (dynamic_cast <XmlPi*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // declp: xml declaration node node object predicate

  Object* xml_declp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "decl-p");
    bool result = (dynamic_cast <XmlDecl*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // doctp: xml document type node node object predicate

  Object* xml_doctp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "doctype-p");
    bool result = (dynamic_cast <XmlDoctype*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }


  // comtp: xml comment node object predicate

  Object* xml_comtp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "comment-p");
    bool result = (dynamic_cast <XmlComment*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // textp: xml text node object predicate

  Object* xml_textp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "text-p");
    bool result = (dynamic_cast <XmlText*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // datap: xml cdata node object predicate

  Object* xml_datap  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "data-p");
    bool result = (dynamic_cast <XmlData*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xrefp: xml reference node object predicate

  Object* xml_xrefp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "reference-p");
    bool result = (dynamic_cast <XmlRef*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // crefp: xml cref node object predicate

  Object* xml_crefp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "cref-p");
    bool result = (dynamic_cast <XmlCref*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // erefp: xml eref node object predicate

  Object* xml_erefp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "eref-p");
    bool result = (dynamic_cast <XmlEref*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // entp: xml entity node object predicate

  Object* xml_entp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "entity-p");
    bool result = (dynamic_cast <XmlEntity*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // gentp: xml general entity node object predicate

  Object* xml_gentp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "ge-p");
    bool result = (dynamic_cast <XmlGe*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // pentp: xml parameter entity node object predicate

  Object* xml_pentp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "pe-p");
    bool result = (dynamic_cast <XmlPe*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // elemp: xml element node object predicate

  Object* xml_elemp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "element-p");
    bool result = (dynamic_cast <XmlElement*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // attlp: xml attribute list node object predicate

  Object* xml_attlp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "attlist-p");
    bool result = (dynamic_cast <XmlAttlist*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // sectp: xml section node object predicate

  Object* xml_sectp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "section-p");
    bool result = (dynamic_cast <XmlSection*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // readp: xml reader object predicate

  Object* xml_readp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "reader-p");
    bool result = (dynamic_cast <XmlReader*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // txtrp: xml texter object predicate

  Object* xml_txtrp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "texter-p");
    bool result = (dynamic_cast <XmlTexter*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // treep: xne tree object predicate

  Object* xml_treep  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xne-tree-p");
    bool result = (dynamic_cast <XneTree*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // condp: xne condition object predicate

  Object* xml_condp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xne-cond-p");
    bool result = (dynamic_cast <XneCond*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xsmnp: xsm node object predicate

  Object* xml_xsmnp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xsm-node-p");
    bool result = (dynamic_cast <XsmNode*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xsmnp: xsm reader object predicate

  Object* xml_xsmrp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xsm-reader-p");
    bool result = (dynamic_cast <XsmReader*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xsmdp: xsm document object predicate

  Object* xml_xsmdp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xsm-document-p");
    bool result = (dynamic_cast <XsmDocument*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xsoip: xso info object predicate

  Object* xml_xsoip  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xso-info-p");
    bool result = (dynamic_cast <XsoInfo*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
}
