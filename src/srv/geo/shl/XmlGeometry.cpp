// ---------------------------------------------------------------------------
// - XmlGeometry.cpp                                                         -
// - afnix:geo service - xml geometry representation class implementation    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Wire.hpp"
#include "Solid.hxx"
#include "Plane.hpp"
#include "XmlTag.hpp"
#include "Cuboid.hpp"
#include "Exception.hpp"
#include "XmlGeometry.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // this procedure returns a geometry by name
  static Geometry* new_geometry (const String& name) {
    // check for geometry names
    if (name == GEO_CUB_NAME) return new Cuboid;
    if (name == GEO_PLN_NAME) return new Plane;
    if (name == GEO_WIR_NAME) return new Wire;
    // nothing found
    return nilp;
  }
  
  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // check if a geometry exists

  bool XmlGeometry::exists (const String& name) { 
    if (name == GEO_CUB_NAME) return true;
    if (name == GEO_PLN_NAME) return true;
    if (name == GEO_WIR_NAME) return true;
    return false;
  }

  // create a geometry by xml node
  
  Geometry* XmlGeometry::togeo (const XmlNode* node) {
    // check for valid tag
    const XmlTag* tag = dynamic_cast<const XmlTag*> (node);
    if (tag == nilp) return nilp;
    // get the tag name and attribute list
    String name = tag->getname ();
    Plist  alst = tag->getalst ();
    // create the geometry and set it
    Geometry* result = new_geometry (name);
    if (result == nilp) return nilp;
    // set the geometry by attribute list
    result->setplst (alst);
    // here it is
    return result;
  }

  // create a xml node by geometry

  XmlNode* XmlGeometry::toxml (const Geometry* geo) {
    // check for nil
    if (geo == nilp) return nilp;
    // get the name and plist
    String name = geo->getname ();
    Plist  plst = geo->getplst ();
    // create a tag by name and set the attibute list
    XmlTag* result = new XmlTag (name);
    result->setalst (plst);
    // here it is
    return result;
  }
}
