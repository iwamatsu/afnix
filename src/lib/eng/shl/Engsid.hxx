// ---------------------------------------------------------------------------
// - Engsid.hpp                                                              -
// - afnix:eng - engine serial id definition                                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_ENGSID_HXX
#define  AFNIX_ENGSID_HXX

#ifndef  AFNIX_CCNF_HPP
#include "ccnf.hpp"
#endif

namespace afnix {
  static const t_byte SERIAL_RESV_ID = 0x20; // reserved  id
  static const t_byte SERIAL_CNST_ID = 0x21; // constant  id
  static const t_byte SERIAL_LEXL_ID = 0x22; // lexical   id
  static const t_byte SERIAL_QUAL_ID = 0x23; // qualified id
  static const t_byte SERIAL_FORM_ID = 0x24; // form      id
  static const t_byte SERIAL_CNTR_ID = 0x25; // counter   id
  static const t_byte SERIAL_PMIZ_ID = 0x26; // promise   id
}

#endif
