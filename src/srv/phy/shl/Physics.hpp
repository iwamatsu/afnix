// ---------------------------------------------------------------------------
// - Physics.hpp                                                             -
// - afnix:phy service - fundamental physical constants class definition     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PHYSICS_HPP
#define  AFNIX_PHYSICS_HPP

#ifndef  AFNIX_PLIST_HPP
#include "Plist.hpp"
#endif

namespace afnix {

  /// The Physics class is a simple class that holds some of the fundamental
  /// physical constants. The class also provides some methods to compute
  /// basic physical values which are part of our daily life.
  /// @author amaury darsch

  class Physics : public Object {
  public:
    /// speed of light in vacuum
    static const t_real UNV_C;
    /// magnetic constant
    static const t_real UNV_U0;
    // electric constant
    static const t_real UNV_E0;
    // constant of gravitation
    static const t_real UNV_G;
    // planck constant
    static const t_real UNV_H;
    // planck constant h/2*PI
    static const t_real UNV_HB;

  public:
    /// boltzmann constant
    static const t_real PCH_K;
    /// boltzmann constant in ev
    static const t_real PCH_KV;
    /// avogadro constant
    static const t_real PCH_NA;
    /// absolute temperature offset
    static const t_real PCH_TK;
    /// default celcius nominal temperature
    static const t_real PCH_NC;
    /// default kelvin nominal temperature
    static const t_real PCH_NK;

  public:
    // elementary charge
    static const t_real EMT_Q;

  public:
    // electron mass
    static const t_real NUC_EM;
    // proton mass
    static const t_real NUC_PM;

  private:
    Plist d_plst;

  public:
    /// create a physics object
    Physics (void);

    /// copy construct this object
    /// @param that the object to copy
    Physics (const Physics& that);

    /// assign an object to this one
    /// @param that the object to assign
    Physics& operator = (const Physics& that);

    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// @return true if the name exists
    bool exists (const String& name) const;

    /// get a physical constant by name
    /// @param name the constant name
    t_real getval (const String& name) const;

    /// @return a copy of the physical constants table
    Plist gettbl (void) const;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
