// ---------------------------------------------------------------------------
// - Mthsid.hxx                                                              -
// - afnix:mth module - serial id definition                                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_MTHSID_HXX
#define  AFNIX_MTHSID_HXX

#ifndef  AFNIX_REAL_HPP
#include "Real.hpp"
#endif

#ifndef  AFNIX_BOOLEAN_HPP
#include "Boolean.hpp"
#endif

#ifndef  AFNIX_INTEGER_HPP
#include "Integer.hpp"
#endif

namespace afnix {
  // the math serial id
  static const t_byte SERIAL_RSPL_ID = 0x60; // rsamples id
  static const t_byte SERIAL_ACPI_ID = 0x61; // anonymous cpi id
  static const t_byte SERIAL_ARVI_ID = 0x62; // anonymous rvi id
  static const t_byte SERIAL_ARMI_ID = 0x63; // anonymous rmi id
  static const t_byte SERIAL_ARPI_ID = 0x64; // anonymous rpi id
  static const t_byte SERIAL_RBLK_ID = 0x65; // real block id

  // serialize a boolean to an output stream
  static inline void mth_wrbool (const bool value, OutputStream& os) {
    Boolean bobj (value);
    bobj.wrstream (os);
  }

  // deserialize a boolean
  static inline t_long mth_rdbool (InputStream& is) {
    Boolean bobj;
    bobj.rdstream (is);
    return bobj.tobool ();
  }

  // serialize an integer to an output stream
  static inline void mth_wrlong (const t_long value, OutputStream& os) {
    Integer iobj (value);
    iobj.wrstream (os);
  }

  // deserialize an integer
  static inline t_long mth_rdlong (InputStream& is) {
    Integer iobj;
    iobj.rdstream (is);
    return iobj.tolong ();
  }

  // serialize a real to an output stream
  static inline void mth_wrreal (const t_real value, OutputStream& os) {
    Real robj (value);
    robj.wrstream (os);
  }

  // deserialize a real
  static inline t_real mth_rdreal (InputStream& is) {
    Real robj;
    robj.rdstream (is);
    return robj.toreal ();
  }
}

#endif
