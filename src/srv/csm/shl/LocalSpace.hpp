// ---------------------------------------------------------------------------
// - LocalSpace.hpp                                                           -
// - afnix:csm module - abstract local space class definition                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_LOCALSPACE_HPP
#define  AFNIX_LOCALSPACE_HPP

#ifndef  AFNIX_WORKSPACE_HPP
#include "WorkSpace.hpp"
#endif

namespace afnix {

  /// The LocalSpace class is a local implementation of the abstract
  /// workspace class. A local space is constructed with a root directory
  /// which serves as a root place for the whole workspace. The local space
  /// is persistent, except for the global temporary directory if it exists.
  /// There is no protection mechanism with respect to the underlying file
  /// system hosting the local space. By default, a localspace is created in
  /// a temporary space so it can be delete also at reboot.
  /// @author amaury darsch

  class LocalSpace : public WorkSpace {
  private:
    /// the root directory
    String d_root;
    /// the temporary directory
    String d_tdir;

  public:
    /// create a default local space
    LocalSpace (void);

    /// create a local space by root directory
    /// @param root the root directory
    LocalSpace (const String& root);

    /// create a local space by name, info and root directory
    /// @param name the workspace name
    /// @param info the workspace info
    /// @param root the root directory
    LocalSpace (const String& name, const String& info, const String& root);

    /// destroy this local space
    ~LocalSpace (void);

    /// @return the class name
    String repr (void) const;

    /// check if a zone exists by name
    /// @param zone the zone to check
    bool iszone (const String& zone) const;

    /// add a new working zone by name
    /// @param zone the zone name
    bool addzone (const String& zone);

    /// check if an entity exists by zone and uri
    /// @param zone the working zone
    /// @param uri  the uri to check
    bool exists (const String& zone, const Uri& uri) const;

    /// map a file path to a workspace uri if possible - no public mapping
    /// @param zone the working zone
    /// @param name the file name to map
    String towuri (const String& zone, const String& name) const;

    /// map a file name to a uri string if possible
    /// @param zone the working zone
    /// @param name the file name to map
    String tonuri (const String& zone, const String& name) const;

    /// get a workspace zone file list
    /// @param zone the working zone
    Strvec* getfiles (const String& zone) const;

    /// get a workspace zone file list
    /// get a workspace zone file table
    /// @param zone the working zone
    PrintTable* tofptbl (const String& zone) const;

    /// get an input stream by zone and uri
    /// @param zone the working zone
    /// @param uri  the uri to open
    InputStream* getis (const String& zone, const Uri& uri) const;
    
    /// get an output stream by zone and uri
    /// @param zone the working zone
    /// @param uri  the uri to open
    OutputStream* getos (const String& zone, const Uri& uri) const;
    
    /// @return the local root directory
    virtual String getroot (void) const;

  private:
    // make the copy constructor private
    LocalSpace (const LocalSpace&);
    // make the assignment operator private
    LocalSpace& operator = (const LocalSpace&);

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
