// ---------------------------------------------------------------------------
// - Serial.cpp                                                              -
// - standard object library - serializable object implementation            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Set.hpp"
#include "Byte.hpp"
#include "Cons.hpp"
#include "List.hpp"
#include "Real.hpp"
#include "Regex.hpp"
#include "Plist.hpp"
#include "Stdsid.hxx"
#include "Vector.hpp"
#include "Strvec.hpp"
#include "Strfifo.hpp"
#include "Relatif.hpp"
#include "Boolean.hpp"
#include "Message.hpp"
#include "Character.hpp"
#include "NameTable.hpp"
#include "QuarkZone.hpp"
#include "PrintTable.hpp"
#include "InputStream.hpp"
#include "cmem.hpp"
#include "ccnv.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the maximum number of serial codes
  static const long SRL_CBAK_MAX = 256;
  // the default block size
  static const long SRL_BLOK_SIZ = 1024;

  // the array of serial callback
  static Serial::t_genser* p_sercbk = nilp;

  // the serial callback deallocator
  static void del_serial_cbk (void) {
    delete [] p_sercbk;
    p_sercbk = nilp;
  }

  // the serial callback allocator
  static void new_serial_cbk (void) {
    if (p_sercbk == nilp) {
      p_sercbk = new Serial::t_genser[SRL_CBAK_MAX];
      for (long i = 0; i < SRL_CBAK_MAX; i++) p_sercbk[i] = nilp;
      c_gcleanup (del_serial_cbk);
    }
  }

  // add a new serial callback
  static void add_serial_cbk (const t_byte sid, Serial::t_genser cbk) {
    new_serial_cbk ();
    if ((sid == 0x00) || (p_sercbk[sid] != nilp))
      throw Exception ("serial-errror", "cannot add callback");
    p_sercbk[sid] = cbk;
  }

  // get a serial object by sid
  static Serial* get_serial_object (const t_byte sid) {
    if ((p_sercbk == nilp) || (p_sercbk[sid] == nilp)) 
      throw Exception ("serial-error", "cannot find object to deserialize");
    Serial::t_genser cbk = p_sercbk[sid];
    return (cbk ());
  }

  // get a block data size by type
  static long get_block_dsiz (const Serial::Block::t_btyp btyp) {
    long result = 0;
    switch (btyp) {
    case Serial::Block::BYTE:
    case Serial::Block::BOOL:
      result = sizeof (t_byte);
      break;
    case Serial::Block::LONG:
      result = sizeof (t_long);
      break;
    case Serial::Block::REAL:
      result = sizeof (t_real);
      break;
    case Serial::Block::RPT2:
      result = 2 * sizeof (t_long) + sizeof (t_real);
      break;
    }
    return result;
  }
   
  // -------------------------------------------------------------------------
  // - block section                                                         -
  // -------------------------------------------------------------------------

  // create a default empty block

  Serial::Block::Block (void) {
    d_btyp = BYTE;
    d_size = 0;
    d_dsiz = 0;
    d_bsiz = 0;
    d_blen = 0;
    p_byte = nilp;
  }

  // create an empty block by sid

  Serial::Block::Block (const t_byte sid) {
    switch (sid) {
    case SERIAL_OBLK_ID:
      d_btyp = BYTE;
      break;
    case SERIAL_BBLK_ID:
      d_btyp = BOOL;
      break;
    case SERIAL_LBLK_ID:
      d_btyp = LONG;
      break;
    case SERIAL_RBLK_ID:
      d_btyp = REAL;
      break;
    case SERIAL_RPT2_ID:
      d_btyp = RPT2;
      break;
    default:
      throw Exception ("serial-error", "invalid serial type for block");
      break;
    }
    d_size = 0;
    d_dsiz = get_block_dsiz (d_btyp);
    d_bsiz = 0;
    d_blen = 0;
    p_byte = nilp;
  }

  // create a serial block by size and type

  Serial::Block::Block (const long size, const t_btyp btyp) {
    // create the block
    d_btyp = btyp;
    d_size = (size <= 0) ? 0 : size;
    d_dsiz = get_block_dsiz (d_btyp);
    d_blen = 0;
    d_bsiz = size * d_dsiz;
    p_byte = (d_bsiz == 0) ? nilp : new t_byte[d_bsiz];
    // clear the block
    clear ();
  }
  
  // copy construct this serial block

  Serial::Block::Block (const Block& that) {
    d_btyp = that.d_btyp;
    d_size = that.d_size;
    d_dsiz = that.d_dsiz;
    d_bsiz = that.d_bsiz;
    d_blen = that.d_blen;
    p_byte = (d_bsiz == 0) ? nilp : new t_byte[d_bsiz];
    for (long k = 0; k < d_bsiz; k++) p_byte[k] = that.p_byte[k];
  }

  // destroy this serial block

  Serial::Block::~Block (void) {
    if (d_bsiz > 0) delete [] p_byte;
  }

  // assign a serial block to this one

  Serial::Block& Serial::Block::operator = (const Block& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // clean locally
    if (d_bsiz > 0) delete[] p_byte;
    // assign locally
    d_btyp = that.d_btyp;
    d_size = that.d_size;
    d_dsiz = that.d_dsiz;
    d_bsiz = that.d_bsiz;
    d_blen = that.d_blen;
    p_byte = (d_bsiz == 0) ? nilp : new t_byte[d_bsiz];
    for (long k = 0; k < d_bsiz; k++) p_byte[k] = that.p_byte[k];
    // done
    return *this;
  }

  // clear the serial block

  void Serial::Block::clear (void) {
    d_blen = 0;
    for (long k = 0; k < d_bsiz; k++) p_byte[k] = nilc;
  }

  // return true if the block is empty

  bool Serial::Block::empty (void) const {
    return (d_blen == 0);
  }

  // return true if the block is full

  bool Serial::Block::full (void) const {
    return (d_blen >= d_size);
  }

  // return the block length

  long Serial::Block::length (void) const {
    return d_blen;
  }

  // add a byte to the block

  void Serial::Block::add (const t_byte bval) {
    if (d_btyp != BYTE) {
      throw Exception ("serial-error", "inconsistent serial block type");
    }
    if (d_blen >= d_size) {
      throw Exception ("serial-error", "cannot add in full serial block");
    }
    p_byte[d_blen++] = bval;
  }

  // get a byte value by index

  t_byte Serial::Block::getbyte (const long index) const {
    if (d_btyp != BYTE) {
      throw Exception ("serial-error", "inconsistent serial block type");
    }
    if ((index < 0) || (index >= d_blen)) {
      throw Exception ("serial-error", "invalid index in serial block");
    }
    return p_byte[index];
  }

  // add a boolean to the block

  void Serial::Block::add (const bool bval) {
    if (d_btyp != BOOL) {
      throw Exception ("serial-error", "inconsistent serial block type");
    }
    if (d_blen >= d_size) {
      throw Exception ("serial-error", "cannot add in full serial block");
    }
    p_byte[d_blen++] = bval ? 0x01 : nilc;
  }

  // get a boolean value by index

  bool Serial::Block::getbool (const long index) const {
    if (d_btyp != BOOL) {
      throw Exception ("serial-error", "inconsistent serial block type");
    }
    if ((index < 0) || (index >= d_blen)) {
      throw Exception ("serial-error", "invalid index in serial block");
    }
    return (p_byte[index] == nilc) ? false : true;
  }

  // add an integer to the block

  void Serial::Block::add (const t_long lval) {
    if (d_btyp != LONG) {
      throw Exception ("serial-error", "inconsistent serial block type");
    }
    if (d_blen >= d_size) {
      throw Exception ("serial-error", "cannot add in full serial block");
    }
    c_ohton (lval, &(p_byte[d_blen++ * d_dsiz]));
  }

  // get an integer value by index

  t_long Serial::Block::getlong (const long index) const {
    if (d_btyp != LONG) {
      throw Exception ("serial-error", "inconsistent serial block type");
    }
    if ((index < 0) || (index >= d_blen)) {
      throw Exception ("serial-error", "invalid index in serial block");
    }
    return c_ontoh (&(p_byte[index * d_dsiz]));
  }

  // add a real to the block

  void Serial::Block::add (const t_real rval) {
    if (d_btyp != REAL) {
      throw Exception ("serial-error", "inconsistent serial block type");
    }
    if (d_blen >= d_size) {
      throw Exception ("serial-error", "cannot add in full serial block");
    }
    c_rhton (rval, &(p_byte[d_blen++ * d_dsiz]));
  }

  // add a real point to the block

  void Serial::Block::add (const t_long xval, const t_long yval, 
			   const t_real rval) {
    if (d_btyp != RPT2) {
      throw Exception ("serial-error", "inconsistent serial block type");
    }
    if (d_blen >= d_size) {
      throw Exception ("serial-error", "cannot add in full serial block");
    }
    // add the x coordinate
    c_ohton (xval, &(p_byte[d_blen * d_dsiz]));
    // add the y coordinate
    c_ohton (yval, &(p_byte[(d_blen * d_dsiz) + sizeof (t_long)]));
    // add the real value
    c_rhton (rval, &(p_byte[(d_blen++ * d_dsiz) + (2 * sizeof (t_long))]));
  }

  // get a real value by index

  t_real Serial::Block::getreal (const long index) const {
    // check index
    if ((index < 0) || (index >= d_blen)) {
      throw Exception ("serial-error", "invalid index in serial block");
    }
    // check for real
    if (d_btyp == REAL) 
      return c_ontor (&(p_byte[index * d_dsiz]));
    // must be 
    if (d_btyp == RPT2) 
      return c_ontor (&(p_byte[(index * d_dsiz) + (2 * sizeof (t_long))]));
    // invalid type
    throw Exception ("serial-error", "inconsistent serial block type");
  }

  // get the block serial code

  t_byte Serial::Block::serialid (void) const {
    t_byte sid = nilc;
    switch (d_btyp) {
    case BYTE:
      sid = SERIAL_OBLK_ID;
      break;
    case BOOL:
      sid = SERIAL_BBLK_ID;
      break;
    case LONG:
      sid = SERIAL_LBLK_ID;
      break;
    case REAL:
      sid = SERIAL_RBLK_ID;
      break;
    case RPT2:
      sid = SERIAL_RPT2_ID;
      break;
    }
    return sid;
  }

  // serialize this block

  void Serial::Block::wrstream (OutputStream& os) const {
    // write size and length
    Serial::wrlong (d_size, os);
    Serial::wrlong (d_blen, os);
    // write the byte array
    if (d_size > 0) {
      long bsiz = os.write ((char*) p_byte, d_bsiz);
      if (bsiz != d_bsiz) {
	throw Exception ("serial-error", "inconsistent size in serial block");
      }
    }      
  }

  // deserialize this block

  void Serial::Block::rdstream (InputStream& is) {
    // read size and length
    d_dsiz = 0;
    d_size = Serial::rdlong (is);
    d_dsiz = get_block_dsiz (d_btyp);
    d_blen = Serial::rdlong (is);
    d_bsiz = d_size * d_dsiz;
    // allocate the byte array
    if (d_bsiz > 0) {
      // allocate the array
      p_byte = new t_byte[d_bsiz];
      // get the array from the stream
      long bsiz = is.copy ((char*) p_byte, d_bsiz);
      if (bsiz != d_bsiz) {
	throw Exception ("serial-error", "inconsistent size in serial block");
      }
    } else {
      p_byte = nilp;
    }
  }

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // register a deserialize callback

  t_byte Serial::setsid (const t_byte sid, t_genser cbk) {
    add_serial_cbk (sid, cbk);
    return sid;
  }

  // return the a standard object by serial id

  Serial* Serial::getserial (const t_byte sid) {
    switch (sid) {
    case SERIAL_NILP_ID:
      return nilp;
      break;
    case SERIAL_BOOL_ID:
      return new Boolean;
      break;
    case SERIAL_BYTE_ID:
      return new Byte;
      break;
    case SERIAL_EOSC_ID:
      return nilp ;
      break;
    case SERIAL_INTG_ID:
      return new Integer;
      break;
    case SERIAL_RELT_ID:
      return new Relatif;
      break;
    case SERIAL_REAL_ID:
      return new Real;
      break;
    case SERIAL_CHAR_ID:
      return new Character;
      break;
    case SERIAL_STRG_ID:
      return new String;
      break;
    case SERIAL_REGX_ID:
      return new Regex;
      break;
    case SERIAL_BUFR_ID:
      return new Buffer;
      break;
    case SERIAL_CONS_ID:
      return new Cons;
      break;
    case SERIAL_VECT_ID:
      return new Vector;
      break;
    case SERIAL_OSET_ID:
      return new Set;
      break;
    case SERIAL_NTBL_ID:
      return new NameTable;
      break;
    case SERIAL_STRV_ID:
      return new Strvec;
      break;
    case SERIAL_PROP_ID:
      return new Property;
      break;
    case SERIAL_PLST_ID:
      return new Plist;
      break;
    case SERIAL_LIST_ID:
      return new List;
      break;
    case SERIAL_STRF_ID:
      return new Strfifo;
      break;
    case SERIAL_PTBL_ID:
      return new PrintTable;
      break;
    case SERIAL_MESG_ID:
      return new Message;
      break;
    case SERIAL_OBLK_ID:
    case SERIAL_BBLK_ID:
    case SERIAL_LBLK_ID:
    case SERIAL_RBLK_ID:
    case SERIAL_RPT2_ID:
      throw Exception ("serial-error", "cannot map internal serial block");
      break;
    default:
      break;
    }
    // check if we can get a callback
    return get_serial_object (sid);
  }

  // check if a nil serial id is present

  bool Serial::isnilid (InputStream& is) {
    is.wrlock ();
    try {
      t_byte sid = is.read ();
      is.pushback ((char) sid);
      is.unlock ();
      return (sid == SERIAL_NILP_ID);
    } catch (...) {
      is.unlock ();
      throw;
    }
  }

  // write a nil id to an output stream

  void Serial::wrnilid (OutputStream& os) {
    os.write ((char) SERIAL_NILP_ID);
  }

  // serialize a boolean to an output stream

  void Serial::wrbool (const bool value, class OutputStream& os) {
    Boolean bobj (value);
    bobj.wrstream (os);
  }

  // serialize a boolean array with a block
  
  void Serial::wrbool (const long size, const bool* data, OutputStream& os) {
    // check for nil
    if (size == 0L) return;
    // create an operating block
    Serial::Block blok (SRL_BLOK_SIZ, Serial::Block::BOOL);
    for (long k = 0L; k < size; k++) {
      blok.add (data[k]);
      if (blok.full () == false) continue;
      Serial::wrblok (blok, os);
      blok.clear ();
    }
    if (blok.empty () == false) Serial::wrblok (blok, os);	
  }

  // deserialize a boolean

  bool Serial::rdbool (InputStream& is) {
    Boolean bobj;
    bobj.rdstream (is);
    return bobj.tobool ();
  }

  // deserialize a boolean array

  bool* Serial::rdbool (InputStream& is, const long size) {
    // check for nill
    if (size == 0L) return nilp;
    // create a data block
    bool* result = new bool[size];
    try {
      // read the data block
      for (long i = 0; i < size; i++) {
	Block blok = Serial::rdblok (is);
	long  blen = blok.length ();
	for (long k = 0; k < blen; k++) result[i+k] = blok.getbool (k);
	i += (blen - 1L);
      }
      return result;
    } catch (...) {
      delete [] result;
      throw;
    }
  }

  // serialize a character to an output stream

  void Serial::wrchar (const t_quad value, class OutputStream& os) {
    Character cobj (value);
    cobj.wrstream (os);
  }

  // deserialize a boolean

  t_quad Serial::rdchar (InputStream& is) {
    Character cobj;
    cobj.rdstream (is);
    return cobj.toquad ();
  }
 
  // serialize an integer to an output stream

  void Serial::wrlong (const t_long value, OutputStream& os) {
    Integer iobj (value);
    iobj.wrstream (os);
  }

  // serialize an integer array with a block
  
  void Serial::wrlong (const long size, const long* data, OutputStream& os) {
    // check for nil
    if (size == 0L) return;
    // create an operating block
    Serial::Block blok (SRL_BLOK_SIZ, Serial::Block::LONG);
    for (long k = 0L; k < size; k++) {
      blok.add ((t_long) data[k]);
      if (blok.full () == false) continue;
      Serial::wrblok (blok, os);
      blok.clear ();
    }
    if (blok.empty () == false) Serial::wrblok (blok, os);	
  }

  // deserialize an integer
  
  t_long Serial::rdlong (InputStream& is) {
    Integer iobj;
    iobj.rdstream (is);
    return iobj.tolong ();
  }

  // deserialize an integer array

  long* Serial::rdlong (InputStream& is, const long size) {
    // check for nill
    if (size == 0L) return nilp;
    // create a data block
    long* result = new long[size];
    try {
      // read the data block
      for (long i = 0; i < size; i++) {
	Block blok = Serial::rdblok (is);
	long  blen = blok.length ();
	for (long k = 0; k < blen; k++) result[i+k] = blok.getlong (k);
	i += (blen - 1);
      }
      return result;
    } catch (...) {
      delete [] result;
      throw;
    }
  }

  // serialize a real to an output stream

  void Serial::wrreal (const t_real value, OutputStream& os) {
    Real robj (value);
    robj.wrstream (os);
  }

  // serialize a real array with a block
  
  void Serial::wrreal (const long size, const t_real* data, OutputStream& os) {
    // check for nil
    if (size == 0L) return;
    // create an operating block
    Serial::Block blok (SRL_BLOK_SIZ, Serial::Block::REAL);
    for (long k = 0L; k < size; k++) {
      blok.add (data[k]);
      if (blok.full () == false) continue;
      Serial::wrblok (blok, os);
      blok.clear ();
    }
    if (blok.empty () == false) Serial::wrblok (blok, os);	
  }

  // deserialize a real
  
  t_real Serial::rdreal (InputStream& is) {
    Real robj;
    robj.rdstream (is);
    return robj.toreal ();
  }

  // deserialize a real data array

  t_real* Serial::rdreal (InputStream& is, const long size) {
    // check for nill
    if (size == 0L) return nilp;
    // create a data block
    t_real* result = new t_real[size];
    try {
      // read the data block
      for (long i = 0; i < size; i++) {
	Block blok = Serial::rdblok (is);
	long  blen = blok.length ();
	for (long k = 0; k < blen; k++) result[i+k] = blok.getreal (k);
	i += (blen - 1);
      }
      return result;
    } catch (...) {
      delete [] result;
      throw;
    }
  }

  // serialize a block to an output stream

  void Serial::wrblok (const Block& blok, OutputStream& os) {
    // write the serial id
    os.write ((char) blok.serialid ());
    // serialize the object
    blok.wrstream (os);
  }

  // deserialize a block

  Serial::Block Serial::rdblok (InputStream& is) {
    // get a block by serial id
    t_byte sid = is.read ();
    Serial::Block blok (sid);
    // read in the blok
    blok.rdstream (is);
    return blok;
  }

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // return the object serial code

  t_byte Serial::serialid (void) const {
    throw Exception ("serial-error", "cannot get serial id for", repr ());
  }

  // serialize an object to an output stream

  void Serial::wrstream (OutputStream& os) const {
    throw Exception ("serial-error", "cannot serialize object", repr ());
  }

  // deserialize an object from an input stream

  void Serial::rdstream (InputStream& is) {
    throw Exception ("serial-error", "cannot deserialize object", repr ());
  }

  // serialize an object with it serial id

  void Serial::serialize (OutputStream& os) const {
    // write the serial id
    os.write ((char) serialid ());
    // serialize the object
    wrstream (os);
  }

  // return an object by deserialization

  Object* Serial::deserialize (InputStream& is) {
    // get a new object by serial id
    t_byte   sid = is.read ();
    Serial* sobj = Serial::getserial (sid);
    if (sobj == nilp) return nilp;
    // read in the object
    sobj->rdstream (is);
    return sobj;
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 4;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_RSRLZ = zone.intern ("read-serial");
  static const long QUARK_WSRLZ = zone.intern ("write-serial");
  static const long QUARK_SERLZ = zone.intern ("serialize");
  static const long QUARK_DERLZ = zone.intern ("deserialize");

  // return true if the given quark is defined

  bool Serial::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? Object::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }

  // apply this object with a set of arguments and a quark

  Object* Serial::apply (Runnable* robj, Nameset* nset, const long quark,
			 Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();

    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_RSRLZ) {
	Object* obj = argv->get (0);
	InputStream* is = dynamic_cast <InputStream*> (obj);
	if (is == nilp) {
	  throw Exception ("type-error", "invalid object with read-serial",
			   Object::repr (obj));
	}
	rdstream (*is);
	return nilp;
      }
      if (quark == QUARK_DERLZ) {
	Object* obj = argv->get (0);
	InputStream* is = dynamic_cast <InputStream*> (obj);
	if (is == nilp) {
	  throw Exception ("type-error", "invalid object with deserialize",
			   Object::repr (obj));
	}
	return deserialize (*is);
      }
      if (quark == QUARK_WSRLZ) {
	Object* obj = argv->get (0);
	OutputStream* os = dynamic_cast <OutputStream*> (obj);
	if (os == nilp) {
	  throw Exception ("type-error", "invalid object with write-serial",
			   Object::repr (obj));
	}
	wrstream (*os);
	return nilp;
      }
      if (quark == QUARK_SERLZ) {
	Object* obj = argv->get (0);
	OutputStream* os = dynamic_cast <OutputStream*> (obj);
	if (os == nilp) {
	  throw Exception ("type-error", "invalid object with serialize",
			   Object::repr (obj));
	}
	serialize (*os);
	return nilp;
      }
    }
    // call the object method
    return Object::apply(robj, nset, quark, argv);
  }
}
