// ---------------------------------------------------------------------------
// - t_vector.cpp                                                            -
// - standard object library - vector class tester                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "String.hpp"

int main (int, char**) {
  using namespace afnix;

  // create a new vector
  Vector  vobj;
  String* hello  = new String ("hello");
  String* world  = new String ("world");
  
  // insert out favorite message
  vobj.add (hello);
  vobj.add (world);

  // check vector size
  if (vobj.length () != 2) return 1;

  // get first object
  Object* object = vobj.get (0);
  String* strobj = dynamic_cast <String*> (object);

  // check for string
  if (strobj  == nilp)    return 1;
  if (*strobj != "hello") return 1;

  // query next string
  object = vobj.get (1);
  strobj = dynamic_cast <String*> (object);

  // check for string
  if (strobj  == nilp)    return 1;
  if (*strobj != "world") return 1;

  // add a string in the middle
  vobj.add (1, new String ("new"));
  if (vobj.length () != 3) return 1;

  // check the new string
  object = vobj.get (1);
  strobj = dynamic_cast <String*> (object);
  if (*strobj != "new") return 1;

  // test the iterator
  for (auto s : vobj) {
    if (dynamic_cast <String*> (s) == nilp) return 1;
  }

  // ok - everything is fine
  return 0;
}
