// ---------------------------------------------------------------------------
// - SioCalls.hpp                                                            -
// - afnix:sio module - i/o specific calls definitions                       -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SIOCALLS_HPP
#define  AFNIX_SIOCALLS_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains specific system procedures that require only
  /// arguments and that are not bounded to a class
  /// @author amaury darsch

  /// remove a list of files by name
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_rmfile (Runnable* robj, Nameset* nset, Cons* args);

  /// create a list of directories by name
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_mkdir (Runnable* robj, Nameset* nset, Cons* args);

  /// create hierarchicaly a list of directories by name
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_mhdir (Runnable* robj, Nameset* nset, Cons* args);

  /// remove a list of directories by name
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_rmdir (Runnable* robj, Nameset* nset, Cons* args);

  /// extract the base name from a path
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_xname (Runnable* robj, Nameset* nset, Cons* args);

  /// extract the directory base path from a path
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_xdir (Runnable* robj, Nameset* nset, Cons* args);

  /// extract the extension from a path
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_xext (Runnable* robj, Nameset* nset, Cons* args);

  /// remove the extension from a path
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_rmext (Runnable* robj, Nameset* nset, Cons* args);

  /// create a temporary file name
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_tmp_name (Runnable* robj, Nameset* nset, Cons* args);

  /// create a temporary path
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_tmp_path (Runnable* robj, Nameset* nset, Cons* args);

  /// build an absolute file path
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_abs_path (Runnable* robj, Nameset* nset, Cons* args);

  /// build a relative file path
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sio_rel_path (Runnable* robj, Nameset* nset, Cons* args);
}


#endif
