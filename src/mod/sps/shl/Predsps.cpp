// ---------------------------------------------------------------------------
// - Predsps.cpp                                                             -
// - afnix:sps module - predicates implementation                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Csv.hpp"
#include "Cons.hpp"
#include "Folio.hpp"
#include "Predsps.hpp"
#include "Boolean.hpp"
#include "Transfer.hpp"
#include "Exception.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // bndp: bundle object predicate

  Object* sps_bndp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "bundle-p");
    bool result =  (dynamic_cast <Bundle*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // celp: cell object predicate

  Object* sps_celp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "cell-p");
    bool result =  (dynamic_cast <Cell*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rcdp: record object predicate

  Object* sps_rcdp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "record-p");
    bool result =  (dynamic_cast <Record*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // shtp: sheet object predicate

  Object* sps_shtp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "sheet-p");
    bool result =  (dynamic_cast <Sheet*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // folp: collection object predicate

  Object* sps_folp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "folio-p");
    bool result =  (dynamic_cast <Folio*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // idxp: index object predicate

  Object* sps_idxp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "index-p");
    bool result =  (dynamic_cast <Index*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xrfp: xref table object predicate

  Object* sps_xrfp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xref-p");
    bool result =  (dynamic_cast <Xref*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // csvp: csv reader object predicate

  Object* sps_csvp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "csv-p");
    bool result =  (dynamic_cast <Csv*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xfer: transfer object predicate

  Object* sps_xfer  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "transfer-p");
    bool result = (dynamic_cast<Transfer*>(obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // rfer: record transfer object predicate

  Object* sps_rfer  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "record-transfer-p");
    bool result = (dynamic_cast<RecordTransfer*>(obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // sfer: sheet transfer object predicate

  Object* sps_sfer  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "sheet-transfer-p");
    bool result =  (dynamic_cast<SheetTransfer*>(obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
}
