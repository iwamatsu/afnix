// ---------------------------------------------------------------------------
// - Material.hpp                                                            -
// - afnix:geo service - material class definition                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_MATERIAL_HPP
#define  AFNIX_MATERIAL_HPP

#ifndef  AFNIX_TEXTURE_HPP
#include "Texture.hpp"
#endif

namespace afnix {

  /// The Texture class is a geometry material descriptor. The material is
  /// defined with a texture and a set of extra parameters such like the
  /// alpha channel.
  /// @author amaury darsch

  class Material : public Texture {
  private:
    /// the alpha channel
    t_real d_achn;
    
  public:
    /// create a default material
    Material (void);

    /// create a material by name
    /// @param name the material name
    Material (const String& name);

    /// copy construct this material
    /// @param that the material to copy
    Material (const Material& that);

    /// assign a material to this one
    /// @param that the material to assign
    Material& operator = (const Material& that);

    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;
    
    /// reset this material
    void reset (void);

    /// @return the material name
    /// set the material by plist
    /// @param plst the material plist
    void setplst (const Plist& plst);

    /// @return the material plist
    Plist getplst (void) const;

    /// @return the alpha channel
    virtual t_real getachn (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
