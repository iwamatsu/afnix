// ---------------------------------------------------------------------------
// - TlsProto.hxx                                                            -
// - afnix:tls service - tls protocol internal                               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSPROTO_HXX
#define  AFNIX_TLSPROTO_HXX

#ifndef  AFNIX_TLSPROTO_HPP
#include "TlsProto.hpp"
#endif

#ifndef  AFNIX_TLSSUITE_HXX
#include "TlsSuite.hxx"
#endif

namespace afnix {
  // the master secret prf label
  static const String TLS_LABL_MS = "master secret";
  // the key expansion prf label
  static const String TLS_LABL_KE = "key expansion";
  // the client finished prf label
  static const String TLS_LABL_CF = "client finished";
  // the server finished prf label
  static const String TLS_LABL_SF = "server finished";
  
  // the server handshake server sequence
  using t_shss = void (TlsProto::*) (OutputStream*, TlsState*) const;
  // the server handshake server change cipher spec
  using t_shsc = bool (TlsProto::*) (OutputStream*, TlsState*) const;
  // the server handshake client sequence
  using t_shcs = TlsHblock* (TlsProto::*) (InputStream*, TlsState*) const;
  // the server handshake client change cipher spec
  using t_shcc = bool (TlsProto::*) (InputStream*, TlsState*) const;

  // the server handshake sequence
  struct s_shss {
    // the send server hello
    t_shss p_shlo;
    // the send server certificate
    t_shss p_scrt;
    // the send server key exchange
    t_shss p_skxh;
    // the send server certificate request
    t_shss p_sreq;
    // the send server done
    t_shss p_sdon;
    // the send change cipher spec
    t_shsc p_sccs;
    // the send server finish
    t_shss p_sfin;
    // the client key exchange
    t_shcs p_ckxh;
    // the client change cipher spec
    t_shcc p_cccs;
    // the client finished
    t_shcs p_cfin;
    // the key exchange message
    TlsInfos* p_mkxh;
    // the finished message
    TlsInfos* p_mfin;
    // the change cipher spec flag
    bool d_cccs;
    // the client verify message buffer
    Buffer d_cvmb;
    // create a default handshake sequence
    s_shss (void) {
      p_mkxh = nilp;
      p_mfin = nilp;
      reset ();
    }
    // create a handshake sequence by cipher code
    s_shss (const t_word cifr) {
      p_mkxh = nilp;
      p_mfin = nilp;
      reset ();
      setcifr (cifr);
    }
    // reset the handshake sequence
    void reset (void) {
      // server send protocol
      p_shlo = nilp;
      p_scrt = nilp;
      p_skxh = nilp;
      p_sreq = nilp;
      p_sdon = nilp;
      p_sccs = nilp;
      p_sfin = nilp;
      // server receive protocol
      p_ckxh = nilp;
      p_cccs = nilp;
      p_cfin = nilp;
      // received client messages and flag
      delete p_mkxh; p_mkxh = nilp;
      delete p_mfin; p_mfin = nilp;
      d_cccs = false;
    }
    
    // set a handshake sequence by cipher code
    void setcifr (const t_word cifr) {
      // set server send protocol
      p_shlo = &TlsProto::sndshlo;
      p_sdon = &TlsProto::sndsdon;
      p_sccs = &TlsProto::sndsccs;
      p_sfin = &TlsProto::sndsfin;
      // set server receive protocol
      p_ckxh = &TlsProto::rcvckxh;
      p_cccs = &TlsProto::rcvcccs;
      p_cfin = &TlsProto::rcvcfin;
      // set cipher specific protocol
      switch (cifr) {
      case TLS_NULL_WITH_NULL_NULL:
	break;
      case TLS_RSA_WITH_NULL_MD5:
	p_scrt = &TlsProto::sndscrt;	
	break;
      case TLS_RSA_WITH_NULL_SHA:
	p_scrt = &TlsProto::sndscrt;	
	break;
      case TLS_RSA_WITH_RC4_128_MD5:
	p_scrt = &TlsProto::sndscrt;	
	break;
      case TLS_RSA_WITH_RC4_128_SHA:
	p_scrt = &TlsProto::sndscrt;	
	break;
      case TLS_RSA_WITH_AES_128_CBC_SHA:
	p_scrt = &TlsProto::sndscrt;	
	break;
      case TLS_RSA_WITH_AES_256_CBC_SHA:
	p_scrt = &TlsProto::sndscrt;	
	break;
      default:
	throw Exception ("tls-error", "invalid cipher code in sequence");
	break;
      }
    }
    // send a server handshake sequence
    void sndss (OutputStream* os, TlsProto* tlsp, TlsState* ssta) {
      // check for nil
      if (tlsp == nilp) return;
      // execute in sequence
      if (p_shlo != nilp) (tlsp->*p_shlo) (os, ssta);
      if (p_scrt != nilp) (tlsp->*p_scrt) (os, ssta);
      if (p_skxh != nilp) (tlsp->*p_skxh) (os, ssta);
      if (p_sreq != nilp) (tlsp->*p_sreq) (os, ssta);
      if (p_sdon != nilp) (tlsp->*p_sdon) (os, ssta);
    }
    // receive a client handshake sequence
    void rcvcs (InputStream* is, TlsProto* tlsp, TlsState* ssta) {
      // check for nil
      if (tlsp == nilp) return;
      // execute in sequence
      if (p_ckxh != nilp) p_mkxh = (tlsp->*p_ckxh) (is, ssta);
    }
    // finish a client handshake sequence
    void fincs (InputStream* is, TlsProto* tlsp, TlsState* ssta) {
      // check for nil
      if (tlsp == nilp) return;
      // save the client verify message buffer
      if (ssta != nilp) d_cvmb = ssta->gethvmb();
      // execute in sequence
      if (p_cccs != nilp) d_cccs = (tlsp->*p_cccs) (is, ssta);
      if (p_cfin != nilp) p_mfin = (tlsp->*p_cfin) (is, ssta);
    }
    // send a client finish sequence
    void sndcs (OutputStream* os, TlsProto* tlsp, TlsState* ssta) {
      // check for nil
      if (tlsp == nilp) return;
      // execute in sequence
      if (p_sccs != nilp) (tlsp->*p_sccs) (os, ssta);
      if (p_sfin != nilp) (tlsp->*p_sfin) (os, ssta);
    }
  };
}

#endif
