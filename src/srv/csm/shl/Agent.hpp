// ---------------------------------------------------------------------------
// - Agent.hpp                                                               -
// - standard object library - cloud agent class definition                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_AGENT_HPP
#define  AFNIX_AGENT_HPP

#ifndef  AFNIX_PLIST_HPP
#include "Plist.hpp"
#endif

namespace afnix {

  /// The Agent class is a base class that models the behavior of a registered
  /// cloud object in the form of an agent. The agent object operates through
  /// the use of parameter methods, namelly information, accessor and mutator.
  /// An agent is registed as soon as its registration id is set. If the
  /// registration id is unset the object is unregistered or anonymous.
  /// The registration id can be anything as long as as it is understood by
  /// the implementation that such registration is to be interpreted somewhere
  /// else.
  /// @author amaury darsch

  class Agent :  public Nameable, public Serial {
  protected:
    /// the key id
    t_long  d_kid;
    /// the registration id
    String  d_rid;
    /// the registration name
    String  d_name;
    /// the registration info
    String  d_info;

  public:
    /// create nil agent
    Agent (void);

    /// create a registered agent by name
    /// @param name the registration name
    Agent (const String& name);

    /// create a registered agent by name and info
    /// @param name the registration name
    /// @param info the registration info
    Agent (const String& name, const String& info);

    /// create a registered agent by rid, name and info
    /// @param rid  the registration id
    /// @param name the registration name
    /// @param info the registration info
    Agent (const String& rid, const String& name, const String& info);

    /// copy construct this registered object
    /// @param that the object to copy
    Agent (const Agent& that);
    
    /// assign an agent to this one
    /// @param that the object to assign
    Agent& operator = (const Agent& that);

    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// @return the agent serial id
    t_byte serialid (void) const;

    /// serialize this agent
    /// @param os the output stream
    void wrstream (OutputStream& os) const;

    /// deserialize this agent
    /// @param is the input stream
    void rdstream (InputStream& os);

    /// @return the registration name
    String getname (void) const;

    /// @return the registration info
    virtual String getinfo (void) const;
    
    /// validate an agent key id
    /// @param kid the key id to validate
    virtual bool iskid (const t_long kid) const;

    /// set the agent key id
    /// @param kid the kid to set
    virtual void setkid (const t_long kid);

    /// @return the key id
    virtual t_long getkid (void) const;

    /// validate an agent registration id
    /// @param rid the rid to validate
    virtual bool isrid (const String& rid) const;

    /// set the registration id
    /// @param rid the rid to set
    virtual void setrid (const String& rid);

    /// @return the registration id
    virtual String getrid (void) const;

    /// @return the agent plist
    virtual Plist getplst (void) const;

  public:
    /// create a object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
