// ---------------------------------------------------------------------------
// - Quaternion.hpp                                                          -
// - afnix:geo service - quaternion object class definition                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_QUATERNION_HPP
#define  AFNIX_QUATERNION_HPP

#ifndef  AFNIX_VECTOR3_HPP
#include "Vector3.hpp"
#endif

namespace afnix {

  /// The Quaternion class is an object that implements the behavior of
  /// quaternion, that is a 4-tuple which is mostly used for object
  /// rotation. Unlike other rotation object, the quaternion is numerically
  /// stable and does not suffer from the 'gimbal lock' problem.
  /// @author amaury darsch

  class Quaternion : public virtual Object {
  public:
    /// compute the opposite of the quaternion
    /// @param q the quaternion to oppose
    friend Quaternion operator - (const Quaternion& q);

    /// add two quaternions together
    /// @param qa the first argument to add
    /// @param qb the second argument to add
    friend Quaternion operator + (const Quaternion& qa, const Quaternion& qb);

    /// substract two quaternions together
    /// @param qa the first argument to add
    /// @param qb the second argument to add
    friend Quaternion operator - (const Quaternion& qa, const Quaternion& qb);

    /// multiply two quaternions together
    /// @param qa the first argument to add
    /// @param qb the second argument to add
    friend Quaternion operator * (const Quaternion& qa, const Quaternion& qb);


  protected:
    /// the x rotation component
    t_real d_x;
    /// the y rotation component
    t_real d_y;
    /// the z rotation component
    t_real d_z;
    /// the real part
    t_real d_w;
    
  public:
    /// create a default quaternion
    Quaternion (void);

    /// create a quaternion by components
    /// @param x the x component
    /// @param y the y component
    /// @param z the z component
    /// @param w the w component
    Quaternion (const t_real x, const t_real y, const t_real z, const t_real w);

    /// copy construct this quaternion
    /// @param that the quaternion to copy
    Quaternion (const Quaternion& that);

    /// assign a quaternion to this one
    /// @param that the quaternion to assign
    Quaternion& operator = (const Quaternion& that);

    /// add a quaternion to this one
    /// @param q the argument to add
    /// @return this added quaternion
    Quaternion& operator += (const Quaternion& q);

    /// multiply a quaternion by a scalar
    /// @param s the scalar factor
    Quaternion& operator *= (const t_real s);

    /// divide a quaternion by a scalar
    /// @param s the scalar factor
    Quaternion& operator /= (const t_real s);

    /// @return the class name
    String repr (void) const;
    
    /// @return a clone of this object
    Object* clone (void) const;

    /// reset this quaternion
    virtual void reset (void);
    
    /// @return the quaternion x component
    virtual t_real getx (void) const;

    /// @return the quaternion y component
    virtual t_real gety (void) const;
    
    /// @return the quaternion z component
    virtual t_real getz (void) const;
    
    /// @return the quaternion w component
    virtual t_real getw (void) const;

    /// set the quaternion at once
    /// @param x the x vector component
    /// @param y the y vector component
    /// @param z the z vector component
    /// @param w the real part
    virtual Quaternion& set (const t_real x, const t_real y, const t_real z,
			     const t_real w);

    /// set the quaternion by angle and vector
    /// @param a the quaternion angle
    /// @param v the quaternion vector
    virtual Quaternion& set (const t_real a, const Rvi& v);
    
    /// @return the quaternion norm
    virtual t_real norm (void) const;

    /// normalize this quaternion
    virtual Quaternion& normalize (void);

    /// conjugate this quaternion
    virtual Quaternion& conjugate (void);

    /// rotate a generic vector
    /// @param v the vector to rotate
    virtual Vector3 rotate (const Rvi& v) const;

    /// check for a x-axis rotation
    virtual bool isrx (void) const;
    
    /// check for a y-axis rotation
    virtual bool isry (void) const;
    
    /// check for a z-axis rotation
    virtual bool isrz (void) const;
    
    /// set the rotation x angle
    /// @param a the rotation angle
    virtual Quaternion& setrx (const t_real a);

    /// set the rotation y angle
    /// @param a the rotation angle
    virtual Quaternion& setry (const t_real a);
    
    /// set the rotation z angle
    /// @param a the rotation angle
    virtual Quaternion& setrz (const t_real a);

    /// set the rotation axis angle at once
    /// @param xa the x-axis angle
    /// @param ya the y-axis angle
    /// @param za the z-axis angle
    virtual void setra (const t_real xa, const t_real ya, const t_real za);
    
    /// @return the quaternion angle
    virtual t_real getra (void) const;

    /// @return the axis rotation angle
    virtual t_real getxa (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
