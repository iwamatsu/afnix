// ---------------------------------------------------------------------------
// - Predsec.hpp                                                             -
// - afnix:sec module - predicates declaration                               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDSEC_HPP
#define  AFNIX_PREDSEC_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {
  
  /// this file contains the predicates associated with the afnix:sec
  /// standard module.
  /// @author amaury darsch

  /// the hasher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_hashp   (Runnable* robj, Nameset* nset, Cons* args);

  /// the key object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_keyp    (Runnable* robj, Nameset* nset, Cons* args);

 /// the signature object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_sgnp    (Runnable* robj, Nameset* nset, Cons* args);

  /// the dsa object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_dsap    (Runnable* robj, Nameset* nset, Cons* args);

  /// the MD2 hasher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_md2p    (Runnable* robj, Nameset* nset, Cons* args);

  /// the MD4 hasher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_md4p    (Runnable* robj, Nameset* nset, Cons* args);

  /// the MD5 hasher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_md5p (Runnable* robj, Nameset* nset, Cons* args);

  /// the SHA-1 hasher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_sha1p (Runnable* robj, Nameset* nset, Cons* args);

  /// the SHA-224 hasher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_sha224p (Runnable* robj, Nameset* nset, Cons* args);

  /// the SHA-256 hasher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_sha256p (Runnable* robj, Nameset* nset, Cons* args);

  /// the SHA-384 hasher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_sha384p (Runnable* robj, Nameset* nset, Cons* args);

  /// the SHA-512 hasher  object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_sha512p (Runnable* robj, Nameset* nset, Cons* args);

  /// the kdf object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_kdfp (Runnable* robj, Nameset* nset, Cons* args);

  /// the hashed kdf object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_hkdfp (Runnable* robj, Nameset* nset, Cons* args);

  /// the kdf1 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_kdf1p (Runnable* robj, Nameset* nset, Cons* args);

  /// the kdf2 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_kdf2p (Runnable* robj, Nameset* nset, Cons* args);

  /// the mac object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_macp (Runnable* robj, Nameset* nset, Cons* args);

  /// the hmac object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_hmacp (Runnable* robj, Nameset* nset, Cons* args);

  /// the des cipher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_desp (Runnable* robj, Nameset* nset, Cons* args);

  /// the aes cipher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_aesp (Runnable* robj, Nameset* nset, Cons* args);

  /// the rc2 cipher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_rc2p (Runnable* robj, Nameset* nset, Cons* args);

  /// the rc4 cipher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_rc4p (Runnable* robj, Nameset* nset, Cons* args);

  /// the rc5 cipher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_rc5p (Runnable* robj, Nameset* nset, Cons* args);

  /// the rsa cipher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_rsap (Runnable* robj, Nameset* nset, Cons* args);

  /// the cipher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_cifrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the block cipher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_bcfrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the public cipher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_pcfrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the input cipher object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sec_icfrp (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
