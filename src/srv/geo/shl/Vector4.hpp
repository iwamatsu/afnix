// ---------------------------------------------------------------------------
// - Vector4.hpp                                                             -
// - afnix:geo service - vector 4 class definition                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_VECTOR4_HPP
#define  AFNIX_VECTOR4_HPP

#ifndef  AFNIX_POINT4_HPP
#include "Point4.hpp"
#endif

#ifndef  AFNIX_RVECTOR_HPP
#include "Rvector.hpp"
#endif

namespace afnix {

  /// The Vector4 class is a 4 dimensional vector class for the afnix geometry
  /// service. A 4 dimensional vector is defined by its (x,y,z,w) components in
  /// a base choosen by the system.
  /// @author amaury darsch

  class Vector4 : public Rvector {    
  public:
    /// create a default vector
    Vector4 (void);

    /// create a vector by components
    /// @param x the x component
    /// @param y the y component
    /// @param z the z component
    /// @param w the w component
    Vector4 (const t_real x, const t_real y, const t_real z, const t_real w);

    /// copy construct this vector
    /// @param that the vector to copy
    Vector4 (const Vector4& that);

    /// assign a vector to this one
    /// @param that the vector to assign
    Vector4& operator = (const Vector4& that);
    
    /// @return the class name
    String repr (void) const;
    
    /// @return a clone of this object
    Object* clone (void) const;

    /// @return the vector x component
    virtual t_real getx (void) const;

    /// @return the vector y component
    virtual t_real gety (void) const;
    
    /// @return the vector z component
    virtual t_real getz (void) const;
    
    /// @return the vector w component
    virtual t_real getw (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
