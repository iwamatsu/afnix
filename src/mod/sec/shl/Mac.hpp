// ---------------------------------------------------------------------------
// - Mac.hpp                                                                 -
// - afnix:sec module - base mac class definition                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_MAC_HPP
#define  AFNIX_MAC_HPP

#ifndef  AFNIX_KEY_HPP
#include "Key.hpp"
#endif

#ifndef  AFNIX_NAMEABLE_HPP
#include "Nameable.hpp"
#endif

#ifndef  AFNIX_INPUTSTREAM_HPP
#include "InputStream.hpp"
#endif

namespace afnix {

  /// The Mac class is a base class that is used to build a message
  /// authentication code. The class operates like a hasher object with
  /// the help of a secret key used to build the MAC result. The key
  /// is set at the object construction. The mac can be computed from
  /// an octet string or from an input stream.
  /// @author amaury darsch

  class Mac : public Nameable {
  protected:
    /// the mac name
    String d_name;
    /// the mac key
    Key    d_mkey;
    
  public:
    /// create a mac object by name and key
    /// @param name the mac name
    /// @param mkey the mac key
    Mac (const String& name, const Key& mkey);

    /// @return the class name
    String repr (void) const;

    /// @return the mac name
    String getname (void) const;

    /// reset this mac
    virtual void reset (void) =0;

    /// @return the mac key
    virtual Key getkey (void) const;

    /// derive a message mac from a string
    /// @param s the string to process
    virtual String derive (const String& msg);

    /// compute a message mac from a string
    /// @param msg the string message to process
    virtual String compute (const String& msg);

    /// compute a message mac from a buffer
    /// @param buf the buffer to process
    virtual String compute (Buffer& buf); 

    /// compute a message mac from an input stream
    /// @param is the input stream
    virtual String compute (InputStream& is); 

    /// @return the mac length
    virtual long length (void) const =0;

    /// @return the mac value by index
    virtual t_byte getbyte (const long index) const =0;

    /// push the mac value into a buffer
    /// @param buf the buffer to fill
    virtual long pushb (Buffer& buf) =0;
    
    /// @return the formatted mac code
    virtual String format (void) const =0;

    /// process a message by data
    /// @param data the data to process
    /// @param size the data size
    virtual void process (const t_byte* data, const long size) =0;
 
    /// process a message with a buffer
    /// @param buf the buffer to process
    virtual void process (Buffer& buf) =0;

    /// process a message with an input stream
    /// @param is the input stream to process
    virtual void process (InputStream& is) =0;

    /// finish processing the mac
    virtual void finish (void) =0;

  private:
    // make the copy constructor private
    Mac (const Mac&);
    // make the assignment operator private
    Mac& operator = (const Mac&);

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
