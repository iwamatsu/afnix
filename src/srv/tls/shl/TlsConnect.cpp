// ---------------------------------------------------------------------------
// - TlsConnect.cpp                                                          -
// - afnix:tls service - tls connect class implementation                    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Md5.hpp"
#include "Sha1.hpp"
#include "Vector.hpp"
#include "TlsAlert.hpp"
#include "TlsShake.hpp"
#include "TlsTypes.hxx"
#include "TlsProto.hxx"
#include "Runnable.hpp"
#include "TlsChello.hpp"
#include "TlsShello.hpp"
#include "TlsCkeyxh.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "TlsConnect.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // this procedure compute the master secret buffer
  static Buffer tls_get_mbuf (const s_shss& shss, TlsState* ssta) {
    // get the key exchange message
    TlsHblock* hblk = dynamic_cast <TlsHblock*> (shss.p_mkxh);
    if ((hblk == nilp) || (ssta == nilp)) {
      throw Exception ("tls-error", "nil object for master secret compute");
    }
    // get the state exchange key
    Key* hkey = (ssta == nilp) ? nilp : ssta->getxkey ();
    // decode the key exchange
    TlsCkeyxh ckxh (hblk, hkey);
    // get the premaster key - no copy so the premaster is destroyed
    // when the key exchanged is destroyed
    long mlen = ckxh.getmlen ();
    const t_byte* mbuf = ckxh.getmbuf ();
    // combine the client and server random
    Buffer seed; seed.add (ssta->getcrnd ()); seed.add (ssta->getsrnd ());
    // call the standard prf
    Buffer result = TlsProto::prf (mlen, mbuf, TLS_LABL_MS, seed, TLS_SIZE_MSX);
    // here it is
    return result;
  }

  // this procedure compute the key expansion block buffer
  static Buffer tls_get_ebuf (TlsState* ssta) {
    if (ssta == nilp) {
      throw Exception ("tls-error", "nil object for key expansion compute");
    }
    // compute the block length
    long blen = 2 * (ssta->getksiz () + ssta->getvsiz () + ssta->gethsiz ());
    // get the master secret buffer
    Buffer mbuf = ssta->getmbuf ();
    // combine the server and client random [opposite to master secret ah! ah!]
    Buffer seed; seed.add (ssta->getsrnd ()); seed.add (ssta->getcrnd ());
    // compute the prf
    return TlsProto::prf (mbuf, TLS_LABL_KE, seed, blen);
  }

  // this procedure verify the finished message block
  static void tls_chk_mfin (const s_shss& shss, TlsState* ssta) {
    // get the handshake finished message
    TlsHblock* hblk = dynamic_cast <TlsHblock*> (shss.p_mfin);
    if ((hblk == nilp) || (ssta == nilp)) {
      throw Exception ("tls-error", "nil object for finished verification");
    }
    // check the block finished size
    if (hblk->length() != TLS_SIZE_FIN) {
      throw Exception ("tls-error", "invalid finished message size");
    }    
    // compute the finished signature and verify
    Buffer finb = TlsProto::cfin (ssta->getmbuf(), shss.d_cvmb);
    if (finb.length() != TLS_SIZE_FIN){
      throw Exception ("tls-error", "invalid finished buffer size");
    }
    // compare the block buffer with the computed buffer
    if (hblk->tobuffer() != finb) {
      throw Exception ("tls-error", "handshake verify failure");
    }
  }

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a tls connect by flag

  TlsConnect::TlsConnect (const bool sflg) {
    d_sflg = sflg;
  }

  // create a tls connect by flag and parameters

  TlsConnect::TlsConnect (const bool sflg, const TlsParams& prms) {
    d_sflg = sflg;
    setprms (prms);
  }

  // return the class name
  
  String TlsConnect::repr (void) const {
    return "TlsConnect";
  }

  // set the tls parameters

  void TlsConnect::setprms (const TlsParams& prms) {
    wrlock ();
    try {
      d_prms = prms;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // perform a handshake operation

  TlsState* TlsConnect::connect (InputStream* is, OutputStream* os) noexcept {
    // check for nil
    if ((is == nilp) || (os == nilp)) return nilp;
    // connect and lock
    wrlock ();
    try {
      // connect the tls socket
      TlsState* result = d_sflg ? getss (is, os) : nilp;
      unlock ();
      return result;
    } catch (const TlsAlert& msg) {
      msg.write (os);
      unlock ();
      return nilp;
    } catch (...) {
      TlsAlert(TlsAlert::TLS_DESC_HFL).write (os);
      unlock ();
      return nilp;
    }
  }

  // -------------------------------------------------------------------------
  // - protected section                                                     -
  // -------------------------------------------------------------------------

  // initialize the server state

  TlsState* TlsConnect::iniss (InputStream* is) {
    // check for nil
    if (is == nilp) return nilp;
    // lock and connect
    wrlock ();
    TlsState*  ssta = nilp;
    TlsChello* chlo = nilp;
    try {
      // check for consistency
      if (d_sflg == false) {
	throw Exception ("tls-error", "invalid server state initialize");
      }
      // create a server state by flag
      ssta = new TlsState (d_sflg, d_prms);
      // get the client hello message
      chlo = dynamic_cast <TlsChello*> (TlsProto().rcvchlo (is, ssta));
      if (chlo == nilp) {
	throw Exception ("tls-error", "cannot get client hello message");
      }
      ssta->addinfo (chlo->getinfo ());
      // update the state version
      ssta->setvers (chlo->getvmaj (), chlo->getvmin ());
      // update the client random
      ssta->setcrnd (chlo->getrand ());
      // update the cipher suite
      ssta->setsuit (chlo->getsuit ());
      // clean and unlock
      delete chlo;
      unlock ();
      return ssta;
    } catch (...) {
      delete ssta;
      delete chlo;
      unlock ();
      throw;
    }
  }

  // get a server state

  TlsState* TlsConnect::getss (InputStream* is, OutputStream* os) {
    // check for nil
    if ((is == nilp) || (os == nilp)) return nilp;
    // lock and connect
    wrlock ();
    TlsState* ssta = nilp;
    TlsProto* tlsp = nilp;
    try {
      // initialize the server state
      ssta = iniss (is);
      // create a tls protocol by state
      tlsp = TlsProto::create (ssta);
      // create a tls handshake sequence
      s_shss shss (ssta->getcifr ());
      // send server handshake sequence
      shss.sndss (os, tlsp, ssta);
      // receive a client handshake sequence
      shss.rcvcs (is, tlsp, ssta);
      // get/set the master key
      ssta->setmbuf (tls_get_mbuf (shss, ssta));
      // get/set the expansion block buffer
      ssta->setebuf (tls_get_ebuf (ssta));      
      // finish a client handshake sequence
      shss.fincs (is, tlsp, ssta);
      // verify the finished message
      tls_chk_mfin (shss, ssta);
      // send the client finish message
      shss.sndcs (os, tlsp, ssta);
      // clean and unlock
      delete tlsp;
      unlock ();
      return ssta;
    } catch (...) {
      delete ssta;
      delete tlsp;
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 2;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_CONNECT = zone.intern ("connect");
  static const long QUARK_SETPRMS = zone.intern ("set-parameters");
 
  // create a new object in a generic way

  Object* TlsConnect::mknew (Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();

    // check for 1 argument
    if (argc == 1) {
      bool sflg = argv->getbool (0);
      return new TlsConnect (sflg);
    }
    // check for 2 arguments
    if (argc == 2) {
      bool sflg = argv->getbool (0);
      Object* obj = argv->get (1);
      TlsParams* prms = dynamic_cast <TlsParams*> (obj);
      if (prms == nilp) {
	throw Exception ("type-error", "invalid object as parameters",
			 Object::repr (obj));
      }
      return new TlsConnect (sflg, *prms);
    }
    // too many arguments
    throw Exception ("argument-error", 
                     "too many argument with tls connect constructor");
  }
  
  // return true if the given quark is defined

  bool TlsConnect::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark
  
  Object* TlsConnect::apply (Runnable* robj, Nameset* nset, const long quark,
			     Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_SETPRMS) {
	Object* obj = argv->get (0);
	TlsParams* prms = dynamic_cast <TlsParams*> (obj);
	if (prms == nilp) {
	  throw Exception ("type-error", "invalid object as parameters",
			   Object::repr (obj));
	}
	setprms (*prms);
	return  nilp;
      }
    }
    // dispatch 2 arguments
    if (argc == 2) {
      if (quark == QUARK_CONNECT) {
	Object* obj = argv->get (0);
	InputStream* is = dynamic_cast <InputStream*> (obj);
	if (is == nilp) {
	  throw Exception ("type-error", "invalid object as input stream",
			   Object::repr (obj));
	}
	obj = argv->get (1);
	OutputStream* os = dynamic_cast <OutputStream*> (obj);
	if (os == nilp) {
	  throw Exception ("type-error", "invalid object as output stream",
			   Object::repr (obj));
	}
	return connect (is, os);
      }
    }
    // call the object method
    return Object::apply (robj, nset, quark, argv);
  }
}
