// ---------------------------------------------------------------------------
// - XmlGeometry.hpp                                                         -
// - afnix:geo service - xml geometry representation class definition        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_XMLGEOMETRY_HPP
#define  AFNIX_XMLGEOMETRY_HPP

#ifndef  AFNIX_XMLNODE_HPP
#include "XmlNode.hpp"
#endif

#ifndef  AFNIX_GEOMETRY_HPP
#include "Geometry.hpp"
#endif

namespace afnix {

  /// The XmlGeometry class is a class designed for the fabrication of geometrys
  /// object from various representation, such like xml node. The class can
  /// also be used in reverse mode to produce a geometry representation of an
  /// object. Note that the class is mostly a set of convenient functions.
  /// @author amaury darsch

  class XmlGeometry {
  public:
    /// @return true if the geometry exists
    static bool exists (const String& name);
    
    /// convert a xml node to a geometry
    /// @param node the xml node to convert
    static Geometry* togeo (const XmlNode* node);

    /// convert a geometry to a xml node
    /// @param geo the geometry object to convert
    static XmlNode* toxml (const Geometry* geo);    
  };
}

#endif
