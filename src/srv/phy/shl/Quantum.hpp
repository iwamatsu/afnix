// ---------------------------------------------------------------------------
// - Quantum.hpp                                                             -
// - afnix:phy service - base quantum function class definition              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_QUANTUM_HPP
#define  AFNIX_QUANTUM_HPP

#ifndef  AFNIX_STRING_HPP
#include "String.hpp"
#endif

namespace afnix {

  /// The Quantum class is a collection of static convenient function. There
  /// purpose is to compute basic value used in quantum physic.
  /// @author amaury darsch

  class Quantum {
  public:
    /// compute the boltzmann energy by temperature
    /// @param temp the absolute temperature
    static t_real getkt (const t_real temp);

    /// compute the thermal voltage by temperature
    /// @param temp the absolute temperature
    static t_real getvth (const t_real temp);

    /// derate the energy bang gap with Varshni's expression
    /// @param temp the absolute temperature
    /// @param eg   the material 0K energy gap
    /// @param ega  the alpha derating coefficient
    /// @param egb  the beta  derating coefficient
    static t_real geteg (const t_real temp,
			 const t_real eg, const t_real ega, const t_real egb);
  };
}

#endif
