# ---------------------------------------------------------------------------
# - GEO0006.als                                                             -
# - afnix:geo service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   point 2 test unit
# @author amaury darsch

# get the service
interp:library "afnix-geo"

# create a point 2 object
trans p2 (afnix:geo:Point2)
assert true (afnix:geo:point2-p p2)
assert "Point2" (p2:repr)

# create a new point and check
trans p2 (afnix:geo:Point2 1.0 2.0)
assert 1.0 (p2:get-x)
assert 2.0 (p2:get-y)
