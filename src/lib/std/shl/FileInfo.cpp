// ---------------------------------------------------------------------------
// - FileInfo.cpp                                                            -
// - standard object library - file info class implementation                -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Integer.hpp"
#include "FileInfo.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "csio.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a new file information by name

  FileInfo::FileInfo (const String& name) {
    d_name = name;
    update ();
  }

  // return the class name

  String FileInfo::repr (void) const {
    return "FileInfo";
  }

  // update the file information structure

  void FileInfo::update (void) {
    wrlock ();
    try {
      // check the file name
      if (d_name.isnil () == true) {
	throw Exception ("name-error", "nil input file name for update");
      }
      // get the file information
      char*    fname = d_name.tochar ();
      s_finfo* finfo = c_finfo (fname);
      // clean and check
      delete [] fname;
      if (finfo == nilp) {
	throw Exception ("open-error", "cannot get file information", d_name);
      }
      // update the information structure
      d_size  = finfo->d_size;
      d_mtime = finfo->d_mtime;
      // clean the structure
      delete finfo;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the file name

  String FileInfo::getname (void) const {
    rdlock ();
    try {
      String result = d_name;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return the input file size

  t_long FileInfo::length (void) const {
    rdlock ();
    try {
      t_long result = d_size;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return the file modification time

  t_long FileInfo::mtime (void) const {
    rdlock ();
    try {
      t_long result = d_mtime;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 3;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_MTIME  = zone.intern ("get-modification-time");
  static const long QUARK_LENGTH = zone.intern ("length");
  static const long QUARK_UPDATE = zone.intern ("update");

  // create a new object in a generic way

  Object* FileInfo::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 1 argument
    if (argc == 1) {
      String name = argv->getstring (0);
      return new FileInfo (name);
    }
    throw Exception ("argument-error", 
		     "invalid arguments with with file information"); 
  }

  // return true if the given quark is defined

  bool FileInfo::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    // check the nameable class
    bool result = hflg ? Nameable::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }

  // apply this object with a set of arguments and a quark

  Object* FileInfo::apply (Runnable* robj, Nameset* nset, const long quark,
			   Vector* argv) {

    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();

    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_MTIME)  return new Integer (mtime  ());
      if (quark == QUARK_LENGTH) return new Integer (length ());
      if (quark == QUARK_UPDATE) {
	update ();
	return nilp;
      }
    }
    // call the nameable class
    return Nameable::apply (robj, nset, quark, argv);
  }
}
