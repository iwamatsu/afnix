// ---------------------------------------------------------------------------
// - Shape.hpp                                                               -
// - afnix:geo service - geometry shape class definition                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SHAPE_HPP
#define  AFNIX_SHAPE_HPP

#ifndef  AFNIX_POINT2_HPP
#include "Point2.hpp"
#endif

#ifndef  AFNIX_GEOMETRY_HPP
#include "Geometry.hpp"
#endif

namespace afnix {

  /// The Shape class is a geometry class for the afnix geometry service.
  /// A shape object is a 2 dimensional object. Typical shapes are polygons
  /// and curves.
  /// @author amaury darsch

  class Shape : public Geometry {
  public:
    /// @return the shape origin
    virtual Point2 getorigin (void) const =0;
      
  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
