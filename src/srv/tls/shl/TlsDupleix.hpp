// ---------------------------------------------------------------------------
// - TlsDupleix.hpp                                                          -
// - afnix:tls service - tls dupleix stream class definition                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSDUPLEIX_HPP
#define  AFNIX_TLSDUPLEIX_HPP

#ifndef  AFNIX_TLSINPUT_HPP
#include "TlsInput.hpp"
#endif

#ifndef  AFNIX_TLSOUTPUT_HPP
#include "TlsOutput.hpp"
#endif

#ifndef  AFNIX_DUPLEIXSTREAM_HPP
#include "DupleixStream.hpp"
#endif

namespace afnix {

  /// The TlsDupleix class is the dupleix stream which encapsulates the
  /// read and write tls operations. The class operates by default in half
  /// dupleix mode. If a full dupleix mode is required the getis and getos
  /// methods must be used to grab independant input and output streams.
  /// @author amaury darsch

  class TlsDupleix : public TlsInfos, public DupleixStream {
  protected:
    /// the tls state
    TlsState* p_tlss;
    /// the input stream
    TlsInput* p_is;
    /// the output stream
    TlsOutput* p_os;

  public:
    /// create a tls dupleix by streams and state
    /// @param is  the input stream
    /// @param os  the output stream
    /// @param sta the tls state
    TlsDupleix (InputStream* is, OutputStream* os, TlsState* sta);

    /// destroy this tls dupleix
    ~TlsDupleix (void);

    /// @return the class name
    String repr (void) const;

    /// reset this object
    void reset (void);

    /// @return the info as a plist
    Plist getinfo (void) const;

    /// close this dupleix stream
    bool close (void);
    
    /// @return true if we are at the eos
    bool iseos (void) const;
    
    /// check if we can read one character
    bool valid (void) const;

    /// @return the next available character
    char read (void);

    /// write one character on the dupleix stream.
    /// @param value the character to write  
    long write (const char value);

    /// write a data buffer to the dupleix stream
    /// @param data the data to write
    long write (const char* data);

    /// @return the tls state
    virtual TlsState* getstate (void) const;

  private:
    // make the copy constructor private
    TlsDupleix (const TlsDupleix&) =delete;
    // make the assignment operator private
    TlsDupleix& operator = (const TlsDupleix&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
