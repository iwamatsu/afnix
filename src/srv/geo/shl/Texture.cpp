// ---------------------------------------------------------------------------
// - Texture.cpp                                                             -
// - afnix:geo service - texture class implementation                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Texture.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the default texture
  static const String XML_TXT_DEF  = "BLACK";
  // the texture xml name
  static const String XML_TAG_NAME = "TEXTURE";
  static const String XML_TAG_INFO = "GEOMETRY TEXTURE";
  // the texture name attribute
  static const String XML_TXT_ATTR = "texture";

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default texture

  Texture::Texture (void) {
    reset ();
  }

  // create a texture by name

  Texture::Texture (const String& name) {
    d_name = name;
  }

  // copy construct this texture

  Texture::Texture (const Texture& that) {
    that.rdlock ();
    try {
      d_name = that.d_name;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }  
  }

  // assign a texture to this one

  Texture& Texture::operator = (const Texture& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      d_name = that.d_name;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return the class name
  
  String Texture::repr (void) const {
    return "Texture";
  }

  // return a clone of this object

  Object* Texture::clone (void) const {
    return new Texture (*this);
  }

  // get the texture name

  String Texture::getname (void) const {
    rdlock ();
    try {
      String result = d_name;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // reset this texture

  void Texture::reset (void) {
    wrlock ();
    try {
      d_name = XML_TXT_DEF;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the texture by plist

  void Texture::setplst (const Plist& plst) {
    wrlock ();
    try {
      reset ();
      if (plst.exists (XML_TXT_ATTR) == true)
	d_name = plst.getpval (XML_TXT_ATTR);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the texture plist

  Plist Texture::getplst (void) const {
    rdlock ();
    try {
      // the generic plist
      Plist result (XML_TAG_NAME, XML_TAG_INFO);
      // add the geometry properties
      result.add (XML_TXT_ATTR, d_name);
      // here it is
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 2;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_SETPLST = zone.intern ("set-plist");
  static const long QUARK_GETPLST = zone.intern ("get-plist");

  // create a new object in a generic way

  Object* Texture::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Texture;
    // check for 1 argument
    if (argc == 1) {
      String name = argv->getstring (0);
      return new Texture (name);
    }
    throw Exception ("argument-error", 
                     "invalid arguments with with texture"); 
  }
  
  // return true if the given quark is defined
  
  bool Texture::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Nameable::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark

  Object* Texture::apply (Runnable* robj, Nameset* nset, const long quark,
			  Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETPLST) return new Plist  (getplst ());
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_SETPLST) {
	Object* obj = argv->get (0);
        Plist* alst = dynamic_cast <Plist*> (obj);
        if ((obj != nilp) && (alst == nilp)) {
          throw Exception ("type-error", "invalid object with set-plist",
                           Object::repr (obj));
        }
        setplst (*alst);
        return nilp;
      }
    }
    // call the nameable method
    return Nameable::apply (robj, nset, quark, argv);
  }
}
