// ---------------------------------------------------------------------------
// - Assistant.hpp                                                           -
// - afnix:csm service - assistant class definition                          -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_ASSISTANT_HPP
#define  AFNIX_ASSISTANT_HPP

#ifndef  AFNIX_VECTOR_HPP
#include "Vector.hpp"
#endif

#ifndef  AFNIX_APPOINTER_HPP
#include "Appointer.hpp"
#endif

namespace afnix {

  /// The Assistant class is a generic class designed to hold various 
  /// component and manage them like an assistant will do. For example,
  /// the class can store several appointer object and distribute slot
  /// for all of them.
  /// @author amaury darsch

  class Assistant : public Object {
  private:
    /// the assistant name
    String d_name;
    /// the assistant info
    String d_info;
    /// the appointer vector
    Vector d_appt;
    /// the appointer index
    long   d_aidx;

  public:
    /// create a default assistant
    Assistant (void);

    /// create an assistant by name
    /// @param name the assistant name
    Assistant (const String& name);

    /// create a assistant by name and info
    /// @param name the assistant name
    /// @param info the assistant info
    Assistant (const String& name, const String& info);

    /// @return the class name
    String repr (void) const;

    /// reset this assistant
    void reset (void);

    /// @return the assistant name
    String getname (void) const;
    
    /// @return the assistant info
    String getinfo (void) const;

    /// @return the number of appointers
    long lenappt (void) const;

    /// add a new appointer
    /// @param appt the appointer to add
    void addappt (Appointer* appt);

    /// @return an appointer by index
    Appointer* getappt (const long index) const;

    /// @return the average appointer time
    t_long getaatm (void) const;

    /// @return the appointer minimum time
    t_long getamtm (void) const;

    /// @return the appointer minimum time by time
    t_long getamtm (const t_long mrtm) const;

    /// @return the total number of allocated slots
    long getsnum (void) const;

    /// get the next available slot by duration
    /// @param dlen the slot duration
    Slot getslot (const t_long dlen);

    /// get the next available slot by time and duration
    /// @param time the requested time
    /// @param dlen the slot duration
    Slot getslot (const t_long time, const t_long dlen);

    /// pushback a slot in the slot pool
    /// @param slot the slot to pushbak
    void pushback (const Slot& slot);

  private:
    // make the copy constructor private
    Assistant (const Assistant&);
    // make the assignment operator private
    Assistant& operator = (const Assistant&);

  public:
    /// create an object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
