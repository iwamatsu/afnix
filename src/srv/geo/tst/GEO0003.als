# ---------------------------------------------------------------------------
# - GEO0003.als                                                             -
# - afnix:geo service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   vector 3 test unit
# @author amaury darsch

# get the service
interp:library "afnix-geo"

# create a vector 3 object
trans v3 (afnix:geo:Vector3)
assert true (afnix:geo:vector3-p v3)
assert "Vector3" (v3:repr)

# create a new vector and check
trans v3 (afnix:geo:Vector3 1.0 2.0 3.0)
assert 1.0 (v3:get-x)
assert 2.0 (v3:get-y)
assert 3.0 (v3:get-z)
