// ---------------------------------------------------------------------------
// - Predsvg.hpp                                                             -
// - afnix:svg service - predicates declaration                              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDSVG_HPP
#define  AFNIX_PREDSVG_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix
  /// svg (scalable vector graphics) service.
  /// @author amaury darsch

  /// the plot2d object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_pltdp (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg transform object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_trfrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg root object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_rootp (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg fragment object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_fragp (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg styling object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_stlgp (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg rectangle object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_rectp (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg circle object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_circp (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg ellipse object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_elpsp (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg line object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_linep (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg polyline object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_plinp (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg polygon object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_pgonp (Runnable* robj, Nameset* nset, Cons* args);

  /// the svg group object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* svg_grpp (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
