// ---------------------------------------------------------------------------
// - Solid.hxx                                                               -
// - afnix:geo service - private solid definition                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SOLID_HXX
#define  AFNIX_SOLID_HXX

#ifndef  AFNIX_STRING_HPP
#include "String.hpp"
#endif

namespace afnix {
  static const String GEO_SLD_NAME = "solid";
  static const String GEO_SLD_INFO = "solid geometry";  

  static const String GEO_CUB_NAME = "cuboid";
  static const String GEO_CUB_INFO = "cuboid solid";  
  static const String GEO_PLN_NAME = "plane";
  static const String GEO_PLN_INFO = "plane solid";
  static const String GEO_WIR_NAME = "wire";
  static const String GEO_WIR_INFO = "wire solid";
}

#endif
