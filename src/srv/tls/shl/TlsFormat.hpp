// ---------------------------------------------------------------------------
// - TlsFormat.hpp                                                           -
// - afnix:tls service - tls format class class definition                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSFORMAT_HPP
#define  AFNIX_TLSFORMAT_HPP

#ifndef  AFNIX_ITEM_HPP
#include "Item.hpp"
#endif

#ifndef  AFNIX_BUFFER_HPP
#include "Buffer.hpp"
#endif

namespace afnix {

  /// The TlsFormat class is a base class that containes the basic definition
  /// supported by this implementation.
  /// @author amaury darsch
  
  class TlsFormat {
  public:
    /// the encoding type
    enum t_efmt {
      EFMT_PEM, // pem encoding / base64
      EFMT_DER  // asn distinguished
    };

    /// the key type format
    enum t_kfmt {
      KFMT_PK1, // pkcs 1 format
      KFMT_PK5  // pkcs 5 format
    };

    /// get the default encoding type
    static t_efmt getedef (void);

    /// get the default key type
    static t_kfmt getkdef (void);
    
    /// get the encoding type by string
    /// @param sfmt the string format
    static t_efmt toefmt (const String& sfmt);

    /// get the key format by string
    /// @param sfmt the string format
    static t_kfmt tokfmt (const String& sfmt);

    /// map an item to an encoding type
    /// @param the item to map
    static t_efmt toefmt (const Item& item);

    /// map an item to an encoding type
    /// @param the item to map
    static t_kfmt tokfmt (const Item& item);

    /// map an encoding type to an item
    /// @param efmt the the encofing format
    static Item toitem (const t_efmt efmt);

    /// map an encoding type to an item
    /// @param efmt the the encofing format
    static Item toitem (const t_kfmt efmt);

    /// map an encoding type to a string
    static String tostring (const t_efmt efmt);

    /// map a key type to a string
    static String tostring (const t_kfmt kfmt);
    
    /// map a file by name to a buffer
    /// @param name the file name to map
    static Buffer* tobuffer (const String& name);
    
  public:
    /// evaluate an object data member
    /// @param robj  the current runnable
    /// @param nset  the current nameset
    /// @param quark the quark to evaluate
    static Object* meval (Runnable* robj, Nameset* nset, const long quark);
  };
}

#endif
