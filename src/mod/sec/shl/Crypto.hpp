// ---------------------------------------------------------------------------
// - Crypto.hpp                                                              -
// - afnix:sec module - cryptographic function definitions                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CRYPTO_HPP
#define  AFNIX_CRYPTO_HPP

#ifndef  AFNIX_HASHER_HPP
#include "Hasher.hpp"
#endif

#ifndef  AFNIX_BLOCKCIPHER_HPP
#include "BlockCipher.hpp"
#endif

namespace afnix {

  /// the Crypto class is a collection of static convenient functions that
  /// are part of the standard cryptographic interface for the afnix engine.
  /// the available functions permits to create vrious hasher or stream cipher
  /// along with their keys.
  /// @author amaury darsch

  class Crypto {
  public:
    /// the supported hasher
    enum t_hasher {
      MD2,    // message digest - 2
      MD4,    // message digest - 4
      MD5,    // message digest - 5
      SHA1,   // secure hash algorithm - 1
      SHA224, // secure hash algorithm - 224
      SHA256, // secure hash algorithm - 256
      SHA384, // secure hash algorithm - 384
      SHA512  // secure hash algorithm - 512
    };
    
    /// create a new hasher by type
    /// @param type the hasher type to create
    static Hasher* mkhasher (const t_hasher type);
    
    /// create a new hasher by name
    /// @param name the hasher name to use
    static Hasher* mkhasher (const String& name);

    /// create a new hasher by bits size
    /// @param bits the requested hasher bits size
    static Hasher* mkhasher (const long bits);
    
    /// the supported cipher
    enum t_cipher {
      AES,    // advanced encryption standard
      RSA,    // PKCS 2.1 encryption standard
      DES,    // data encryption standard
      RC2,    // RC2 cipher
      RC4,    // RC4 cipher
      RC5     // RC5 cipher
    };
    
    /// create a new block cipher by type and key
    /// @param type the cipher type to create
    /// @param key  the cipher key to use
    static Cipher* mkcipher (const t_cipher type, const Key& key);

    /// create a new block cipher by name
    /// @param name the cipher name to use
    /// @param key  the cipher key to use
    static Cipher* mkcipher (const String& name, const Key& key);

    /// create a new block cipher by type and key
    /// @param type the cipher type to create
    /// @param key  the cipher key to use
    /// @param rflg the reverse flag
    static Cipher* mkcipher (const t_cipher type, const Key& key, 
			     const bool rflg);
    
    /// create a new block cipher by name
    /// @param name the cipher name to use
    /// @param key  the cipher key to use
    /// @param rflg the reverse flag
    static Cipher* mkcipher (const String& name, const Key& key, 
			     const bool rflg);
  };
}
  
#endif
