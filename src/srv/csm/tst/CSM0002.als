# ---------------------------------------------------------------------------
# - CSM0002.als                                                             -
# - afnix:csm service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   appointer test unit
# @author amaury darsch

# get the service
interp:library "afnix-sys"
interp:library "afnix-csm"

# create an appointer
const  appt (afnix:csm:Appointer)
assert true (afnix:csm:appointer-p appt)

# get a slot for the next 10 seconds
trans  slot (appt:get-slot 10)
assert true (afnix:csm:slot-p slot)
assert 0    (slot:get-time)
assert 10   (slot:get-duration)

# get a 1 hour slot
trans slot  (appt:get-slot 3600)
assert 10   (slot:get-time)
assert 3600 (slot:get-duration)

# add a saturday and sunday blocked day rule
appt:set-blocked-day 6
appt:set-blocked-day 0

# set a valid block time between 8AM to 12AM and 2PM to 6PM
const ambt (* 3600 8)
const amet (* 3600 12)
const pmbt (* 3600 14)
const pmet (* 3600 18)
appt:set-valid-block-time ambt amet
appt:set-valid-block-time pmbt pmet

# reset the date to 1970-01-01 - it is a thursday
trans date (afnix:sys:Date 1970 1 1)
appt:set-date date
trans date (appt:get-date)
assert "1970-01-01T00:00:00Z" (date:to-iso true)

# get the next available slot 1H - Thu 1st 8AM
trans  slot (appt:get-slot 3600)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-01T08:00:00Z" (date:to-iso true)

# get the next available slot 2H - Thu 1st 9AM
trans  slot (appt:get-slot 7200)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-01T09:00:00Z" (date:to-iso true)

# get the next available slot 4H - Thu 1st 2PM
trans  slot (appt:get-slot 14400)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-01T14:00:00Z" (date:to-iso true)

# clone the appointer and check
const  capt (appt:clone)

# get the next available slot 4H - Fri 2nd 8AM
trans  slot (capt:get-slot 14400)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-02T08:00:00Z" (date:to-iso true)

# get the next available slot 4H - Fri 2nd 2PM
trans  slot (capt:get-slot 14400)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-02T14:00:00Z" (date:to-iso true)

# get the next available slot 4H - Mon 5th 8AM
trans  slot (capt:get-slot 14400)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-05T08:00:00Z" (date:to-iso true)

# check the original appointer at the time we left it
trans  slot (appt:get-slot 14400)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-02T08:00:00Z" (date:to-iso true)

# reset time  and add new rules
appt:reset
# set a maximum lot rule
appt:set-maximum-slots 2
# set a special day Mon Jan 5th
appt:set-special-day 1 5
# check date
trans date (afnix:sys:Date 1970 1 1)
appt:set-date date
trans date (appt:get-date)
assert "1970-01-01T00:00:00Z" (date:to-iso true)

# get the next available slot 1H - Thu 1st 8AM
trans  slot (appt:get-slot 3600)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-01T08:00:00Z" (date:to-iso true)

# get the next available slot 2H - Thu 1st 9AM
trans  slot (appt:get-slot 7200)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-01T09:00:00Z" (date:to-iso true)

# check number of slots
assert 2 (appt:get-slot-number)

# get the next available slot 4H - Fri 2nd 8AM
trans  slot (appt:get-slot 14400)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-02T08:00:00Z" (date:to-iso true)

# get the next available slot 2H - Fri 2nd 2PM
trans  slot (appt:get-slot 7200)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-02T14:00:00Z" (date:to-iso true)

# get the next available slot 2H - Tue 6th 8AM
trans  slot (appt:get-slot 7200)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-06T08:00:00Z" (date:to-iso true)
# pushback the slot and check again
assert 5    (appt:get-slot-number)
appt:pushback slot
trans  slot (appt:get-slot 7200)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert "1970-01-06T08:00:00Z" (date:to-iso true)
assert 5    (appt:get-slot-number)
