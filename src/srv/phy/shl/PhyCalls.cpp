// ---------------------------------------------------------------------------
// - PhyCalls.cpp                                                            -
// - afnix:phy service - physics specific calls implementation               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "Real.hpp"
#include "Vector.hpp"
#include "Quantum.hpp"
#include "PhyCalls.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // compute the thermal voltage by temperature

  Object* phy_getvth (Runnable* robj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    Vector* argv = Vector::eval (robj, nset, args);
    long    argc = (argv == nilp) ? 0 : argv->length ();
    try {
      if (argc == 1) {
	t_real temp = argv->getreal (0);
	delete argv; argv = nilp;
	return new Real (Quantum::getvth (temp));
      }
      throw Exception ("argument-error", 
		       "too many arguments with getvth");
    } catch (...) {
      delete argv;
      throw;
    }
  }
}
