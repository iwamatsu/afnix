// ---------------------------------------------------------------------------
// - XhtmlEquiv.cpp                                                          -
// - afnix:wax module - xhtml meta http-equiv node class implementation      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Exception.hpp"
#include "XhtmlEquiv.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the meta node name
  static const String XML_TAG_NAME  = "meta";
  // the meta data name attribute
  static const String XML_NAME_ATTR = "http-equiv";
  // the meta data content attribute
  static const String XML_CNTS_ATTR = "content";
  // the default http-equiv name value
  static const String DEF_NAME_ATTR = "context-type";
  // the default http-equiv contents
  static const String DEF_CNTS_ATTR = "text/html; charset=utf-8";

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default xhtml meta http-equiv node

  XhtmlEquiv::XhtmlEquiv (void) : XmlTag (XML_TAG_NAME) {
    d_eflg = true;
    setattr (XML_NAME_ATTR, DEF_NAME_ATTR);
    setattr (XML_CNTS_ATTR, DEF_CNTS_ATTR);
  }

  // create a xhtml meta http-equiv node by name and content

  XhtmlEquiv::XhtmlEquiv (const String& name, 
			 const String& cnts) : XmlTag (XML_TAG_NAME) {
    d_eflg = true;
    setattr (XML_NAME_ATTR, name);
    setattr (XML_CNTS_ATTR, cnts);
  }

  // return the class name

  String XhtmlEquiv::repr (void) const {
    return "XhtmlEquiv";
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // create a new object in a generic way

  Object* XhtmlEquiv::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new XhtmlEquiv;
    // check for 2 arguments
    if (argc == 2) {
      String name = argv->getstring (0);
      String cnts = argv->getstring (1);
      return new XhtmlEquiv (name, cnts);
    }
    // wrong arguments
    throw Exception ("argument-error", 
		     "too many arguments with xhtml equiv constructor");
  }
}
