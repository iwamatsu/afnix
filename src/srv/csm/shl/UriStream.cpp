// ---------------------------------------------------------------------------
// - UriStream.cpp                                                           -
// - afnix:www module - uri stream class implementation                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "TcpClient.hpp"
#include "UriStream.hpp"
#include "Exception.hpp"
#include "InputFile.hpp"
#include "OutputFile.hpp"
#include "HttpStream.hpp"
#include "HttpRequest.hpp"
#include "HttpResponse.hpp"

namespace afnix {
  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the uri file scheme
  static const String URI_FILE_SCHM = "file";
  // the uri http scheme
  static const String URI_HTTP_SCHM = "http";
  // the uri default path
  static const String URI_PATH_XDEF = "/";

  // get a file input stream
  static InputStream* get_file_istream (const Uri& uri) {
    try {
      // get the system path from the uri path
      String path = uri.getsysp ();
      // open a file for input
      return new InputFile (path);
    } catch (...) {
      throw Exception ("uristream-error", "cannot access uri", uri.getname ());
    }
  }

  // get a file output stream
  static OutputStream* get_file_ostream (const Uri& uri) {
    try {
      // get the system path from the uri path
      String path = uri.getsysp ();
      // open a file for output
      return new OutputFile (path);
    } catch (...) {
      throw Exception ("uristream-error", "cannot access uri", uri.getname ());
    }
  }

  // get a http input stream
  static InputStream* get_http_istream (const Uri& uri) {
    // create a http request by uri
    HttpRequest hrq (uri);
    // get the uri host
    String host = uri.gethost ();
    // get the port
    long   port = uri.getport ();
    // open a tcp socket
    TcpClient* s = new TcpClient (host, port);
    // write the request
    hrq.write (*s);
    // create a response
    HttpResponse* hrs = new HttpResponse (*s);
    try {
      // check for another location
      if (hrs->ishloc () == false) {
	InputStream* result = new HttpStream (hrs, s);
	delete hrs;
	return result;
      } else {
	Uri huri = hrs->gethloc ();
	delete hrs;
	return get_http_istream (huri);
      }
      // invalid response
      throw Exception ("uristream-error", "cannot access uri", uri.getname ());
    } catch (...) {
      delete hrs;
      throw;
    }
  }
  
  // get the content input stream by uri

  static InputStream* uri_create_istream (const Uri& uri) {
    // get the uri scheme
    String sch = uri.getscheme ();
    // select from the uri scheme
    if (sch == URI_FILE_SCHM) return get_file_istream (uri);
    if (sch == URI_HTTP_SCHM) return get_http_istream (uri);
    // invalid uri scheme to process
    throw Exception ("uristream-error", "invalid uri stream scheme", sch);
  }
  
  // get the content output stream by uri name

  static OutputStream* uri_create_ostream (const Uri& uri) {
    // get the uri scheme
    String sch = uri.getscheme ();
    // select from the uri scheme
    if (sch == URI_FILE_SCHM) return get_file_ostream (uri);
    // invalid uri scheme to process
    throw Exception ("uristream-error", "invalid uri stream scheme", sch);
  }
  
  // -------------------------------------------------------------------------
  // - public section                                                         -
  // -------------------------------------------------------------------------

  // create a input stream by uri

  InputStream* UriStream::istream (const Uri& uri) {
    return uri_create_istream (uri);
  }

  // create a input stream by string

  InputStream* UriStream::istream (const String& suri) {
    // normalize the string uri
    String nuri = Uri::sysname (suri);
    // create a requets uri
    Uri uri = nuri;
    // create the input stream
    return uri_create_istream (uri);
  }

  // create an output stream by uri

  OutputStream* UriStream::ostream (const Uri& uri) {
    return uri_create_ostream (uri);
  }

  // create a output stream by string

  OutputStream* UriStream::ostream (const String& suri) {
    // normalize the string uri
    String nuri = Uri::sysname (suri);
    // create a requets uri
    Uri uri = nuri;
    // create the output stream
    return uri_create_ostream (uri);
  } 
}
