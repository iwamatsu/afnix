# ----------------------------------------------------------------------------
# - std-tls-inets                                                            -
# - afnix:tls inet server class module                                       -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - global section                                                           -
# ----------------------------------------------------------------------------

# the inet server class
const afnix:tls:inets (class)
# the inet server nameset
try (const AFNIX:TLS:INETS (nameset AFNIX:TLS))

# ----------------------------------------------------------------------------
# - private section                                                          -
# ----------------------------------------------------------------------------

# the option messages
const AFNIX:TLS:INETS:U-CLS-MSG "axi [i afnix-std-tls cmd-tls-inets] [options]"
const AFNIX:TLS:INETS:H-LCO-MSG "    [h]              print this help message"
const AFNIX:TLS:INETS:V-LCO-MSG "    [v]              print system version"
const AFNIX:TLS:INETS:C-LCO-MSG "    [c path:..:path] set the certificate list"
const AFNIX:TLS:INETS:K-LCO-MSG "    [k path]         set the key"
const AFNIX:TLS:INETS:T-LCO-MSG "    [t]              connect with tcp socket"
const AFNIX:TLS:INETS:P-LCO-MSG "    [p]              show the tls parameters"
const AFNIX:TLS:INETS:D-LCO-MSG "    [d]              set the debug flag"

# ----------------------------------------------------------------------------
# - initial section                                                          -
# ----------------------------------------------------------------------------

# preset the inet server class

trans afnix:tls:inets:preset (argv) {
  # preini the server class
  this:preini argv
  # postdo the server class
  this:postdo
}

# preini the inet server class
# @param argv the argument vector

trans afnix:tls:inets:preini (argv) {
  # create an option class and bind it
  const this:opts (afnix:sys:Options AFNIX:TLS:INETS:U-CLS-MSG)
  # register the options
  this:opts:add-unique-option 'd' AFNIX:TLS:INETS:D-LCO-MSG
  this:opts:add-unique-option 'p' AFNIX:TLS:INETS:P-LCO-MSG
  this:opts:add-unique-option 't' AFNIX:TLS:INETS:T-LCO-MSG
  this:opts:add-string-option 'k' AFNIX:TLS:INETS:K-LCO-MSG
  this:opts:add-string-option 'c' AFNIX:TLS:INETS:C-LCO-MSG
  this:opts:add-unique-option 'v' AFNIX:TLS:INETS:V-LCO-MSG
  this:opts:add-unique-option 'h' AFNIX:TLS:INETS:H-LCO-MSG
  # parse the options
  try (this:opts:parse argv) {
    this:opts:usage (interp:get-error-stream)
    afnix:sys:exit 1
  }
  # check for the help option
  if (this:opts:get-unique-option 'h') {
    this:opts:usage (interp:get-output-stream)
    afnix:sys:exit 0
  }
  # check for the version option
  if (this:opts:get-unique-option 'v') {
    println (afnix:tls:get-copyright-message)
    println (afnix:tls:get-revision-message)
    afnix:sys:exit 0
  }
}

# postdo the inet server class

trans afnix:tls:inets:postdo nil {
  # create a tcp server socket
  const this:host AFNIX:TLS:SERVER-HOST
  const this:port AFNIX:TLS:SERVER-PORT
  const this:dbug (this:opts:get-unique-option 'd')
  const this:topt (this:opts:get-unique-option 't')
  # create the tls parameters
  const this:prms (afnix:tls:TlsParams this:host this:port)
  const this:popt (this:opts:get-unique-option 'p')
  # check for a certificate list
  if (this:opts:get-unique-option 'c') {
    const cert (this:opts:get-string-option 'c')
    this:prms:set-certificate cert
  }
  # check for a certificate key
  if (this:opts:get-unique-option 'k') {
    const ckey (this:opts:get-string-option 'k')
    this:prms:set-certificate-key ckey
  }
}

# execute the command

trans afnix:tls:inets:run nil {
  # show the tls parameters
  if this:popt (this:show-tls-params)
  # connect the tls socket and get the connected socket
  const cs (if this:topt (this:accept-tcp-socket) (this:accept-tls-socket))
  if (nil-p cs) (return)
  # report in debug mode
  if this:dbug (afnix:tls:write-error-plist (cs:get-info))
  # set the read loop
  const is (cs:get-input-stream)
  const rl nil (is) {
    # loop until eof
    while (is:valid-p) {
      trans line (is:readln)
      println line
    }
  }
    # launch the read thread
  const rt (launch (rl))
  # launch the write thread
  rt:wait
}

# ----------------------------------------------------------------------------
# - report section                                                          -
# ----------------------------------------------------------------------------

trans afnix:tls:inets:show-tls-params nil {
  # get the cipher suite table
  const ptbl (this:prms:get-info)
  # format the table
  afnix:tls:write-error-plist ptbl
}

# ----------------------------------------------------------------------------
# - process section                                                          -
# ----------------------------------------------------------------------------

# connect the socket with a tcp socket

trans afnix:tls:inets:accept-tcp-socket nil {
  # create a tcp server
  const this:ssrv (afnix:net:TcpServer (this:prms:to-server-sock-params))
  # accept with a tcp socket
  const s (this:ssrv:accept)
  # create a tls connect object
  const co (afnix:tls:TlsConnect true this:prms)
  # get the connected socket state
  trans ssta (co:connect s s)
  # create a tls dupleix
  afnix:tls:TlsDupleix (s:get-input-stream) (s:get-output-stream) ssta
}

# connect the socket with a tls socket

trans afnix:tls:inets:accept-tls-socket nil {
  # create a tls server
  const this:ssrv (afnix:tls:TlsServer this:prms)
  # accept a full dupleix connection
  this:ssrv:accept-full-dupleix
}
