// ---------------------------------------------------------------------------
// - Predwax.hpp                                                             -
// - afnix:wax service - predicates declaration                              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDWAX_HPP
#define  AFNIX_PREDWAX_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix
  /// wax application management (wax) service.
  /// @author amaury darsch

  /// the xsm html object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_xsmhp (Runnable* robj, Nameset* nset, Cons* args);

  /// the json writer object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_jsonp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xml mime object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_xmlmp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml mime object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_xhtmp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml root object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_rootp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml html object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_htmlp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml head object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_headp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml body object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_bodyp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml th object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_thp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml td object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_tdp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml tr object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_trp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml col object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_colp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml cgr object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_cgrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml caption object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_captp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml table element object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_telemp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml table head object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_theadp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml table body object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_tbodyp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml table foot object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_tfootp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml table object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_tablep (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml title object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_titlep (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml meta object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_metap (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml link object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_linkp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml equiv object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_equivp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml style object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_stylep (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml canvas object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_canvasp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml script object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_scriptp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml img object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_imgp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml div object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_divp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml pre object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_prep (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml ref object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_refp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml para object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_parap (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml emph object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_emphp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml text italic object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_txtip (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml text bold object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_txtbp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml text teletype object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_txttp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml hr object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_hrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml br object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_brp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml ul object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_ulp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml list item object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_lip (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml form object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_formp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml input text object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_textp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml input submit object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_subtp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml select option object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_optnp (Runnable* robj, Nameset* nset, Cons* args);

  /// the xhtml input hidden object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* wax_hidep (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
