// ---------------------------------------------------------------------------
// - TlsSuite.hpp                                                            -
// - afnix:tls service - tls cipher suite class definition                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSSUITE_HPP
#define  AFNIX_TLSSUITE_HPP

#ifndef  AFNIX_HMAC_HPP
#include "Hmac.hpp"
#endif

#ifndef  AFNIX_CIPHER_HPP
#include "Cipher.hpp"
#endif

#ifndef  AFNIX_TLSINFOS_HPP
#include "TlsInfos.hpp"
#endif

namespace afnix {

  /// The TlsSuite class is the tls supported cipher suite class definition.
  /// The class is designed to store the cipher defnitions as well as the
  /// cipher suite which appears during the handshake process.
  /// @author amaury darsch

  class TlsSuite : public TlsInfos {
  public:
    /// the key exchange suite
    enum t_exch {
      TLS_EXCH_NIL,
      TLS_EXCH_RSA
    };
    /// the cipher suite
    enum t_cifr {
      TLS_CIFR_NIL,
      TLS_CIFR_RC4,
      TLS_CIFR_AES
    };
    /// the cipher block mode
    enum t_cmod {
      TLS_CMOD_NIL,
      TLS_CMOD_CBC
    };
    /// the hash suite
    enum t_hash {
      TLS_HASH_NIL,
      TLS_HASH_MD5,
      TLS_HASH_SHA
    };
    /// the cipher info structure
    struct s_cinfo {
      /// the cipher name
      String d_name;
      /// the rfc reference
      String d_rfcr;
      /// the minimum tls version
      String d_vmin;
      /// the maximum tls version
      String d_vmax;
      /// the tls code
      t_word d_code;
      /// the key exchange
      t_exch d_exch;
      /// the cipher algorithm
      t_cifr d_cifr;
      /// the cipher block mode
      t_cmod d_cmod;
      /// the cipher key size
      long   d_ksiz;
      /// the cipher block size
      long   d_bsiz;
      /// the cipher iv size
      long   d_vsiz;
      /// the hash algorithm
      t_hash d_hash;
      /// the hash size
      long   d_hsiz;
      /// the certificate flag
      bool   d_cert;
      /// the key exchange flag
      bool   d_kxch;
      /// create a null cipher info
      s_cinfo (void);
      /// create a cipher info with a brace initializer list
      s_cinfo (const String& name, const String& rfcr, const String& vmin,
	       const String& vmax, const t_word  code, const t_exch  exch,
	       const t_cifr  cifr, const t_cmod  cmod, const long    ksiz,
	       const long    bsiz, const long    vsiz, const t_hash  hash,
	       const long    hsiz, const bool    cert, const bool    kxch);
      /// copy construct this structure
      s_cinfo (const s_cinfo&);
      /// assign a structure to this one
      s_cinfo& operator = (const s_cinfo&);
      /// create a cipher by mode, key buffer and initial vector
      Cipher* getcipher (const bool    rflg,
			 const Buffer& kbuf, const Buffer& kiv) const;
      /// create a hmac by key buffer
      Hmac* gethmac (const Buffer& kbuf) const;
    };

    /// convert a cipher code into a tls code
    /// @param ucod the upper byte
    /// @param lcod the lower byte
    static t_word toword (const t_byte ucod, const t_byte lcod);

    /// convert a tls suite code into a upper code
    /// @param code the tls suite code
    static t_byte toucod (const t_word code);

    /// convert a tls suite code into a lower code
    /// @param code the tls suite code
    static t_byte tolcod (const t_word code);

    /// convert a tls suite code into a string
    /// @param code the tls suite code
    static String toscod (const t_word code);

  private:
    /// the suite size
    long    d_size;
    /// the suite length
    long    d_slen;
    /// the suite list
    t_word* p_slst;

  public:
    /// create a default suite
    TlsSuite (void);

    /// create a suite by size
    /// @param size the suite size
    TlsSuite (const long size);

    /// destroy this suite
    ~TlsSuite (void);

    /// @return the class name
    String repr (void) const;

    /// reset this suite
    void reset (void);

    /// @return the suite info as a plist
    Plist getinfo (void) const;

    /// @return the number of ciphers
    virtual long length (void) const;

    /// get the next tls suite code by index
    /// @param idx the tls index index
    virtual t_word get (const long idx) const;

    /// pop the next valid cipher code
    virtual t_word pop (void) const;

    /// add a new cipher by code
    /// @param code the cipher code
    virtual void add (const t_word code);

    /// add a new cipher by tls code
    /// @param ucod the upper byte
    /// @param lcod the lower byte
    virtual void add (const t_byte ucod, const t_byte lcod);

    /// check if a cipher is valid by code
    /// @param code the the cipher code
    virtual bool isvalid (const t_word code) const;

    /// check if a cipher is valid by tls code
    /// @param ucod the upper byte
    /// @param lcod the lower byte
    virtual bool isvalid (const t_byte ucod, const t_byte lcod) const;

    /// check if a cipher needs a certificate
    /// @param code the the cipher code
    virtual bool iscert (const t_word code) const;
    
    /// check if a cipher needs a key exchange
    /// @param code the the cipher code
    virtual bool iskxch (const t_word code) const;
    
    /// get a cipher info by code
    /// @param code the the cipher code
    virtual s_cinfo getcinfo (const t_word code) const;

    /// get a cipher info by tls code
    /// @param ucod the upper byte
    /// @param lcod the lower byte
    virtual s_cinfo getcinfo (const t_byte ucod, const t_byte lcod) const;

    /// get the cipher suite as a print table
    virtual PrintTable* gettinfo (void) const;

    /// create a cipher by code, key buffer and mode
    /// @param cifr the cipher code
    /// @param kbuf the cipher key buffer
    /// @param mode the cipher reverse flag
    virtual Cipher* getcipher (const t_word  cifr, const bool   rflg,
			       const Buffer& kbuf, const Buffer& kiv) const;

    /// create a hmac by code and key buffer
    /// @param cifr the cipher code
    /// @param kbuf the hmac key buffer
    virtual Hmac* gethmac (const t_word  cifr, const Buffer& kbuf) const;
    
  private:
    // make the copy constructor private
    TlsSuite (const TlsSuite&) =delete;
    // make the assignment operator private
    TlsSuite& operator = (const TlsSuite&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
