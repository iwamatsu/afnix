// ---------------------------------------------------------------------------
// - Solid.cpp                                                               -
// - afnix:geo service - solid geoemtry class implementation                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Solid.hpp"
#include "Solid.hxx"
#include "Vector.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the default axis rotation angle
  static const t_real GEO_RX_DEF   = 0.0;
  static const t_real GEO_RY_DEF   = 0.0;
  static const t_real GEO_RZ_DEF   = 0.0;

  // the solid xml name
  static const String XML_TAG_NAME = GEO_SLD_NAME;
  static const String XML_TAG_INFO = GEO_SLD_INFO;
  // the axis rotation angles
  static const String XML_RX_ATTR  = "rx";
  static const String XML_RY_ATTR  = "ry";
  static const String XML_RZ_ATTR  = "rz";

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // set the solid by plist

  void Solid::setplst (const Plist& plst) {
    wrlock ();
    try {
      // set the material
      d_matl.setplst (plst);
      // extract quaternion info
      t_real rx = plst.exists (XML_RX_ATTR) ?
	plst.toreal (XML_RX_ATTR) : GEO_RX_DEF;
      t_real ry = plst.exists (XML_RY_ATTR) ?
	plst.toreal (XML_RY_ATTR) : GEO_RY_DEF;
      t_real rz = plst.exists (XML_RZ_ATTR) ?
	plst.toreal (XML_RZ_ATTR) : GEO_RZ_DEF;
      d_qobj.setra (rx, ry, rz);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the solid property list

  Plist Solid::getplst (void) const {
    rdlock ();
    try {
      // the material plist
      Plist result = d_matl.getplst ();
      result.setname (XML_TAG_NAME); result.setinfo (XML_TAG_INFO);
      // set the quaternion rotation if any
      if (d_qobj.isrx () == true) result.add (XML_RX_ATTR, d_qobj.getxa ());
      if (d_qobj.isry () == true) result.add (XML_RY_ATTR, d_qobj.getxa ());
      if (d_qobj.isrz () == true) result.add (XML_RZ_ATTR, d_qobj.getxa ());
      // here it is
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the solid material

  Material Solid::getmatl (void) const {
    rdlock ();
    try {
      Material result = d_matl;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the quaternion object

  void Solid::setqo (const Quaternion& q) {
    wrlock ();
    try {
      d_qobj = q;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the solid quaternion

  Quaternion Solid::getqo (void) const {
    rdlock ();
    try {
      Quaternion result = d_qobj;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the x-axis angle

  void Solid::setrx (const t_real a) {
    wrlock ();
    try {
      d_qobj.setrx (a);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the y-axis angle

  void Solid::setry (const t_real a) {
    wrlock ();
    try {
      d_qobj.setry (a);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the z-axis angle

  void Solid::setrz (const t_real a) {
    wrlock ();
    try {
      d_qobj.setrz (a);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 6;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_SETRX     = zone.intern ("set-rotation-x");
  static const long QUARK_SETRY     = zone.intern ("set-rotation-y");
  static const long QUARK_SETRZ     = zone.intern ("set-rotation-z");
  static const long QUARK_SETQOBJ   = zone.intern ("set-orientation");
  static const long QUARK_GETQOBJ   = zone.intern ("get-orientation");
  static const long QUARK_GETORIGIN = zone.intern ("get-origin");

  // return true if the given quark is defined
  
  bool Solid::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Geometry::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark

  Object* Solid::apply (Runnable* robj, Nameset* nset, const long quark,
			Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETQOBJ)   return new Quaternion (getqo ());
      if (quark == QUARK_GETORIGIN) return new Point3 (getorigin ());
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_SETQOBJ) {
	Object* obj = argv->get (1);
	Quaternion* qobj = dynamic_cast <Quaternion*> (obj);
	if (qobj == nilp) {
	  throw Exception ("type-error", "invalid object with set-orientation",
			   Object::repr (qobj));
	}
	setqo (*qobj);
	return nilp;
      }
      if (quark == QUARK_SETRX) {
	t_real a = argv->getreal (0);
	setrx (a);
	return nilp;
      }
      if (quark == QUARK_SETRY) {
	t_real a = argv->getreal (0);
	setry (a);
	return nilp;
      }
      if (quark == QUARK_SETRZ) {
	t_real a = argv->getreal (0);
	setrz (a);
	return nilp;
      }
    }
    // call the geometry method
    return Geometry::apply (robj, nset, quark, argv);
  }
}
