# ---------------------------------------------------------------------------
# - CSM0001.als                                                             -
# - afnix:csm service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   slot test unit
# @author amaury darsch

# get the service
interp:library "afnix-csm"

# create a slot
const  slot (afnix:csm:Slot)
assert true (afnix:csm:slot-p slot)

# check default time and duration
assert 0 (slot:get-time)
assert 0 (slot:get-duration)

# check default time and duration
assert 0 (slot:get-time)
assert 0 (slot:get-duration)

# check time and duration
slot:set-time 2000
assert 2000 (slot:get-time)
slot:set-duration 1200
assert 1200 (slot:get-duration)
slot:set-slot 123 456
assert 123 (slot:get-time)
assert 456 (slot:get-duration)


# check index
assert 0 (slot:get-index)
slot:set-index 1
assert 1 (slot:get-index)

