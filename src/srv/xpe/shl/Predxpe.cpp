// ---------------------------------------------------------------------------
// - Predxpe.cpp                                                             -
// - afnix:xpe service - predicates implementation                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "Predxpe.hpp"
#include "Boolean.hpp"
#include "XmlPlist.hpp"
#include "Exception.hpp"
#include "XmlInclude.hpp"
#include "XmlProcessor.hpp"
#include "XmlPrintTable.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // procp: xml processor object predicate

  Object* xpe_procp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xml-processor-p");
    bool result = (dynamic_cast <XmlProcessor*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xcntp: xml content object predicate

  Object* xpe_xcntp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xml-content-p");
    bool result = (dynamic_cast <XmlContent*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xftrp: xml feature object predicate

  Object* xpe_xftrp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xml-feature-p");
    bool result = (dynamic_cast <XmlFeature*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xincp: xml include object predicate

  Object* xpe_xincp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xml-include-p");
    bool result = (dynamic_cast <XmlInclude*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xplstp: xml plist object predicate

  Object* xpe_xplstp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xml-plist-p");
    bool result = (dynamic_cast <XmlPlist*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // xptblp: xml print table object predicate

  Object* xpe_xptblp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "xml-print-table-p");
    bool result = (dynamic_cast <XmlPrintTable*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
}
