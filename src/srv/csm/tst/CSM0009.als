# ---------------------------------------------------------------------------
# - CSM0009.als                                                             -
# - afnix:csm service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   saas test unit
# @author amaury darsch

# get the module
interp:library "afnix-csm"

# create a default saas
const name "saas"
const info "afnix saas test"
const saas (afnix:csm:Saas name info)

# check representation
assert true   (afnix:csm:raas-p saas)
assert true   (afnix:csm:saas-p saas)
assert "Saas" (saas:repr)

# check accessor
assert name (saas:get-name)
assert info (saas:get-info)

# check host and port
const host "localhost"
const port 21900
saas:set-host host
saas:set-port port
assert host (saas:get-host)
assert port (saas:get-port)

# check rid
assert "default@localdomain" (saas:get-rid)
