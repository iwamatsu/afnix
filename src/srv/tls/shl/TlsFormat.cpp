// ---------------------------------------------------------------------------
// - TlsFormat.cpp                                                           -
// - afnix:tls service - tls format class implementation                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "System.hpp"
#include "TlsFormat.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "InputMapped.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the tls encoding format extension
  static const String TLS_XEXT_PEM = "PEM";
  static const String TLS_XEXT_DER = "DER";
  // the tls key format
  static const String TLS_KFMT_PK1 = "PKCS1";
  static const String TLS_KFMT_PK5 = "PKCS5";

  // the item eval quarks
  static const long QUARK_FMT = String::intern ("Format");
  static const long QUARK_PEM = String::intern ("PEM");
  static const long QUARK_DER = String::intern ("DER");
  static const long QUARK_PK1 = String::intern ("PKCS1");
  static const long QUARK_PK5 = String::intern ("PKCS5");

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // get the default encoding type

  TlsFormat::t_efmt TlsFormat::getedef (void) {
    return EFMT_PEM ;
  }

  // get the default key type

  TlsFormat::t_kfmt TlsFormat::getkdef (void) {
    return KFMT_PK1 ;
  }
  
  // convert a string to an encoding format

  TlsFormat::t_efmt TlsFormat::toefmt (const String& sfmt) {
    String x = sfmt.toupper ();
    if (x == TLS_XEXT_PEM) return TlsFormat::EFMT_PEM;
    if (x == TLS_XEXT_DER) return TlsFormat::EFMT_DER;
    throw Exception ("tls-error", "invalid format extension", sfmt);
  }

  // convert a string to a key format

  TlsFormat::t_kfmt TlsFormat::tokfmt (const String& sfmt) {
    String k = sfmt.toupper ();
    if (k == TLS_KFMT_PK1) return TlsFormat::KFMT_PK1;
    if (k == TLS_KFMT_PK5) return TlsFormat::KFMT_PK5;
    throw Exception ("tls-error", "invalid key format", sfmt);
  }

  // map an item to an encoding format
  
  TlsFormat::t_efmt TlsFormat::toefmt (const Item& item) {
    // check for a key item
    if (item.gettid () != QUARK_FMT)
      throw Exception ("item-error", "item is not a tls format item");
    // map the item to the enumeration
    long quark = item.getquark ();
    if (quark == QUARK_PEM) return TlsFormat::EFMT_PEM;
    if (quark == QUARK_DER) return TlsFormat::EFMT_DER;
    throw Exception ("item-error", "cannot map item to tls format encoding");
  }
  
  // map an item to a key format
  
  TlsFormat::t_kfmt TlsFormat::tokfmt (const Item& item) {
    // check for a key item
    if (item.gettid () != QUARK_FMT)
      throw Exception ("item-error", "item is not a tls format item");
    // map the item to the enumeration
    long quark = item.getquark ();
    if (quark == QUARK_PK1) return TlsFormat::KFMT_PK1;
    if (quark == QUARK_PK5) return TlsFormat::KFMT_PK5;
    throw Exception ("item-error", "cannot map item to tls key format");
  }
  
  // map an encoding mode to an item
  
  Item TlsFormat::toitem (const t_efmt efmt) {
    switch (efmt) {
    case EFMT_PEM:
      return Item (QUARK_FMT, QUARK_PEM);
      break;
    case EFMT_DER:
      return Item (QUARK_FMT, QUARK_DER);
      break;
    }
    throw Exception ("item-error", "cannot map encoding format to item");
  }

  // map a key mode to an item
  
  Item TlsFormat::toitem (const t_kfmt kfmt) {
    switch (kfmt) {
    case KFMT_PK1:
      return Item (QUARK_FMT, QUARK_PK1);
      break;
    case KFMT_PK5:
      return Item (QUARK_FMT, QUARK_PK5);
      break;
    }
    throw Exception ("item-error", "cannot map key format to item");
  }

  // map an encoding mode to a a string
  
  String TlsFormat::tostring (const t_efmt efmt) {
    switch (efmt) {
    case EFMT_PEM:
      return TLS_XEXT_PEM;
	break;
    case EFMT_DER:
      return TLS_XEXT_DER;
      break;
    }
    throw Exception ("tls-error", "cannot map encoding type to string");
  }

  // map an encoding mode to a a string
  
  String TlsFormat::tostring (const t_kfmt kfmt) {
    switch (kfmt) {
    case KFMT_PK1:
      return TLS_KFMT_PK1;
	break;
    case EFMT_DER:
      return TLS_KFMT_PK5;
      break;
    }
    throw Exception ("tls-error", "cannot map key type to string");
  }

  // create a buffer by name - using file extension

  Buffer* TlsFormat::tobuffer (const String& name) {
    // get the extension
    String x = System::xext (name);
    // check for pem format
    if (x == TLS_XEXT_PEM) {
      throw Exception ("tls-error", "unimplemented support for pem", name);
    }
    // default support to der
    InputMapped im (name);
    return im.tobuffer ();
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // evaluate an object data member

  Object* TlsFormat::meval (Runnable* robj, Nameset* nset, const long quark) {
    if (quark == QUARK_PEM)
      return new Item (QUARK_FMT, QUARK_PEM);
    if (quark == QUARK_DER)
      return new Item (QUARK_FMT, QUARK_DER);
    if (quark == QUARK_PK1)
      return new Item (QUARK_FMT, QUARK_PK1);
    if (quark == QUARK_PK5)
      return new Item (QUARK_FMT, QUARK_PK5);
    throw Exception ("eval-error", "cannot evaluate member",
                     String::qmap (quark));
  }
}
