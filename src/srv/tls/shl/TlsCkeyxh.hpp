// ---------------------------------------------------------------------------
// - TlsCkeyxh.hpp                                                           -
// - afnix:tls service - tls client key exchange message class definition    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSCKEYXH_HPP
#define  AFNIX_TLSCKEYXH_HPP

#ifndef  AFNIX_KEY_HPP
#include "Key.hpp"
#endif

#ifndef  AFNIX_TLSHBLOCK_HPP
#include "TlsHblock.hpp"
#endif

namespace afnix {

  /// The TlsCkeyxh class is the tls client key exchange message class. The
  /// class is built by decoding a handshake message block. Since the original
  /// message is encoded, a key is necessary for decoding the message block.
  /// @author amaury darsch

  class TlsCkeyxh : public TlsInfos {
  private:
    /// the premaster length
    long d_mlen;
    /// the premaster key
    t_byte* p_mbuf;
    
  public:
    /// create an empty client hello
    TlsCkeyxh (void);

    /// create a client hello by handshake block
    /// @aparam hblk the handshake block
    TlsCkeyxh (TlsHblock* hblk); 

    /// create a client hello by handshake block
    /// @aparam hblk the handshake block
    /// @aparam hkey the handshake key
    TlsCkeyxh (TlsHblock* hblk, Key* hkey); 

    /// destroy this block
    ~TlsCkeyxh (void);

    /// @return the class name
    String repr (void) const;

    /// reset this block
    void reset (void);

    /// @return the premaster length
    virtual long getmlen (void) const;

    /// @return the premaster key (no copy)
    virtual const t_byte* getmbuf (void) const;
    
    /// @return the block info as a plist
    Plist getinfo (void) const;

    /// decode the hanshake block
    /// @param hblk the block to decode
    /// @param hkey the handshake key
    virtual void decode (TlsHblock* hblk, Key* hkey);

  private:
    // make the copy constructor private
    TlsCkeyxh (const TlsCkeyxh&) =delete;
    // make the assignment operator private
    TlsCkeyxh& operator = (const TlsCkeyxh&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
  };
}

#endif
