// ---------------------------------------------------------------------------
// - Property.cpp                                                            -
// - standard object library - property class implementation                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Real.hpp"
#include "Stdsid.hxx"
#include "Vector.hpp"
#include "Utility.hpp"
#include "Boolean.hpp"
#include "Integer.hpp"
#include "Property.hpp"
#include "Runnable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "InputStream.hpp"
#include "OutputStream.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a new default property

  Property::Property (void) {
    d_name.clear ();
    d_info.clear ();
    p_pval = nilp;
  }

  // create a new property by name

  Property::Property (const String& name) {
    d_name = name;
    d_info.clear ();
    p_pval = nilp;
  }

  // create a new property by name and boolean value
  
  Property::Property (const String& name, const bool bval) { 
    d_name = name;
    d_info.clear ();
    p_pval = new Boolean (bval);
  }

  // create a new property by name and integer value
  
  Property::Property (const String& name, const t_long ival) { 
    d_name = name;
    d_info.clear ();
    p_pval = new Integer (ival);
  }

  // create a new property by name and real value
  
  Property::Property (const String& name, const t_real rval) { 
    d_name = name;
    d_info.clear ();
    p_pval = new Real (rval);
  }

  // create a new property by name and value
  
  Property::Property (const String& name, const Literal& lval) { 
    d_name = name;
    d_info.clear ();
    p_pval = dynamic_cast <Literal*> (lval.clone ());
  }

  // create a new property by name, info and boolean value
  
  Property::Property (const String& name, const String& info,
		      const bool    bval) { 
    d_name = name;
    d_info = info;
    p_pval = new Boolean (bval);
  }

  // create a new property by name, info and integer value
  
  Property::Property (const String& name, const String& info, 
		      const t_long  ival) { 
    d_name = name;
    d_info = info;
    p_pval = new Integer (ival);
  }

  // create a new property by name, info and real value
  
  Property::Property (const String& name, const String& info, 
		      const t_real  rval) { 
    d_name = name;
    d_info = info;
    p_pval = new Real (rval);
  }

  // create a new property by name, info and value
  
  Property::Property (const String&  name, const String& info,
		      const Literal& lval) { 
    d_name = name;
    d_info = info;
    p_pval = dynamic_cast <Literal*> (lval.clone ());
  }
  
  // copy constructor for this property class

  Property::Property (const Property& that) {
    that.rdlock ();
    try {
      d_name = that.d_name;
      d_info = that.d_info;
      p_pval = nilp;
      if (that.p_pval != nilp) {
	p_pval = dynamic_cast <Literal*> (that.p_pval->clone ());
      }
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }

  // copy move this property

  Property::Property (Property&& that) {
    that.wrlock();
    try {
      d_name = that.d_name; that.d_name.clear();
      d_info = that.d_info; that.d_info.clear();
      p_pval = that.p_pval; that.p_pval = nilp;
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
  
  // destroy this property

  Property::~Property (void) {
    delete p_pval;
  }

  // assign a property with a property
  
  Property& Property::operator = (const Property& that) {
    wrlock ();
    that.rdlock ();
    try {
      d_name = that.d_name;
      d_info = that.d_info;
      delete p_pval; p_pval = nilp;
      if (that.p_pval != nilp) {
	p_pval = dynamic_cast <Literal*> (that.p_pval->clone ());
      }
      unlock ();
      return *this;
    } catch (...) {
      that.unlock ();
      unlock ();
      throw;
    }
  }
  
  // move a property to this one

  Property& Property::operator = (Property&& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.wrlock ();
    try {
      d_name = that.d_name; that.d_name.clear();
      d_info = that.d_info; that.d_info.clear();
      p_pval = that.p_pval; that.p_pval = nilp;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }
  
  // return the class name

  String Property::repr (void) const {
    return "Property";
  }

  // return a clone of this object

  Object* Property::clone (void) const {
    return new Property (*this);
  }

  // return the property serial code

  t_byte Property::serialid (void) const {
    return SERIAL_PROP_ID;
  }

  // serialize this property

  void Property::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      d_name.wrstream (os);
      d_info.wrstream (os);
      if (p_pval == nilp) {
	Serial::wrnilid (os);
      } else {
	p_pval->serialize (os);
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this property

  void Property::rdstream (InputStream& is) {
    wrlock ();
    try {
      d_name.rdstream (is);
      d_info.rdstream (is);
      p_pval = dynamic_cast <Literal*> (Serial::deserialize (is));
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the property name

  String Property::getname (void) const {
    rdlock ();
    try {
      String result = d_name;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the property name

  void Property::setname (const String& name) {
    wrlock ();
    try {
      d_name = name;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the property info

  void Property::setinfo (const String& info) {
    wrlock ();
    try {
      d_info = info;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the property info

  String Property::getinfo (void) const {
    rdlock ();
    try {
      String result = d_info;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear this property

  void Property::clear (void) {
    wrlock ();
    try {
      d_name.clear ();
      d_info.clear ();
      Object::dref (p_pval); p_pval = nilp;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // set the property by boolean value

  void Property::setpval (const bool bval) {
    wrlock ();
    try {
      delete p_pval;
      p_pval = new Boolean (bval);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the property by integer value

  void Property::setpval (const t_long ival) {
    wrlock ();
    try {
      delete p_pval;
      p_pval = new Integer (ival);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the property by real value

  void Property::setpval (const t_real rval) {
    wrlock ();
    try {
      delete p_pval;
      p_pval = new Real (rval);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the property value

  void Property::setpval (const Literal& lval) {
    wrlock ();
    try {
      delete p_pval;
      p_pval = dynamic_cast <Literal*> (lval.clone ());
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the property string value

  String Property::getpval (void) const {
    rdlock ();
    try {
      String result = (p_pval == nilp) ? "" : p_pval->tostring ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // convert the property as a literal

  Literal* Property::toliteral (void) const {
    rdlock ();
    try {
      Literal* result = (p_pval == nilp)
	? nilp
	: dynamic_cast<Literal*> (p_pval->clone());
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // convert the property value to a boolean

  bool Property::tobool (void) const {
    rdlock ();
    try {
      // check for a boolean literal
      Boolean* bval = dynamic_cast <Boolean*> (p_pval);
      bool   result = (bval == nilp) ? false : bval->tobool ();
      // fallback to a string literal
      if ((bval == nilp) && (p_pval != nilp)) {
	result = Utility::tobool (p_pval->tostring ());
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // convert the property value to an integer boolean

  bool Property::toboil (void) const {
    rdlock ();
    try {
      // check for a boolean literal
      Boolean* bval = dynamic_cast <Boolean*> (p_pval);
      bool   result = (bval == nilp) ? false : bval->tobool ();
      // fallback to a string literal
      if ((bval == nilp) && (p_pval != nilp)) {
	result = Utility::toboil (p_pval->tostring ());
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // convert the property value to an integer

  t_long Property::tolong (void) const {
    rdlock ();
    try {
      // check for an integer literal
      Integer* ival = dynamic_cast <Integer*> (p_pval);
      t_long result = (ival == nilp) ? 0LL : ival->tolong ();
      // fallback to a string literal
      if ((ival == nilp) && (p_pval != nilp)) {
	result = Utility::tolong (p_pval->tostring ());
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // convert the property value to a real

  t_real Property::toreal (void) const {
    rdlock ();
    try {
      // check for a real literal
      Real*    rval = dynamic_cast <Real*> (p_pval);
      t_real result = (rval == nilp) ? 0.0 : rval->toreal ();
      // fallback to a string literal
      if ((rval == nilp) && (p_pval != nilp)) {
	result = Utility::toreal (p_pval->tostring ());
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the property name/value by boolean value

  void Property::set (const String& name, const bool bval) {
    wrlock ();
    try {
      d_name = name;
      delete p_pval;
      p_pval = new Boolean (bval);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the property name/value by integer value

  void Property::set (const String& name, const t_long ival) {
    wrlock ();
    try {
      d_name = name;
      delete p_pval;
      p_pval = new Integer (ival);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the property name/value by real value

  void Property::set (const String& name, const t_real rval) {
    wrlock ();
    try {
      d_name = name;
      delete p_pval;
      p_pval = new Real (rval);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the property name/value at once

  void Property::set (const String& name, const Literal& lval) {
    wrlock ();
    try {
      d_name = name;
      delete p_pval;
      p_pval = dynamic_cast <Literal*> (lval.clone ());
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // format the property value with with style

  String Property::format (const Style& lstl) const {
    rdlock ();
    try {
      String result = (p_pval != nilp) ? p_pval->format (lstl) : getpval ();
      unlock ();
      return result;	
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 11;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_SET     = zone.intern ("set");
  static const long QUARK_CLEAR   = zone.intern ("clear");
  static const long QUARK_SETNAME = zone.intern ("set-name");
  static const long QUARK_GETNAME = zone.intern ("get-name");
  static const long QUARK_SETINFO = zone.intern ("set-info");
  static const long QUARK_GETINFO = zone.intern ("get-info");
  static const long QUARK_SETPVAL = zone.intern ("set-value");
  static const long QUARK_GETPVAL = zone.intern ("get-value");
  static const long QUARK_GETBVAL = zone.intern ("get-boolean-value");
  static const long QUARK_GETIVAL = zone.intern ("get-integer-value");
  static const long QUARK_GETRVAL = zone.intern ("get-real-value");

  // create a new object in a generic way

  Object* Property::mknew (Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) return new Property;
    // check for 1 argument
    if (argc == 1) {
      String name = argv->getstring (0);
      return new Property (name);
    }
    // check for 2 arguments
    if (argc == 2) {
      String   name = argv->getstring (0);
      Object*   obj = argv->get (1);
      Literal* lobj = dynamic_cast <Literal*> (obj);
      if (lobj == nilp) {
	throw Exception ("type-error", "invalid object with property",
			 Object::repr (obj));
      }
      return new Property (name, *lobj);
    }
    // check for 3 arguments
    if (argc == 3) {
      String   name = argv->getstring (0);
      String   info = argv->getstring (1);
      Object*   obj = argv->get (2);
      Literal* lobj = dynamic_cast <Literal*> (obj);
      if (lobj == nilp) {
	throw Exception ("type-error", "invalid object with property",
			 Object::repr (obj));
      }
      return new Property (name, info, *lobj);
    }	     
    throw Exception ("argument-error", 
		     "too many argument with property constructor");
  }

  // return true if the given quark is defined

  bool Property::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? Serial::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }

  // apply this object with a set of arguments and a quark

  Object* Property::apply (Runnable* robj, Nameset* nset, const long quark,
			   Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();

    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETNAME) return new String  (getname ());
      if (quark == QUARK_GETINFO) return new String  (getinfo ());
      if (quark == QUARK_GETPVAL) return new String  (getpval ());
      if (quark == QUARK_GETBVAL) return new Boolean (tobool  ());
      if (quark == QUARK_GETIVAL) return new Integer (tolong  ());
      if (quark == QUARK_GETRVAL) return new Real    (toreal  ());
      if (quark == QUARK_CLEAR) {
	clear ();
	return nilp;
      }
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_SETNAME) {
	String name = argv->getstring (0);
	setname (name);
	return nilp;
      }
      if (quark == QUARK_SETINFO) {
	String info = argv->getstring (0);
	setinfo (info);
	return nilp;
      }
      if (quark == QUARK_SETPVAL) {
	Object*   obj = argv->get (0);
	Literal* lobj = dynamic_cast <Literal*> (obj);
	if (lobj == nilp) {
	  throw Exception ("type-error", "invalid object with set-value",
			   Object::repr (obj));
	}
	setpval (*lobj);
	return nilp;
      }
    }
    // dispatch 2 argument
    if (argc == 2) {
      if (quark == QUARK_SET) {
	String   name = argv->getstring (0);
	Object*   obj = argv->get (1);
	Literal* lobj = dynamic_cast <Literal*> (obj);
	if (lobj == nilp) {
	  throw Exception ("type-error", "invalid object with set-value",
			   Object::repr (obj));
	}
	set (name, *lobj);
	return nilp;
      }
    }
    // call the serial method
    return Serial::apply (robj, nset, quark, argv);
  }
}
