// ---------------------------------------------------------------------------
// - Session.hpp                                                             -
// - afnix:csm service - session class definition                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SESSION_HPP
#define  AFNIX_SESSION_HPP

#ifndef  AFNIX_NAMEABLE_HPP
#include "Nameable.hpp"
#endif

#ifndef  AFNIX_COOKIE_HPP
#include "Cookie.hpp"
#endif

namespace afnix {

  /// The Session class is a simple class that defines a session to be 
  /// associated with a cloud transaction. The session object is designed 
  /// to be persistent so that its data information can be retreived at
  /// any time. A session object has also the particularity to have a 
  /// limited lifetime. A session object is created by name with an
  /// identifier. The session object is designed to hold a variety of
  /// parameters that are suitable for both the authentication and the
  /// session lifetime. A session is primarily defined by name with an
  /// optional information string. Each user can be associated with a unique
  /// registration identifier (urid) which is generally produced by the 
  /// authentication platform. The registration id can be also uniquelly 
  /// mapped to a unique registration key (krid) those interprtation is up to
  /// the user. The user registration id is generally a unique user/account
  /// identifier for the authentication platform while the registration key
  /// is a unique key that can be used for single sign on (sso) operations.
  /// A session key is automatically when the session is created. Such key
  /// used to generate a session hash id which can be used as a cookie value.
  /// The cookie name is also stored in the session object. When a cookie is
  /// generated, the session hash name is combined with the session hash id
  /// for the cookie production.
  /// @author amaury darsch

  class Session : public Nameable, public Serial {
  private:
    /// the session name
    String d_name;
    /// the session user
    String d_user;
    /// the registration id
    String d_urid;
    /// the session key rid
    String d_krid;
    /// the session key
    String d_skey;
    /// the session hash name
    String d_shnm;
    /// the session hash id
    String d_shid;
    /// the session path
    String d_path;
    /// the session creation time
    t_long d_ctim;
    /// the session modification time
    t_long d_mtim;
    /// the session maximum age
    t_long d_mage;

  public:
    /// create an empty session
    Session (void);

    /// create a session by name
    /// @param name the session name
    Session (const String& name);

    /// create a session by name and path
    /// @param name the session name
    /// @param user the session user
    Session (const String& name, const String& user);

    /// create a session by name and maximum age
    /// @param name the session name
    /// @param user the session user
    /// @param mage the maximum age
    Session (const String& name, const String& user, const t_long mage);

    /// copy construct this object
    /// @param that the object to copy
    Session (const Session& that);

    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// @return the session serial id
    t_byte serialid (void) const;

    /// serialize this session
    /// @param os the output stream
    void wrstream (OutputStream& os) const;

    /// deserialize this session
    /// @param is the input stream
    void rdstream (InputStream& os);

    /// @return the session name
    String getname (void) const;

    /// assign an object to this one
    /// @param that the object to assign
    Session& operator = (const Session& that);

    /// set the session user
    /// @param user the session user
    virtual void setuser (const String& user);

    /// @return the session user
    virtual String getuser (void) const;

    /// set the session user rid
    /// @param urid the user rid
    virtual void seturid (const String& srid);

    /// @return the session user rid
    virtual String geturid (void) const;

    /// set the session key rid
    /// @param krid the session key rid
    virtual void setkrid (const String& krid);

    /// @return the session key rid
    virtual String getkrid (void) const;

    /// set the session hash id
    /// @param shid the session hash id
    virtual void setshid (const String& shid);

    /// @return the session hash id
    virtual String getshid (void) const;

    /// set the session path
    /// @param path the session path
    virtual void setpath (const String& path);

    /// @return the session path
    virtual String getpath (void) const;

    /// @return the session creation time
    virtual t_long getctim (void) const;

    /// @return the session modification time
    virtual t_long getmtim (void) const;

    /// @return true if a session has expired
    virtual bool isetim (void) const;

    /// set the session expiration time
    /// @param time the session allowed time
    virtual void setetim (const t_long time);

    /// @return the session expiration time
    virtual t_long getetim (void) const;

    /// @return the session remaining time
    virtual t_long getrtim (void) const;

    /// set the session maximum age
    /// @param mage the session age
    virtual void setmage (const t_long mage);

    /// @return the session maximum age
    virtual t_long getmage (void) const;

    /// bake a cookie by name
    /// @param name the cookie name
    virtual Cookie* getcookie (const String& name);

    /// close a session by cookie name
    /// @param the cookie name
    virtual Cookie* close (void);

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
