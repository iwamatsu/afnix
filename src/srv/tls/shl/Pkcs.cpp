// ---------------------------------------------------------------------------
// - Pkcs.cpp                                                                -
// - afnix:tls service - public key cryptographic standard implementation    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Pkcs.hpp"
#include "System.hpp"
#include "Vector.hpp"
#include "Utility.hpp"
#include "Integer.hpp"
#include "Runnable.hpp"
#include "TlsFormat.hpp"
#include "InputFile.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "AsnBuffer.hpp"
#include "AsnInteger.hpp"
#include "AsnSequence.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // get the pkcs1 node from the asn buffer
  static AsnSequence* pkcs1_topseq (const AsnBuffer& abuf) {
    // map the buffer to a node
    AsnNode* node = abuf.mapnode ();
    // check for a sequence
    AsnSequence* pseq = dynamic_cast <AsnSequence*> (node);
    if (pseq == nilp) {
      delete node;
      throw Exception ("pkcs-error", "cannot map pkcs sequence node");
    }
    // check the sequence length
    if (pseq->getnlen () != 9) {
      delete node;
      throw Exception ("pkcs-error", "invalid pkcs sequence length");
    }
    return pseq;
  }

  // get the pkcs1 relatif number by index
  static Relatif pkcs1_torval (const AsnSequence* pseq, const long index) {
    // check for nil
    if (pseq == nilp) {
      throw Exception ("pkcs-error", "invalid nil pkcs sequence node");
    }
    // get the version node [0]
    AsnNode* node = pseq->getnode (index);
    AsnInteger* inod = dynamic_cast <AsnInteger*> (node);
    if (inod == nilp) {
      throw Exception ("pkcs-error", "cannot map integer node");
    }
    return inod->torelatif ();
  }

  // -------------------------------------------------------------------------
  // - pkcs section                                                         -
  // -------------------------------------------------------------------------

  // create a default pkcs
  
  Pkcs::Pkcs (void) {
    d_efmt = TlsFormat::getedef ();
  }

  // create a pkcs by encoding

  Pkcs::Pkcs (const TlsFormat::t_efmt efmt) {
    d_efmt = efmt;
  }

  // copy construct this pkcs object

  Pkcs::Pkcs (const Pkcs& that) {
    that.rdlock ();
    try {
      d_efmt = that.d_efmt;
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
  
  // assign a pkcs to this one

  Pkcs& Pkcs::operator = (const Pkcs& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      d_efmt = that.d_efmt;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }

  // get the encoding format

  TlsFormat::t_efmt Pkcs::getefmt (void) const {
    rdlock ();
    try {
      TlsFormat::t_efmt result = d_efmt;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - pkcs object section                                                   -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 1;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_GETEFMT = zone.intern ("get-encoding-format");

  // return true if the given quark is defined

  bool Pkcs::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark

  Object* Pkcs::apply (Runnable* robj, Nameset* nset, const long quark,
			Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETEFMT) {
	return new Item (TlsFormat::toitem (getefmt ()));
      }
    }
    // call the object method
    return Object::apply (robj, nset, quark, argv);
  }
  
  // -------------------------------------------------------------------------
  // - pkcs1 section                                                         -
  // -------------------------------------------------------------------------

  // create a default pkcs key
  
  Pkcs1::Pkcs1 (void) {
    p_pkey = nilp;
    reset ();
  }
  
  // create a pkcs key by input stream

  Pkcs1::Pkcs1 (InputStream* is) {
    p_pkey = nilp;
    parse (is);
  }

  // create a pkcs key by path

  Pkcs1::Pkcs1 (const String& path) :
    Pkcs (TlsFormat::toefmt(System::xext(path))) {
    // preset objects
    p_pkey = nilp;
    // create the input file
    InputFile is (path);
    // parse the file
    parse (&is);
    // close the file
    is.close ();
  }
  
  // copy construct this pkcs key

  Pkcs1::Pkcs1 (const Pkcs1& that) {
    that.rdlock ();
    try {
      // set base object
      Pkcs::operator = (that);
      // copy locally
      p_pkey = (that.p_pkey == nilp) ? nilp : new Key (*that.p_pkey);
      Object::iref (p_pkey);
      // here it is
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
      
  // destroy this pkcs key

  Pkcs1::~Pkcs1 (void) {
    Object::dref (p_pkey);
  }
  
  // return the object class name

  String Pkcs1::repr (void) const {
    return "Pkcs1";
  }

  // return a clone of this object

  Object* Pkcs1::clone (void) const {
    return new Pkcs1 (*this);
  }
  
  // assign a pkcs key to this one

  Pkcs1& Pkcs1::operator = (const Pkcs1& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      // assign base object
      Pkcs::operator = (that);
      // copy locally
      Object::dref (p_pkey);
      p_pkey = (that.p_pkey == nilp) ? nilp : new Key (*that.p_pkey);
      Object::iref (p_pkey);
      // here it is
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }
  
  // reset the pkcs key

  void Pkcs1::reset (void) {
    wrlock ();
    try {
      Object::dref (p_pkey); p_pkey = nilp;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // parse an input stream

  void Pkcs1::parse (InputStream* is) {
    wrlock ();
    AsnSequence* pseq = nilp;
    try {
      // check for valid stream
      if (is == nilp) {
	throw Exception ("pkcs-error", "invalid nil input stream to read");
      }
      // check for valid format
      if (d_efmt == TlsFormat::EFMT_PEM) {
	throw Exception ("pkcs-error", "unimplemented pkcs pem reader");
      }
      // reset the object
      reset ();
      // create an asn buffer
      AsnBuffer abuf (is);
      // map the buffer to a sequence
      pseq = pkcs1_topseq (abuf);
      // get the object version
      long vers = pkcs1_torval(pseq,0).tolong ();
      if (vers != 0L) {
	throw Exception ("pkcs-error", "invalid pkcs key version");
      }
      // get the key element
      Relatif pmod = pkcs1_torval (pseq, 1);
      Relatif pexp = pkcs1_torval (pseq, 2);
      Relatif sexp = pkcs1_torval (pseq, 3);
      Relatif spvp = pkcs1_torval (pseq, 4);
      Relatif spvq = pkcs1_torval (pseq, 5);
      Relatif crtp = pkcs1_torval (pseq, 6);
      Relatif crtq = pkcs1_torval (pseq, 7);
      Relatif crti = pkcs1_torval (pseq, 8);
      // create a key vector argument
      Vector kvec;
      kvec.add (new Relatif (pmod));
      kvec.add (new Relatif (pexp));
      kvec.add (new Relatif (sexp));
      kvec.add (new Relatif (spvp));
      kvec.add (new Relatif (spvq));
      kvec.add (new Relatif (crtp));
      kvec.add (new Relatif (crtq));
      kvec.add (new Relatif (crti));
      // create the key
      p_pkey = new Key (Key::KRSA, kvec);
      Object::iref (p_pkey);
      // clean and return
      delete pseq;
      unlock ();
    } catch (...) {
      delete pseq;
      unlock ();
      throw;
    }
  }

  // get the pkcs key

  Key* Pkcs1::getkey (void) const {
    rdlock ();
    try {
      Key* result = p_pkey;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - pkcs1 object section                                                  -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZON1_LENGTH = 1;
  static QuarkZone  zon1 (QUARK_ZON1_LENGTH);

  // the object supported quarks
  static const long QUARK_GETKEY1 = zone.intern ("get-key");

  // create a new object in a generic way
 
  Object* Pkcs1::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();

    // create a default object
    if (argc == 0) return new Pkcs1;
    // check for 1 argument
    if (argc == 1) {
      Object* obj = argv->get (0);
      // check for a string
      String* path = dynamic_cast <String*> (obj);
      if (path != nilp) return new Pkcs1 (*path);
      // check for an input stream
      InputStream* is = dynamic_cast <InputStream*> (obj);
      if (is != nilp) return new Pkcs1 (is);
      // invalid object
      throw Exception ("type-error", "invalif object with pkcs",
		       Object::repr (obj));
    }
    // too many arguments
    throw Exception ("argument-error",
                     "too many argument with pkcs constructor");
  }

  // return true if the given quark is defined

  bool Pkcs1::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zon1.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Pkcs::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark

  Object* Pkcs1::apply (Runnable* robj, Nameset* nset, const long quark,
			Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETKEY1) {
	rdlock ();
	try {
	  Object* result = getkey ();
	  robj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // call the pkcs method
    return Pkcs::apply (robj, nset, quark, argv);
  }
}
