// ---------------------------------------------------------------------------
// - Dsa.cpp                                                                 -
// - afnix:sec module - dss/dsa signature class implementation               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Dsa.hpp"
#include "Vector.hpp"
#include "Crypto.hpp"
#include "Integer.hpp"
#include "Runnable.hpp"
#include "QuarkZone.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // dsa constants
  static const char* DSA_ALGO_NAME = "DSA";

  // get a hasher object by length
  static Hasher* dsa_get_hash (const Key& key) {
    // get the key q prime value
    Relatif q = key.getrkey (Key::KDSA_QPRM);
    // get the hash length
    long hlen = q.getmsb ();
    // map the hash object
    if (hlen == 160) return Crypto::mkhasher (Crypto::SHA1);
    if (hlen == 224) return Crypto::mkhasher (Crypto::SHA224);
    if (hlen == 256) return Crypto::mkhasher (Crypto::SHA256);
    throw Exception ("dsa-error", "invalid dsa hash length");
  }

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------
  
  // create a default dsa signature

  Dsa::Dsa (void) : Signer (DSA_ALGO_NAME) {
    // initialize
    p_hash = nilp;
    d_sk   = 0;
    // create the default key
    Key key (Key::KDSA);
    // set the key
    setkey (key);
  }

  // create a dsa signature by key

  Dsa::Dsa (const Key& key) : Signer (DSA_ALGO_NAME) {
    // initialize
    p_hash = nilp;
    d_sk   = 0;
    // set the key
    setkey (key);
  }

  // create a dsa signature by key and secret value

  Dsa::Dsa (const Key& key, const Relatif& sk) : Signer (DSA_ALGO_NAME) {
    // initialize
    p_hash = nilp;
    d_sk   = sk;
    // set the key
    setkey (key);
  }
  // destroy this signature

  Dsa::~Dsa (void) {
    delete p_hash;
  }

  // return the class name

  String Dsa::repr (void) const {
    return "Dsa";
  }

  // reset this dsa signature
  
  void Dsa::reset (void) {
    wrlock ();
    try {
      // reset the hasher
      if (p_hash != nilp) p_hash->reset ();
      // done
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    } 
  }

  // set the signature key
  
  void Dsa::setkey (const Key& key) {
    wrlock ();
    try {
      // check the key
      if (key.gettype () != Key::KDSA) {
	throw Exception ("dsa-error", "invalid key type for dsa signature");
      }
      // set the signature key
      Signer::setkey (key);
      // map the hash object by key
      delete p_hash;
      p_hash = dsa_get_hash (key);
      // reset everything
      reset ();
      // done
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // process a message by data
  
  void Dsa::process (const t_byte* data, const long size) {
    wrlock ();
    try {
      if (p_hash != nilp) p_hash->process (data, size);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
 
  // process a message with a buffer
  
  void Dsa::process (Buffer& buf) {
    try {
      if (p_hash != nilp) p_hash->process (buf);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // process a message with an input stream
  
  void Dsa::process (InputStream& is) {
    try {
      if (p_hash != nilp) p_hash->process (is);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // finish the signature processing

  Signature Dsa::finish (void) {
    rdlock ();
    try {
      // finish hash processing
      if (p_hash != nilp) p_hash->finish ();
      // extract hash result
      Relatif z = (p_hash == nilp) ? 0 : p_hash->gethval ();
      // extract key values
      Relatif p = d_skey.getrkey (Key::KDSA_PPRM);
      Relatif q = d_skey.getrkey (Key::KDSA_QPRM);
      Relatif k = (d_sk > 0) ? d_sk : Relatif::random (q);
      Relatif g = d_skey.getrkey (Key::KDSA_PGEN);
      Relatif x = d_skey.getrkey (Key::KDSA_SKEY);
      // compute signature value
      Relatif r = Relatif::mme (g, k, p) % q;
      Relatif s = (Relatif::mmi (k, q) * (z + x*r)) % q;
      // generate the signature
      Signature result (Signature::SDSA, r, s);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object setion                                                         -
  // -------------------------------------------------------------------------

  // create a new object in a generic way
  
  Object* Dsa::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Dsa;
    // check for 1 argument
    if (argc == 1) {
      // check for a key
      Object* obj = argv->get (0);
      Key*    key = dynamic_cast <Key*> (obj);
      if (key != nilp) return new Dsa (*key);
      throw Exception ("argument-error", 
		       "invalid arguments with dsa", Object::repr (obj));
    }
    // check for 2 arguments
    if (argc == 2) {
      // check for a key
      Object* obj = argv->get (0);
      Key*    key = dynamic_cast <Key*> (obj);
      if (key == nilp) {
	throw Exception ("argument-error", 
			 "invalid arguments with dsa", Object::repr (obj));
      }
      // check for a relatif
      obj = argv->get (1);
      Relatif* rel = dynamic_cast <Relatif*> (obj);
      if (rel == nilp) {
	throw Exception ("argument-error", 
			 "invalid arguments with dsa", Object::repr (obj));
      }
      return new Dsa (*key, *rel);
    }
    throw Exception ("argument-error", "too many arguments with dsa");
  }
}
