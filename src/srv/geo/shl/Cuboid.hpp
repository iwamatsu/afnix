// ---------------------------------------------------------------------------
// - Cuboid.hpp                                                              -
// - afnix:geo service - rectangular cuboid class definition                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CUBOID_HPP
#define  AFNIX_CUBOID_HPP

#ifndef  AFNIX_SOLID_HPP
#include "Solid.hpp"
#endif

namespace afnix {

  /// The cuboid class is a rectangular cuboid, which is a solid convex
  /// polyhedron represented by a point of origin and a vector. The cuboid
  /// vector can be viewed as the width, height and depth of the cuboid.
  /// @author amaury darsch

  class Cuboid : public Solid {
  protected:
    /// the cuboid origin
    Point3 d_o;
    /// the cuboid apex
    Point3 d_a;

  public:
    /// create a unity cuboid
    Cuboid (void);

    /// create a cuboid by points
    /// @param o the cuboid origin
    /// @param a the cuboid apex
    Cuboid (const Point3& o, const Point3& a);

    /// copy construct this cuboid
    /// @param that the object to copy
    Cuboid (const Cuboid& that);
    
    /// assign a cuboid to this one
    /// @param that the object to copy
    Cuboid& operator = (const Cuboid& that);

    /// @return the class name
    String repr (void) const;
    
    /// @return a clone of this object
    Object* clone (void) const;
    
    /// reset this cuboid
    void reset (void);
        
    /// @return the geometry name
    String getname (void) const;

    /// set the geometry by plist
    /// @param plst the geometry plist
    void setplst (const Plist& plst);

    /// @return the geometry plist
    Plist getplst (void) const;
    
    /// @return the cuboid origin
    Point3 getorigin (void) const;

    /// @return the cuboid apex
    virtual Point3 getapex (void) const;

    /// set a cuboid by points
    /// @param o the cuboid origin
    /// @param a the cuboid apex
    virtual void set (const Point3& o, const Point3& a);

    /// normalize the cuboid coordinates
    virtual void normalize (void);
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
