# ----------------------------------------------------------------------------
# - std-adp-optdb                                                            -
# - afnix:adp option database module                                         -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - global section                                                           -
# ----------------------------------------------------------------------------

# the option database class
const afnix:adp:optdb (class)

# ----------------------------------------------------------------------------
# - private section                                                          -
# ----------------------------------------------------------------------------

# the option messages
const AFNIX:ADP:U-CLS-MSG "axi [i afnix-std-adp -m --] [options] uri"
const AFNIX:ADP:H-LCO-MSG "    [h]              print this help message"
const AFNIX:ADP:V-LCO-MSG "    [v]              print system version"
const AFNIX:ADP:P-LCO-MSG "    [p]              parse only the source content"
const AFNIX:ADP:X-LCO-MSG "    [x]              enable xhtml mode"
const AFNIX:ADP:M-LCO-MSG "    [m]              enable nroff manual mode"
const AFNIX:ADP:L-LCO-MSG "    [l]              enable latex manual mode"
const AFNIX:ADP:O-LCO-MSG "    [o]              set output file name"

# ----------------------------------------------------------------------------
# - initial section                                                          -
# ----------------------------------------------------------------------------

# initialize the option descriptors
trans afnix:adp:optdb:preset (argv) {
  # create an option class and bind it
  trans this:super (afnix:sys:Options AFNIX:ADP:U-CLS-MSG)

  # register the options
  this:add-string-option 'o' AFNIX:ADP:O-LCO-MSG
  this:add-unique-option 'l' AFNIX:ADP:L-LCO-MSG
  this:add-unique-option 'm' AFNIX:ADP:M-LCO-MSG
  this:add-unique-option 'x' AFNIX:ADP:X-LCO-MSG
  this:add-unique-option 'p' AFNIX:ADP:P-LCO-MSG
  this:add-unique-option 'v' AFNIX:ADP:V-LCO-MSG
  this:add-unique-option 'h' AFNIX:ADP:H-LCO-MSG

  # parse the options
  try (this:parse argv) {
    this:usage (interp:get-error-stream)
    afnix:sys:exit 1
  }
  # check for the help option
  if (this:get-unique-option 'h') {
    this:usage (interp:get-output-stream)
    afnix:sys:exit 0
  }
  # check for the version option
  if (this:get-unique-option 'v') {
    println (afnix:adp:get-copyright-message)
    println (afnix:adp:get-revision-message)
    afnix:sys:exit 0
  }
  # check for the output file name
  if (this:get-unique-option 'o') {
    trans afnix:adp:system-oflg true
    trans afnix:adp:system-onam (this:get-string-option 'o')
  }
  # get the requested uri argument
  const varg (this:get-vector-arguments)
  if (!= (varg:length) 1) {
    this:usage (interp:get-error-stream)
    afnix:sys:exit 1
  }
  const this:uri (varg:get 0)
}

# ----------------------------------------------------------------------------
# - methods section                                                          -
# ----------------------------------------------------------------------------

# @return the requested uri name

trans afnix:adp:optdb:get-uri-name nil {
  afnix:adp:get-uri-name this:uri
}
