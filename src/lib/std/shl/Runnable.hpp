// ---------------------------------------------------------------------------
// - Runnable.hpp                                                            -
// - standard object library - runnable abstract class definition            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_RUNNABLE_HPP
#define  AFNIX_RUNNABLE_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif

namespace afnix {

  /// The Runnable class is the virtual class used as an interface to the 
  /// thread management system. The class define a single method called 'run'
  /// which is called when the thread is created.
  /// @author amaury darsch

  class Runnable : public Object  {
  public:
    /// evaluate the runnable form
    virtual Object* run (void) =0;

    /// break the runnable in a nameset with an object
    /// @param nset   the nameset to loop
    /// @param object the object to break on
    virtual bool bpt (Nameset* nset, Object* object) =0;

    /// run the read-eval loop on the standard streams
    /// @return false if something bad happen
    virtual bool loop (void) =0;

    /// loop in the context of a nameset and an input stream
    /// @param nset the nameset to loop
    /// @param is   the input stream to use
    virtual bool loop (Nameset* nset, class InputStream* is) =0;

    /// run the read-eval loop with a file
    /// @param fname the file name to read
    /// @return false if something bad happen
    virtual bool loop (const String& fname) =0;

    /// run the read-eval loop with a file
    /// @param fname the file name to read
    virtual void load (const String& fname) =0;

    /// post an object in this runnable class
    /// @param object the object to post
    virtual void post (Object* object) =0;

    /// evaluate a form in a thread
    /// @param form the form to evaluate
    virtual Object* launch (Object* form) =0;

    /// evaluate a form in a thread
    /// @param tobj the thread to run
    /// @param form the form to evaluate
    virtual Object* launch (Object* tobj, Object* form) =0;

    /// @return the runnable input stream
    virtual class InputStream* getis (void) const =0;

    /// @return the runnable output stream
    virtual class OutputStream* getos (void) const =0;

    /// @return the runnable error stream
    virtual class OutputStream* getes (void) const =0;

    /// @return the runnable global nameset
    virtual Nameset* getgset (void) const =0;

    /// set the assert flag
    /// @param flag the flag to set
    virtual void setasrt (const bool flag) =0;

    /// @return the runnable assert flag
    virtual bool getasrt (void) const =0;

    /// @return the next flag for this runnable
    virtual bool getnext (void) const =0;

    /// set the next flag
    /// @param flag the flag to set
    virtual void setnext (const bool flag) =0;

  public:
    /// evaluate an object in this runnable
    /// @param object the object to evaluate
    virtual Object* eval (Object* object) = 0;
  };
}

#endif
