# ---------------------------------------------------------------------------
# - NWG0004.als                                                             -
# - afnix:nwg module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   mime test unit
# @author amaury darsch

# get the module
interp:library "afnix-nwg"

# check the mime extension predicate
assert true  (afnix:nwg:mime-extension-p "txt")
assert true  (afnix:nwg:mime-extension-p "axc")
assert false (afnix:nwg:mime-extension-p "aaa")

# check the mime value predicate
assert true  (afnix:nwg:mime-value-p "text/plain")
assert true  (afnix:nwg:mime-value-p "application/x-afnix-compiled")

# check the extension to mime
assert "text/plain" (afnix:nwg:extension-to-mime "txt")
assert "application/xhtml+xml" (afnix:nwg:extension-to-mime "xht")
assert "application/x-afnix-compiled" (afnix:nwg:extension-to-mime "axc")

# check xml mime type
assert false (afnix:nwg:mime-xml-p "text/plain")
assert true  (afnix:nwg:mime-xml-p "application/xml")
