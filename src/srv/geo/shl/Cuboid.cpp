// ---------------------------------------------------------------------------
// - Cuboid.cpp                                                              -
// - afnix:geo service - cuboid class implementation                         -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Solid.hxx"
#include "Vector.hpp"
#include "Cuboid.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the default cuboid parameters
  static const t_real GEO_XO_DEF   = 0.0;
  static const t_real GEO_YO_DEF   = 0.0;
  static const t_real GEO_ZO_DEF   = 0.0;
  static const t_real GEO_XA_DEF   = 1.0;
  static const t_real GEO_YA_DEF   = 1.0;
  static const t_real GEO_ZA_DEF   = 1.0;
  
  // the geometry name
  static const String XML_TAG_NAME = GEO_CUB_NAME;
  static const String XML_TAG_INFO = GEO_CUB_INFO;
  // the cuboid origin attributes
  static const String XML_XO_ATTR  = "xo";
  static const String XML_YO_ATTR  = "yo";
  static const String XML_ZO_ATTR  = "zo";
  // the cuboid apex attributes
  static const String XML_XA_ATTR  = "xa";
  static const String XML_YA_ATTR  = "ya";
  static const String XML_ZA_ATTR  = "za";
  
  // this procedure swap two values
  static inline void swap (t_real& x, t_real& y) {
    t_real t = x;
    x = y;
    y = t;
  }
  
  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a unity cuboid

  Cuboid::Cuboid (void) {
    reset ();
  }
  
  // create a cuboid by points

  Cuboid::Cuboid (const Point3& o, const Point3& a) {
    set (o, a); 
  }

  // copy construct this object

  Cuboid::Cuboid (const Cuboid& that) {
    that.rdlock ();
    try {
      Solid::operator = (that);
      d_o = that.d_o;
      d_a = that.d_a;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }  
  }

  // assign a cuboid to this one

  Cuboid& Cuboid::operator = (const Cuboid& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      Solid::operator = (that);
      d_o = that.d_o;
      d_a = that.d_a;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return the class name

  String Cuboid::repr (void) const {
    return "Cuboid";
  }

  // return a clone of this object

  Object* Cuboid::clone (void) const {
    return new Cuboid (*this);
  }

  // reset this cuboid

  void Cuboid::reset (void) {
    wrlock ();
    try {
      d_o = {GEO_XO_DEF, GEO_YO_DEF, GEO_ZO_DEF};
      d_a = {GEO_XA_DEF, GEO_YA_DEF, GEO_ZA_DEF};
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the cuboid name

  String Cuboid::getname (void) const {
    rdlock ();
    try {
      String result = GEO_CUB_NAME;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the cuboid by plist

  void Cuboid::setplst (const Plist& plst) {
    wrlock ();
    try {
      // set the solid by plist
      Solid::setplst (plst);
      // get the cuboid parameters
      t_real xo = plst.exists (XML_XO_ATTR) ?
	plst.toreal (XML_XO_ATTR) : GEO_XO_DEF;
      t_real yo = plst.exists (XML_YO_ATTR) ?
	plst.toreal (XML_YO_ATTR) : GEO_YO_DEF;
      t_real zo = plst.exists (XML_ZO_ATTR) ?
	plst.toreal (XML_ZO_ATTR) : GEO_ZO_DEF;
      t_real xa = plst.exists (XML_XA_ATTR) ?
	plst.toreal (XML_XA_ATTR) : GEO_XA_DEF;
      t_real ya = plst.exists (XML_YA_ATTR) ?
	plst.toreal (XML_YA_ATTR) : GEO_YA_DEF;
      t_real za = plst.exists (XML_ZA_ATTR) ?
	plst.toreal (XML_ZA_ATTR) : GEO_ZA_DEF;
      // set the cuboid geometry
      d_o = {xo, yo, zo};
      d_a = {xa, ya, za};
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the cuboid plist

  Plist Cuboid::getplst (void) const {
    rdlock ();
    try {
      // the solid plist
      Plist result = Solid::getplst ();
      result.setname (XML_TAG_NAME); result.setinfo (XML_TAG_INFO);
      // add the geometry properties
      result.add (XML_XO_ATTR, d_o.getx ());
      result.add (XML_YO_ATTR, d_o.gety ());
      result.add (XML_ZO_ATTR, d_o.getz ());
      result.add (XML_XA_ATTR, d_a.getx ());
      result.add (XML_YA_ATTR, d_a.gety ());
      result.add (XML_ZA_ATTR, d_a.getz ());
      // here it is
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the cuboid origin

  Point3 Cuboid::getorigin (void) const {
    rdlock ();
    try {
      Point3 result = d_o;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  
  // get the cuboid apex

  Point3 Cuboid::getapex (void) const {
    rdlock ();
    try {
      Point3 result = d_a;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the cuboid points

  void Cuboid::set (const Point3& o, const Point3& a) {
    wrlock ();
    try {
      // set the cuboid coordinates
      d_o = o;
      d_a = a;
      // normalize
      normalize ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // normalize the cuboid points

  void Cuboid::normalize (void) {
    wrlock ();
    try {
      // get the point coordinates
      t_real xo = d_o.getx ();
      t_real yo = d_o.gety ();
      t_real zo = d_o.getz ();
      t_real xa = d_a.getx ();
      t_real ya = d_a.gety ();
      t_real za = d_a.getz ();
      // readjust with position
      if (xa < xo) swap (xo, xa);
      if (ya < yo) swap (yo, ya);
      if (za < zo) swap (zo, za);
      // reset origin and apex
      d_o = {xo, yo, zo};
      d_a = {xa, ya, za};
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 2;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_NMRL    = zone.intern ("normalize");
  static const long QUARK_GETAPEX = zone.intern ("get-apex");

  // create a new object in a generic way

  Object* Cuboid::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Cuboid;
    // invalid arguments
    throw Exception ("argument-error", 
                     "invalid arguments with with cuboid"); 
  }

  // return true if the given quark is defined
  
  bool Cuboid::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Solid::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark

  Object* Cuboid::apply (Runnable* robj, Nameset* nset, const long quark,
			 Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETAPEX) return new Point3 (getapex ());
      if (quark == QUARK_NMRL) {
	normalize ();
	return nilp;
      }
    }
    // call the solid method
    return Solid::apply (robj, nset, quark, argv);
  }
}
