// ---------------------------------------------------------------------------
// - GeoGroup.cpp                                                            -
// - afnix:geo service - geometry group class implementation                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Integer.hpp"
#include "Runnable.hpp"
#include "GeoGroup.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default group

  GeoGroup::GeoGroup (void) {
    reset ();
  }
  
  // copy construct this group

  GeoGroup::GeoGroup (const GeoGroup& that) {
    that.rdlock ();
    try {
      d_geom = that.d_geom;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }  
  }

  // assign a group to this one

  GeoGroup& GeoGroup::operator = (const GeoGroup& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      d_geom = that.d_geom;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return the class name

  String GeoGroup::repr (void) const {
    return "GeoGroup";
  }

  // return a clone of this object

  Object* GeoGroup::clone (void) const {
    return new GeoGroup (*this);
  }

  // reset this group

  void GeoGroup::reset (void) {
    wrlock ();
    try {
      d_geom.reset ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the group length

  long GeoGroup::length (void) const {
    rdlock ();
    try {
      long result = d_geom.length ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // add a geometry to a group

  void GeoGroup::add (Geometry* geo) {
    wrlock ();
    try {
      if (geo != nilp) d_geom.add (geo);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a geometry by position

  Geometry* GeoGroup::get (const long idx) const {
    rdlock ();
    try {
      Geometry* result = dynamic_cast <Geometry*> (d_geom.get (idx));
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 4;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_ADD    = zone.intern ("add");
  static const long QUARK_GET    = zone.intern ("get");
  static const long QUARK_RESET  = zone.intern ("reset");
  static const long QUARK_LENGTH = zone.intern ("length");

  // create a new object in a generic way

  Object* GeoGroup::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new GeoGroup;
    // invalid group
    throw Exception ("argument-error", 
                     "invalid arguments with with geometric group"); 
  }

  // return true if the given quark is defined
  
  bool GeoGroup::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark

  Object* GeoGroup::apply (Runnable* robj, Nameset* nset, const long quark,
			   Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_LENGTH) return new Integer (length ());
      if (quark == QUARK_RESET) {
	reset ();
	return nilp;
      }
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_ADD) {
	Object* obj = argv->get (0);
	Geometry* geo = dynamic_cast <Geometry*> (obj);
	if ((geo == nilp) && (obj != nilp)) {
	  throw Exception ("type-error", "invalid object as geometry",
			   Object::repr (obj));
	}
	add (geo);
	return nilp;
      }
      if (quark == QUARK_GET) {
	rdlock ();
	try {
	  long idx = argv->getlong (0);
	  Geometry* geo = get (idx);
	  robj->post (geo);
	  unlock ();
	  return geo;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // call the object method
    return Object::apply (robj, nset, quark, argv);
  }
}
