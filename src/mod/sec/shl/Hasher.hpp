// ---------------------------------------------------------------------------
// - Hasher.hpp                                                              -
// - afnix:sec module - base message hasher class definition                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_HASHER_HPP
#define  AFNIX_HASHER_HPP

#ifndef  AFNIX_RELATIF_HPP
#include "Relatif.hpp"
#endif

#ifndef  AFNIX_NAMEABLE_HPP
#include "Nameable.hpp"
#endif

#ifndef  AFNIX_BLOCKBUFFER_HPP
#include "BlockBuffer.hpp"
#endif

namespace afnix {

  /// The Hasher class is a base class that is used to build a message
  /// hash. The hash result is stored in an array of bytes and can be
  /// retreived byte by byte or as a formatted octet string.
  /// @author amaury darsch

  class Hasher : public BlockBuffer, public Nameable {
  protected:
    /// the hasher name
    String  d_name;
    /// the hash size
    long    d_hlen;
    /// the result length
    long    d_rlen;
    /// the hash result
    t_byte* p_hash;

    /// update the hasher state with the buffer data
    virtual bool update (void) =0;

  public:
    /// create a hasher object by name and size
    /// @param name the hasher name
    /// @param size the block size
    /// @param hlen the hash length
    Hasher (const String& name, const long size, const long hlen);

    /// create a hasher object by name, size and result length
    /// @param name the hasher name
    /// @param size the block size
    /// @param hlen the hash length
    /// @param rlen the result length
    Hasher (const String& name, const long size, const long hlen, 
	    const long rlen);

    /// destroy the hasher
    ~Hasher (void);

    /// @return the class name
    String repr (void) const;

    /// reset this hasher
    void reset (void);

    /// @return the hasher name
    String getname (void) const;

    /// @return the hasher length
    virtual long gethlen (void) const;

    /// @return the hasher result length
    virtual long getrlen (void) const;

    /// @return the hash value by index
    virtual t_byte getbyte (const long index) const;

    /// push the hash value into a buffer
    /// @param buf the buffer to fill
    virtual long pushb (Buffer& buf);

    /// hash and push the hash into a buffer
    /// @param obuf the buffer to fill
    /// @param ibuf the buffer to hash
    virtual long pushb (Buffer& obuf, Buffer& ibuf);
    
    /// @return the hash value as a relatif
    virtual Relatif gethval (void) const;

    /// @return the hash result as a relatif
    virtual Relatif getrval (void) const;

    /// derive a message hasher from an octet string
    /// @param s the string to process
    virtual String derive (const String& msg);

    /// compute a message hasher from a string
    /// @param msg the string message to process
    virtual String compute (const String& msg);

    /// compute a message hasher from a buffer
    /// @param buf the buffer to process
    virtual String compute (Buffer& buf); 

    /// compute a message hasher from an input stream
    /// @param is the input stream
    virtual String compute (InputStream& is); 

    /// check if a string is a valid hash
    /// @param s the string to check
    virtual bool ishash (const String& s) const;

    /// @return the formatted message hash
    virtual String format (void) const;

    /// process a message by data
    /// @param data the data to process
    /// @param size the data size
    virtual void process (const t_byte* data, const long size);
 
    /// process a message with a buffer
    /// @param buf the buffer to process
    virtual void process (Buffer& buf);

    /// process a message with an input stream
    /// @param is the input stream to process
    virtual void process (InputStream& is);

    /// finish processing by padding the data
    virtual void finish (void) =0;

  private:
    // make the copy constructor private
    Hasher (const Hasher&);
    // make the assignment operator private
    Hasher& operator = (const Hasher&);

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
