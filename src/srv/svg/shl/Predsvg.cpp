// ---------------------------------------------------------------------------
// - Predsvg.cpp                                                             -
// - afnix:svg service - predicates implementation                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "Boolean.hpp"
#include "Predsvg.hpp"
#include "Plot2d.hpp"
#include "SvgRoot.hpp"
#include "SvgRect.hpp"
#include "SvgLine.hpp"
#include "SvgGroup.hpp"
#include "SvgCircle.hpp"
#include "Transform.hpp"
#include "Exception.hpp"
#include "SvgEllipse.hpp"
#include "SvgPolygon.hpp"
#include "SvgPolyline.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // pltdp: plot2d object predicate

  Object* svg_pltdp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "plot2d-p");
    bool result = (dynamic_cast <Plot2d*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // trfrp: svg transform object predicate
  
  Object* svg_trfrp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "transform-p");
    bool result = (dynamic_cast <Transform*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rootp: svg root object predicate

  Object* svg_rootp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "svg-root-p");
    bool result = (dynamic_cast <SvgRoot*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // fragp: svg fragment object predicate

  Object* svg_fragp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "svg-fragment-p");
    bool result = (dynamic_cast <SvgFragment*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // stlgp: svg styling object predicate

  Object* svg_stlgp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "svg-styling-p");
    bool result = (dynamic_cast <SvgStyling*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // rectp: svg rectangle object predicate

  Object* svg_rectp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "svg-rect-p");
    bool result = (dynamic_cast <SvgRect*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // circp: svg circle object predicate

  Object* svg_circp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "svg-circle-p");
    bool result = (dynamic_cast <SvgCircle*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // elpsp: svg ellipse object predicate

  Object* svg_elpsp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "svg-ellipse-p");
    bool result = (dynamic_cast <SvgEllipse*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // linep: svg line object predicate

  Object* svg_linep (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "svg-line-p");
    bool result = (dynamic_cast <SvgLine*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // plinp: svg polyline object predicate

  Object* svg_plinp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "svg-polyline-p");
    bool result = (dynamic_cast <SvgPolyline*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // pgonp: svg polygon object predicate

  Object* svg_pgonp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "svg-polygon-p");
    bool result = (dynamic_cast <SvgPolygon*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // grpp: svg group object predicate

  Object* svg_grpp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "svg-group-p");
    bool result = (dynamic_cast <SvgGroup*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
}
