// ---------------------------------------------------------------------------
// - Pkcs.hpp                                                                -
// - afnix:tls service - public key cryptographic standard class definition  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PKCS_HPP
#define  AFNIX_PKCS_HPP

#ifndef  AFNIX_XKEY_HPP
#include "Xkey.hpp"
#endif

#ifndef  AFNIX_TLSFORMAT_HPP
#include "TlsFormat.hpp"
#endif

#ifndef  AFNIX_INPUTSTREAM_HPP
#include "InputStream.hpp"
#endif

namespace afnix {

  /// The Pkcs class is a base class for the public key cryptographic
  /// standard. The class defines the basic encoding format (DER/PEM)
  /// which is supported for reading or writing file.
  /// @author amaury darsch
  
  class Pkcs : public virtual Object {
  protected:
    /// the encoding format
    TlsFormat::t_efmt d_efmt;

  public:
    /// create a default pkcs
    Pkcs (void);

    /// create a pkcs by encoding
    /// @param efmt the encoding format
    Pkcs (const TlsFormat::t_efmt efmt);

    /// copy construct a pkcs object
    /// @param that the object to copy
    Pkcs (const Pkcs& that);

    /// assign a pkcs to this one
    /// @param that the object to copy
    Pkcs& operator = (const Pkcs& that);

    /// reset this pkcs object
    virtual void reset (void) =0;
    
    /// parse an object by stream
    virtual void parse (InputStream* is) =0;

    /// @return pkcs format
    virtual TlsFormat::t_efmt getefmt (void) const;
    
  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };

  /// The Pkcs1 is the rsa cryptography standard. Originally defined by
  /// RSA Security Inc, it is now a standard track referenced as RFC3447.
  /// @author amaury darsch

  class Pkcs1 : public Pkcs {
  private:
    /// the pkcs key
    Key* p_pkey;

  public:
    /// create a default pkcs key
    Pkcs1 (void);
    
    /// create a pkcs key by input stream
    /// @param is the input stream to read
    Pkcs1 (InputStream* is);

    /// create a pkcs key by path
    /// @param path the input file path
    Pkcs1 (const String& path);
    
    /// copy construct a pkcs key object
    /// @param that the object to copy
    Pkcs1 (const Pkcs1& that);

    /// destroy this pkcs key
    ~Pkcs1 (void);
    
    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// assign a pkcs key to this one
    /// @param that the object to copy
    Pkcs1& operator = (const Pkcs1& that);

    /// reset this pkcs object
    void reset (void);
    
    /// parse an object by stream
    void parse (InputStream* is);

    /// @return the pkcs key
    virtual Key* getkey (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
