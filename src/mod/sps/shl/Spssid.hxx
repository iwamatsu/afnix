// ---------------------------------------------------------------------------
// - Spssid.hxx                                                              -
// - afnix:sps module - serial id definition                                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SPSSID_HXX
#define  AFNIX_SPSSID_HXX

#ifndef  AFNIX_CCNF_HPP
#include "ccnf.hpp"
#endif

namespace afnix {
  static const t_byte SERIAL_LSTK_ID = 0x40; // lstack id
  static const t_byte SERIAL_CELL_ID = 0x41; // cell id
  static const t_byte SERIAL_RECD_ID = 0x42; // record id
  static const t_byte SERIAL_SHTT_ID = 0x43; // sheet id
}

#endif
