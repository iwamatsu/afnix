// ---------------------------------------------------------------------------
// - WorkSpace.cpp                                                           -
// - afnix:csm module - abstract workspace class implementation              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Boolean.hpp"
#include "WorkSpace.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // default workspace name, infor and flags
  static const String WSP_NAME_DEF = "";
  static const String WSP_INFO_DEF = "";
  static const String WSP_ZPUB_DEF = "public";
  static const bool   WSP_GTMP_DEF = false;

  // -------------------------------------------------------------------------
  // - public section                                                       -
  // -------------------------------------------------------------------------

  // the workspace uri scheme
  const String WorkSpace::URI_SCHM_DEF = "edws";

  // map a name to a zone uri
  
  String WorkSpace::tozuri (const String& name) {
    String result = WorkSpace::URI_SCHM_DEF + ":///" + name;
    return result;
  }

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default workspace

  WorkSpace::WorkSpace (void) {
    d_name = WSP_NAME_DEF;
    d_info = WSP_NAME_DEF;
    d_zpub = WSP_ZPUB_DEF;
    d_gtmp = WSP_GTMP_DEF;
  }

  // create a workspace by name and info

  WorkSpace::WorkSpace (const String& name, const String& info) {
    d_name = name;
    d_info = info;
    d_zpub = WSP_ZPUB_DEF;
    d_gtmp = WSP_GTMP_DEF;
  }

  // get the workspace name

  String WorkSpace::getname (void) const {
    rdlock ();
    try {
      String result = d_name;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the workspace name

  void WorkSpace::setname (const String& name) {
    wrlock ();
    try {
      d_name = name;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the workspace public zone

  String WorkSpace::getzpub (void) const {
    rdlock ();
    try {
      String result = d_zpub;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the workspace public zone name

  void WorkSpace::setzpub (const String& zone) {
    wrlock ();
    try {
      d_zpub = zone;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the workspace info

  String WorkSpace::getinfo (void) const {
    rdlock ();
    try {
      String result = d_info;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the workspace info

  void WorkSpace::setinfo (const String& info) {
    wrlock ();
    try {
      d_info = info;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the workspace global temporary

  void WorkSpace::setgtmp (const bool gtmp) {
    wrlock ();
    try {
      d_gtmp = gtmp;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the workspace global temporary

  bool WorkSpace::getgtmp (void) const {
    rdlock ();
    try {
      bool result = d_gtmp;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // check if an entity exists by zone and normalized uri

  bool WorkSpace::exists (const String& zone, const String& nuri) const {
    rdlock ();
    try {
      // map the string to uri
      Uri uri (nuri);
      // check by uri
      bool result = exists (zone, uri);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get an input stream by zone and normalized uri

  InputStream* WorkSpace::getis (const String& zone, const String& nuri) const {
    rdlock ();
    try {
      // map the string to uri
      Uri uri (nuri);
      // get the input stream
      InputStream* is = getis (zone, uri);
      unlock ();
      return is;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get an output stream by zone and normalized uri

  OutputStream* WorkSpace::getos (const String& zone, 
				  const String& nuri) const {
    rdlock ();
    try {
      // map the string to uri
      Uri uri (nuri);
      // get the input stream
      OutputStream* os = getos (zone, uri);
      unlock ();
      return os;
    } catch (...) {
      unlock ();
      throw;
    }
  }

 
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 16;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_ZONEP    = zone.intern ("zone-p");
  static const long QUARK_GETIS    = zone.intern ("get-input-stream");
  static const long QUARK_GETOS    = zone.intern ("get-output-stream");
  static const long QUARK_TOWURI   = zone.intern ("to-workspace-uri");
  static const long QUARK_TONURI   = zone.intern ("to-normalized-uri");
  static const long QUARK_EXISTSP  = zone.intern ("exists-p");
  static const long QUARK_ADDZONE  = zone.intern ("add-zone");
  static const long QUARK_SETNAME  = zone.intern ("set-name");
  static const long QUARK_GETINFO  = zone.intern ("get-info");
  static const long QUARK_SETINFO  = zone.intern ("set-info");
  static const long QUARK_GETZPUB  = zone.intern ("get-public-zone");
  static const long QUARK_SETZPUB  = zone.intern ("set-public-zone");
  static const long QUARK_SETGTMP  = zone.intern ("set-global-temporary");
  static const long QUARK_GETGTMP  = zone.intern ("get-global-temporary");
  static const long QUARK_TOFPTBL  = zone.intern ("to-files-table");
  static const long QUARK_GETFILES = zone.intern ("get-files");

  // return true if the given quark is defined

  bool WorkSpace::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? Nameable::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }

  // apply this object with a set of arguments and a quark
  
  Object* WorkSpace::apply (Runnable* robj, Nameset* nset, const long quark,
			    Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETINFO) return new String  (getinfo ());
      if (quark == QUARK_GETZPUB) return new String  (getzpub ());
      if (quark == QUARK_GETGTMP) return new Boolean (getgtmp ());
    }
    // check for 1 argument
    if (argc == 1) {
      if (quark == QUARK_SETNAME) {
	String name = argv->getstring (0);
	setname (name);
	return nilp;
      }
      if (quark == QUARK_SETINFO) {
	String info = argv->getstring (0);
	setinfo (info);
	return nilp;
      }
      if (quark == QUARK_SETZPUB) {
	String zone = argv->getstring (0);
	setzpub (zone);
	return nilp;
      }
      if (quark == QUARK_SETGTMP) {
	bool gtmp = argv->getbool (0);
	setgtmp (gtmp);
	return nilp;
      }
      if (quark == QUARK_ADDZONE) {
	String zone = argv->getstring (0);
	bool result = addzone (zone);
	return new Boolean (result);
      }
      if (quark == QUARK_ZONEP) {
	String zone = argv->getstring (0);
	return new Boolean (iszone (zone));
      }
      if (quark == QUARK_GETFILES) {
	String zone = argv->getstring (0);
	return getfiles (zone);
      }
      if (quark == QUARK_TOFPTBL) {
	String zone = argv->getstring (0);
	return tofptbl (zone);
      }
    }
    // check for 2 arguments
    if (argc == 2) {
      if (quark == QUARK_EXISTSP) {
	// get the zone
	String zone = argv->getstring (0);
	// check for a a uri
	Object* obj = argv->get (1);
	Uri*    uri = dynamic_cast <Uri*> (obj);
	if (uri != nilp) return new Boolean (exists (zone, *uri));
	// check for a string
	String* sobj = dynamic_cast <String*> (obj);
	if (sobj != nilp) return new Boolean (exists (zone, *sobj));
	// invalid object
	throw Exception ("type-error", "invalid object for workspace exists",
			 Object::repr (obj));
      }
      if (quark == QUARK_TOWURI) {
	String zone = argv->getstring (0);
	String name = argv->getstring (1);
	return new String (towuri (zone, name));
      }
      if (quark == QUARK_TONURI) {
	String zone = argv->getstring (0);
	String name = argv->getstring (1);
	return new String (tonuri (zone, name));
      }
      if (quark == QUARK_GETIS) {
	// get the zone
	String zone = argv->getstring (0);
	// check for a a uri
	Object* obj = argv->get (1);
	Uri*    uri = dynamic_cast <Uri*> (obj);
	if (uri != nilp) return getis (zone, *uri);
	// check for a string
	String* sobj = dynamic_cast <String*> (obj);
	if (sobj != nilp) return getis (zone, *sobj);
	// invalid object
	throw Exception ("type-error", 
			 "invalid object for workspace input stream",
			 Object::repr (obj));
      }
      if (quark == QUARK_GETOS) {
	// get the zone
	String zone = argv->getstring (0);
	// check for a a uri
	Object* obj = argv->get (1);
	Uri*    uri = dynamic_cast <Uri*> (obj);
	if (uri != nilp) return getos (zone, *uri);
	// check for a string
	String* sobj = dynamic_cast <String*> (obj);
	if (sobj != nilp) return getos (zone, *sobj);
	// invalid object
	throw Exception ("type-error", 
			 "invalid object for workspace output stream",
			 Object::repr (obj));
      }
    }
    // call the nameable method
    return Nameable::apply (robj, nset, quark, argv);
  }
}
