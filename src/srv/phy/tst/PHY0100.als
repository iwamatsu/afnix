# ---------------------------------------------------------------------------
# - PHY0100.als                                                             -
# - afnix:phy service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   physics functions test unit
# @author amaury darsch

# get the module
interp:library "afnix-phy"

# check the thermal voltage at 300K
const  vth  (afnix:phy:get-thermal-voltage 300.0)
assert true (vth:?= 0.02585)
