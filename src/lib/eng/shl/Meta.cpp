// ---------------------------------------------------------------------------
// - Meta.cpp                                                                -
// - afnix engine - meta class implementation                                -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Meta.hpp"
#include "Runnable.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create an empty meta class

  Meta::Meta (void) {
    p_eval = nilp;
    p_func = nilp;
  }

  // create a new meta class

  Meta::Meta (t_eval func) {
    p_eval = func;
    p_func = nilp;
  }

  // create a new meta class

  Meta::Meta (t_meta func) {
    p_eval = nilp;
    p_func = func;
  }

  // create a new meta class with an evaluator and a constructor

  Meta::Meta (t_eval feval, t_meta fmeta) {
    p_eval = feval;
    p_func = fmeta;
  }

  // return the class name

  String Meta::repr (void) const {
    return "Meta";
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // evaluate a quark for this meta object

  Object* Meta::eval (Runnable* robj, Nameset* nset, const long quark) {
    rdlock ();
    try {
      if (p_eval == nilp) {
	Object* result = Object::eval (robj, nset, quark);
	robj->post (result);
	unlock ();
	return result;
      }
      Object* result = p_eval (robj, nset, quark);
      robj->post (result);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // generate a new object
  
  Object* Meta::apply (Runnable* robj, Nameset* nset, Cons* args) {
    rdlock ();
    // check for a function to apply
    if (p_func == nilp) {
      unlock ();
      throw Exception ("apply-error", "trying to apply a nil meta class");
    }
    // initialize agument vector
    Vector* argv = nilp;
    try {
      argv = Vector::eval (robj,nset,args);
    } catch (...) {
      unlock ();
      throw;
    }
    try {      
      Object::iref (argv);
      Object* result = p_func (argv);
      Object::dref (argv);
      robj->post (result);
      unlock ();
      return result;
    } catch (...) {
      Object::dref (argv);
      unlock ();
      throw;
    }
  }
}
