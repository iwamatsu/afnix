// ---------------------------------------------------------------------------
// - TlsCkeyxh.cpp                                                           -
// - afnix:tls service - tls client key exchange class implementation        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Crypto.hpp"
#include "TlsCkeyxh.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // map a key type to a ciher type
  static Crypto::t_cipher tls_tocifr (const Key& key) {
    switch (key.gettype ()) {
    case Key::KRSA:
      return Crypto::RSA;
      break;
    default:
      break;
    }
    throw Exception ("tls-error", "cannot map exchange key to cipher");
  }
  
  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default key exchange

  TlsCkeyxh::TlsCkeyxh (void) {
    d_mlen = 0L;
    p_mbuf = nilp;
    reset ();
  }

  // create a key exchange by message block

  TlsCkeyxh::TlsCkeyxh (TlsHblock* hblk) {
    d_mlen = 0L;
    p_mbuf = nilp;
    decode (hblk, nilp);
  }
  
  // create a key exchange by message block and key

  TlsCkeyxh::TlsCkeyxh (TlsHblock* hblk, Key* hkey) {
    d_mlen = 0L;
    p_mbuf = nilp;
    decode (hblk, hkey);
  }

  // destroy this  block
  
  TlsCkeyxh::~TlsCkeyxh (void) {
    delete [] p_mbuf; p_mbuf = nilp;
    reset ();
  }

  // return the class name
  
  String TlsCkeyxh::repr (void) const {
    return "TlsCkeyxh";
  }

  // reset the key exchange

  void TlsCkeyxh::reset (void) {
    wrlock ();
    try {
      d_mlen = 0L;
      delete [] p_mbuf; p_mbuf = nilp;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the premaster buffer length

  long TlsCkeyxh::getmlen (void) const {
    rdlock ();
    try {
      long result = d_mlen;
      // here it is
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the premaster buffer - no copy

  const t_byte* TlsCkeyxh::getmbuf (void) const {
    rdlock ();
    try {
      const t_byte* result = p_mbuf;
      // here it is
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the key exchange info as a plist

  Plist TlsCkeyxh::getinfo (void) const {
    rdlock ();
    try {
      // create a result plist
      Plist plst;
      // here it is
      unlock ();
      return plst;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // decode the message block

  void TlsCkeyxh::decode (TlsHblock* hblk, Key* hkey) {
    wrlock ();
    try {
      // reset eveything
      reset ();
      if (hblk == nilp) {
	unlock ();
	return;
      }
      // the pre-master buffer
      Buffer mbuf;
      // check for a key
      if (hkey != nilp) {
	// create a decoding cipher by key
	Cipher* cifr = Crypto::mkcipher (tls_tocifr (*hkey), *hkey, true);
	if (cifr == nilp) {
	  throw Exception ("tls-error", "cannot create key exchange cipher");
	}
	// map the handshake to a buffer
	Buffer hbuf = hblk->tobuffer();
	// the first 2 bytes should be the block size
	t_byte hbyt = (t_byte) hbuf.read ();
	t_byte lbyt = (t_byte) hbuf.read ();
	long   hlen = (((t_word) hbyt) << 8) + ((t_word) lbyt);
	// check the current buffer size
	if (hbuf.length () != hlen) {
	  throw Exception ("tls-error", "inconsistent encrypted exchange size");
	}
	long blen = cifr->stream (mbuf, hbuf);
	if (blen != mbuf.length ()) {
	  throw Exception ("tls-error",
			   "internal error while decoding client key exchange");
	}
      } else {
	mbuf = hblk->tobuffer ();
      }
      // copy the pre-master secret
      d_mlen = mbuf.length ();
      delete [] p_mbuf; p_mbuf = (d_mlen > 0L) ? new t_byte[d_mlen] : nilp;
      if (mbuf.copy ((char*) p_mbuf, d_mlen) != d_mlen) {
	throw Exception ("tls-error", "cannot extract pre-master secret");
      }
      mbuf.reset ();
      // done
      unlock ();
    } catch (...) {
      reset ();
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // create a new object in a generic way

  Object* TlsCkeyxh::mknew (Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) return new TlsCkeyxh;
    // check for 1 argument
    if (argc == 1) {
      Object*     obj = argv->get (0);
      TlsHblock* hblk = dynamic_cast<TlsHblock*> (obj);
      if ((hblk == nilp) && (obj != nilp)) {
	throw Exception ("type-error", 
			 "invalid object as tls client key exchange block",
			 Object::repr (obj));
      }
      return new TlsCkeyxh (hblk);
    }
    // too many arguments
    throw Exception ("argument-error", 
                     "too many argument with tls client key exchange");
  }
}
