// ---------------------------------------------------------------------------
// - Plane.cpp                                                               -
// - afnix:geo service - solid plane with texture class implementation       -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Real.hpp"
#include "Solid.hxx"
#include "Plane.hpp"
#include "Vector.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the default plane parameters
  static const t_real GEO_XO_DEF   = 0.0;
  static const t_real GEO_YO_DEF   = 0.0;
  static const t_real GEO_ZO_DEF   = 0.0;
  static const t_real GEO_WS_DEF   = 0.0;
  static const t_real GEO_HS_DEF   = 0.0;
  
  // the geometry xml name
  static const String XML_TAG_NAME = GEO_PLN_NAME;
  static const String XML_TAG_INFO = GEO_PLN_INFO;
  // the plane origin attributes
  static const String XML_XO_ATTR  = "xo";
  static const String XML_YO_ATTR  = "yo";
  static const String XML_ZO_ATTR  = "zo";
  // the plane size attributes
  static const String XML_WS_ATTR  = "ws";
  static const String XML_HS_ATTR  = "hs";
  
  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a unity plane

  Plane::Plane (void) {
    reset ();
  }
  
  // create a plane by origin and size

  Plane::Plane (const Point3& o, const t_real w, const t_real h) {
    set (o, w, h); 
  }

  // copy construct this object

  Plane::Plane (const Plane& that) {
    that.rdlock ();
    try {
      Solid::operator = (that);
      d_o = that.d_o;
      d_w = that.d_w;
      d_h = that.d_h;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }  
  }

  // assign a plane to this one

  Plane& Plane::operator = (const Plane& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      Solid::operator = (that);
      d_o = that.d_o;
      d_w = that.d_w;
      d_h = that.d_h;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return the class name

  String Plane::repr (void) const {
    return "Plane";
  }

  // return a clone of this object

  Object* Plane::clone (void) const {
    return new Plane (*this);
  }

  // reset this plane

  void Plane::reset (void) {
    wrlock ();
    try {
      d_o = {GEO_XO_DEF, GEO_YO_DEF, GEO_ZO_DEF};
      d_w = GEO_WS_DEF;
      d_h = GEO_HS_DEF;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the plane name

  String Plane::getname (void) const {
    rdlock ();
    try {
      String result = GEO_PLN_NAME;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the plane by plist

  void Plane::setplst (const Plist& plst) {
    wrlock ();
    try {
      // set the solid by plist
      Solid::setplst (plst);
      // get he plane parameters
      t_real xo = plst.exists (XML_XO_ATTR) ?
	plst.toreal (XML_XO_ATTR) : GEO_XO_DEF;
      t_real yo = plst.exists (XML_YO_ATTR) ?
	plst.toreal (XML_YO_ATTR) : GEO_YO_DEF;
      t_real zo = plst.exists (XML_ZO_ATTR) ?
	plst.toreal (XML_ZO_ATTR) : GEO_ZO_DEF;
      t_real ws = plst.exists (XML_WS_ATTR) ?
	plst.toreal (XML_WS_ATTR) : GEO_WS_DEF;
      t_real hs = plst.exists (XML_HS_ATTR) ?
	plst.toreal (XML_HS_ATTR) : GEO_HS_DEF;
      // set the plane geometry
      d_o = {xo, yo, zo};
      d_w = ws;
      d_h = hs;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the plane plist

  Plist Plane::getplst (void) const {
    rdlock ();
    try {
      // the solid plist
      Plist result = Solid::getplst ();
      result.setname (XML_TAG_NAME); result.setinfo (XML_TAG_INFO);
      // add the geometry properties
      result.add (XML_XO_ATTR, d_o.getx ());
      result.add (XML_YO_ATTR, d_o.gety ());
      result.add (XML_ZO_ATTR, d_o.getz ());
      result.add (XML_WS_ATTR, d_w);
      result.add (XML_HS_ATTR, d_h);
      // here it is
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the plane origin

  Point3 Plane::getorigin (void) const {
    rdlock ();
    try {
      Point3 result = d_o;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the plane width

  t_real Plane::getw (void) const {
    rdlock ();
    try {
      t_real result = d_w;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the plane height

  t_real Plane::geth (void) const {
    rdlock ();
    try {
      t_real result = d_h;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the plane points

  void Plane::set (const Point3& o, const t_real w, const t_real h) {
    wrlock ();
    try {
      // set plane coordinates
      d_o = o;
      d_w = w;
      d_h = h;
      // normalize coordinates
      normalize ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // normalize a plane

  void Plane::normalize (void) {
    wrlock ();
    try {
      // get the point coordinates
      t_real xo = d_o.getx ();
      t_real yo = d_o.gety ();
      t_real zo = d_o.getz ();
      // readjust with position
      d_w = (d_w < 0.0) ? (xo += d_w),-d_w : d_w;
      d_h = (d_h < 0.0) ? (yo += d_h),-d_h : d_h;
      // reset origin
      d_o = {xo, yo, zo};
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 3;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_GETW = zone.intern ("get-width");
  static const long QUARK_GETH = zone.intern ("get-height");
  static const long QUARK_NMRL = zone.intern ("normalize");

  // create a new object in a generic way

  Object* Plane::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Plane;
    // invalid arguments
    throw Exception ("argument-error", 
                     "invalid arguments with with plane"); 
  }

  // return true if the given quark is defined
  
  bool Plane::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Solid::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark

  Object* Plane::apply (Runnable* robj, Nameset* nset, const long quark,
			Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETW) return new Real (getw ());
      if (quark == QUARK_GETH) return new Real (geth ());
      if (quark == QUARK_NMRL) {
	normalize ();
	return nilp;
      }
    }
    // call the solid method
    return Solid::apply (robj, nset, quark, argv);
  }
}
