// ---------------------------------------------------------------------------
// - NwgCalls.cpp                                                            -
// - afnix:nwg module - specific calls implementation                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Uri.hpp"
#include "Cons.hpp"
#include "Mime.hpp"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "NwgCalls.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // check if a mime extension is defined

  Object* nwg_mextp (Runnable* robj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    Vector* argv = Vector::eval (robj, nset, args);
    long    argc = (argv == nilp) ? 0 : argv->length ();
    try {
      if (argc == 1) {
	String mext = argv->getstring (0);
	delete argv; argv = nilp;
	return new Boolean (Mime::ismext (mext));
      }
      throw Exception ("argument-error", 
		       "too many arguments with mime-extension-p");
    } catch (...) {
      delete argv;
      throw;
    }
  }

  // check if a mime value is defined

  Object* nwg_mvalp (Runnable* robj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    Vector* argv = Vector::eval (robj, nset, args);
    long    argc = (argv == nilp) ? 0 : argv->length ();
    try {
      if (argc == 1) {
	String mval = argv->getstring (0);
	delete argv; argv = nilp;
	return new Boolean (Mime::ismval (mval));
      }
      throw Exception ("argument-error", 
		       "too many arguments with mime-value-p");
    } catch (...) {
      delete argv;
      throw;
    }
  }

  // check for a valid xml mime value

  Object* nwg_xmlmp (Runnable* robj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    Vector* argv = Vector::eval (robj, nset, args);
    long    argc = (argv == nilp) ? 0 : argv->length ();
    try {
      if (argc == 1) {
	String mval = argv->getstring (0);
	delete argv; argv = nilp;
	return new Boolean (Mime::isxmlm (mval));
      }
      throw Exception ("argument-error", 
		       "too many arguments with mime-xml-p");
    } catch (...) {
      delete argv;
      throw;
    }
  }

  // get a mime value by extension

  Object* nwg_tomime (Runnable* robj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    Vector* argv = Vector::eval (robj, nset, args);
    long    argc = (argv == nilp) ? 0 : argv->length ();
    try {
      if (argc == 1) {
	String mext = argv->getstring (0);
	delete argv; argv = nilp;
	return new String (Mime::tomime (mext, false));
      }
      if (argc == 2) {
	String mext = argv->getstring (0);
	bool   dflg = argv->getbool   (1);
	delete argv; argv = nilp;
	return new String (Mime::tomime (mext, dflg));
      }
      throw Exception ("argument-error", 
		       "too many arguments with extension-to-mime");
    } catch (...) {
      delete argv;
      throw;
    }
  }

  // check for a normal uri string

  Object* nwg_surip (Runnable* robj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    Vector* argv = Vector::eval (robj, nset, args);
    long    argc = (argv == nilp) ? 0 : argv->length ();
    try {
      if (argc == 1) {
	String suri = argv->getstring (0);
	delete argv; argv= nilp;
	return new Boolean (Uri::isuri (suri));
      }
      throw Exception ("argument-error", 
		       "too many arguments with string-uri-p");
    } catch (...) {
      delete argv;
      throw;
    }
  }

  // normalize a uri name by adding a scheme if any

  Object* nwg_nrmunm (Runnable* robj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    Vector* argv = Vector::eval (robj, nset, args);
    long    argc = (argv == nilp) ? 0 : argv->length ();
    try {
      if (argc == 1) {
	String name = argv->getstring (0);
	delete argv; argv = nilp;
	return new String (Uri::nrmname (name));
      }
      if (argc == 2) {
	String name = argv->getstring (0);
	bool   flag = argv->getbool   (1);
	delete argv; argv = nilp;
	Uri uri = Uri::nrmname (name);
	if (flag == true) uri.nrmauth ();
	return new String (uri.getanam ());
      }
      throw Exception ("argument-error", 
		       "too many arguments with normalize-uri-name");
    } catch (...) {
      delete argv;
      throw;
    }
  }

  // normalize a uri name by prioritizing the system path

  Object* nwg_sysunm (Runnable* robj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    Vector* argv = Vector::eval (robj, nset, args);
    long    argc = (argv == nilp) ? 0 : argv->length ();
    try {
      if (argc == 1) {
	String path = argv->getstring (0);
	delete argv; argv= nilp;
	return new String (Uri::sysname (path));
      }
      throw Exception ("argument-error", 
		       "too many arguments with system-uri-name");
    } catch (...) {
      delete argv;
      throw;
    }
  }

  // normalize a path by uri name

  Object* nwg_pthunm (Runnable* robj, Nameset* nset, Cons* args) {
    // evaluate the arguments
    Vector* argv = Vector::eval (robj, nset, args);
    long    argc = (argv == nilp) ? 0 : argv->length ();
    try {
      if (argc == 1) {
	String path = argv->getstring (0);
	delete argv; argv= nilp;
	return new String (Uri::pthname (path));
      }
      throw Exception ("argument-error", 
		       "too many arguments with path-uri-name");
    } catch (...) {
      delete argv;
      throw;
    }
  }
}
