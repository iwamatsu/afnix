// ---------------------------------------------------------------------------
// - TlsDupleix.cpp                                                          -
// - afnix:tls service - tls dupleix stream class implementation             -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Boolean.hpp"
#include "TlsAlert.hpp"
#include "Runnable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "TlsDupleix.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a tls dupleix by streams and state

  TlsDupleix::TlsDupleix (InputStream* is, OutputStream* os, TlsState* sta) {
    // save the state
    Object::iref (p_tlss = sta);
    // set the streams
    Object::iref (p_is = new TlsInput (is, sta));
    Object::iref (p_os = new TlsOutput (os, sta));
  }

  // destroy this tls dupleix

  TlsDupleix::~TlsDupleix (void) {
    reset ();
  }

  // return the class name
  
  String TlsDupleix::repr (void) const {
    return "TlsDupleix";
  }

  // reset this tls dupleix

  void TlsDupleix::reset (void) {
    wrlock ();
    try {
      Object::dref (p_tlss);
      Object::dref (p_is); p_os = nilp;
      Object::dref (p_os); p_os = nilp;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the tls dupleix info

  Plist TlsDupleix::getinfo (void) const {
    rdlock ();
    try {
      Plist result;
      if (p_tlss != nilp) result = p_tlss->getinfo();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // close this dupleix stream

  bool TlsDupleix::close (void) {
    wrlock ();
    try {
      Object::dref (p_is); p_is = nilp;
      Object::dref (p_os); p_os = nilp;
      unlock ();
      return true;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // return true if the eos flag is set
  
  bool TlsDupleix::iseos (void) const {
    wrlock ();
    try {
      if (d_sbuf.length () != 0) {
	unlock ();
	return false;
      }
      bool result = (p_is == nilp) ? true : p_is->iseos ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // check if we can read a character
  
  bool TlsDupleix::valid (void) const {
    wrlock ();
    try {
      // check local buffer
      if (d_sbuf.length () != 0) {
	unlock ();
	return true;
      }
      // check stream validity
      bool result = (p_is == nilp) ? false : p_is->valid ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // read one character from the dupleix

  char TlsDupleix::read (void) {
    wrlock ();
    try {
      // check valid stream
      if (p_is == nilp) {
	char result = eosc;
	unlock ();
	return result;
      }
      // check the pushback buffer first
      if (d_sbuf.empty () == false) {
	char result = d_sbuf.read ();
	unlock ();
	return result;
      }
      // read a character
      char result = p_is->read ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // write one character to the dupleix stream
  
  long TlsDupleix::write (const char value) {
    wrlock ();
    try {
      long result = (p_os == nilp) ? 0L : p_os->write (value);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // write a data buffer to the socket

  long TlsDupleix::write (const char* value) {
    // lock and write
    wrlock ();
    try {
      long result = (p_os == nilp) ? 0L : p_os->write (value);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
 
  // get the tls state

  TlsState* TlsDupleix::getstate (void) const {
    rdlock ();
    try {
      TlsState* result = p_tlss;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 1;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_GETSTATE = zone.intern ("get-state");
 
  // create a new object in a generic way

  Object* TlsDupleix::mknew (Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();

    // check for 3 arguments
    if (argc == 3) {
      Object* obj = argv->get (0);
      // check for an input stream
      InputStream* is = dynamic_cast<InputStream*> (obj);
      if (is == nilp) {
	throw Exception ("type-error", "invalid object as input stream",
			 Object::repr (obj));
      }
      // check for an output stream
      obj = argv->get (1);
      OutputStream* os = dynamic_cast<OutputStream*> (obj);
      if (os == nilp) {
	throw Exception ("type-error", "invalid object as output stream",
			 Object::repr (obj));
      }
      // check for a state
      obj = argv->get (2);
      TlsState* sta =  dynamic_cast<TlsState*> (obj);
      if (sta == nilp) {
	throw Exception ("type-error", "invalid object as tls state",
			 Object::repr (obj));
      }
      return new TlsDupleix (is, os, sta);
    }
    // too many arguments
    throw Exception ("argument-error", 
                     "too many argument with tls dupleix constructor");
  }

  // return true if the given quark is defined

  bool TlsDupleix::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? TlsInfos::isquark (quark, hflg) : false;
      if (result == false) {
	result = hflg ? DupleixStream::isquark (quark, hflg) : false;
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark
  
  Object* TlsDupleix::apply (Runnable* robj, Nameset* nset, const long quark,
			     Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETSTATE) {
	rdlock ();
	try {
	  TlsState* result = getstate ();
	  robj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // check the tls infos
    if (TlsInfos::isquark (quark, true) == true) {
      return TlsInfos::apply (robj, nset, quark, argv);
    }
    // fallback on the dupleix stream 
    return DupleixStream::apply (robj, nset, quark, argv);
  }
}
