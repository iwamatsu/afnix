// ---------------------------------------------------------------------------
// - t_cthr.cpp                                                              -
// - standard platform library - thread function tester                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "cthr.hpp"

// a simple global variable
static int status = 1;
// this simple function is run in a thread and check for its self
// equality

static void* thr_test (void* args) {
  using namespace afnix;

  // make sure we are not the master
  if (c_thrmaster () == true) {
    status = -1;
    return nilp;
  }
  // check for argument and execution
  if (args != 0) {
    status = -1;
    return nilp;
  }
  void* thr = c_thrself ();
  if (thr == 0) {
    status = -1;
    return nilp;
  }
  if (c_threqual (thr) == false) {
    status = -1;
    return nilp;
  }
  if (c_thrgetgid (thr) != 0) {
    status = -1;
    return nilp;
  }
  // set status and exit
  status = 0;
  return nilp;
}

int main (int, char**) {
  using namespace afnix;

  // with no thread - check that getting self still work
  void* thr = c_thrself ();
  if (thr != 0) return -1;

  // check for equal here
  if (c_threqual (thr) == false) return -1;

  // check for master
  if (c_thrmaster () == false) return -1;

  // start a new thread and eventually wait
  s_targ targ;
  targ.p_func = thr_test;
  thr = c_thrstart (targ);
  c_thrwait (thr);

  // test and destroy
  if (status != 0) return -1;
  if (c_thrgetres (thr) != 0) return -1;
  c_thrdestroy (thr);

  // here it is
  return 0;
}
