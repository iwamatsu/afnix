# ---------------------------------------------------------------------------
# - CSM0053.als                                                             -
# - afnix:csm service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   cart test unit
# @author amaury darsch

# get the service
interp:library "afnix-csm"

# create a nil cart
trans cart (afnix:csm:Cart)

# check predicate and representation
assert true   (afnix:csm:cart-p cart)
assert "Cart" (cart:repr)

# create an integer carrier agent
const ica (afnix:csm:Carrier 1)

# check for empty
assert true  (cart:empty-p)
assert 0     (cart:length)

# add the carrier
trans  kid   (cart:add-agent ica)
assert false (cart:empty-p)
assert 1     (cart:length)
assert true  (cart:exists-p kid)

# get the carrier by key id and check
trans  agt   (cart:get-agent kid)
assert 1     (agt:get-object)

# test the rid
cart:set-rid "RID"
assert true  (cart:rid-p "RID")
assert "RID" (cart:get-rid)
