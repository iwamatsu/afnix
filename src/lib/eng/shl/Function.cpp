// ---------------------------------------------------------------------------
// - Function.cpp                                                            -
// - afnix engine - function class implementation                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Runnable.hpp"
#include "Function.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default function

  Function::Function (void) {
    p_func = nilp;
  }

  // create a new function

  Function::Function (t_func func) {
    p_func = func;
  }

  // return the class name

  String Function::repr (void) const {
    return "Function";
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // apply this function

  Object* Function::apply (Runnable* robj, Nameset* nset, Cons* args) {
    Object* result = (p_func == nilp) ? nilp : p_func (robj, nset, args);
    robj->post (result);
    return result;
  }
}
