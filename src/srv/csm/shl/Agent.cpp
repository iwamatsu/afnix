// ---------------------------------------------------------------------------
// - Agent.cpp                                                               -
// - afnix:csm service - cloud agent class implementation                    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Agent.hpp"
#include "Csmsid.hxx"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "Runnable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // this procedure returns a new agent for deserialization
  static Serial* mksob (void) {
    return new Agent;
  }
  // register this agent serial id
  static const t_byte SERIAL_ID = Serial::setsid (SERIAL_AGNT_ID, mksob);

  // the agent request plist
  static const String PN_AGT_RID  = "PN-AGT-RID";
  static const String PI_AGT_RID  = "AGENT REGISTRATION ID";
  static const String PN_AGT_NAME = "PN-AGT-NAME";
  static const String PI_AGT_NAME = "AGENT NAME";
  static const String PN_AGT_INFO = "PN-AGT-INFO";
  static const String PI_AGT_INFO = "AGENT INFO";
  
  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a nil agent
  
  Agent::Agent (void) {
    d_kid = 0LL;
  }

  // create an agent by name

  Agent::Agent (const String& name) {
    d_kid  = 0LL;
    d_name = name;
  }

  // create an agent by name and info

  Agent::Agent (const String& name, const String& info) {
    d_kid  = 0LL;
    d_name = name;
    d_info = info;
  }

  // create an agent by rid, name and info

  Agent::Agent (const String& rid, const String& name, const String& info) {
    d_kid  = 0LL;
    d_rid  = rid;
    d_name = name;
    d_info = info;
  }

  // copy construct this agent

  Agent::Agent (const Agent& that) {
    that.rdlock ();
    try {
      d_kid  = that.d_kid;
      d_rid  = that.d_rid;
      d_name = that.d_name;
      d_info = that.d_info;
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
  
  // assign an agent to this one

  Agent& Agent::operator = (const Agent& that) {
    // check for self assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      d_kid  = that.d_kid;
      d_rid  = that.d_rid;
      d_name = that.d_name;
      d_info = that.d_info;
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }

  // return the agent class name

  String Agent::repr (void) const {
    return "Agent";
  }

  // return a clone of this object

  Object* Agent::clone (void) const {
    return new Agent (*this);
  }

  // return the agent serial code

  t_byte Agent::serialid (void) const {
    return SERIAL_ID;
  }

  // serialize this agent

  void Agent::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      csm_wrlong (d_kid, os);
      d_rid.wrstream  (os);
      d_name.wrstream (os);
      d_info.wrstream (os);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this agent

  void Agent::rdstream (InputStream& is) {
    wrlock ();
    try {
      d_kid = csm_rdlong (is);
      d_rid.rdstream  (is);
      d_name.rdstream (is);
      d_info.rdstream (is);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the registration name

  String Agent::getname (void) const {
    rdlock ();
    try {
      String result = d_name;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the agent info

  String Agent::getinfo (void) const {
    rdlock ();
    try {
      String result = d_info;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // validate a agent key id

  bool Agent::iskid (const t_long kid) const {
    rdlock ();
    try {
      bool result = (d_kid == kid);
      unlock ();
      return result;
    } catch  (...) {
      unlock ();
      throw;
    }
  }

  // set the key id

  void Agent::setkid (const t_long kid) {
    wrlock ();
    try {
      d_kid = kid;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the key id

  t_long Agent::getkid (void) const {
    rdlock ();
    try {
      t_long result = d_kid;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // validate a agent registration id

  bool Agent::isrid (const String& rid) const {
    rdlock ();
    try {
      bool result = (d_rid == rid);
      unlock ();
      return result;
    } catch  (...) {
      unlock ();
      throw;
    }
  }

  // set the registration id

  void Agent::setrid (const String& rid) {
    wrlock ();
    try {
      d_rid = rid;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the registration id

  String Agent::getrid (void) const {
    rdlock ();
    try {
      String result = d_rid;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // request an information list

  Plist Agent::getplst (void) const {
    Plist result;
    result.add (PN_AGT_RID,  PI_AGT_RID,  d_rid);
    result.add (PN_AGT_NAME, PI_AGT_NAME, d_name);
    result.add (PN_AGT_INFO, PI_AGT_INFO, d_info);
    return result;
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 7;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_ISKIDP  = zone.intern ("kid-p");
  static const long QUARK_ISRIDP  = zone.intern ("rid-p");
  static const long QUARK_SETRID  = zone.intern ("set-rid");
  static const long QUARK_GETKID  = zone.intern ("get-kid");
  static const long QUARK_SETKID  = zone.intern ("set-kid");
  static const long QUARK_GETRID  = zone.intern ("get-rid");
  static const long QUARK_GETINFO = zone.intern ("get-info");
  static const long QUARK_GETPLST = zone.intern ("get-plist");

  // create a new object in a generic way

  Object* Agent::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // create a default agent
    if (argc == 0) return new Agent;
    // check for 1 argument
    if (argc == 1) {
      String name = argv->getstring (0);
      return new Agent (name);
    }
    // check for 2 arguments
    if (argc == 2) {
      String name = argv->getstring (0);
      String info = argv->getstring (1);
      return new Agent (name, info);
    }
    // check for 3 arguments
    if (argc == 3) {
      String  rid = argv->getstring (0);
      String name = argv->getstring (1);
      String info = argv->getstring (2);
      return new Agent (rid, name, info);
    }
    throw Exception ("argument-error",
                     "too many argument with agent constructor");
  }

  // return true if the given quark is defined

  bool Agent::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Nameable::isquark (quark, hflg) : false;
      if (result == false) {
	result = hflg ? Serial::isquark (quark, hflg) : false;
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Agent::apply (Runnable* robj, Nameset* nset, const long quark,
			Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETKID)  return new Integer (getkid  ());
      if (quark == QUARK_GETRID)  return new String  (getrid  ());
      if (quark == QUARK_GETINFO) return new String  (getinfo ());
      if (quark == QUARK_GETPLST) return new Plist   (getplst ());
    }
    // check for 1 argument
    if (argc == 1) {
      if (quark == QUARK_ISKIDP) {
	t_long kid = argv->getlong (0);
	return new Boolean (iskid (kid));
      }
      if (quark == QUARK_ISRIDP) {
	String rid = argv->getstring (0);
	return new Boolean (isrid (rid));
      }
      if (quark == QUARK_SETKID) {
	t_long kid = argv->getlong (0);
	setkid (kid);
	return nilp;
      }
      if (quark == QUARK_SETRID) {
	String rid = argv->getstring (0);
	setrid (rid);
	return nilp;
      }
    }
    // check the nameable method
    if (Nameable::isquark (quark, true) == true) {
      return Nameable::apply (robj, nset, quark, argv);
    }
    // call the serial method
    return Serial::apply (robj, nset, quark, argv);
  }
}
