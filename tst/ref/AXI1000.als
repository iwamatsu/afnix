# ---------------------------------------------------------------------------
# - AXI1000.als                                                              -
# - afnix engine test module                                                -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   hash value comparision
# @author amaury darsch

# compute string hashid - 32 bit form
const hashid-32 (s) {
  const len (s:length)
  trans cnt 0
  trans val 0
  trans sht 17
  do {
    # cmpute hash for this character
    trans c (s:get cnt)
    trans i (c:to-integer)
    val:= (val:xor (i:shl sht))
    # adjust shift index
    if (< (sht:-= 7) 0) (sht:+= 24)
  } (< (cnt:++) len)
  eval val
}

# compute string hashid - 64 bit form
const hashid-64 (s) {
  const len (s:length)
  trans cnt 0
  trans val 0
  trans sht 47
  do {
    # cmpute hash for this character
    trans c (s:get cnt)
    trans i (c:to-integer)
    val:= (val:xor (i:shl sht))
    # adjust shift index
    if (< (sht:-= 7) 0) (sht:+= 56)
  } (< (cnt:++) len)
  eval val
}

# compute a string hashid
const hashid (s) {
  if (== interp:machine-size "32") (return (hashid-32 s))
  if (== interp:machine-size "64") (return (hashid-64 s))
  throw "test-error" "invalid machine size"
}

# test our favorite string
const hello "hello world"

assert (hello:hashid) (hashid hello)
