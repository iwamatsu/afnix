# ---------------------------------------------------------------------------
# - CSM0006.als                                                             -
# - afnix:csm service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   localspace test unit
# @author amaury darsch

# get the module
interp:library "afnix-csm"

# create a default localspace
const lspc (afnix:csm:LocalSpace)
assert true (afnix:csm:workspace-p  lspc)
assert true (afnix:csm:localspace-p lspc)
# check the name
assert "LocalSpace" (lspc:repr)

# create a new zone
const zone "afnix-test-zone"
lspc:add-zone zone
# get the zone content
trans zcnt (lspc:get-files zone)
assert 0 (zcnt:length)

# add a new entry
trans name "afnix-text-file"
trans zuri "edws:///afnix-text-file"
trans nuri "file:///tmp/.local-space-tmp/afnix-test-zone/afnix-text-file"

trans os (lspc:get-output-stream zone zuri)
os:write "hello"
os:close

# check existence
assert true (lspc:exists-p zone zuri)

# get the zone content
trans zcnt  (lspc:get-files zone)
assert 1    (zcnt:length)
assert name (zcnt:get 0)

# check normalized uri
assert nuri (lspc:to-normalized-uri zone name)
