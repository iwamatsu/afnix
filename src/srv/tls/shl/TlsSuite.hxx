// ---------------------------------------------------------------------------
// - TlsSuite.hxx                                                            -
// - afnix:tls service - tls cipher suite internals                          -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSSUITE_HXX
#define  AFNIX_TLSSUITE_HXX

#ifndef  AFNIX_TLSSUITE_HPP
#include "TlsSuite.hpp"
#endif

namespace afnix {
  // rfc::2246 cipher suite
  static const t_word TLS_NULL_WITH_NULL_NULL      = 0x0000U;
  static const t_word TLS_RSA_WITH_NULL_MD5        = 0x0001U;
  static const t_word TLS_RSA_WITH_NULL_SHA        = 0x0002U;
  static const t_word TLS_RSA_WITH_RC4_128_MD5     = 0x0004U;
  static const t_word TLS_RSA_WITH_RC4_128_SHA     = 0x0005U;
  // rfc::3268 cipher suite
  static const t_word TLS_RSA_WITH_AES_128_CBC_SHA = 0x002FU;
  static const t_word TLS_RSA_WITH_AES_256_CBC_SHA = 0x0035U;
}

#endif
