// ---------------------------------------------------------------------------
// - Libnwg.cpp                                                              -
// - afnix:nwg module - declaration & implementation                         -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Meta.hpp"
#include "Libnwg.hpp"
#include "Prednwg.hpp"
#include "UriPath.hpp"
#include "UriQuery.hpp"
#include "NwgCalls.hpp"
#include "Function.hpp"
#include "HttpStream.hpp"
#include "HttpRequest.hpp"
#include "HttpResponse.hpp"

namespace afnix {

  // initialize the afnix:nwg module

  Object* init_afnix_nwg (Interp* interp, Vector* argv) {
    // make sure we are not called from something crazy
    if (interp == nilp) return nilp;

    // create the afnix:nwg nameset
    Nameset* aset = interp->mknset ("afnix");
    Nameset* gset = aset->mknset   ("nwg");

    // bind all symbols in the afnix:nwg nameset
    gset->symcst ("Uri",                new Meta (Uri::mknew));
    gset->symcst ("Cookie",             new Meta (Cookie::mknew));
    gset->symcst ("UriPath",            new Meta (UriPath::mknew));
    gset->symcst ("UriQuery",           new Meta (UriQuery::mknew));
    gset->symcst ("CookieJar",          new Meta (CookieJar::mknew));
    gset->symcst ("HttpStream",         new Meta (HttpStream::mknew));
    gset->symcst ("HttpRequest",        new Meta (HttpRequest::mknew));
    gset->symcst ("HttpResponse",       new Meta (HttpResponse::mknew));

    // bind the predicates
    gset->symcst ("uri-p",              new Function (nwg_urip));
    gset->symcst ("mime-p",             new Function (nwg_mimep));
    gset->symcst ("cookie-p",           new Function (nwg_cookp));
    gset->symcst ("uri-path-p",         new Function (nwg_uripp));
    gset->symcst ("uri-query-p",        new Function (nwg_uriqp));
    gset->symcst ("cookie-jar-p",       new Function (nwg_cjarp));
    gset->symcst ("mime-xml-p",         new Function (nwg_xmlmp));
    gset->symcst ("mime-value-p",       new Function (nwg_mvalp));
    gset->symcst ("http-proto-p",       new Function (nwg_protop));
    gset->symcst ("http-stream-p",      new Function (nwg_hstrmp));
    gset->symcst ("http-request-p",     new Function (nwg_hrqstp));
    gset->symcst ("http-response-p",    new Function (nwg_hrespp));
    gset->symcst ("mime-extension-p",   new Function (nwg_mextp));
    
    // bind other functions
    gset->symcst ("path-uri-name",      new Function (nwg_pthunm));
    gset->symcst ("string-uri-p",       new Function (nwg_surip));
    gset->symcst ("system-uri-name",    new Function (nwg_sysunm));
    gset->symcst ("extension-to-mime",  new Function (nwg_tomime));
    gset->symcst ("normalize-uri-name", new Function (nwg_nrmunm));

    // not used but needed
    return nilp;
  }
}

extern "C" {
  afnix::Object* dli_afnix_nwg (afnix::Interp* interp, afnix::Vector* argv) {
    return init_afnix_nwg (interp, argv);
  }
}
