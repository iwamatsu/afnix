// ---------------------------------------------------------------------------
// - Saas.cpp                                                                -
// - afnix:csm service - software a service class implementation             -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Uri.hpp"
#include "Saas.hpp"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "Integer.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {
  
  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the default host name
  static const String SAAS_HOST_XDEF = "localhost";
  // the default host port
  static const long   SAAS_PORT_XDEF = 0L;
  // the default host protocol
  static const String SAAS_PRTO_XDEF = "tcp";

  // the host property name
  static const String SAAS_HOST_NAME = "SAAS-HOST-NAME";
  static const String SAAS_HOST_INFO = "SAAS HOST NAME";
  // the port property name
  static const String SAAS_PORT_NAME = "SAAS-HOST-PORT";
  static const String SAAS_PORT_INFO = "SAAS HOST PORT VALUE";
  // the host property name
  static const String SAAS_PRTO_NAME = "SAAS-HOST-PROTOCOL";
  static const String SAAS_PRTO_INFO = "SAAS HOST PROTOCOL";


  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------
  
  // create a service by name

  Saas::Saas (const String& name) : Raas (name) {
    Plist::add (SAAS_HOST_NAME, SAAS_HOST_INFO, SAAS_HOST_XDEF);
    Plist::add (SAAS_PORT_NAME, SAAS_PORT_INFO, (t_long) SAAS_PORT_XDEF);
    Plist::add (SAAS_PRTO_NAME, SAAS_PRTO_INFO, SAAS_PRTO_XDEF);
  }

  // create a service by name and info

  Saas::Saas (const String& name, const String& info) : Raas (name, info) {
    Plist::add (SAAS_HOST_NAME, SAAS_HOST_INFO, SAAS_HOST_XDEF);
    Plist::add (SAAS_PORT_NAME, SAAS_PORT_INFO, (t_long) SAAS_PORT_XDEF);
    Plist::add (SAAS_PRTO_NAME, SAAS_PRTO_INFO, SAAS_PRTO_XDEF);
  }

  // create a service by plist

  Saas::Saas (const Plist& plst) : Raas (plst) {
    if (plst.exists (SAAS_HOST_NAME) == false) sethost (SAAS_HOST_XDEF);
    if (plst.exists (SAAS_PORT_NAME) == false) setport (SAAS_PORT_XDEF);
    if (plst.exists (SAAS_PRTO_NAME) == false) setprto (SAAS_PRTO_XDEF);
  }

  // copy construct this account

  Saas::Saas (const Saas& that) {
    that.rdlock ();
    try {
      // copy the base service
      Raas::operator = (that);
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }

  // assign a service to this one

  Saas& Saas::operator = (const Saas& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      // copy the base service
      Raas::operator = (that);
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }

  // get the class name

  String Saas::repr (void) const {
    return "Saas";
  }

  // return a clone of this object

  Object* Saas::clone (void) const {
    return new Saas (*this);
  }

  // set the service host name

  void Saas::sethost (const String& host) {
    wrlock ();
    try {
      if (Plist::exists (SAAS_HOST_NAME) == true) {
	Plist::set (SAAS_HOST_NAME, host);
      } else {
	Plist::add (SAAS_HOST_NAME, SAAS_HOST_INFO, host);
      }	
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the service host name

  String Saas::gethost (void) const {
    rdlock ();
    try {
      String result = Plist::getpval (SAAS_HOST_NAME);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the service port

  void Saas::setport (const long port) {
    wrlock ();
    try {
      if (Plist::exists (SAAS_PORT_NAME) == true) {
	Plist::set (SAAS_PORT_NAME, (t_long) port);
      } else {
	Plist::add (SAAS_PORT_NAME, SAAS_PORT_INFO, (t_long) port);
      }	
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the service port

  long Saas::getport (void) const {
    rdlock ();
    try {
      long result = Plist::tolong (SAAS_PORT_NAME);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the service protocol

  void Saas::setprto (const String& prto) {
    wrlock ();
    try {
      if (Plist::exists (SAAS_PRTO_NAME) == true) {
	Plist::set (SAAS_PRTO_NAME, prto);
      } else {
	Plist::add (SAAS_PRTO_NAME, SAAS_PRTO_INFO, prto);
      }	
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the service protocol

  String Saas::getprto (void) const {
    rdlock ();
    try {
      String result = Plist::getpval (SAAS_PRTO_NAME);;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // check if a service has the same authority

  bool Saas::isauth (const Saas& xas) const {
    rdlock ();
    try {
      bool result = ((gethost () == xas.gethost ()) && 
		     (getport () == xas.getport ()) &&
		     (getprto () == xas.getprto ()));
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------
  
  // the quark zone
  static const long QUARK_ZONE_LENGTH = 7;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_SETHOST = zone.intern ("set-host");
  static const long QUARK_GETHOST = zone.intern ("get-host");
  static const long QUARK_SETPORT = zone.intern ("set-port");
  static const long QUARK_GETPORT = zone.intern ("get-port");
  static const long QUARK_SETPRTO = zone.intern ("set-protocol");
  static const long QUARK_GETPRTO = zone.intern ("get-protocol");
  static const long QUARK_ISAUTHP = zone.intern ("authority-p");

  // create a new object in a generic way

  Object* Saas::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();

    // check for 1 argument
    if (argc == 1) {
      Object* obj = argv->get (0);
      // check for a string
      String* sobj = dynamic_cast <String*> (obj);
      if (sobj != nilp) return new Saas (*sobj);
      // check for a plist
      Plist* pobj = dynamic_cast <Plist*> (obj);
      if (pobj != nilp) return new Saas (*pobj);
      // invalid argument
      throw Exception ("type-error", "invalid object with saas constructor",
		       Object::repr (obj));
    }
    // check for 2 arguments
    if (argc == 2) {
      String name = argv->getstring (0);
      String info = argv->getstring (1);
      return new Saas (name, info);
    }
    // invalid arguments
    throw Exception ("argument-error", 
                     "invalid arguments with saas constructor");
  }

  // return true if the given quark is defined

  bool Saas::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? Raas::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Saas::apply (Runnable* robj, Nameset* nset, const long quark,
		       Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETHOST) return new String  (gethost ());
      if (quark == QUARK_GETPORT) return new Integer (getport ());
      if (quark == QUARK_GETPRTO) return new String  (getprto ());
    }
    if (argc == 1) {
      if (quark == QUARK_SETHOST) {
        String host = argv->getstring (0);
        sethost (host);
        return nilp;
      }
      if (quark == QUARK_SETPORT) {
        long port = argv->getlong (0);
        setport (port);
        return nilp;
      }
      if (quark == QUARK_SETPRTO) {
        String prto = argv->getstring (0);
        setprto (prto);
        return nilp;
      }
      if (quark == QUARK_ISAUTHP) {
	Object* obj = argv->get (0);
	Saas*   xas = dynamic_cast <Saas*> (obj);
	if (xas == nilp) {
	  throw Exception ("type-error", "invalid object for authority-p",
			   Object::repr (obj));
	}
	return new Boolean (isauth (*xas));
      }		   
    }
    // call the raas methods
    return Raas::apply (robj, nset, quark, argv);
  }
}
