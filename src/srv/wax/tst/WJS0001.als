# ---------------------------------------------------------------------------
# - WJS0001.als                                                             -
# - afnix:wax service test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   wax json writer test unit
# @author amaury darsch

# get the module
interp:library "afnix-mth"
interp:library "afnix-sio"
interp:library "afnix-wax"

# create a test sample array
const data (afnix:mth:Rsamples 2)
data:set-name "WJS0001.json"
data:set-info "json mime tester "
data:set-number-precision 5
trans row  (data:new-row 0.0)
data:set row 0 0.0
data:set row 1 0.0
trans row  (data:new-row 0.1)
data:set row 0 1.0
data:set row 1 1.1

# create a json mime
const  json (afnix:wax:JsonMime data)
assert true (afnix:wax:json-mime-p json)
assert "JsonMime" (json:repr)
assert "application/json" (json:get-mime)

# create an output string
const os  (afnix:sio:OutputString)
const buf (Buffer)

# write the data and compare
json:write os
json:write buf
assert (os:to-string) (buf:to-string)

