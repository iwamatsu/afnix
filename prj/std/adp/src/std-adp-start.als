# ----------------------------------------------------------------------------
# - std-adp-start                                                            -
# - afnix:adp starter module                                                 -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - project load section                                                     -
# ----------------------------------------------------------------------------

interp:load "std-adp-prjld"
interp:load "std-adp-postl"

# ----------------------------------------------------------------------------
# - main section                                                             -
# ----------------------------------------------------------------------------

# check for parse mode
if (afnix:adp:options:get-unique-option 'p') {
  try {
    # create a new context
    const ctx (afnix:adp:ctxdb)
    # get the uri name from the arguments
    const uri (afnix:adp:options:get-uri-name)
    # process source content by uri
    ctx:process-source-content uri
  } {
    errorln "error: " what:about
    afnix:sys:exit 1
  }
}

# check for xhtml mode
if (afnix:adp:options:get-unique-option 'x') {
  try {
    # load the xhtml module writer
    interp:load "std-adp-xhtmw"
    # create a new context
    const ctx (afnix:adp:xhtwr)
    # get the uri name from the arguments
    const uri (afnix:adp:options:get-uri-name)
    # process the uri
    ctx:process uri
  } {
    errorln "error: " what:about
    afnix:sys:exit 1
  }
}

# check for nroff manual mode
if (afnix:adp:options:get-unique-option 'm') {
  try {
    # load the nroff module writer
    interp:load "std-adp-manmw"
    # create a new context
    const ctx (afnix:adp:manwr)
    # get the uri name from the arguments
    const uri (afnix:adp:options:get-uri-name)
    # process the uri
    ctx:process uri
  } {
    errorln "error: " what:about
    afnix:sys:exit 1
  }
}

# check for latex mode
if (afnix:adp:options:get-unique-option 'l') {
  try {
    # load the tex module writer
    interp:load "std-adp-texmw"
    # create a new context
    const ctx (afnix:adp:texwr)
    # get the uri name from the arguments
    const uri (afnix:adp:options:get-uri-name)
    # process the uri
    ctx:process uri
  } {
    errorln "error: " what:about
    afnix:sys:exit 1
  }
}

# unload the project
interp:load "std-adp-punld"
