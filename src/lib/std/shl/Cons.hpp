// ---------------------------------------------------------------------------
// - Cons.hpp                                                                -
// - standard object library - cons cell class definition                    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CONS_HPP
#define  AFNIX_CONS_HPP

#ifndef  AFNIX_SERIAL_HPP
#include "Serial.hpp"
#endif

#ifndef  AFNIX_MONITOR_HPP
#include "Monitor.hpp"
#endif

#ifndef  AFNIX_ITERABLE_HPP
#include "Iterable.hpp"
#endif

#ifndef  AFNIX_COLLECTABLE_HPP
#include "Collectable.hpp"
#endif

namespace afnix {

  /// The Cons class is the class used to create generic forms. A cons
  /// cell consists of a car and a cdr. The car holds an object while the cdr
  /// is the link to the next cell. When a cons cell is destroyed, the car and
  /// the cdr are destroyed as well. A cons cell is either a normal cell or
  /// a block cell. In the case of block cell, the car hold the cons cell 
  /// which is a list of forms.
  /// @author amaury darsch

  class Cons : public Iterable, public Collectable, public Serial {
  public:
    /// the cons cell type
    enum t_type {
      NORMAL = 0x00, // normal form
      BLOCK  = 0x01  // block  form
    };
    
  private:
    /// the confine pointer
    void*   p_cptr;
    /// the cons cell type
    t_type  d_type;
    /// the car holds the object
    Object* p_car;
    /// the cdr is always a cons
    Cons*   p_cdr;
    /// the synchronize monitor
    Monitor* p_mon;
    /// the breakpoint flag
    bool    d_bpt;

  public:
    /// create a new cons cell with a null car and cdr
    Cons (void);

    /// create a new cons cell with a type
    /// @param type the cons cell type
    Cons (t_type type);

    /// create a cons cell with a car
    /// @param car the car of this cons cell
    Cons (Object* car);

    /// create a cons cell with a type and a car
    /// @param type the cons cell type
    /// @param car the car of this cons cell
    Cons (t_type type, Object* car);

    /// copy constructor for this cons cell
    /// @param that the cons to copy
    Cons (const Cons& that);

    /// destroy this cons cell
    ~Cons (void);

    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// @return the cons cell serial code
    t_byte serialid (void) const;

    /// serialize this cons cell to an output stream
    /// @param os the output stream to write
    void wrstream (class OutputStream& os) const;

    /// deserialize a cons cell from an input stream
    /// @param is the input steam to read in
    void rdstream (class InputStream& is);

    /// release this cons cell
    void release (void);

    /// assign a cons cell to another one
    /// @param that the cons cell to assign
    Cons& operator = (const Cons& that);

    /// add an object to the last cdr of this cons with a new cons cell
    /// @param object the object to add
    void add (Object* object);

    /// set the car of this cons cell
    /// @param car the car to set
    void setcar (Object* object);

    /// set the cdr of this cons cell
    /// @param cdr the cdr to set
    void setcdr (Cons* cdr);
  
    /// @return the car of this cons cell or nil.
    Object* getcar (void) const;

    /// @return the cdr of this cons cell or nil
    Cons* getcdr (void) const;

    /// @return the car of the cdr or nil
    Object* getcadr (void) const;

    /// @return the car of the cdr of the cdr or nil
    Object* getcaddr (void) const;

    /// @return the car of the cdr of the cdr of the cdr or nil
    Object* getcadddr (void) const;

    /// @return the length of this cons cell
    long length (void) const;

    /// @return true if the cdr of this cons cell is nil
    bool isnil (void) const;

    /// @return true if the cons cell is of type block
    bool isblock (void) const;

    /// @return an object by index
    Object* get (const long index) const;

    /// install the form synchronizer
    void mksync (void);

    /// set the cons cell breakpoint
    /// @param bpt the breakpoint to set
    void setbpt (const bool bpt);

    /// @return a new iterator for this cons
    Iterator* makeit (void);

  private:
    // make the cons iterator a friend
    friend class Consit;

  public:
    /// evaluate a cons cell. Each car is evaluated and placed in the 
    /// resulting cell with the whole form returned
    /// @param robj the current runnable
    /// @param nset the current nameset
    /// @param cons the cons cell to evaluate    
    static Cons* eval (Runnable* robj, Nameset* nset, Cons* cons);

    /// evaluate a cons cell and return a new cons cell or the object
    /// @param robj   the current runnable
    /// @param nset   the current nameset
    /// @param object the object to evaluate
    static Object* mkobj (Runnable* robj, Nameset* nset, Object* object);

    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// set an object to the car of this cons cell
    /// @param robj   the current runnable
    /// @param nset   the current nameset
    /// @param object the object to set
    Object* vdef (Runnable* robj, Nameset* nset, Object* object);

    /// evaluate this object within the calling nameset
    /// @param robj   the current runnable
    /// @param nset   the current nameset 
    Object* eval (Runnable* robj, Nameset* nset);

    /// apply this object with a set of arguments and a quark
    /// @param robj   robj the current runnable
    /// @param nset   the current nameset    
    /// @param quark  the quark to apply these arguments
    /// @param argv   the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
  
  /// The Consit class is the iterator for the cons class. Such iterator
  /// is constructed with the "makeit" cons method. The iterator is reset to
  /// the beginning of the cons cell.
  /// @author amaury darsch

  class Consit : public Iterator {
  private:
    /// the cons cell to iterate
    Cons* p_cons;
    /// the current cons cell
    Cons* p_cell;

  public:
    /// create a new iterator from a cons
    /// @param cons the list to iterate
    Consit (Cons* cons);

    /// destroy this cons iterator
    ~Consit (void);

    /// @return the class name
    String repr (void) const;

    /// reset the iterator to the begining
    void begin (void);

    /// reset the iterator to the end
    void end (void);

    /// move the iterator to the next position
    void next (void);
 
    /// move the iterator to the previous position
    void prev (void);

    /// @return the object at the current position
    Object* getobj (void) const;

    /// @return true if the iterator is at the end
    bool isend (void) const;

  private:
    // make the copy constructor private
    Consit (const Consit&);
    // make the assignment operator private
    Consit& operator = (const Consit&);
  };
}

#endif
