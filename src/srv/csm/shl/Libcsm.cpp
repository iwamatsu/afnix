// ---------------------------------------------------------------------------
// - Libcsm.cpp                                                              -
// - afnix:csm service - declaration & implementation                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Saas.hpp"
#include "Meta.hpp"
#include "Libcsm.hpp"
#include "CartSet.hpp"
#include "Predcsm.hpp"
#include "CsmCalls.hpp"
#include "Function.hpp"
#include "Delegate.hpp"
#include "Assistant.hpp"
#include "SessionSet.hpp"
#include "LocalSpace.hpp"

namespace afnix {

  // initialize the afnix:csm service

  Object* init_afnix_csm (Interp* interp, Vector* argv) {
    // make sure we are not called from something crazy
    if (interp == nilp) return nilp;

    // create the afnix:csm nameset
    Nameset* aset = interp->mknset ("afnix");
    Nameset* gset = aset->mknset   ("csm");

    // bind all symbols in the afnix:csm nameset
    gset->symcst ("Slot",             new Meta (Slot::mknew));
    gset->symcst ("Saas",             new Meta (Saas::mknew));
    gset->symcst ("Cart",             new Meta (Cart::mknew));
    gset->symcst ("Agent",            new Meta (Agent::mknew));
    gset->symcst ("Carrier",          new Meta (Carrier::mknew));
    gset->symcst ("CartSet",          new Meta (CartSet::mknew));
    gset->symcst ("Session",          new Meta (Session::mknew));
    gset->symcst ("Delegate",         new Meta (Delegate::mknew));
    gset->symcst ("Appointer",        new Meta (Appointer::mknew));
    gset->symcst ("Assistant",        new Meta (Assistant::mknew));
    gset->symcst ("SessionSet",       new Meta (SessionSet::mknew));
    gset->symcst ("LocalSpace",       new Meta (LocalSpace::mknew));

    // bind the predicates
    gset->symcst ("slot-p",           new Function (csm_slotp));
    gset->symcst ("raas-p",           new Function (csm_raasp));
    gset->symcst ("saas-p",           new Function (csm_saasp));
    gset->symcst ("cart-p",           new Function (csm_cartp));
    gset->symcst ("agent-p",          new Function (csm_agtp));
    gset->symcst ("carrier-p",        new Function (csm_cagtp));
    gset->symcst ("session-p",        new Function (csm_sessp));
    gset->symcst ("cart-set-p",       new Function (csm_csetp));
    gset->symcst ("delegate-p",       new Function (csm_dagtp));
    gset->symcst ("appointer-p",      new Function (csm_apptp));
    gset->symcst ("workspace-p",      new Function (csm_wspcp));
    gset->symcst ("assistant-p",      new Function (csm_asstp));
    gset->symcst ("localspace-p",     new Function (csm_lspcp));
    gset->symcst ("session-set-p",    new Function (csm_ssetp));

    // bind other functions
    gset->symcst ("to-workspace-uri", new Function (csm_tozuri));

    // not used but needed
    return nilp;
  }
}

extern "C" {
  afnix::Object* dli_afnix_csm (afnix::Interp* interp, afnix::Vector* argv) {
    return init_afnix_csm (interp, argv);
  }
}
