// ---------------------------------------------------------------------------
// - Serial.hpp                                                              -
// - standard object library - serializable object abstract class definition -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SERIAL_HPP
#define  AFNIX_SERIAL_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif

namespace afnix {

  /// The Serial class is an abstract class that defines the object 
  /// interface for serialization. An object serialization is performed 
  /// with the "wrstream" virtual method. The deserialization is performed
  /// with the "rdstream" virtual method.
  /// @author amaury darsch

  class Serial : public virtual Object {
  public:
    /// The Block class is a block serialization class that
    /// abstract the processus of serializing a fixed block of data
    class Block {
    public:
      /// the block enumeration
      enum t_btyp {
	BYTE, // byte block
	BOOL, // bool block
	LONG, // long block
	REAL, // real block
	RPT2  // real point 2D
      };

    private:
      /// the block type
      t_btyp d_btyp;
      /// the block data size
      long   d_size;
      /// the single data size
      long   d_dsiz;
      /// the block byte size
      long   d_bsiz;
      /// the block length
      long   d_blen;
      /// the block array
      t_byte* p_byte;

    public:
      /// create a default empty block
      Block (void);

      /// create an empty block by id
      /// @param sid the serial id
      Block (const t_byte sid);

      /// create a serial block by size and type
      /// @param size the block size
      /// @param btyp the block type
      Block (const long size, const t_btyp btyp);

      /// copy construct this block
      /// @param that the block to copy
      Block (const Block& that);

      /// destroy this serial block
      ~Block (void);

      /// assign a block to this one
      /// @param that the block to assign
      Block& operator = (const Block& that);

      /// clear the serial block
      void clear (void);

      /// @return true if the block is empty
      bool empty (void) const;

      /// @return true if the block is full
      bool full (void) const;

      /// @return the block length
      long length (void) const;

      /// add a byte to the block
      /// @param bval  the byte value
      void add (const t_byte bval);

      /// get a byte value by index
      /// @param index the block index
      t_byte getbyte (const long index) const;

      /// add a boolean to the block
      /// @param bval  the boolean value
      void add (const bool lval);

      /// get a boolean value by index
      /// @param index the block index
      bool getbool (const long index) const;

      /// add an integer to the block
      /// @param lval  the integer value
      void add (const t_long lval);

      /// get an integer value by index
      /// @param index the block index
      t_long getlong (const long index) const;

      /// add a real to the block
      /// @param rval  the real value
      void add (const t_real rval);

      /// add a real point by coordinates
      /// @param xval the x coordinate
      /// @param yval the y coordinate
      /// @param rval the real value
      void add (const t_long xval, const t_long yval, const t_real rval);

      /// get a real value by index
      /// @param index the block index
      t_real getreal (const long index) const;

      /// @return the block serial code
      t_byte serialid (void) const;

      /// serialize this block
      /// @param os the output stream to write
      void wrstream (class OutputStream& os) const;

      // deserialize this block
      /// @param ss the input stream to read
      void rdstream (class InputStream& is);
    };

  public:
    /// the default serial constructor prototype
    using t_genser = Serial* (*) (void);

    /// register a deserialize callback
    static t_byte setsid (const t_byte sid, t_genser cbk);

    /// @return a serial object by serial id
    static Serial* getserial (const t_byte sid);

    /// @return true if a nil object is serialized
    static bool isnilid (class InputStream& is);

    /// write a nil id to an output stream
    /// serialize an object to an output stream
    static void wrnilid (class OutputStream& os);

    /// serialize a boolean to an output stream
    /// @param value the boolean value
    /// @param os    the output stream
    static void wrbool (const bool value, class OutputStream& os);

    /// serialize a boolean array with a block
    /// @param size the array size
    /// @param data the array data
    /// @param os   the output stream
    static void wrbool (const long size, const bool* data, OutputStream& os);

    /// deserialize a boolean
    /// @param is the input stream to read
    static bool rdbool (class InputStream& is);

    /// deserialize a boolean data block
    /// @param is the input stream
    /// @param size the array size
    static bool* rdbool (InputStream& is, const long size);

    /// serialize a character to an output stream
    /// @param value the character value
    /// @param os    the output stream
    static void wrchar (const t_quad value, class OutputStream& os);

    /// deserialize a character
    /// @param is the input stream to read
    static t_quad rdchar (class InputStream& is);

    /// serialize an integer to an output stream
    /// @param value the integer value
    /// @param os    the output stream
    static void wrlong (const t_long value, class OutputStream& os);

    /// serialize an integer array with a block
    /// @param size the array size
    /// @param data the array data
    /// @param os   the output stream
    static void wrlong (const long size, const long* data, OutputStream& os);

    /// deserialize an integer
    /// @param is the input stream to read
    static t_long rdlong (class InputStream& is);

    /// deserialize an integer data block
    /// @param is the input stream
    /// @param size the array size
    static long* rdlong (InputStream& is, const long size);

    /// serialize a real to an output stream
    /// @param value the real value
    /// @param os    the output stream
    static void wrreal (const t_real value, class OutputStream& os);

    /// serialize a real array with a block
    /// @param size the array size
    /// @param data the array data
    /// @param os   the output stream
    static void wrreal (const long size, const t_real* data, OutputStream& os);

    /// deserialize a real
    /// @param is the input stream to read
    static t_real rdreal (class InputStream& is);

    /// deserialize a real data block
    /// @param is the input stream
    /// @param size the array size
    static t_real* rdreal (InputStream& is, const long size);

    /// serialize a block to an output stream
    /// @param blok the block to serialize
    /// @param os    the output stream
    static void wrblok (const Block& blok, class OutputStream& os);

    /// deserialize a block
    /// @param is the input stream to read
    static Block rdblok (class InputStream& is);

  public:
    /// @return the object serial code
    virtual t_byte serialid (void) const;

    /// serialize an object to an output stream
    /// @param os the output stream to write
    virtual void wrstream (class OutputStream& os) const;

    /// deserialize an object from an input stream
    /// @param is the input steam to read in
    virtual void rdstream (class InputStream& is);

    /// serialize an object with its serial id
    /// @param os the output stream to write
    virtual void serialize (class OutputStream& os) const;

    /// @return an object by deserialization
    static Object* deserialize (class InputStream& is);

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
