// ---------------------------------------------------------------------------
// - Solid.hpp                                                               -
// - afnix:geo service - solid geometry class definition                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SOLID_HPP
#define  AFNIX_SOLID_HPP

#ifndef  AFNIX_POINT3_HPP
#include "Point3.hpp"
#endif

#ifndef  AFNIX_GEOMETRY_HPP
#include "Geometry.hpp"
#endif

#ifndef  AFNIX_MATERIAL_HPP
#include "Material.hpp"
#endif

#ifndef  AFNIX_QUATERNION_HPP
#include "Quaternion.hpp"
#endif

namespace afnix {

  /// The Solidhape class is a geoemtry class for the afnix geometry service.
  /// A solid object is a 3 dimensional object. Typical solids are polyhedrons
  /// and surfaces. Some special case do exists when one or several dimensions
  /// are null.
  /// @author amaury darsch

  class Solid : public Geometry {
  protected:
    /// the solid material
    Material d_matl;
    /// the solid orientation
    Quaternion d_qobj;
    
  public:
    /// set the solid by plist
    /// @param plst the solid plist
    void setplst (const Plist& plst);

    /// @return the solid plist
    Plist getplst (void) const;
    
    /// @return the solid material
    virtual Material getmatl (void) const;

    /// set the solid orientation
    /// @param q the quaternion object
    virtual void setqo (const Quaternion& q);

    /// @return the quaternion object
    virtual Quaternion getqo (void) const;
    
    /// set the x-axis rotation angle
    /// @param a the rotation angle
    virtual void setrx (const t_real a);

    /// set the y-axis rotation angle
    /// @param a the rotation angle
    virtual void setry (const t_real a);

    /// set the z-axis rotation angle
    /// @param a the rotation angle
    virtual void setrz (const t_real a);
    
    /// @return the solid origin
    virtual Point3 getorigin (void) const =0;

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
