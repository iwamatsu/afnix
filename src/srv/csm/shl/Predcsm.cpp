// ---------------------------------------------------------------------------
// - Predcsm.cpp                                                             -
// - afnix:csm service - predicates implementation                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "Saas.hpp"
#include "CartSet.hpp"
#include "Predcsm.hpp"
#include "Boolean.hpp"
#include "Delegate.hpp"
#include "Assistant.hpp"
#include "Exception.hpp"
#include "SessionSet.hpp"
#include "LocalSpace.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // agtp: agent object predicate

  Object* csm_agtp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "agent-p");
    bool result = (dynamic_cast <Agent*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // cagtp: carrier agent object predicate

  Object* csm_cagtp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "carrier-p");
    bool result = (dynamic_cast <Carrier*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // dagtp: delegate agent object predicate

  Object* csm_dagtp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "delegate-p");
    bool result = (dynamic_cast <Delegate*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // cartp: cart object predicate

  Object* csm_cartp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "cart-p");
    bool result = (dynamic_cast <Cart*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // csetp: cart set object predicate

  Object* csm_csetp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "cart-set-p");
    bool result = (dynamic_cast <CartSet*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // sessp: session object predicate
  
  Object* csm_sessp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "session-p");
    bool result = (dynamic_cast <Session*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // ssetp: session set object predicate
  
  Object* csm_ssetp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "session-set-p");
    bool result = (dynamic_cast <SessionSet*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // slotp: slot object predicate

  Object* csm_slotp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "slot-p");
    bool result = (dynamic_cast <Slot*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // apptp: appointer object predicate

  Object* csm_apptp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "appointer-p");
    bool result = (dynamic_cast <Appointer*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // asstp: assistant object predicate

  Object* csm_asstp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "assistant-p");
    bool result = (dynamic_cast <Assistant*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }


  // wspcp: workspace predicate

  Object* csm_wspcp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "workspace-p");
    bool result = (dynamic_cast <WorkSpace*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // lspcp: localspace predicate

  Object* csm_lspcp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "localspace-p");
    bool result = (dynamic_cast <LocalSpace*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // raas: raas predicate

  Object* csm_raasp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "raas-p");
    bool result = (dynamic_cast <Raas*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // saas: saas predicate

  Object* csm_saasp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "saas-p");
    bool result = (dynamic_cast <Saas*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
}
