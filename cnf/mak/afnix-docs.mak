# ----------------------------------------------------------------------------
# - afnix-docs                                                               -
# - afnix documentation rule makefile configuration                          -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - documentation definitions                                                -
# ----------------------------------------------------------------------------

DOCETC		= $(DIRDOC)/etc
DOCDTD		= $(DIRDOC)/dtd
DOCXML		= $(DIRDOC)/xml

ifeq ($(REFCSS),)
REFCSS		= $(DOCETC)/style.css
endif
ifeq ($(REFDTD),)
REFDTD		= $(DOCDTD)/afnix.dtd
endif
ifeq ($(REFIMG),)
REFIMG		= $(DOCETC)/afnix.png
endif
ifeq ($(ADPAXL),)
ADPAXL		= $(BLDPRJ)/axl/afnix-std-adp.axl
endif
ifeq ($(ADPPFX),)
ADPPFX		= $(BLDDIR)
endif
ifeq ($(ADPLIB),)
ADPLIB		= $(BLDLIB)
endif
ifeq ($(ADPEXE),)
ADPEXE		= "axi"
endif

# ----------------------------------------------------------------------------
# - documentation definitions                                                -
# ----------------------------------------------------------------------------

XMLMAN		= $(wildcard *.xml)
XHTTRG          = $(MANUAL:.xml=.xht)
MANTRG          = $(MANUAL:.xml=.man)
TEXTRG          = $(MANUAL:.xml=.tex)

# ----------------------------------------------------------------------------
# - documentation control                                                    -
# ----------------------------------------------------------------------------

ifeq ($(XHTMAN),)
XHTMAN		= $(XHTTRG)
endif
ifeq ($(MANMAN),)
MANMAN		= $(MANTRG)
endif
ifeq ($(TEXMAN),)
TEXMAN		= $(TEXTRG)
endif

DOCTRG		= xht man
XHTOPT		= "-f assert -i $(ADPAXL) -m -- -x -o $(XHTTRG)"
MANOPT		= "-f assert -i $(ADPAXL) -m -- -m -o $(MANTRG)"
TEXOPT		= "-f assert -i $(ADPAXL) -m -- -l -o $(TEXTRG)"

# ----------------------------------------------------------------------------
# - documentation rules                                                      -
# ----------------------------------------------------------------------------

ifneq ($(MANUAL),)
doc: $(DOCTRG)
.PHONY: doc

# rule: xht
# this rule generate a xhtml documentation

xht: $(XHTTRG)
.PHONY: xht

$(XHTTRG) : $(XMLMAN)
	@$(AEXEC) --prefix=$(ADPPFX) --libdir=$(ADPLIB) \
                  --binexe=$(ADPEXE) --binopt=$(XHTOPT) $(MANUAL)
	@$(MKDIR) $(DOCXHT)
	@$(CP) $(REFCSS) $(DOCXHT)
	@$(CP) $(REFIMG) $(DOCXHT)
	@$(CP) $(XHTTRG) $(DOCXHT)/$(XHTMAN)

# rule: man
# this rule generate a groff documentation

man: $(MANTRG)
.PHONY: man

$(MANTRG) : $(XMLMAN)
	@$(AEXEC) --prefix=$(ADPPFX) --libdir=$(ADPLIB) \
                  --binexe=$(ADPEXE) --binopt=$(MANOPT) $(MANUAL)
	@$(MKDIR) $(DOCMAN)
	@$(CP) $(MANTRG) $(DOCMAN)/$(MANMAN)

# rule: tex
# this rule generate a latex documentation

tex: $(TEXTRG)
.PHONY: tex

$(TEXTRG) : $(XMLMAN)
	@$(AEXEC) --prefix=$(ADPPFX) --libdir=$(ADPLIB) \
                  --binexe=$(ADPEXE) --binopt=$(TEXOPT) $(MANUAL)
	@$(MKDIR) $(DOCTEX)
	@$(CP) $(TEXTRG) $(DOCTEX)/$(TEXMAN)

endif

# ----------------------------------------------------------------------------
# - generic clean                                                            -
# ----------------------------------------------------------------------------

clean::
	@$(RM) *~ core *.core
.PHONY: clean
