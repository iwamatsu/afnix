// ---------------------------------------------------------------------------
// - TlsProto.cpp                                                            -
// - afnix:tls service - tls protocol class implementation                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Md5.hpp"
#include "Sha1.hpp"
#include "Hmac.hpp"
#include "Vector.hpp"
#include "Integer.hpp"
#include "TlsData.hpp"
#include "TlsTypes.hxx"
#include "TlsProto.hxx"
#include "TlsUtils.hpp"
#include "TlsShake.hpp"
#include "TlsCspec.hpp"
#include "TlsAlert.hpp"
#include "TlsChello.hpp"
#include "TlsCkeyxh.hpp"
#include "TlsFinish.hpp"
#include "TlsShello.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // create a tls protocol by version

  TlsProto* TlsProto::create (const t_byte vmaj, const t_byte vmin) {
    // check version
    String vers = tls_vers_tostring (vmaj, vmin);
    if (tls_vers_isvalid (vmaj, vmin) == false) {
      throw Exception ("tls-error", "invalid tls version", vers);
    }
    // process major version
    if (vmaj == TLS_VMAJ_3XX) {
      if (vmin == TLS_VMIN_301) return new TlsProto;
    }
    throw Exception ("tls-error", "cannot create tls protocol version", vers);
  }

  // create a tls protocol by state

  TlsProto* TlsProto::create (TlsState* sta) {
    // check for nil
    if (sta == nilp) return nilp;
    // get version and check
    t_byte vmaj = sta->getvmaj ();
    t_byte vmin = sta->getvmin ();
    // create the protocol
    return TlsProto::create (vmaj, vmin);
  }

  // the standard prf for the tls
  
  Buffer TlsProto::prf (const long    slen, const t_byte* sbuf,
			const String& labl, const Buffer& seed,
			const long rlen) {
    // check for valid arguments
    if ((slen < 2L) || (sbuf == nilp)) return nilp;
    try {
      // compute the subkey address and adjust odd length
      long  sl = slen / 2L;
      const t_byte* s1 = &(sbuf[0]);
      const t_byte* s2 = &(sbuf[sl]);
      if ((sl % 2L) != 0L) sl++;
      // create the hmac keys and hmac object
      Key kmd (Key::KMAC, sl, s1); Hmac hmd (kmd, new Md5);
      Key ksh (Key::KMAC, sl, s2); Hmac hsh (ksh, new Sha1);
      // create a buffer that merges the label and seed
      Buffer hbuf; hbuf.add (labl); hbuf.add (seed);
      // get the hash buffers
      Buffer bmd = TlsUtils::phash (&hmd, hbuf, rlen);
      Buffer bsh = TlsUtils::phash (&hsh, hbuf, rlen);
      // paranoid checkc
      if ((bmd.length () != rlen) || (bsh.length () != rlen)) {
	throw Exception ("tls-error", "inconsistent p_hash buffer size");
      }
      // combine both hash buffer
      Buffer result (rlen); result.setrflg (false);
      for (long k = 0L; k < rlen; k++) {
	result.add ((char) (bmd.get (k) ^ bsh.get (k)));
      }
      return result;
    } catch (...) {
      delete [] sbuf;
      throw;
    }
  }

  
  // the standard prf for the tls
  
  Buffer TlsProto::prf (const Buffer& sbuf, const String& labl,
			const Buffer& seed, const long    rlen) {
    t_byte* cbuf = nilp;
    try {
      // get the buffer length and content
      long clen = sbuf.length ();
      cbuf = (t_byte*) sbuf.tochar ();
      // compute the prf
      Buffer result = prf (clen, cbuf, labl, seed, rlen);
      delete [] cbuf;
      return result;
    } catch (...) {
      delete [] cbuf;
      throw;
    }
  }

  // the client verify computation

  Buffer TlsProto::cfin (const Buffer& mbuf, const Buffer& vbuf) {
    // create a seed buffer
    Buffer seed;
    Md5  hl; Buffer bl = vbuf; hl.pushb (seed, bl);
    Sha1 hr; Buffer br = vbuf; hr.pushb (seed, br);
    // compute the finished signature
    return TlsProto::prf (mbuf, TLS_LABL_CF, seed, TLS_SIZE_FIN);
  }

  // the server verify computation

  Buffer TlsProto::sfin (const Buffer& mbuf, const Buffer& vbuf) {
    // create a seed buffer
    Buffer seed;
    Md5  hl; Buffer bl = vbuf; hl.pushb (seed, bl);
    Sha1 hr; Buffer br = vbuf; hr.pushb (seed, br);
    // compute the finished signature
    return TlsProto::prf (mbuf, TLS_LABL_SF, seed, TLS_SIZE_FIN);
  }
  
  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default decoder

  TlsProto::TlsProto (void) {
  }

  // return the class name
  
  String TlsProto::repr (void) const {
    return "TlsProto";
  }

  // get a record by input stream and cipher

  TlsRecord* TlsProto::getrcd (InputStream* is, TlsState* sta) const {
    rdlock ();
    try {
      if (is == nilp) {
	unlock ();
	return nilp;
      }
      if (sta == nilp) {
	TlsRecord* rcd = new TlsRecord (is);
	unlock ();
	return rcd;
      }
      // get the decoding cipher
      Cipher* dc = (sta == nilp) ? nilp : sta->getcbcf ();
      // get the record, possibly with the hmac
      TlsRecord* rcd = new TlsRecord (is, dc);
      if (rcd != nilp) {
	// check the record hmac
	bool status = rcd->chkhmc(sta->getchmc(), sta->newcnum());
	if (status == false) {
	  throw Exception ("tls-error", "invalid client hmac record");
	}
      }
      unlock ();
      return rcd;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a message by record
  
  TlsMessage* TlsProto::getmsg (TlsRecord* rcd) const {
    // check for nil
    if (rcd == nilp) return nilp;
    // lock and decode
    rdlock ();
    try {
      // get the record type and check
      t_byte type = rcd->gettype ();
      // prepare result
      TlsMessage* result = nilp;
      // map the record
      switch (type) {
      case TLS_TYPE_CCS:
	result = new TlsCspec (rcd);
	break;
      case TLS_TYPE_ALT:
	result = new TlsAlert (rcd);
	break;
      case TLS_TYPE_HSK:
	result = new TlsShake (rcd);
	break;
      case TLS_TYPE_APP:
	result = new TlsData (rcd);
	break;
      default:
	throw Exception ("tls-error", "cannot decode record into a message");
	break;
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
   
  // get a message by input stream and state

  TlsMessage* TlsProto::getmsg (InputStream* is, TlsState* sta) const {
    rdlock ();
    TlsRecord* rcd = nilp;
    try {
      // get the next available record
      rcd = getrcd (is, sta);
      // decode the record
      TlsMessage* msg = getmsg (rcd);
      unlock ();
      return msg;
    } catch (...) {
      delete rcd;
      unlock ();
      throw;
    }
  }
  
  // encode a tls message

  long TlsProto::encode (OutputStream* os, TlsMessage* msg) const {
    // check for nil
    if ((msg == nilp) || (os == nilp)) return 0L;
    rdlock ();
    try {
      long result = encode (os, msg, nilp);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // encode a tls message

  long TlsProto::encode (OutputStream* os,TlsMessage* msg,TlsState* sta) const {
    // check for nil
    if ((msg == nilp) || (os == nilp)) return 0L;
    rdlock ();
    try {
      if (sta == nilp) {
	long result = msg->write (os);
	unlock ();
	return result;
      }
      // get the hmac and sequence number
      Hmac*  hmac = sta->getshmc ();
      t_octa snum = sta->newsnum ();
      if (msg->addhmc (hmac, snum) == false) {
	throw Exception ("tls-error", "cannot add tls message hmac");
      }
      // get the decoding cipher
      Cipher* ec = sta->getsbcf ();
      // write the message
      long result = msg->write (os, ec);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // decode a handshake block

  TlsInfos* TlsProto::decode (TlsHblock* hblk) const {
    // check for nil
    if (hblk == nilp) return nilp;
    // lock and decode
    rdlock ();
    try {
      // get the block type and check
      t_byte type = hblk->gettype ();
      // prepare result
      TlsInfos* result = nilp;
      // map the record
      switch (type) {
      case TLS_HSHK_CLH:
	result = new TlsChello (hblk);
	break;
      case TLS_HSHK_CKE:
	result = new TlsCkeyxh (hblk);
	break;
      case TLS_HSHK_FIN:
	result = new TlsFinish (hblk);
	break;
      default:
	throw Exception ("tls-error", "cannot decode handshake block");
	break;
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // receive a client hello by input stream and state

  TlsInfos* TlsProto::rcvchlo (InputStream* is, TlsState* sta) const {
    rdlock ();
    TlsHblock*  blk = nilp;
    try {
      // get the next available message
      TlsMessage* msg = getmsg (is, sta);
      if (msg == nilp) {
	unlock ();
	return nilp;
      }
      // push the message in the message buffer [for verify]
      if (sta != nilp) msg->pushb (sta->gethvmb ());
      // map it to a handshake message
      TlsShake* shk = dynamic_cast <TlsShake*> (msg);
      if (shk == nilp) {
	throw Exception ("tls-error", "cannot get handshake message");
      }
      // create a handshake iterator [this will destroy shk/msg]
      TlsShakeit sit (shk);
      // get the handshake block
      blk = dynamic_cast <TlsHblock*> (sit.getobj ());
      if (blk == nilp) {
	throw Exception ("tls-error", "cannot get handshake block");
      }
      // move to the end and check
      sit.next ();
      if (sit.isend () == false) {
	throw Exception ("tls-error", "inconsistent handshake message");
      }
      // get the client block
      TlsInfos* hlo = decode (blk);
      if (hlo == nilp) {
	throw Exception ("tls-error", "cannot decode client hello block");
      }
      delete blk;
      unlock ();
      return hlo;
    } catch (...) {
      delete blk;
      unlock ();
      throw;
    }
  }

  // receive a client key exchange by input stream and state

  TlsHblock* TlsProto::rcvckxh (InputStream* is, TlsState* sta) const {
    rdlock ();
    TlsHblock* blk = nilp;
    try {
      // get the next available message
      TlsMessage* msg = getmsg (is, sta);
      if (msg == nilp) {
	unlock ();
	return nilp;
      }
      // push the message in the message buffer [for verify]
      if (sta != nilp) msg->pushb (sta->gethvmb ());
      // map it to a handshake message
      TlsShake* shk = dynamic_cast <TlsShake*> (msg);
      if (shk == nilp) {
	throw Exception ("tls-error", "cannot get handshake message");
      }
      // create a handshake iterator [this will destroy msg/shk]
      TlsShakeit sit (shk);
      // get the handshake block
      blk = dynamic_cast <TlsHblock*> (sit.getobj ());
      if (blk == nilp) {
	throw Exception ("tls-error", "cannot get handshake block");
      }
      // move to the end and check
      sit.next ();
      if (sit.isend () == false) {
	throw Exception ("tls-error", "inconsistent handshake message");
      }
      unlock ();
      return blk;
    } catch (...) {
      delete blk;
      unlock ();
      throw;
    }
  }

  // receive a client change cipher spec

  bool TlsProto::rcvcccs (InputStream* is, TlsState* sta) const {
    rdlock ();
    TlsMessage* msg = nilp;
    try {
      // get the next available message
      if ((msg = getmsg (is, sta)) == nilp) {
	unlock ();
	return false;
      }
      // map it to a change cipher spec
      TlsCspec* ccs = dynamic_cast <TlsCspec*> (msg);
      if (ccs == nilp) {
	throw Exception ("tls-error", "cannot get change cipher spec message");
      }
      // update the state with the new cipher
      bool result = (sta == nilp) ? false : sta->chgccs ();
      delete msg;
      unlock ();
      return result;
    } catch (...) {
      delete msg;
      unlock ();
      throw;
    }
  }

  // receive a client finished by input stream and state

  TlsHblock* TlsProto::rcvcfin (InputStream* is, TlsState* sta) const {
    rdlock ();
    TlsHblock* blk = nilp;
    try {
      // get the next available message
      TlsMessage* msg = getmsg (is, sta);
      if (msg == nilp) {
	unlock ();
	return nilp;
      }
      // push the message in the message buffer [for verify]
      if (sta != nilp) msg->pushb (sta->gethvmb ());
      // map it to a handshake message
      TlsShake* shk = dynamic_cast <TlsShake*> (msg);
      if (shk == nilp) {
	throw Exception ("tls-error", "cannot get handshake message");
      }
      // create a handshake iterator [this will destroy msg/shk]
      TlsShakeit sit (shk);
      // get the handshake block
      blk = dynamic_cast <TlsHblock*> (sit.getobj ());
      if (blk == nilp) {
	throw Exception ("tls-error", "cannot get handshake block");
      }
      // move to the end and check
      sit.next ();
      if (sit.isend () == false) {
	throw Exception ("tls-error", "inconsistent handshake message");
      }
      unlock ();
      return blk;
    } catch (...) {
      delete blk;
      unlock ();
      throw;
    }
  }

  // get a server hello chunk by state

  TlsChunk TlsProto::toshlo (TlsState* sta) const {
    // lock and generate
    rdlock ();
    try {
      // check for nil first
      if (sta == nilp) {
	throw Exception ("tls-error", "cannot generate server hello chunk");
      }
      // gather the server hello information
      t_byte vmaj = sta->getvmaj ();
      t_byte vmin = sta->getvmin ();
      t_word cifr = sta->getcifr ();
      // create a server hello
      TlsShello shlo (vmaj, vmin, cifr);
      // update the state random sequence
      sta->setsrnd (shlo.getrand ());
      // map it to a chunk
      TlsChunk result = shlo.tochunk ();
      // update the info list
      sta->addinfo (shlo.getinfo ());
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // map a server hello by state

  TlsMessage* TlsProto::getshlo (TlsState* sta) const {
    // check for nil first
    if (sta == nilp) return nilp;
    // lock and map
    TlsShake* hsk = nilp;
    rdlock ();
    try {
      // create a tls handshake by state
      hsk = new TlsShake (sta->getvmaj (), sta->getvmin());
      // get the  server hello chunk by state
      TlsChunk chk = toshlo (sta);
      // add the chunk block to the record
      hsk->add (TLS_HSHK_SRH, chk);
      unlock ();
      return hsk;
    } catch (...) {
      delete hsk;
      unlock ();
      throw;
    }
  }
  
  // send a server hello by state
  
  void TlsProto::sndshlo (OutputStream* os, TlsState* sta) const {
    TlsMessage* shlo = nilp;
    rdlock ();
    try {
      // create a server hello message and encode it
      shlo = getshlo (sta);
      if (shlo == nilp) {
	throw Exception ("tls-error", "cannot get server hello message");
      }
      // push the message in the message buffer [for verify]
      if (sta != nilp) shlo->pushb (sta->gethvmb ());
      // encode the message
      encode (os, shlo);
      delete shlo;
      unlock ();
    } catch (...) {
      delete shlo;
      unlock ();
      throw;
    }
  }

  // map a server certificate by state

  TlsMessage* TlsProto::getscrt (TlsState* sta) const {
    // check for nil first
    if (sta == nilp) return nilp;
    // lock and map
    TlsShake* hsk = nilp;
    rdlock ();
    try {
      // get the state certificate
      TlsCerts* cert = sta->getcert ();
      if (cert == nilp) {
	unlock ();
	return nilp;
      }
      // create a tls handshake by state
      hsk = new TlsShake (sta->getvmaj (), sta->getvmin());
      // get the certificate chunk
      TlsChunk chk = cert->tochunk ();
      // add the chunk block to the record
      hsk->add (TLS_HSHK_CRT, chk);
      unlock ();
      return hsk;
    } catch (...) {
      delete hsk;
      unlock ();
      throw;
    }
  }

  // send a server certificate
  
  void TlsProto::sndscrt (OutputStream* os, TlsState* sta) const {
    TlsMessage* cert = nilp;
    rdlock ();
    try {
      // get the certificate message
      cert = getscrt (sta);
      // check for a valid message to push and encode
      if (cert != nilp) {
	// push the message in the message buffer [for verify]
	if (sta != nilp) cert->pushb (sta->gethvmb ());
	// encode the message
	encode (os, cert);
      }
      delete cert;
      unlock ();
    } catch (...) {
      delete cert;
      unlock ();
      throw;
    }
  }

  // map a server done by state

  TlsMessage* TlsProto::getsdon (TlsState* sta) const {
    // check for nil first
    if (sta == nilp) return nilp;
    // lock and map
    TlsShake* hsk = nilp;
    rdlock ();
    try {
      // create a tls handshake by state
      hsk = new TlsShake (sta->getvmaj (), sta->getvmin());
      // add the chunk block to the record
      hsk->add (TLS_HSHK_SHD, TlsChunk());
      unlock ();
      return hsk;
    } catch (...) {
      delete hsk;
      unlock ();
      throw;
    }
  }

  // send a certificate
  
  void TlsProto::sndsdon (OutputStream* os, TlsState* sta) const {
    TlsMessage* done = nilp;
    rdlock ();
    try {
      // get the server done message and encode it
      done = getsdon (sta);
      // check for valid message
      if (done != nilp) {
	// push the message in the message buffer [for verify]
	if (sta != nilp) done->pushb (sta->gethvmb ());
	// encode the message
	encode (os, done);
      }
      delete done;
      unlock ();
    } catch (...) {
      delete done;
      unlock ();
      throw;
    }
  }

  // map a server change cipher spec by state

  TlsMessage* TlsProto::getsccs (TlsState* sta) const {
    // check for nil first
    if (sta == nilp) return nilp;
    // lock and map
    TlsCspec* ccs = nilp;
    rdlock ();
    try {
      // create a tls change cipher spec by state
      ccs = new TlsCspec (sta->getvmaj (), sta->getvmin());
      unlock ();
      return ccs;
    } catch (...) {
      delete ccs;
      unlock ();
      throw;
    }
  }
  
  // send the server change cipher spec by state

  bool TlsProto::sndsccs (OutputStream* os, TlsState* sta) const {
    TlsMessage* sccs = nilp;
    rdlock ();
    try {
      // get the server finish message and encode it
      sccs = getsccs (sta);
      // check for valid message and encode it
      if (sccs == nilp) {
	unlock ();
	return false;
      }
      encode (os, sccs, sta);
      // update the state with the new cipher
      bool result = (sta == nilp) ? false : sta->chgscs ();
      // done
      delete sccs;
      unlock ();
      return result;
    } catch (...) {
      delete sccs;
      unlock ();
      throw;
    }
  }
  
  // map a server finish by state

  TlsMessage* TlsProto::getsfin (TlsState* sta) const {
    // check for nil first
    if (sta == nilp) return nilp;
    // lock and map
    TlsShake* hsk = nilp;
    rdlock ();
    try {
      // create a tls handshake by state
      hsk = new TlsShake (sta->getvmaj (), sta->getvmin());
      // compute the finished signature
      Buffer mbuf = sta->getmbuf();
      Buffer finb = TlsProto::sfin (sta->getmbuf(), sta->gethvmb());
      if (finb.length() != TLS_SIZE_FIN){
	throw Exception ("tls-error", "invalid finished buffer size");
      }
      // add the chunk block to the record
      hsk->add (TLS_HSHK_FIN, TlsChunk(finb));
      unlock ();
      return hsk;
    } catch (...) {
      delete hsk;
      unlock ();
      throw;
    }
  }
  
  // send the server finished message

  void TlsProto::sndsfin (OutputStream* os, TlsState* sta) const {
    TlsMessage* mfin = nilp;
    rdlock ();
    try {
      // get the server finish message and encode it
      mfin = getsfin (sta);
      // check for valid message and encode it
      if (mfin != nilp) encode (os, mfin, sta);
      // done
      delete mfin;
      unlock ();
    } catch (...) {
      delete mfin;
      unlock ();
      throw;
    }
  }

  // pop the next available buffer

  Buffer TlsProto::popb (InputStream* is, TlsState* sta) const {
    rdlock ();
    try {
      // get the next available message
      TlsMessage* msg = getmsg (is, sta);
      if (msg == nilp) {
	throw Exception ("tls-error", "cannot pop tls message in pop buffer");
      }
      // check for a data message
      TlsData* data = dynamic_cast<TlsData*> (msg);
      if (data != nilp) {
	Buffer result;
	data->pushb (result);
	unlock ();
	return result;
      }
      // check for an alert
      TlsAlert* alrt = dynamic_cast<TlsAlert*> (msg);
      if (alrt != nilp) {
	TlsAlert result = *alrt;
	throw result;
      }
      // invalid message in loop
      throw Exception ("tls-error", "invalid message in pop buffer",
		       Object::repr (msg));
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // push a buffer to the stream

  long TlsProto::pushb (OutputStream* os, TlsState* sta,
			const Buffer& buf) const {
    rdlock();
    TlsMessage* msg = nilp;
    try {
      // check for valid state
      if (sta == nilp) {
	throw Exception ("tls-error", "invalid nil state in push buffer");
      }
      // create a tls data message
      msg = new TlsData (sta->getvmaj(), sta->getvmin());
      // push the buffer in the message
      msg->add (buf);
      // encode the message
      long result = encode (os, msg, sta);
      // done
      delete msg;
      unlock ();
      return result;
    } catch (...) {
      delete msg;
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 2;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_DECODE = zone.intern ("decode");
  static const long QUARK_GETMSG = zone.intern ("get-message");

  // create a new object in a generic way

  Object* TlsProto::mknew (Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) return new TlsProto;
    // too many arguments
    throw Exception ("argument-error", 
                     "too many argument with tls decoder constructor");
  }

  // return true if the given quark is defined

  bool TlsProto::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark
  
  Object* TlsProto::apply (Runnable* robj, Nameset* nset, const long quark,
			   Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_DECODE) {
	Object*     obj = argv->get (0);
	TlsHblock* hblk = dynamic_cast<TlsHblock*> (obj);
	if (hblk == nilp) {
	  throw Exception ("type-error", "invalid object as handshake block",
			   Object::repr (obj));
	}
	return decode (hblk);
      }
      if (quark == QUARK_GETMSG) {
	Object*    obj = argv->get (0);
	TlsRecord* rcd = dynamic_cast<TlsRecord*> (obj);
	if (rcd == nilp) {
	  throw Exception ("type-error", "invalid object as record",
			   Object::repr (obj));
	}
	return getmsg (rcd);
      }
    }
    // call the object method
    return Object::apply (robj, nset, quark, argv);
  }
}
