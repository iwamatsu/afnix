// ---------------------------------------------------------------------------
// - Nwgsid.hxx                                                              -
// - afnix:nwg module - serial id definition                                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_NWGSID_HXX
#define  AFNIX_NWGSID_HXX

#ifndef  AFNIX_INTEGER_HPP
#include "Integer.hpp"
#endif

namespace afnix {
  // the nwg serial id
  static const t_byte SERIAL_SESS_ID = 0x50; // session id

  // serialize an integer to an output stream
  static inline void nwg_wrlong (const t_long value, OutputStream& os) {
    Integer iobj (value);
    iobj.wrstream (os);
  }

  // deserialize an integer
  static inline t_long nwg_rdlong (InputStream& is) {
    Integer iobj;
    iobj.rdstream (is);
    return iobj.tolong ();
  }
}

#endif
