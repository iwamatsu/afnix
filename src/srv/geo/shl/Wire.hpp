// ---------------------------------------------------------------------------
// - Wire.hpp                                                                -
// - afnix:geo service - solid wire class definition                         -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_WIRE_HPP
#define  AFNIX_WIRE_HPP

#ifndef  AFNIX_SOLID_HPP
#include "Solid.hpp"
#endif

namespace afnix {

  /// The wire class is a solid line, which is similar to a line, except
  /// that a radius is used to define the solid size.
  /// @author amaury darsch

  class Wire : public Solid {
  protected:
    /// the wire origin
    Point3 d_o;
    /// the wire apex
    Point3 d_a;

  public:
    /// create a unity wire
    Wire (void);

    /// create a wire by points
    /// @param o the wire origin
    /// @param a the wire apex
    Wire (const Point3& o, const Point3& a);

    /// copy construct this wire
    /// @param that the object to copy
    Wire (const Wire& that);
    
    /// assign a wire to this one
    /// @param that the object to copy
    Wire& operator = (const Wire& that);

    /// @return the class name
    String repr (void) const;
    
    /// @return a clone of this object
    Object* clone (void) const;
    
    /// reset this wire
    void reset (void);
        
    /// @return the geometry name
    String getname (void) const;

    /// set the geometry by plist
    /// @param plst the geometry plist
    void setplst (const Plist& plst);

    /// @return the geometry plist
    Plist getplst (void) const;
    
    /// @return the wire origin
    Point3 getorigin (void) const;

    /// @return the wire apex
    virtual Point3 getapex (void) const;

    /// set a wire by points
    /// @param o the wire origin
    /// @param a the wire apex
    virtual void set (const Point3& o, const Point3& a);

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
