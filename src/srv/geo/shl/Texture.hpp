// ---------------------------------------------------------------------------
// - Texture.hpp                                                             -
// - afnix:geo service - texture class definition                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TEXTURE_HPP
#define  AFNIX_TEXTURE_HPP

#ifndef  AFNIX_PLIST_HPP
#include "Plist.hpp"
#endif

#ifndef  AFNIX_NAMEABLE_HPP
#include "Nameable.hpp"
#endif

namespace afnix {

  /// The Texture class is a a gemetry texture descriptor. The texture is
  /// defined by name, which is later interpreted as an intrinsinc object
  /// like color or as another object like an image. It is during the
  /// rendering process that the texture is mapped onto the obejct.
  /// @author amaury darsch

  class Texture : public Nameable {
  private:
    /// the texture name
    String d_name;
    
  public:
    /// create a default texture
    Texture (void);

    /// create a texture by name
    /// @param name the texture name
    Texture (const String& name);

    /// copy construct this texture
    /// @param that the texture to copy
    Texture (const Texture& that);

    /// assign a texture to this one
    /// @param that the texture to assign
    Texture& operator = (const Texture& that);

    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;
    
    /// @return the texture name
    String getname (void) const;

    /// reset this texture
    virtual void reset (void);

    /// @return the texture name
    /// set the texture by plist
    /// @param plst the texture plist
    virtual void setplst (const Plist& plst);

    /// @return the texture plist
    virtual Plist getplst (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
