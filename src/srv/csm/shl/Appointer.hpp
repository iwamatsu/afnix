// ---------------------------------------------------------------------------
// - Appointer.hpp                                                           -
// - afnix:csm service - appointer class definition                          -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_APPOINTER_HPP
#define  AFNIX_APPOINTER_HPP

#ifndef  AFNIX_SET_HPP
#include "Set.hpp"
#endif

#ifndef  AFNIX_DATE_HPP
#include "Date.hpp"
#endif

#ifndef  AFNIX_SLOT_HPP
#include "Slot.hpp"
#endif

namespace afnix {

  /// The Appointer class is a class design to allocate time slot in a 
  /// calendar in order to fill an agenda. The appointer do not store the
  /// slot but rather acts as a generator. the appointer algorith operates
  /// with rules that permits to allocate the next available slot. The 
  /// basic rules permits to define regular blocked days and special blocked 
  /// days. Another rule permits to define an operating time period. Multiple
  /// time periods are allowed. A maximum daly slots rule is also available.
  /// @author amaury darsch

  class Appointer : public virtual Object {
  private:
    /// the initial time
    t_long d_time;
    /// the daily slot number
    long d_dsnm;
    /// the number of slots
    long d_snum;
    /// the pushback set
    Set  d_pset;
    /// the rule structure
    struct s_rule* p_rule;

  public:
    /// create a default appointer
    Appointer (void);

    /// create an appointer by time
    /// @param time the initial time
    Appointer (const t_long time);
    
    /// copy construct this appointer
    /// @param that the appointer to copy
    Appointer (const Appointer& that);

    /// destroy this appointer
    ~Appointer (void);

    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// reset this appointer
    void reset (void);

    /// set the appointer time
    /// @param time the time to set
    virtual void settime (const t_long time);

    /// @return the appointer time
    virtual t_long gettime (void) const;

    /// @return the appointer minimum time
    virtual t_long getamtm (void) const;

    /// @return the appointer minimum time by time
    virtual t_long getamtm (const t_long mrtm) const;

    /// set the initial time by date
    /// @param date the date time to set
    virtual void setdate (const Date& date);

    /// @return the appointer date
    virtual Date getdate (void) const;

    /// @return the number of slots
    virtual long getsnum (void) const;

    /// set a block day rule
    /// @param wday the week day index to block
    virtual void setbday (const long wday);

    /// set a special day rule
    /// @param ymon the special year month
    /// @param mday the special month day
    virtual void setsday (const long ymon, const long mday);

    /// set a max slt rule
    /// @param maxs the maximum number of slots
    virtual void setmaxs (const long maxs);

    /// set a valid block time rule
    /// @param bbtm the block begin time
    /// @param betm the block end time
    virtual void setvbtm (const t_long bbtm, const t_long betm);

    /// get the next available slot by duration
    /// @param dlen the slot duration
    virtual Slot getslot (const t_long dlen);

    /// get the next available slot by duration and time
    /// @param dlen the slot duration
    virtual Slot getslot (const t_long time, const t_long dlen);

    /// pushback a slot in the slot pool
    /// @param slot the slot to pushback
    virtual void pushback (const Slot& slot);

  private:
    // make the assignment operator private
    Appointer& operator = (const Appointer&);

  public:
    /// create a object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
