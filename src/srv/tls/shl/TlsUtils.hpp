// ---------------------------------------------------------------------------
// - TlsUtils.hpp                                                            -
// - afnix:tls service - tls utility class definition                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSUTILS_HPP
#define  AFNIX_TLSUTILS_HPP

#ifndef  AFNIX_MAC_HPP
#include "Mac.hpp"
#endif

namespace afnix {

  /// The TlsUtils class is a collection of static convenient functions. Their
  /// purpose is to group these general functions into a single umbrella.
  /// @author amaury darsch

  class TlsUtils {
  public:
    /// create a random array by size and time flag
    /// @param size the array size
    /// @param tflg the time flag
    static t_byte* random (const long size, const bool tflg);

    /// the standard p_hash function of the tls
    /// @param hmac the hash mac object
    /// @param seed the mac seed buffer
    /// @param rlen he requested byte length
    static Buffer phash (Mac* hmac, const Buffer& seed, const long rlen);
  };
}

#endif
