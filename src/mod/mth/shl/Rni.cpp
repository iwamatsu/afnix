// ---------------------------------------------------------------------------
// - Rni.cpp                                                                 -
// - afnix:mth module - real non linear interface implementation             -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Rni.hpp"
#include "Vector.hpp"
#include "Runnable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
 
namespace afnix {

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 2;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_GETRHS = zone.intern ("getrhs");
  static const long QUARK_GETLHS = zone.intern ("getlhs");

  // return true if the given quark is defined

  bool Rni::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true){
      unlock ();
      return true;
    }
    bool result = hflg ? Object::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Rni::apply (Runnable* robj, Nameset* nset, const long quark,
                      Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();

    // dispatch 1 argument
    if (argc == 0) {
      if (quark == QUARK_GETLHS) {
	rdlock ();
	try {
	  // get the vector operand
	  Object* obj = argv->get (0);
	  Rvi*    vop = dynamic_cast <Rvi*> (obj);
	  if (vop == nilp) {
	    throw Exception ("type-error", "invalid object for getlhs",
			     Object::repr (obj));
	  }
	  // get the lhs by vector operand
	  Rmi* result = getlhs (vop);
	  robj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_GETRHS) {
	rdlock ();
	try {
	  // get the vector operand
	  Object* obj = argv->get (0);
	  Rvi*    vop = dynamic_cast <Rvi*> (obj);
	  if (vop == nilp) {
	    throw Exception ("type-error", "invalid object for getlhs",
			     Object::repr (obj));
	  }
	  // get the rhs by vector operand
	  Rvi* result = getrhs (vop);
	  robj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // call the object
    return Object::apply (robj, nset, quark, argv);
  }
}

