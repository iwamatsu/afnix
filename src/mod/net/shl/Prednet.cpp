// ---------------------------------------------------------------------------
// - Prednet.cpp                                                             -
// - afnix:net module - predicates implementation                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "Prednet.hpp"
#include "Address.hpp"
#include "Boolean.hpp"
#include "TcpClient.hpp"
#include "UdpClient.hpp"
#include "TcpServer.hpp"
#include "UdpServer.hpp"
#include "Multicast.hpp"
#include "Exception.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // addressp: address object predicate

  Object* net_addressp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "address-p");
    bool result =  (dynamic_cast <Address*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // sockoptp: socket option object predicate

  Object* net_sockoptp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "sockopt-p");
    bool result =  (dynamic_cast <Sockopt*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // sockprmsp: socket parameters object predicate

  Object* net_sockprmsp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "sock-params-p");
    bool result =  (dynamic_cast <SockParams*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // socketp: socket object predicate

  Object* net_socketp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "socket-p");
    bool result =  (dynamic_cast <Socket*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // datagramp: datagram object predicate

  Object* net_datagramp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "datagram-p");
    bool result =  (dynamic_cast <Datagram*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // tcpsocketp: tcp socket object predicate

  Object* net_tcpsocketp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "tcp-socket-p");
    bool result =  (dynamic_cast <TcpSocket*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // udpsocketp: udp socket object predicate

  Object* net_udpsocketp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "udp-socket-p");
    bool result =  (dynamic_cast <UdpSocket*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // tcpclientp: tcp client object predicate

  Object* net_tcpclientp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "tcp-client-p");
    bool result =  (dynamic_cast <TcpClient*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // udpclientp: udp client object predicate

  Object* net_udpclientp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "udp-client-p");
    bool result =  (dynamic_cast <UdpClient*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // tcpserverp: tcp server object predicate

  Object* net_tcpserverp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "tcp-server-p");
    bool result =  (dynamic_cast <TcpServer*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // udpserverp: udp server object predicate

  Object* net_udpserverp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "udp-server-p");
    bool result =  (dynamic_cast <UdpServer*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // multicastp: multicast object predicate

  Object* net_multicastp  (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "multicast-p");
    bool result =  (dynamic_cast <Multicast*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

}
