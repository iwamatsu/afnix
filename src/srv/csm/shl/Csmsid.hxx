// ---------------------------------------------------------------------------
// - Csmsid.hxx                                                              -
// - afnix:csm service - serial id definition                                -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CSMSID_HXX
#define  AFNIX_CSMSID_HXX

#ifndef  AFNIX_INTEGER_HPP
#include "Integer.hpp"
#endif

namespace afnix {
  // the csm agent serial id
  static const t_byte SERIAL_RCO_ID  = 0x70;
  static const t_byte SERIAL_AGNT_ID = 0x70;
  // the csm carrier agent serial id
  static const t_byte SERIAL_CAGT_ID = 0x71;
  // the csm delegate agent serial id
  static const t_byte SERIAL_DAGT_ID = 0x72;
  // the csm session serial id
  static const t_byte SERIAL_SESS_ID = 0x75;

  // serialize an integer to an output stream
  static inline void csm_wrlong (const t_long value, OutputStream& os) {
    Integer iobj (value);
    iobj.wrstream (os);
  }

  // deserialize an integer
  static inline t_long csm_rdlong (InputStream& is) {
    Integer iobj;
    iobj.rdstream (is);
    return iobj.tolong ();
  }
}

#endif
