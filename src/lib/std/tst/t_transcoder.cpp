// ---------------------------------------------------------------------------
// - t_transcoder.cpp                                                        -
// - standard object library - transcoder class tester                       -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Transcoder.hpp"

int main (int, char**) {
  using namespace afnix;
  
  // create a transcoder
  Transcoder* coder = new Transcoder;

  // check for the default coder
  if (coder->gettmod () != Encoding::DEFAULT) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-1 codeset
  coder->settmod (Encoding::I8859_01);
  if (coder->gettmod () != Encoding::I8859_01) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-2 codeset
  coder->settmod (Encoding::I8859_02);
  if (coder->gettmod () != Encoding::I8859_02) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-3 codeset
  coder->settmod (Encoding::I8859_03);
  if (coder->gettmod () != Encoding::I8859_03) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-4 codeset
  coder->settmod (Encoding::I8859_04);
  if (coder->gettmod () != Encoding::I8859_04) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-5 codeset
  coder->settmod (Encoding::I8859_05);
  if (coder->gettmod () != Encoding::I8859_05) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-6 codeset
  coder->settmod (Encoding::I8859_06);
  if (coder->gettmod () != Encoding::I8859_06) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-7 codeset
  coder->settmod (Encoding::I8859_07);
  if (coder->gettmod () != Encoding::I8859_07) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-8 codeset
  coder->settmod (Encoding::I8859_08);
  if (coder->gettmod () != Encoding::I8859_08) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-9 codeset
  coder->settmod (Encoding::I8859_09);
  if (coder->gettmod () != Encoding::I8859_09) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-10 codeset
  coder->settmod (Encoding::I8859_10);
  if (coder->gettmod () != Encoding::I8859_10) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-11 codeset
  coder->settmod (Encoding::I8859_11);
  if (coder->gettmod () != Encoding::I8859_11) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // check for the iso-8859-13 codeset
  coder->settmod (Encoding::I8859_13);
  if (coder->gettmod () != Encoding::I8859_13) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }
  // check for the iso-8859-14 codeset
  coder->settmod (Encoding::I8859_14);
  if (coder->gettmod () != Encoding::I8859_14) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }
  // check for the iso-8859-15 codeset
  coder->settmod (Encoding::I8859_15);
  if (coder->gettmod () != Encoding::I8859_15) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }
  // check for the iso-8859-16 codeset
  coder->settmod (Encoding::I8859_16);
  if (coder->gettmod () != Encoding::I8859_16) return 1;
  for (long i = 0; i < 256; i++) {
    char cval = i;
    if (coder->valid (cval) == true) {
      t_quad uval = coder->encode (cval);
      if (coder->valid (uval) == false) return 1;
      if (coder->decode  (uval) != cval)  return 1;
    }
  }

  // delete everything
  delete coder;
  return 0;
}
