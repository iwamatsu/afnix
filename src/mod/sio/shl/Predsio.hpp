// ---------------------------------------------------------------------------
// - Predsio.hpp                                                             -
// - afnix:sio module - predicates declaration                               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDSIO_HPP
#define  AFNIX_PREDSIO_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix:sys
  /// standard module.
  /// @author amaury darsch

  /// the named fifo predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_fifop   (Runnable* robj, Nameset* nset, Cons* args);

  /// the pathname predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_pathp   (Runnable* robj, Nameset* nset, Cons* args);

  /// the pathlist predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_plstp   (Runnable* robj, Nameset* nset, Cons* args);
  
  /// the directory predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_dirp    (Runnable* robj, Nameset* nset, Cons* args);

  /// the file predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_filep   (Runnable* robj, Nameset* nset, Cons* args);

  /// the terminal object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_termp   (Runnable* robj, Nameset* nset, Cons* args);

  /// the intercom object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_icomp (Runnable* robj, Nameset* nset, Cons* args);

  /// the logtee object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_logtp   (Runnable* robj, Nameset* nset, Cons* args);

  /// the directory object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_odirp   (Runnable* robj, Nameset* nset, Cons* args);

  /// the file info predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_infop   (Runnable* robj, Nameset* nset, Cons* args);

  /// the transcoder object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_trscp   (Runnable* robj, Nameset* nset, Cons* args);

  /// the stream object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_strmp   (Runnable* robj, Nameset* nset, Cons* args);

  /// the input object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_inputp  (Runnable* robj, Nameset* nset, Cons* args);

  /// the input file object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_ifilep  (Runnable* robj, Nameset* nset, Cons* args);

  /// the output file object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_ofilep  (Runnable* robj, Nameset* nset, Cons* args);

  /// the input terminal object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_itermp  (Runnable* robj, Nameset* nset, Cons* args);

  /// the output terminal object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_otermp  (Runnable* robj, Nameset* nset, Cons* args);

  /// the selector object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_selectp (Runnable* robj, Nameset* nset, Cons* args); 

  /// the output object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_outputp (Runnable* robj, Nameset* nset, Cons* args); 

  /// the input string object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_inpstrp (Runnable* robj, Nameset* nset, Cons* args); 

  /// the nput map object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_inpmapp (Runnable* robj, Nameset* nset, Cons* args); 

  /// the output string object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_outstrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the output buffer object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_outbufp (Runnable* robj, Nameset* nset, Cons* args);

  /// the input output object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* sio_inpoutp (Runnable* robj, Nameset* nset, Cons* args);
}


#endif
