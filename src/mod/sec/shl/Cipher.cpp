// ---------------------------------------------------------------------------
// - Cipher.cpp                                                              -
// - afnix:sec module - base cipher class implementation                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cipher.hpp"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "Integer.hpp"
#include "QuarkZone.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default cipher

  Cipher::Cipher (void) {
    d_rflg = false;
  }

  // create a cipher by name

  Cipher::Cipher (const String& name) {
    d_name = name;
    d_rflg = false;
  }

  // create a cipher by name and key

  Cipher::Cipher (const String& name, const Key& key) {
    d_name = name;
    d_ckey = key;
    d_rflg = false;
  }

  // return the class name

  String Cipher::repr (void) const {
    return "Cipher";
  }

  // return the cipher name
    
  String Cipher::getname (void) const {
    rdlock ();
    try {
      String result = d_name;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the cipher key

  void Cipher::setkey (const Key& key) {
    wrlock ();
    try {
      d_ckey = key;
      reset  ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the cipher key

  Key Cipher::getkey (void) const {
    rdlock ();
    try {
      Key result = d_ckey;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the cipher reverse flag

  void Cipher::setrflg (const bool rflg) {
    wrlock ();
    try {
      d_rflg = rflg;
      reset  ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // return the cipher reverse flag

  bool Cipher::getrflg (void) const {
    rdlock ();
    bool result = d_rflg;
    unlock ();
    return result;
  }

  // normalize a data size

  t_long Cipher::waist (const t_long size) const {
    rdlock ();
    try {
      t_long result = (size < 0) ? 0 : size;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // stream an input buffer into an output buffer

  long Cipher::stream (Buffer& ob, Buffer& ib) {
    wrlock ();
    try {
      long result = 0L;
      while (ib.empty() == false) {
	result += d_rflg ? decode (ob, ib) : encode (ob, ib);
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // stream an input stream into an output buffer

  long Cipher::stream (Buffer& ob, InputStream& is) {
    wrlock ();
    try {
      long result = d_rflg ? decode (ob, is) : encode (ob, is);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // stream an input stream into an output stream

  long Cipher::stream (OutputStream& os, InputStream& is) {
    wrlock ();
    try {
      long result = d_rflg ? decode (os, is) : encode (os, is);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 6;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_RESET   = zone.intern ("reset");
  static const long QUARK_STREAM  = zone.intern ("stream");
  static const long QUARK_SETKEY  = zone.intern ("set-key");
  static const long QUARK_GETKEY  = zone.intern ("get-key");
  static const long QUARK_SETRFLG = zone.intern ("set-reverse");
  static const long QUARK_GETRFLG = zone.intern ("get-reverse");

  // return true if the given quark is defined

  bool Cipher::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? Nameable::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }

  // apply this object with a set of arguments and a quark
  
  Object* Cipher::apply (Runnable* robj, Nameset* nset, const long quark,
			 Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETKEY)  return new Key (getkey ());
      if (quark == QUARK_GETRFLG) return new Boolean (getrflg ());
      if (quark == QUARK_RESET) {
	reset ();
	return nilp;
      }
    }
    // check for 1 argument
    if (argc == 1) {
      if (quark == QUARK_SETRFLG) {
	bool rflg = argv->getbool (0);
	setrflg (rflg);
	return nilp;
      }
      if (quark == QUARK_SETKEY) {
	Object* obj = argv->get (0);
	Key*    key = dynamic_cast <Key*> (obj);
	if (key != nilp) {
	  setkey (*key);
	  return nilp;
	}
      }
    }
    // check for 2 arguments
    if (argc == 2) {
      if (quark == QUARK_STREAM) {
	// get the first object
	Object* obj = argv->get (0);
	// check for a buffer
	Buffer* ob = dynamic_cast <Buffer*> (obj);
	if (ob != nilp) {
	  // get the second object
	  obj = argv->get (1);
	  // check for a buffer
	  Buffer* ib = dynamic_cast <Buffer*> (obj);
	  if (ib != nilp) return new Integer (stream (*ob, *ib));
	  // check for an input stream
	  InputStream* is = dynamic_cast <InputStream*> (obj);
	  if (is != nilp) return new Integer (stream (*ob, *is));
	  // type error
	  throw Exception ("type-error", "invalid object for cipher stream",
			   Object::repr (obj));
	}
	// check for a stream
	OutputStream* os = dynamic_cast <OutputStream*> (obj);
	if (os != nilp) {
	  // get the second object
	  obj = argv->get (1);
	  // check for an input stream
	  InputStream* is = dynamic_cast <InputStream*> (obj);
	  if (is != nilp) return new Integer (stream (*os, *is));
	  // type error
	  throw Exception ("type-error", "invalid object for cipher stream",
			   Object::repr (obj));
	}
	// invalid object
	throw Exception ("type-error", "invalid object for cipher stream",
			 Object::repr (obj));
      }
    }
    // call the nameable method
    return Nameable::apply (robj, nset, quark, argv);
  }
}
