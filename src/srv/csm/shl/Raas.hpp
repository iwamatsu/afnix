// ---------------------------------------------------------------------------
// - Raas.hpp                                                                -
// - afnix:csm service - registration as a service class definition          -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_RAAS_HPP
#define  AFNIX_RAAS_HPP

#ifndef  AFNIX_PLIST_HPP
#include "Plist.hpp"
#endif

namespace afnix {

  /// The Raas class is an abstract class for registering cloud architecture
  /// services. The class encapsulate the service name ad its activity mode.
  /// Normally, a cloud architecture is divided into the iaas, the paas and
  /// the saas.
  /// @author amaury darsch

  class Raas : public Plist {
  protected:
    /// the activity mode
    bool d_mode;

  public:
    /// create a default service
    Raas (void);

    /// create a service by name
    /// @param name he service name
    Raas (const String& name);

    /// create a service by name and info
    /// @param name he service name
    /// @param info he service info
    Raas (const String& name, const String& info);

    /// create a service by plist
    /// @param plst the service plist
    Raas (const Plist& plst);

    /// copy construct this service
    /// @param that the object to copy
    Raas (const Raas& that);

    /// assign a service to this one
    /// @param that the object to assign
    Raas& operator = (const Raas& that);

    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// set the registration id
    /// @param rid the rid to set
    virtual void setrid (const String& rid);

    /// @return the service registration id
    virtual String getrid (void) const;

    /// set the service active mode
    /// @param mode the service active mode
    virtual void setmode (const bool mode);

    /// @return the service active mode
    virtual bool getmode (void) const;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
