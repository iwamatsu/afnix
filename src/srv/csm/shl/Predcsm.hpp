// ---------------------------------------------------------------------------
// - Predcsm.hpp                                                             -
// - afnix:csm service - predicates declaration                              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDCSM_HPP
#define  AFNIX_PREDCSM_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix:csm
  /// standard service.
  /// @author amaury darsch

  /// the agent object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_agtp (Runnable* robj, Nameset* nset, Cons* args);

  /// the carrier agent object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_cagtp (Runnable* robj, Nameset* nset, Cons* args);

  /// the delegate agent object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_dagtp (Runnable* robj, Nameset* nset, Cons* args);
  
  /// the cart object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_cartp (Runnable* robj, Nameset* nset, Cons* args);

  /// the cart set object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_csetp (Runnable* robj, Nameset* nset, Cons* args);

  /// the session object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_sessp (Runnable* robj, Nameset* nset, Cons* args);

  /// the session set object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_ssetp (Runnable* robj, Nameset* nset, Cons* args);

  /// the slot object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_slotp (Runnable* robj, Nameset* nset, Cons* args);

  /// the appointer object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_apptp (Runnable* robj, Nameset* nset, Cons* args);

  /// the assistant object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_asstp (Runnable* robj, Nameset* nset, Cons* args);

  /// the workspace object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_wspcp (Runnable* robj, Nameset* nset, Cons* args);

  /// the localspace object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_lspcp (Runnable* robj, Nameset* nset, Cons* args);

  /// the raas object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_raasp (Runnable* robj, Nameset* nset, Cons* args);

  /// the saas object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* csm_saasp (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
