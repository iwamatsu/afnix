# ---------------------------------------------------------------------------
# - CSM0050.als                                                             -
# - afnix:csm service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   agent test unit
# @author amaury darsch

# get the service
interp:library "afnix-csm"

# create a nil agent
trans agt (afnix:csm:Agent)

# check predicate and representation
assert true  (afnix:csm:agent-p agt)
assert "Agent" (agt:repr)

# create an agent by name and info
trans  agt (afnix:csm:Agent "ONE" "ONE DIGIT")
assert ""    (agt:get-rid)
assert "ONE" (agt:get-name)
assert "ONE DIGIT" (agt:get-info)

# test the rid
agt:set-rid "RID"
assert true  (agt:rid-p "RID")
assert "RID" (agt:get-rid)

