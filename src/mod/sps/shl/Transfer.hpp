// ---------------------------------------------------------------------------
// - Transfer.hpp                                                            -
// - afnix:sps module - sps objects transfer class definition                -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TRANSFER_HPP
#define  AFNIX_TRANSFER_HPP

#ifndef  AFNIX_FOLIO_HPP
#include "Folio.hpp"
#endif

namespace afnix {

  /// The Transfer class is a base class for the spreadsheet importation and
  /// exportation process. The class mostly defines the import and export
  /// methods which are used to transfer data from a record, sheet or folio.
  /// @author amaury darch

  class Transfer : public virtual Object {
  public:
    /// the transfer format
    enum t_xfer {
      XFER_CSV, // csv format
      XFER_SPS  // afnix format
    };

  protected:
    /// the transfer format
    t_xfer d_xfer;
    /// the break sequence
    String d_sbrk;
    /// the locale vector
    Vector d_locv;
    
  public:
    /// create a default transfer
    Transfer (void);

    /// create a transfer by format
    /// @param xfmt the transfer format
    /// @param sbrk the break sequence
    Transfer (const String& xfmt, const String& sbrk);

    /// set the transfer format
    /// @param xfmt the transfer format
    virtual void setxfmt (const String& xfmt);

    /// set the break sequence
    /// @param sbrk the break sequence
    virtual void setsbrk (const String& sbrk);

    /// add a locale definition
    /// @param ldef the locale descriptor
    virtual void addldef (const String& ldef);
    
    /// import cons data into the record
    /// @param cons the cons cell to use
    virtual void import (Cons* cons) =0;

    /// import stream data into the record
    /// @param is the input stream to use
    virtual void import (InputStream* is);

    /// import data from a file
    /// @param name the input file to use
    virtual void import (const String& name);

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv); 
  };
  
  /// The RecordTransfer class is the class that permits to import/export
  /// data into or from a record. The importation process is done by
  /// accumulating object from a form.
  /// @author amaury darsch

  class RecordTransfer : public Transfer {
  protected:
    /// the transfer record
    Record* p_trcd;

  public:
    /// create an empty transfer
    RecordTransfer (void);

    /// create a transfer with a record
    /// @param rcd the transfer record
    RecordTransfer (Record* rcd);

    /// create a transfer by format
    /// @param xfmt the transfer format
    /// @param sbrk the break sequence
    RecordTransfer (const String& xfmt, const String& sbrk);

    /// create a transfer with a record and format
    /// @param rcd the transfer record
    /// @param xfmt the transfer format
    /// @param sbrk the break sequence
    RecordTransfer (Record* rcd, const String& xfmt, const String& sbrk);

    /// destroy this transfer
    ~RecordTransfer (void);

    /// @return the object name
    String repr (void) const;

    /// import cons data into the record
    /// @param cons the cons cell to use
    void import (Cons* cons);

    /// @return the transfer record
    virtual Record* getrcd (void) const;

    /// set the transfer record
    /// @param rcd the transfer record to set
    virtual void setrcd (Record* rcd);

  private:
    // make the copy constructor private
    RecordTransfer (const RecordTransfer&) =delete;
    // make the assignment operator private
    RecordTransfer& operator = (const RecordTransfer&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };

  /// The SheetTransfer class is a base class that permits to import/export
  /// data into a sheet. The importation process is done by accumulating
  /// data forms into a record.
  /// @author amaury darsch

  class SheetTransfer : public Transfer {
  protected:
    /// the transfer sheet
    Sheet* p_tsht;

  public:
    /// create an empty transfer
    SheetTransfer (void);

    /// create a transfer with a sheet
    /// @param sht the transfer sheet
    SheetTransfer (Sheet* sht);

    /// create a transfer by format
    /// @param xfmt the transfer format
    /// @param sbrk the break sequence
    SheetTransfer (const String& xfmt, const String& sbrk);

    /// create a transfer with a sheet and format
    /// @param sht the transfer sheet
    /// @param xfmt the transfer format
    /// @param sbrk the break sequence
    SheetTransfer (Sheet* sht, const String& xfmt, const String& sbrk);

    /// destroy this transfer
    ~SheetTransfer (void);

    /// @return the object name
    String repr (void) const;

    /// import cons data into the sheet
    /// @param cons the cons cell to use
    void import (Cons* cons);

    /// @return the transfer sheet
    virtual Sheet* getsht (void) const;

    /// set the transfer sheet
    /// @param sht the sheet to set
    virtual void setsht (Sheet* sht);

    /// acquire the transfer sheet
    virtual Sheet* acquire (void);

  private:
    // make the copy constructor private
    SheetTransfer (const SheetTransfer&) =delete;
    // make the assignment operator private
    SheetTransfer& operator = (const SheetTransfer&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
