// ---------------------------------------------------------------------------
// - Predtls.hpp                                                             -
// - afnix:tls service - predicates declaration                              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDTLS_HPP
#define  AFNIX_PREDTLS_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix
  /// tls service.
  /// @author amaury darsch
 
  /// the tls block object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_blkp (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls header object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_hdrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls record object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_recp (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls message object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_msgp (Runnable* robj, Nameset* nset, Cons* args);
 
  /// the tls handshake message object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_shkp (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls state object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_stap (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls parameters object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_prmp (Runnable* robj, Nameset* nset, Cons* args);
  
  /// the tls connect object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_conp (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls dupleix object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_dpxp (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls server object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_srvp (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls server hello object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_shlop (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls client hello object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_chlop (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls suite object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_suitep (Runnable* robj, Nameset* nset, Cons* args);

  /// the tls certificate list object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_certsp (Runnable* robj, Nameset* nset, Cons* args);
  
  /// the X509 algorithm object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_xalgop (Runnable* robj, Nameset* nset, Cons* args);

  /// the pkcs object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_pkcsp (Runnable* robj, Nameset* nset, Cons* args);
  
  /// the pkcs1 object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_pkcs1p (Runnable* robj, Nameset* nset, Cons* args);
  
  /// the X509 certificate object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* tls_x509p (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
