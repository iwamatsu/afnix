// ---------------------------------------------------------------------------
// - CartSet.hpp                                                             -
// - afnix:csm service - cart set class definition                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CARTSET_HPP
#define  AFNIX_CARTSET_HPP

#ifndef  AFNIX_CART_HPP
#include "Cart.hpp"
#endif

#ifndef  AFNIX_HASHTABLE_HPP
#include "HashTable.hpp"
#endif

namespace afnix {

  /// The CartSet class is a collection of registered cart. In the presence
  /// of a large set of registered objects, it is necessary to store them in
  /// a set. The cart set is implemented with a hash table, those objects are
  /// registered cart. A control flag is used to decide if an anonymous cart
  /// can be managed.
  /// @author amaury darsch

  class CartSet : public virtual Object {
  protected:
    /// the cart hashtable
    HashTable d_lht;
    /// the anonymous flag
    bool d_acf;
    /// the anonymous cart
    Cart d_aco;

  public:
    /// create an empty cart set
    CartSet (void);

    /// create an anonymous cart set
    /// @param acf the anonymous control flag
    CartSet (const bool acf);

    /// @return the class name
    String repr (void) const;

    /// reset this cart set
    virtual void reset (void);

    /// @return the number of cart
    virtual long length (void) const;

    /// @return true if the cart set is empty
    virtual bool empty (void) const;

    /// check if a cart exists by rid
    /// @param rid the cart rid to validate
    virtual bool exists (const String& rid) const;

    /// get a cart by index
    /// @param idx the cart index
    virtual Cart* get (const long idx) const;

    /// locate a cart by rid
    /// @param rid the cart rid to find
    virtual Cart* lookup (const String& rid) const;

    /// add a agent in this cart set
    /// @param agt the agent to add
    virtual t_long addagt (Agent* agt);

    /// get a agent by key and registration id 
    /// @param rid the cart rid
    /// @param kid the agent key id
    virtual Agent* getagt (const String& rid, const t_long kid) const;

  private:
    // make the copy constructor private
    CartSet (const CartSet&);
    // make the assignment operator private
    CartSet& operator = (const CartSet&);

  public:
    /// create a object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
