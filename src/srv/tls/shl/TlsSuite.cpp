// ---------------------------------------------------------------------------
// - TlsSuite.cpp                                                            -
// - afnix:tls service - tls cipher suite class implementation               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Aes.hpp"
#include "Rc4.hpp"
#include "Md5.hpp"
#include "Sha1.hpp"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "Integer.hpp"
#include "Utility.hpp"
#include "TlsSuite.hxx"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // the cipher list definition
  static const TlsSuite::s_cinfo TLS_LIST_CFR[] = {
    // 0x00, 0x00
    {
      "TLS_NULL_WITH_NULL_NULL", "2246", "1.0", "",
      TLS_NULL_WITH_NULL_NULL,
      TlsSuite::TLS_EXCH_NIL, 
      TlsSuite::TLS_CIFR_NIL, TlsSuite::TLS_CMOD_NIL, 0L, 0L, 0L,
      TlsSuite::TLS_HASH_NIL, 0L, false, false
    },
    // 0x00, 0x01
    {
      "TLS_RSA_WITH_NULL_MD5", "2246", "1.0", "",
      TLS_RSA_WITH_NULL_MD5,
      TlsSuite::TLS_EXCH_RSA, 
      TlsSuite::TLS_CIFR_NIL, TlsSuite::TLS_CMOD_NIL, 0L, 0L, 0L,
      TlsSuite::TLS_HASH_MD5, 16L, true, false
    },
    // 0x00, 0x02
    {
      "TLS_RSA_WITH_NULL_SHA", "2246", "1.0", "",
      TLS_RSA_WITH_NULL_SHA,
      TlsSuite::TLS_EXCH_RSA, 
      TlsSuite::TLS_CIFR_NIL, TlsSuite::TLS_CMOD_NIL, 0L, 0L, 0L,
      TlsSuite::TLS_HASH_SHA, 20L, true, false
    },
    // 0x00, 0x04
    {
      "TLS_RSA_WITH_RC4_128_MD5", "2246", "1.0", "",
      TLS_RSA_WITH_RC4_128_MD5,
      TlsSuite::TLS_EXCH_RSA, 
      TlsSuite::TLS_CIFR_RC4, TlsSuite::TLS_CMOD_NIL, 16L, 0L, 0L,
      TlsSuite::TLS_HASH_MD5, 16L, true, false
    },
    // 0x00, 0x05
    {
      "TLS_RSA_WITH_RC4_128_SHA", "2246", "1.0", "",
      TLS_RSA_WITH_RC4_128_SHA,
      TlsSuite::TLS_EXCH_RSA, 
      TlsSuite::TLS_CIFR_RC4, TlsSuite::TLS_CMOD_NIL, 16L, 0L, 0L,
      TlsSuite::TLS_HASH_SHA, 20L, true, false
    },
    // 0x00, 0x2F
    {
      "TLS_RSA_WITH_AES_128_CBC_SHA", "3268", "1.0", "",
      TLS_RSA_WITH_AES_128_CBC_SHA,
      TlsSuite::TLS_EXCH_RSA, 
      TlsSuite::TLS_CIFR_AES, TlsSuite::TLS_CMOD_CBC, 16L, 16L, 16L,
      TlsSuite::TLS_HASH_SHA, 20L, true, false
    },
    // 0x00, 0x35
    {
      "TLS_RSA_WITH_AES_256_CBC_SHA", "3268", "1.0", "",
      TLS_RSA_WITH_AES_256_CBC_SHA,
      TlsSuite::TLS_EXCH_RSA, 
      TlsSuite::TLS_CIFR_AES, TlsSuite::TLS_CMOD_CBC, 32L, 16L, 16L,
      TlsSuite::TLS_HASH_SHA, 20L, true, false
    }
  };
  // the cipher list size
  static const long TLS_SIZE_CFR = 
    sizeof (TLS_LIST_CFR) / sizeof (TlsSuite::s_cinfo);

  // find a cipher info structure index by code
  static inline long tls_find_cinfo (const t_word code) {
    for (long k = 0L; k < TLS_SIZE_CFR; k++) {
      if (TLS_LIST_CFR[k].d_code == code) return k;
    }
    // not found
    return -1;
  }

  // fill a property list with a cipher info by index
  static Plist& tls_add_plist (Plist& plst, const TlsSuite::s_cinfo& cinf) {
    // get name and info
    String name = cinf.d_name;
    String info = cinf.d_rfcr + ':' + cinf.d_vmin + ':' + cinf.d_vmax;
    String scod = TlsSuite::toscod (cinf.d_code);
    plst.add (name, info, scod);
    return plst;
  }

  // get a cipher info print table
  static PrintTable* tls_get_table (void) {
    // create a five columns table
    PrintTable* ptbl = new PrintTable (5);
    // set the header
    ptbl->sethead (0, "NAME");
    ptbl->sethead (1, "RFC");
    ptbl->sethead (2, "MINV");
    ptbl->sethead (3, "MAXV");
    ptbl->sethead (4, "CODE");
    // loop and fill table
    for (long k = 0L; k < TLS_SIZE_CFR; k++) {
      long row = ptbl->add ();
      ptbl->set (row, 0, TLS_LIST_CFR[k].d_name);
      ptbl->set (row, 1, TLS_LIST_CFR[k].d_rfcr);
      ptbl->set (row, 2, TLS_LIST_CFR[k].d_vmin);
      ptbl->set (row, 3, TLS_LIST_CFR[k].d_vmax);
      ptbl->set (row, 4, TlsSuite::toscod (TLS_LIST_CFR[k].d_code));
    }
    return ptbl;
  }

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // create a default cipher info structure

  TlsSuite::s_cinfo::s_cinfo (void) {
    d_name = "TLS_NULL_WITH_NULL_NULL";
    d_rfcr = "2246";
    d_vmin = "1.0";
    d_vmax = "";
    d_code = 0x0000U;
    d_exch = TLS_EXCH_NIL;
    d_cifr = TLS_CIFR_NIL;
    d_cmod = TLS_CMOD_NIL;
    d_ksiz = 0L;
    d_bsiz = 0L;
    d_vsiz = 0L;
    d_hash = TLS_HASH_NIL;
    d_hsiz = 0L;
    d_cert = false;
    d_kxch = false;
  }

  // create a cipher with a brace initializer list

  TlsSuite::s_cinfo::s_cinfo (const String& name, const String& rfcr, 
			      const String& vmin, const String& vmax, 
			      const t_word  code, const t_exch  exch,
			      const t_cifr  cifr, const t_cmod  cmod,
			      const long    ksiz, const long    bsiz,
			      const long    vsiz, const t_hash  hash,
			      const long    hsiz, const bool    cert,
			      const bool    kxch) {
    d_name = name;
    d_rfcr = rfcr;
    d_vmin = vmin;
    d_vmax = vmax;
    d_code = code;
    d_exch = exch;
    d_cifr = cifr;
    d_cmod = cmod;
    d_ksiz = ksiz;
    d_bsiz = bsiz;
    d_vsiz = vsiz;
    d_hash = hash;
    d_hsiz = hsiz;
    d_cert = cert;
    d_kxch = kxch;
  }
  
  // copy construct this cipher info

  TlsSuite::s_cinfo::s_cinfo (const s_cinfo& that) {
    d_name = that.d_name;
    d_rfcr = that.d_rfcr;
    d_vmin = that.d_vmin;
    d_vmax = that.d_vmax;
    d_code = that.d_code;
    d_exch = that.d_exch;
    d_cifr = that.d_cifr;
    d_cmod = that.d_cmod;
    d_ksiz = that.d_ksiz;
    d_bsiz = that.d_bsiz;
    d_vsiz = that.d_vsiz;
    d_hash = that.d_hash;
    d_hsiz = that.d_hsiz;
    d_cert = that.d_cert;
    d_kxch = that.d_kxch;
  }

  // assign a cipher info to this one

  TlsSuite::s_cinfo& TlsSuite::s_cinfo::operator = (const s_cinfo& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // assign localy
    d_name = that.d_name;
    d_rfcr = that.d_rfcr;
    d_vmin = that.d_vmin;
    d_vmax = that.d_vmax;
    d_code = that.d_code;
    d_exch = that.d_exch;
    d_cifr = that.d_cifr;
    d_cmod = that.d_cmod;
    d_ksiz = that.d_ksiz;
    d_bsiz = that.d_bsiz;
    d_vsiz = that.d_vsiz;
    d_hash = that.d_hash;
    d_hsiz = that.d_hsiz;
    d_cert = that.d_cert;
    d_kxch = that.d_kxch;
    return *this;
  }

  // create a cipher by key buffer

  Cipher* TlsSuite::s_cinfo::getcipher (const bool   rflg, const Buffer& kbuf,
					const Buffer& kiv) const {
    // the cipher result
    Cipher* result = nilp;
    switch (d_cifr) {
    case TLS_CIFR_NIL:
      break;
    case TLS_CIFR_AES:
	result = new Aes (kbuf, rflg);
	break;
    case TLS_CIFR_RC4:
	result = new Rc4 (kbuf, rflg);
	break;
    };
    // set the block cipher mode
    BlockCipher* bc = dynamic_cast <BlockCipher*> (result);
    if (bc != nilp) {
      switch (d_cmod) {
      case TLS_CMOD_NIL:
	bc->setcmod (BlockCipher::CBM_ECBM);
	break;
      case TLS_CMOD_CBC:
	bc->setcmod (BlockCipher::CBM_CBCM);
	break;
      };
      // force the padding to nist mode
      bc->setpmod (BlockCipher::CPM_N800);
      // set the initial vector
      bc->setiv (kiv);
    }
    // here it is
    return result;
  }

  // get a mac by key buffer

  Hmac* TlsSuite::s_cinfo::gethmac (const Buffer& kbuf) const {
    // the hmac result
    Hmac* result = nilp;
    switch (d_hash) {
    case TLS_HASH_NIL:
      break;
    case TLS_HASH_MD5:
      result = new Hmac (Key (Key::KMAC, kbuf), new Md5);
	break;
    case TLS_HASH_SHA:
      result = new Hmac (Key (Key::KMAC, kbuf), new Sha1);
	break;
    };
    return result;
  }
  
  // convert a cipher code to a word
  
  t_word TlsSuite::toword (const t_byte ucod, const t_byte lcod) {
    t_word result = ucod;
    result = result << 8; result += lcod;
    return result;
  }

  // convert a tls suite code to a upper code

  t_byte TlsSuite::toucod (const t_word code) {
    t_byte result = (t_byte) (code >> 8);
    return result;
  }

  // convert a tls suite code to a upper code

  t_byte TlsSuite::tolcod (const t_word code) {
    t_byte result = (t_byte) (code & 0x00FFU);
    return result;
  }

  // convert a tls suite code to a string
  String TlsSuite::toscod (const t_word code) {
    t_byte ucod = (t_byte) (code >> 8);
    t_byte lcod = (t_byte) (code & 0x00FF);
    String sval = Utility::tohexa (ucod, true, true) + ", " +
      Utility::tohexa (lcod, true, true);
    return sval;
  }

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default suite

  TlsSuite::TlsSuite (void) {
    d_size = 0L;
    d_slen = 0L;
    p_slst = nilp;
  }

  // create a suite by size

  TlsSuite::TlsSuite (const long size) {
    if (size < 0L) {
      throw Exception ("tls-error", "invalid suite size");
    }
    d_size = size;
    d_slen = 0L;
    p_slst = (d_size == 0L) ? nilp : new t_word[d_size];
    reset ();
  }

  // destroy this suite

  TlsSuite::~TlsSuite (void) {
    reset ();
    delete [] p_slst;
  }

  // return the class name
  
  String TlsSuite::repr (void) const {
    return "TlsSuite";
  }

  // reset the 

  void TlsSuite::reset (void) {
    wrlock ();
    try {
      d_slen = 0L;
      for (long k = 0L; k < d_size; k++) p_slst[k] = nilw;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the cipher list info as a plist

  Plist TlsSuite::getinfo (void) const {
    rdlock ();
    try {
      // create a result plist
      Plist plst;
      // loop in the list
      for (long k = 0L; k < d_slen; k++) {
	if (isvalid (p_slst[k]) == true) {
	  tls_add_plist (plst, getcinfo (p_slst[k]));
	}
      }
      // here it is
      unlock ();
      return plst;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the number of ciphers in the list

  long TlsSuite::length (void) const {
    rdlock ();
    try {
      long result = d_slen;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // pop the first valid cipher code

  t_word TlsSuite::pop (void) const {
    rdlock ();
    try {
      for (long k = 0L; k < d_slen; k++) {
	if (isvalid (p_slst[k]) == true) {
	  t_word result = p_slst[k];
	  unlock ();
	  return result;
	}
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
    throw Exception ("tls-error", "cannot find any valid cipher");
  }

  // get a tls suite code by index

  t_word TlsSuite::get (const long idx) const {
    rdlock ();
    try {
      if ((idx < 0L) || (idx >= d_slen)) {
	throw Exception ("tls-error", "invalid tls suite index");
      }
      t_word result = p_slst[idx];
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add a new cipher in the list by code

  void TlsSuite::add (const t_word code) {
    wrlock ();
    try {
      // check size invariant
      if (d_slen > d_size) {
	throw Exception ("internal-error", "inconsisten tls suite size");
      }
      // check for resize
      if (d_slen == d_size) {
	long    size = (d_size == 0L) ? 1L : 2L * d_size;
	t_word* slst = new t_word[size];
	for (long k = 0;      k < d_size; k++) slst[k] = p_slst[k];
	for (long k = d_size; k < size;   k++) slst[k] = nilw;
	d_size = size;
	delete [] p_slst; p_slst = slst;
      }
      // here it is
      p_slst[d_slen++] = code;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add a new cipher in the list by tls code
  
  void TlsSuite::add (const t_byte ucod, const t_byte lcod) {
    wrlock ();
    try {
      // convert into a word code
      t_word code = TlsSuite::toword (ucod, lcod);
      // and now push it
      add (code);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // check if a cipher is valid by code

  bool TlsSuite::isvalid (const t_word code) const {
    rdlock ();
    try {
      // find a standard structure info index by code
      long cidx = tls_find_cinfo (code);
      // format result
      bool result = (cidx == -1) ? false : true;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // check if a cipher is valid by code
  
  bool TlsSuite::isvalid (const t_byte ucod, const t_byte lcod) const {
    rdlock ();
    try {
      // convert into a word code
      t_word code = TlsSuite::toword (ucod, lcod);
      // and now test for it
      bool result = isvalid (code);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // check if a cipher needs a certificate

  bool TlsSuite::iscert (const t_word code) const {
    rdlock ();
    try {
      // find a standard structure info index by code
      long cidx = tls_find_cinfo (code);
      if (cidx == -1L) {
	unlock ();
	return false;
      }
      // get the cipher info
      TlsSuite::s_cinfo cnfo = TLS_LIST_CFR[cidx];
      unlock ();
      return cnfo.d_cert;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // check if a cipher needs a key exchange

  bool TlsSuite::iskxch (const t_word code) const {
    rdlock ();
    try {
      // find a standard structure info index by code
      long cidx = tls_find_cinfo (code);
      if (cidx == -1L) {
	unlock ();
	return false;
      }
      // get the cipher info
      TlsSuite::s_cinfo cnfo = TLS_LIST_CFR[cidx];
      unlock ();
      return cnfo.d_kxch;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // find a cipher info structure by code

  TlsSuite::s_cinfo TlsSuite::getcinfo (const t_word code) const {
    rdlock ();
    try {
      // find a standard structure info index by code
      long cidx = tls_find_cinfo (code);
      if (cidx == -1) {
	throw Exception ("tls-error", "cannot find cipher info with code",
			 Utility::tohexa (code, true, true));
      }
      TlsSuite::s_cinfo result = TLS_LIST_CFR[cidx];
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // find a cipher info structure by tls code

  TlsSuite::s_cinfo TlsSuite::getcinfo (const t_byte ucod, 
					const t_byte lcod) const {
    rdlock ();
    try {
      // convert into a word code
      t_word code = TlsSuite::toword (ucod, lcod);
      // and get the cipher info
      TlsSuite::s_cinfo result = getcinfo (code);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get cipher suite as a print table

  PrintTable* TlsSuite::gettinfo (void) const {
    rdlock ();
    try {
      PrintTable* result = tls_get_table ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a cipher by code and key buffer

  Cipher* TlsSuite::getcipher (const t_word  cifr, const bool   rflg,
			       const Buffer& kbuf, const Buffer& kiv) const {
    rdlock ();
    try {
      // locate the cipher info
      s_cinfo cinfo = getcinfo (cifr);
      // create the cipher result
      Cipher* result = cinfo.getcipher (rflg, kbuf, kiv);
      // force the padding to none with block cipher
      auto* bc = dynamic_cast<BlockCipher*> (result);
      if (bc != nilp) bc->setpmod (BlockCipher::CPM_NONE);
      // here it is
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a hmac by code and key buffer

  Hmac* TlsSuite::gethmac (const t_word  cifr, const Buffer& kbuf) const {
    rdlock ();
    try {
      // locate the cipher info
      s_cinfo cinfo = getcinfo (cifr);
      // create the hmac result
      Hmac* result = cinfo.gethmac (kbuf);
      // here it is
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 3;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_LENGTH   = zone.intern ("length");
  static const long QUARK_VALIDP   = zone.intern ("valid-p");
  static const long QUARK_GETTINFO = zone.intern ("get-info-table");

  // create a new object in a generic way

  Object* TlsSuite::mknew (Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) return new TlsSuite;
    // check for 1 argument
    if (argc == 1) {
      long size = argv->getlong (0);
      return new TlsSuite (size);
    }
    // too many arguments
    throw Exception ("argument-error", 
                     "too many argument with tls cipher suite");
  }

  // return true if the given quark is defined

  bool TlsSuite::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? TlsInfos::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }
  
  // apply this object with a set of arguments and a quark

  Object* TlsSuite::apply (Runnable* robj, Nameset* nset, const long quark,
			   Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_LENGTH)   return new Integer (length ());
      if (quark == QUARK_GETTINFO) return gettinfo ();
    }
    // check for 1 argument
    if (argc == 1) {
      if (quark == QUARK_VALIDP) {
	t_word code = argv->getword (0);
	return new Boolean (isvalid (code));
      }
    }
    // call the tls infos method
    return TlsInfos::apply (robj, nset, quark, argv);
  }
}
