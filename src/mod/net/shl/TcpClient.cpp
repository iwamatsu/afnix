// ---------------------------------------------------------------------------
// - TcpClient.cpp                                                           -
// - afnix:net module - tcp client socket implementation                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "TcpClient.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a tcp client by host and port

  TcpClient::TcpClient (const String& host, t_word port) : TcpSocket (false) {
    // get the host address
    Address addr (host);
    // create the socket
    create (addr);
    // connect with the server
    if (connect (port, addr, true) == false)
      throw Exception ("client-error", "cannot connect socket");
  }

  // create a tcp client by ip address and port

  TcpClient::TcpClient (const Address& addr, t_word port) : TcpSocket (false) {
    // create the socket
    create (addr);
    // connect with the server
    if (connect (port, addr, true) == false)
      throw Exception ("client-error", "cannot connect socket");
  }

  // return the class name

  String TcpClient::repr (void) const {
    return "TcpClient";
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // create a new object in a generic way

  Object* TcpClient::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    if (argc != 2) 
      throw Exception ("argument-error", 
                       "invalid arguments with with tcp client"); 
    // get the tcp client arguments
    Object* obj = argv->get (0);
    if (dynamic_cast <String*> (obj) != nilp) {
      String host = argv->getstring (0);
      long   port = argv->getlong (1);
      return new TcpClient (host, port);
    }
    Address* addr = dynamic_cast <Address*> (obj);
    if (addr != nilp) {
      long   port = argv->getlong (1);
      return new TcpClient (*addr, port);
    }
    throw Exception ("argument-error", "invalid object with tcp client",
		     Object::repr (obj));
  }
}
