// ---------------------------------------------------------------------------
// - Promise.cpp                                                             -
// - afnix engine - promise class implementation                             -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Engsid.hxx"
#include "Promise.hpp"
#include "Boolean.hpp"
#include "Runnable.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // this procedure returns a new promise object for deserialization
  static Serial* mksob (void) {
    return new Promise;
  }

  // register this qualified serial id
  static const t_byte SERIAL_ID = Serial::setsid (SERIAL_PMIZ_ID, mksob);

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default promise

  Promise::Promise (void) {
    p_form   = nilp;
    p_object = nilp;
    d_delay  = true;
  }

  // create a new promise with a form

  Promise::Promise (Object* form) {
    p_form   = Object::iref (form);
    p_object = nilp;
    d_delay  = true;
  }

  // destroy this promise

  Promise::~Promise (void) {
    Object::dref (p_form);
    Object::dref (p_object);
  }

  // return the class name

  String Promise::repr (void) const {
    return "Promise";
  }

  // return the promise serial code

  t_byte Promise::serialid (void) const {
    return SERIAL_ID;
  }

 // serialize this promise

  void Promise::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      // serialize the form
      if (p_form == nilp) {
	Serial::wrnilid (os);
      } else {
	Serial* sobj = dynamic_cast <Serial*> (p_form);
	if (sobj == nilp) {
	  throw Exception ("serial-error", "cannot serialize object", 
			   p_form->repr ());
	}
	sobj->serialize (os);
      }
      // serialize the evaluated object
      if (p_object == nilp) {
	Serial::wrnilid (os);
      } else {
	Serial* sobj = dynamic_cast <Serial*> (p_object);
	if (sobj == nilp) {
	  throw Exception ("serial-error", "cannot serialize object", 
			   p_object->repr ());
	}
	sobj->serialize (os);
      }
      // serialize the delay flag
      Boolean bval = d_delay;
      bval.wrstream (os);
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this promise

  void Promise::rdstream (InputStream& is) {
    wrlock ();
    try {
      // clean the promise
      Object::dref (p_form);
      Object::dref (p_object);
      // get the form
      p_form = Serial::deserialize (is);
      // get the object
      p_object = Serial::deserialize (is);
      // get the delay flag
      Boolean bval;
      bval.rdstream (is);
      d_delay = bval.tobool ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // force the evaluation of this promise

  Object* Promise::force (Runnable* robj, Nameset* nset) {
    wrlock ();
    try {
      if (d_delay == true) {
	p_object = (p_form == nilp) ? nilp : p_form->eval (robj, nset);
	Object::iref (p_object);
	d_delay  = false;
      }
      robj->post (p_object);
      unlock ();
      return p_object;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // evaluate this promise

  Object* Promise::eval (Runnable* robj, Nameset* nset) {
    rdlock ();
    try {
      Object* result = d_delay ? this : p_object;
      robj->post (result);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
}
