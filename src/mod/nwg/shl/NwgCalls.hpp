// ---------------------------------------------------------------------------
// - NwgCalls.hpp                                                            -
// - afnix:nwg module - specific calls definitions                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_NWGCALLS_HPP
#define  AFNIX_NWGCALLS_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains specific system procedures that require only
  /// arguments and that are not bounded to a class
  /// @author amaury darsch

  /// check if a mime extension is defined
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* nwg_mextp (Runnable* robj, Nameset* nset, Cons* args);

  /// check if a mime value is defined
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* nwg_mvalp (Runnable* robj, Nameset* nset, Cons* args);

  /// check for a valid xml mime value
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* nwg_xmlmp (Runnable* robj, Nameset* nset, Cons* args);

  /// get a mime value by extension
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* nwg_tomime (Runnable* robj, Nameset* nset, Cons* args);

  /// check for a valid uri string
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* nwg_surip (Runnable* robj, Nameset* nset, Cons* args);

  /// normalize a uri name by adding a missing scheme if any
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* nwg_nrmunm (Runnable* robj, Nameset* nset, Cons* args);

  /// normalize a uri name by prioritizing the system path
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* nwg_sysunm (Runnable* robj, Nameset* nset, Cons* args);

  /// normalize a path by uri name
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* nwg_pthunm (Runnable* robj, Nameset* nset, Cons* args);
}


#endif
