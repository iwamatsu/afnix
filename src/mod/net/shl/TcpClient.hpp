// ---------------------------------------------------------------------------
// - TcpClient.hpp                                                           -
// - afnix:net module - tcp client socket class definition                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TCPCLIENT_HPP
#define  AFNIX_TCPCLIENT_HPP

#ifndef  AFNIX_TCPSOCKET_HPP
#include "TcpSocket.hpp"
#endif

namespace afnix {

  /// The TcpClient class is a socket class used by a client object. A Tcp
  /// client is created by specifiying the server address and port. Once the
  /// client socket is created, the read and write methods can be used.
  /// @author amaury darsch

  class TcpClient : public TcpSocket {
  public:
    /// create a tcp socket and connect to the specified host.
    /// @param host the host name to connect
    /// @param port the port to listen
    TcpClient (const String& host, t_word port);

    /// create a tcp socket and connect to the specified host.
    /// @param addr the ip address to connect
    /// @param port the port to listen
    TcpClient (const Address& addr, t_word port);

    /// @return the class name
    String repr (void) const;
  
  private:
    // make the copy constructor private
    TcpClient (const TcpClient&);
    // make the assignment operator private
    TcpClient& operator = (const TcpClient&);

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
  };
}

#endif
