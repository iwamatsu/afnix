# ---------------------------------------------------------------------------
# - GEO0009.als                                                             -
# - afnix:geo service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   quaternion test unit
# @author amaury darsch

# get the service
interp:library "afnix-geo"

# create a quaternion object
trans q (afnix:geo:Quaternion)
assert true (afnix:geo:quaternion-p q)
assert "Quaternion" (q:repr)

# set by value and check
q:set 1.0 2.0 3.0 4.0
assert 1.0 (q:get-x)
assert 2.0 (q:get-y)
assert 3.0 (q:get-z)
assert 4.0 (q:get-w)

# set by axis and angle
const a 45.0
q:set-rotation-z a
assert true (a:?= (q:get-angle))
# create a vector to rotate
trans v (afnix:geo:Vector3 1.0 0.0 0.0)
# rotate and check
trans r (q:rotate v)
trans n (r:norm)
trans l 0.707106781
assert true (l:?= (r:get-x))
assert true (l:?= (r:get-y))
assert true (n:?= 1.0)
