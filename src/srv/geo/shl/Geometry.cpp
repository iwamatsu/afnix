// ---------------------------------------------------------------------------
// - Geometry.cpp                                                            -
// - afnix:geo service - base geometry class implementation                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Geometry.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // get the geometry uuid

  String Geometry::getuuid (void) const {
    rdlock ();
    try {
      String result = d_uuid;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 4;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_RESET   = zone.intern ("reset");
  static const long QUARK_GETUUID = zone.intern ("get-uuid");
  static const long QUARK_SETPLST = zone.intern ("set-plist");
  static const long QUARK_GETPLST = zone.intern ("get-plist");

  // return true if the given quark is defined
  
  bool Geometry::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Nameable::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark

  Object* Geometry::apply (Runnable* robj, Nameset* nset, const long quark,
			Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETUUID)   return new String (getuuid ());
      if (quark == QUARK_GETPLST)   return new Plist  (getplst ());
      if (quark == QUARK_RESET) {
	reset ();
	return nilp;
      }
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_SETPLST) {
	Object* obj = argv->get (0);
        Plist* alst = dynamic_cast <Plist*> (obj);
        if ((obj != nilp) && (alst == nilp)) {
          throw Exception ("type-error", "invalid object with set-plist",
                           Object::repr (obj));
        }
        setplst (*alst);
        return nilp;
      }
    }
    // call the nameable method
    return Nameable::apply (robj, nset, quark, argv);
  }
}
