// ---------------------------------------------------------------------------
// - Point1.hpp                                                              -
// - afnix:geo service - point 1 class definition                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_POINT1_HPP
#define  AFNIX_POINT1_HPP

#ifndef  AFNIX_RPOINT_HPP
#include "Rpoint.hpp"
#endif

namespace afnix {
  
  /// The Point1 class is a 1 dimensional point class for the afnix geometry
  /// service. A 1 dimensional point is defined by its (x) coordinate.
  /// @author amaury darsch

  class Point1 : public Rpoint {
  public:
    /// create a default point
    Point1 (void);

    /// create a point by coordinate
    /// @param x the x coordinate
    Point1 (const t_real x);

    /// copy construct this point
    /// @param that the point to copy
    Point1 (const Point1& that);

    /// assign a point to this one
    /// @param that the point to assign
    Point1& operator = (const Point1& that);

    /// @return the class name
    String repr (void) const;
    
    /// @return a clone of this object
    Object* clone (void) const;
    
    /// @return the point x coordinate
    virtual t_real getx (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
