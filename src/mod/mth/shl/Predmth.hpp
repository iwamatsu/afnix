// ---------------------------------------------------------------------------
// - Predmth.hpp                                                             -
// - afnix:mth module - predicates declaration                               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDMTH_HPP
#define  AFNIX_PREDMTH_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix:mth
  /// standard module.
  /// @author amaury darsch

  /// the real samples array object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rsap (Runnable* robj, Nameset* nset, Cons* args);

  /// the real matrix datum object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rmdp (Runnable* robj, Nameset* nset, Cons* args);

  /// the cpi object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_cpip (Runnable* robj, Nameset* nset, Cons* args);

  /// the rvi object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rvip (Runnable* robj, Nameset* nset, Cons* args);

  /// the rmi object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rmip (Runnable* robj, Nameset* nset, Cons* args);

  /// the solver object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_slvp (Runnable* robj, Nameset* nset, Cons* args);

  /// the direct object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_dlsp (Runnable* robj, Nameset* nset, Cons* args);

  /// the parallel object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_parp (Runnable* robj, Nameset* nset, Cons* args);

  /// the qr direct object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_qrdp (Runnable* robj, Nameset* nset, Cons* args);

  /// the mgs direct object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_mgsp (Runnable* robj, Nameset* nset, Cons* args);

  /// the iterative object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_ilsp (Runnable* robj, Nameset* nset, Cons* args);

  /// the cgs solver object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_cgsp (Runnable* robj, Nameset* nset, Cons* args);

  /// the bcs solver object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_bcsp (Runnable* robj, Nameset* nset, Cons* args);

  /// the qmr solver object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_tqmrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the linear object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_lnrp (Runnable* robj, Nameset* nset, Cons* args);

  /// the solver factory object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_sfcp (Runnable* robj, Nameset* nset, Cons* args);

  /// the linear factory object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_lfcp (Runnable* robj, Nameset* nset, Cons* args);

  /// the newton object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_ntwp (Runnable* robj, Nameset* nset, Cons* args);

  /// the rfi object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rfip (Runnable* robj, Nameset* nset, Cons* args);

  /// the integer plane point datum object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_ippdp (Runnable* robj, Nameset* nset, Cons* args);

  /// the permute object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_prmtp (Runnable* robj, Nameset* nset, Cons* args);

  /// the rvector object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rvectp (Runnable* robj, Nameset* nset, Cons* args);

  /// the rblock object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rblokp (Runnable* robj, Nameset* nset, Cons* args);

  /// the rmatrix object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rmtrxp (Runnable* robj, Nameset* nset, Cons* args);

  /// the rfunction object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rfuncp (Runnable* robj, Nameset* nset, Cons* args);

  /// the rpolynom object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rpolyp (Runnable* robj, Nameset* nset, Cons* args);

  /// the real givens matrix object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rgvnsp (Runnable* robj, Nameset* nset, Cons* args);

  /// the analytic object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_anap (Runnable* robj, Nameset* nset, Cons* args);

  /// the rpi object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rpip (Runnable* robj, Nameset* nset, Cons* args);

  /// the rpoint object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_rptp (Runnable* robj, Nameset* nset, Cons* args);

  /// the mean object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_meanp (Runnable* robj, Nameset* nset, Cons* args);

  /// the covariance object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_covp (Runnable* robj, Nameset* nset, Cons* args);

  /// the fitter object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_fitp (Runnable* robj, Nameset* nset, Cons* args);

  /// the lufit object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* mth_lufp (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
