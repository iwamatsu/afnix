# ----------------------------------------------------------------------------
# - std-adp-param                                                            -
# - afnix:adp parameters definition module                                   -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - project section                                                          -
# ----------------------------------------------------------------------------

# system revision
const AFNIX:ADP:MAJOR        1
const AFNIX:ADP:MINOR        0
const AFNIX:ADP:PATCH        0

# default application title
const AFNIX:ADP:TITLE        "afnix documentation processor"
# the copyright holder
const AFNIX:ADP:HOLDER       "© 1999-2017 Amaury Darsch [amaury@afnix.org]"

# ----------------------------------------------------------------------------
# - default section                                                          -
# ----------------------------------------------------------------------------

# the adp writing mode
const AFNIX:ADP:WRITER-MODE  (enum XHT MAN TEX)

# default output file flag
const AFNIX:ADP:SYSTEM-OFLG  false
# default output file
const AFNIX:ADP:SYSTEM-ONAM  "adp.txt"

# ----------------------------------------------------------------------------
# - xhtml writer section                                                     -
# ----------------------------------------------------------------------------

# the default style file
const AFNIX:ADP-SYSTEM-XCSS  "style.css"

# ----------------------------------------------------------------------------
# - session section                                                          -
# ----------------------------------------------------------------------------

# the default style file
trans afnix:adp:system-xcss  AFNIX:ADP-SYSTEM-XCSS
# the default output flag
trans afnix:adp:system-oflg  AFNIX:ADP:SYSTEM-OFLG
# the default output name
trans afnix:adp:system-onam  AFNIX:ADP:SYSTEM-ONAM
