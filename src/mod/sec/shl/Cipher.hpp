// ---------------------------------------------------------------------------
// - Cipher.hpp                                                              -
// - afnix:sec module - base cipher class definition                         -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CIPHER_HPP
#define  AFNIX_CIPHER_HPP

#ifndef  AFNIX_KEY_HPP
#include "Key.hpp"
#endif

#ifndef  AFNIX_NAMEABLE_HPP
#include "Nameable.hpp"
#endif

#ifndef  AFNIX_INPUTSTREAM_HPP
#include "InputStream.hpp"
#endif

#ifndef  AFNIX_OUTPUTSTREAM_HPP
#include "OutputStream.hpp"
#endif

namespace afnix {

  /// The Cipher class is a base class that is used to implement a cipher.
  /// A cipher is used to encrypt or decrypt a message. There are basically
  /// two types of ciphers, namely symmetric cipher and asymmetric ciphers.
  /// For the base class operations, only the cipher name and key are needed.
  /// A reverse flag controls whether or not an encryption operation must
  /// be reversed. A reset method can also be used to reset the internal
  /// cipher state.
  /// @author amaury darsch

  class Cipher : public Nameable {
  protected:
    /// the cipher name
    String  d_name;
    /// the cipher key
    Key     d_ckey;
    /// the reverse flag
    bool    d_rflg;

  public:
    /// create a default cipher
    Cipher (void);

    /// create a cipher by name
    /// @param name the cipher name
    Cipher (const String& name);

    /// create a cipher by name and key
    /// @param name the cipher name
    /// @param key  the cipher key
    Cipher (const String& name, const Key& key);

    /// @return the class name
    String repr (void) const;

    /// @return the cipher name
    String getname (void) const;

    /// reset this cipher
    virtual void reset (void) =0;

    /// set the cipher key
    /// @param key the key to set
    virtual void setkey (const Key& key);

    /// @return the cipher key
    virtual Key getkey (void) const;

    /// set the cipher reverse flag
    /// @param rflg the flag to set
    virtual void setrflg (const bool rflg);

    /// @return the reverse flag
    virtual bool getrflg (void) const;

    /// normalize a data size
    /// @param size the size to normalize
    virtual t_long waist (const t_long size) const;

    /// stream an input buffer into an output buffer
    /// @param ob the output buffer to write
    /// @param ib the input  buffer to read
    /// @return the number of processed bytes
    virtual long stream (Buffer& ob, Buffer& ib);

    /// stream an input stream into an output buffer
    /// @param ob the output buffer to write
    /// @param is the input  stream to read
    /// @return the number of processed bytes
    virtual long stream (Buffer& ob, InputStream& is);

    /// stream an input stream into an output stream
    /// @param os the output stream to write
    /// @param is the input stream to read
    /// @return the number of processed bytes
    virtual long stream (OutputStream& os , InputStream& is);

  protected:
    /// encode an input buffer into an output buffer
    /// @param ob the output buffer to write
    /// @param ib the input  buffer to read
    /// @return the number of processed bytes
    virtual long encode (Buffer& ob, Buffer& ib) =0;

    /// encode an input stream into an output buffer
    /// @param ob the output buffer to write
    /// @param is the input  stream to read
    /// @return the number of processed bytes
    virtual long encode (Buffer& ob, InputStream& is) =0;

    /// encode an input stream into an output stream
    /// @param os the output stream to write
    /// @param is the input stream to read
    /// @return the number of processed bytes
    virtual long encode (OutputStream& os, InputStream& is) =0;

    /// decode an input buffer into an output buffer
    /// @param ob the output buffer to write
    /// @param ib the input  buffer to read
    /// @return the number of processed bytes
    virtual long decode (Buffer& ob, Buffer& ib) =0;

    /// decode an input stream into an output buffer
    /// @param ob the output buffer to write
    /// @param is the input  stream to read
    /// @return the number of processed bytes
    virtual long decode (Buffer& ob, InputStream& is) =0;

    /// decode an input stream into an output stream
    /// @param os the output stream to write
    /// @param is the input stream to read
    /// @return the number of processed bytes
    virtual long decode (OutputStream& os, InputStream& is) =0;
    
  private:
    // make the copy constructor private
    Cipher (const Cipher&);
    // make the assignment operator private
    Cipher& operator = (const Cipher&);

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
