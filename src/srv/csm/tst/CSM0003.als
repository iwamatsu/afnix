# ---------------------------------------------------------------------------
# - CSM0003.als                                                             -
# - afnix:csm service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   assistant test unit
# @author amaury darsch

# get the service
interp:library "afnix-sys"
interp:library "afnix-csm"

# create an assistant
const name "Mr Smith"
const info "The super assistant"
const asst (afnix:csm:Assistant name info)

# check type, name and info
assert true (afnix:csm:assistant-p asst)
assert name (asst:get-name)
assert info (asst:get-info)

# create an appointer
const  appt (afnix:csm:Appointer)

# add a saturday and sunday blocked day rule
appt:set-blocked-day 6
appt:set-blocked-day 0
# set a valid block time between 8AM to 12AM and 2PM to 6PM
const ambt (* 3600 8)
const amet (* 3600 12)
const pmbt (* 3600 14)
const pmet (* 3600 18)
appt:set-valid-block-time ambt amet
appt:set-valid-block-time pmbt pmet

# reset the date to 1970-01-01 - it is a thursday
trans date (afnix:sys:Date 1970 1 1)
appt:set-date date

# add the apponter to the assistant
asst:add-appointer (appt:clone)
asst:add-appointer (appt:clone)
assert 2 (asst:length-appointer)

# get the next available slot 1H - Thu 1st 8AM - index 0
trans  slot (asst:get-slot 3600)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert 0    (slot:get-index)
assert "1970-01-01T08:00:00Z" (date:to-iso true)

# get the next available slot 2H - Thu 1st 8AM - index 1
trans  slot (asst:get-slot 7200)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert 1    (slot:get-index)
assert "1970-01-01T08:00:00Z" (date:to-iso true)

# get the next available slot 2H - Thu 1st 9AM - index 0
trans  slot (asst:get-slot 7200)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert 0    (slot:get-index)
assert "1970-01-01T09:00:00Z" (date:to-iso true)

# get the next available slot 2H - Thu 1st 10AM - index 1
trans  slot (asst:get-slot 7200)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert 1    (slot:get-index)
assert "1970-01-01T10:00:00Z" (date:to-iso true)

# get the next available slot 4H - Thu 1st 2PM - index 0
trans  slot (asst:get-slot 14400)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert 0    (slot:get-index)
assert "1970-01-01T14:00:00Z" (date:to-iso true)

# get the next available slot 2H - Thu 1st 2PM - index 1
trans  slot (asst:get-slot 7200)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert 1    (slot:get-index)
assert "1970-01-01T14:00:00Z" (date:to-iso true)

# get the next available slot 4H - Fri 2nd 8AM - index 0
trans  slot (asst:get-slot 14400)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert 0    (slot:get-index)
assert "1970-01-02T08:00:00Z" (date:to-iso true)

# get the next available slot 2H - Thu 1st 4PM - index 1
trans  slot (asst:get-slot 7200)
trans  time (slot:get-time)
trans  date (afnix:sys:Date time)
assert 1    (slot:get-index)
assert "1970-01-01T16:00:00Z" (date:to-iso true)
