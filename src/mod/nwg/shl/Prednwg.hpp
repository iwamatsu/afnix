// ---------------------------------------------------------------------------
// - Prednwg.hpp                                                             -
// - afnix:nwg module - predicates declaration                               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDNWG_HPP
#define  AFNIX_PREDNWG_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix:nwg
  /// standard module.
  /// @author amaury darsch

  /// the uri object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* nwg_urip (Runnable* robj, Nameset* nset, Cons* args);

  /// the uri path object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* nwg_uripp (Runnable* robj, Nameset* nset, Cons* args);

  /// the uri query object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* nwg_uriqp (Runnable* robj, Nameset* nset, Cons* args);

  /// the mime document object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* nwg_mimep (Runnable* robj, Nameset* nset, Cons* args);

  /// the http proto object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* nwg_protop (Runnable* robj, Nameset* nset, Cons* args);

  /// the http request object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* nwg_hrqstp (Runnable* robj, Nameset* nset, Cons* args);

  /// the http response object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* nwg_hrespp (Runnable* robj, Nameset* nset, Cons* args);

  /// the http stream object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* nwg_hstrmp (Runnable* robj, Nameset* nset, Cons* args);

  /// the cookie object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* nwg_cookp (Runnable* robj, Nameset* nset, Cons* args);

  /// the cookie jar object predicate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* nwg_cjarp (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
