// ---------------------------------------------------------------------------
// - Predgeo.cpp                                                             -
// - afnix:geo service - predicates implementation                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "Plane.hpp"
#include "Cuboid.hpp"
#include "Boolean.hpp"
#include "Predgeo.hpp"
#include "Vector1.hpp"
#include "Vector2.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"
#include "Exception.hpp"
#include "Quaternion.hpp"

namespace afnix {

  // this procedure checks that we have one argument only and returns
  // the evaluated object
  static inline Object* get_obj (Runnable* robj, Nameset* nset, Cons* args,
                                 const String& pname) {
    Object* car = nilp;
    if ((args == nilp) || (args->length () != 1))
      throw Exception ("argument-error", "illegal arguments with predicate",
                       pname);
    car = args->getcar ();
    return (car == nilp) ? nilp : car->eval (robj,nset);
  }

  // pnt1: point1 object predicate

  Object* geo_pnt1p (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "point1-p");
    bool result = (dynamic_cast <Point1*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // pnt2: point2 object predicate

  Object* geo_pnt2p (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "point2-p");
    bool result = (dynamic_cast <Point2*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // pnt3: point3 object predicate

  Object* geo_pnt3p (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "point3-p");
    bool result = (dynamic_cast <Point3*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // pnt4: point4 object predicate

  Object* geo_pnt4p (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "point4-p");
    bool result = (dynamic_cast <Point4*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // vec1: vector1 object predicate

  Object* geo_vec1p (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "vector1-p");
    bool result = (dynamic_cast <Vector1*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // vec2: vector2 object predicate

  Object* geo_vec2p (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "vector2-p");
    bool result = (dynamic_cast <Vector2*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // vec3: vector3 object predicate

  Object* geo_vec3p (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "vector3-p");
    bool result = (dynamic_cast <Vector3*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
  
  // vec4: vector4 object predicate

  Object* geo_vec4p (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "vector4-p");
    bool result = (dynamic_cast <Vector4*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // qtrn: quternion object predicate

  Object* geo_qtrnp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "quaternion-p");
    bool result = (dynamic_cast <Quaternion*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // sld: solid object predicate

  Object* geo_sldp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "solid-p");
    bool result = (dynamic_cast <Solid*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // cub: cuboid object predicate
  
  Object* geo_cubp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "cuboid-p");
    bool result = (dynamic_cast <Cuboid*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }

  // pln: cuboid object predicate
  
  Object* geo_plnp (Runnable* robj, Nameset* nset, Cons* args) {
    Object* obj = get_obj (robj, nset, args, "plane-p");
    bool result = (dynamic_cast <Plane*> (obj) == nilp) ? false : true;
    Object::cref (obj);
    return new Boolean (result);
  }
}
