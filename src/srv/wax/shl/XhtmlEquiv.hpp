// ---------------------------------------------------------------------------
// - XhtmlEquiv.hpp                                                          -
// - afnix:wax service - xhtml meta http-equiv node class definition         -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_XHTMLEQUIV_HPP
#define  AFNIX_XHTMLEQUIV_HPP

#ifndef  AFNIX_XMLTAG_HPP
#include "XmlTag.hpp"
#endif

namespace afnix {

  /// The XhtmlEquiv class is a xhtml meta node used in the head node to
  /// add http equivalent data. The node is primarily used to add
  /// content information.
  /// @author amaury darsch

  class XhtmlEquiv : public XmlTag {
  public:
    /// create a default xhtml equiv node
    XhtmlEquiv (void);

    /// create a xhtml equiv node by name and content
    /// @param name the name attribute
    /// @param cnts the content attribute
    XhtmlEquiv (const String& name, const String& cnts);

    /// @return the class name
    String repr (void) const;

  private:
    // make the copy constructor private
    XhtmlEquiv (const XhtmlEquiv&);
    // make the assignment operator private
    XhtmlEquiv& operator = (const XhtmlEquiv&);

  public:
    /// create a new object in a generic object
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
  };
}

#endif
