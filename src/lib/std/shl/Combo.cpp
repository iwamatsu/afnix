// ---------------------------------------------------------------------------
// - Combo.cpp                                                               -
// - standard object library - combo object class implementation             -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cons.hpp"
#include "Combo.hpp"
#include "Runnable.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------
  
  // create a new combo with a quark
  
  Combo::Combo (const long quark, Object* crobj) {
    d_type      = QUARK;
    d_cval.qval = quark;
    p_robj      = Object::iref (crobj);
    d_aflg      = false;
  }

  // create a new combo with an object

  Combo::Combo (Object* coval, Object* crobj) {
    d_type      = OBJECT;
    d_cval.oval = Object::iref (coval);
    p_robj      = Object::iref (crobj);
    d_aflg      = false;
  }

  // create a new combo with a quark and an apply flag

  Combo::Combo (const long quark, Object* crobj, const bool aflag) {
    d_type      = QUARK;
    d_cval.qval = quark;
    p_robj      = Object::iref (crobj);
    d_aflg      = aflag;
  }

  // create a new combo with an object and an apply flag

  Combo::Combo (Object* coval, Object* crobj, const bool aflg) {
    d_type      = OBJECT;
    d_cval.oval = Object::iref (coval);
    p_robj      = Object::iref (crobj);
    d_aflg      = aflg;
  }

  // destroy this combo

  Combo::~Combo (void) {
    if (d_type == OBJECT) Object::dref (d_cval.oval);
    Object::dref (p_robj);
  }

  // return the class name

  String Combo::repr (void) const {
    return "Combo";
  }

  // check if a combo is looping

  bool Combo::isequal (Object* obj) const {
    // map the combo object
    Combo* cobj = dynamic_cast <Combo*> (obj);
    if (cobj == nilp) return false;
    // lock and compare
    rdlock ();
    cobj->rdlock ();
    try {
      // check the type
      if (d_type != cobj->d_type) {
	unlock ();
	return false;
      }
      // check the quark type
      if ((d_type == QUARK) && (d_cval.qval != cobj->d_cval.qval)) {
	unlock ();
	return false;
      }
      // check the object type
      if ((d_type == OBJECT) && (d_cval.oval != cobj->d_cval.oval)) {
	unlock ();
	return false;
      }
      // we are looping
      unlock ();
      cobj->unlock ();
      return true;
    } catch (...) {
      unlock ();
      cobj->unlock ();
      throw;
    }
  }

  // get the apply flag

  bool Combo::getaflg (void ) const {
    rdlock ();
    try {
      bool result = d_aflg;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the apply flag

  void Combo::setaflg (const bool aflg) {
    wrlock ();
    try {
      d_aflg = aflg;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // set an object as a const object by quark
  
  Object* Combo::cdef (Runnable* robj, Nameset* nset, const long quark,
		       Object* object) {
    wrlock ();
    try {
      Object* result = nilp;
      if (d_type == OBJECT) {
	result = d_cval.oval->cdef (robj, nset, quark, object);
      }
      if (d_type == QUARK) {
	throw Exception ("combo-error", "invalid cdef call in quark mode");
      }
      robj->post (result);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // set an object by quark

  Object* Combo::vdef (Runnable* robj, Nameset* nset, const long quark,
		       Object* object) {
    wrlock ();
    try {
      Object* result = nilp;
      if (d_type == OBJECT) {	
	result = d_cval.oval->vdef (robj, nset, quark, object);
      }
      if (d_type == QUARK) {
	throw Exception ("combo-error", "invalid vdef call in quark mode");
      }
      robj->post (result);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // unreference an object by quark
  
  Object* Combo::udef (Runnable* robj, Nameset* nset, const long quark) {
    wrlock ();
    try {
      Object* result = nilp;
      if (d_type == OBJECT) {
	result = d_cval.oval->udef (robj, nset, quark);
      }
      if (d_type == QUARK) {
	throw Exception ("combo-error", "invalid udef call in quark mode");
      }
      robj->post (result);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // evaluate an object by quark

  Object* Combo::eval (Runnable* robj, Nameset* nset, const long quark) {
    // get the read lock
    rdlock ();
    try {
      Object* result = nilp;
      if (d_type == QUARK) {
	Object* oval = p_robj->eval (robj, nset, d_cval.qval);
	Object::iref (oval);
	try {
	  if (isequal (oval) == true) {
	    // set local result
	    result = oval;
	    // adjust reference
	    Object::cref (oval);
	  } else {
	    // evaluate local result
	    result = (oval == nilp) ? nilp : oval->eval (robj, nset, quark);
	    // clean intermediate object
	    Object::dref (oval);
	  }
	} catch (...) {
	  Object::dref (oval);
	  throw;
	}
      }
      if (d_type == OBJECT) {
	throw Exception ("combo-error", "invalid eval call in object mode");
      }
      robj->post (result);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments
  
  Object* Combo::apply (Runnable* robj, Nameset* nset, Cons* args) {
    // pre-evaluate argument list
    Cons* carg = nilp;
    try {
      Object* result = nilp;
      switch (d_type) {
      case Combo::QUARK:
	if (d_aflg == true) 
	  result = p_robj->Object::apply (robj, nset, d_cval.qval, args);
	else
	  result = p_robj->apply (robj, nset, d_cval.qval, args);
	break;
      case Combo::OBJECT:
	// evaluate the arguments as a list
	carg = Cons::eval (robj, nset, args);
	// object evaluation combo
	if (d_aflg == true)
	  result = p_robj->Object::apply (robj, nset, d_cval.oval, carg);
	else
	  result = p_robj->apply (robj, nset, d_cval.oval, carg);
	break;
      }
      robj->post (result);
      delete carg;
      return result;
    } catch (...) {
      delete carg;
      throw;
    }
  }
}
