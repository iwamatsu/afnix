// ---------------------------------------------------------------------------
// - Carrier.hpp                                                             -
// - afnix:csm service - object carrier agent class definition               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CARRIER_HPP
#define  AFNIX_CARRIER_HPP

#ifndef  AFNIX_AGENT_HPP
#include "Agent.hpp"
#endif

namespace afnix {

  /// The Carrier class is a cloud agent used to transport an object. An
  /// object is registed as soon as its registration id is set in the agent.
  /// If the registration id is unse the object is unregistered or anonymous.
  /// @author amaury darsch

  class Carrier : public Agent {
  protected:
    /// the agent object
    Object* p_aobj;

  public:
    /// create nil carrier
    Carrier (void);

    /// create an anonymous carrier
    /// @param aobj the agent object 
    Carrier (Object* aobj);

    /// create a carrier object by name
    /// @param aobj the agent object
    /// @param name the agent name
    Carrier (Object* aobj, const String& name);

    /// create a carrier object by name and info
    /// @param aobj the agent object
    /// @param name the agent name
    /// @param info the agent info
    Carrier (Object* aobj, const String& name, const String& info);

    /// create a carrier object by rid, name and info
    /// @param aobj the agent object
    /// @param name the agent name
    /// @param info the agent info
    /// @param rid  the registration id
    Carrier (Object* aobj, const String& rid, const String& name,
	     const String& info);

    /// copy construct this carrier object
    /// @param that the object to copy
    Carrier (const Carrier& that);

    /// destroy this carrier object
    ~Carrier (void);

    /// assign a carrier to this one
    /// @param that the object to assign
    Carrier& operator = (const Carrier& that);
    
    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// @return the carrier serial id
    t_byte serialid (void) const;

    /// serialize this object
    /// @param os the output stream
    void wrstream (OutputStream& os) const;

    /// deserialize this object
    /// @param is the input stream
    void rdstream (InputStream& os);

    /// @return the agent object
    virtual Object* getobj (void) const;

  public:
    /// create a object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
