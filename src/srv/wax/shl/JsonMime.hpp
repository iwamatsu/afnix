// ---------------------------------------------------------------------------
// - JsonMime.hpp                                                            -
// - afnix:wax service - json mime class definition                          -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_JSONMIME_HPP
#define  AFNIX_JSONMIME_HPP

#ifndef  AFNIX_MIME_HPP
#include "Mime.hpp"
#endif

namespace afnix {

  /// The JsonMime class is a generic javascript object notation
  /// generic mime writer. The class operates with an objectr which is
  /// mapped internally into a json equivalent object.
  /// @author amaury darsch

  class JsonMime : public Mime {
  private:
    /// the mime object
    Object* p_mobj;

  public:
    /// create a default json mime
    JsonMime (void);

    /// create a json mime by object
    /// @param  name
    JsonMime (Object* mobj);

    /// destroy this writer
    ~JsonMime (void);

    /// @return the class name
    String repr (void) const;

    /// write a node into a buffer
    /// @param buf the buffer to write
    void write (Buffer& buf) const;
    
    /// write a node into an output stream
    /// @param os the output stream to write
    void write (OutputStream& os) const;

    /// @return the mime object
    virtual Object* getobj (void) const;

  private:
    // make the copy constructor private
    JsonMime (const JsonMime&);
    // make the assignment operator private
    JsonMime& operator = (const JsonMime&);
    
  public:
    /// create an object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
