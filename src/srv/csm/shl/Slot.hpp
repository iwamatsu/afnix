// ---------------------------------------------------------------------------
// - Slot.hpp                                                                -
// - afnix:csm service - slot class definition                               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SLOT_HPP
#define  AFNIX_SLOT_HPP

#ifndef  AFNIX_STRING_HPP
#include "String.hpp"
#endif

namespace afnix {

  /// The Slot class is a base class design to handle a basic time slot
  /// event. The class is defined with a date and a duration. Various class
  /// can be derived from it such like the appointment. The slot class
  /// is primarily used to build an agenda.
  /// @author amaury darsch

  class Slot : public virtual Object {
  protected:
    /// the slot time
    t_long d_time;
    /// the duration time
    t_long d_dlen;
    /// the slot index
    long   d_sidx;

  public:
    /// create a default slot
    Slot (void);

    /// create a slot by time and duration
    /// @param time the slot time
    /// @param dlen the duration time
    Slot (const t_long time, const t_long dlen);

    /// copy construct this slot
    /// @param that the slot to copy
    Slot (const Slot& that);

    /// @return the class name
    String repr (void) const;

    /// assign a slot to this one
    /// @param that the slot to assign
    Slot& operator = (const Slot& that);

    /// reset this slot
    virtual void reset (void);

    /// set the slot time
    /// @param time the slot time to set
    virtual void settime (const t_long time);

    /// @return the slot time
    virtual t_long gettime (void) const;

    /// set the slot duration
    /// @param dlen the duration time to set
    virtual void setdlen (const t_long dlen);

    /// @return the slot duration
    virtual t_long getdlen (void) const;

    /// set the slot at once
    /// @param time the slot time to set
    /// @param dlen the duration time to set
    virtual void setslot (const t_long time, const t_long dlen);

    /// set the slot index
    /// @param sidx the slot index to set
    virtual void setsidx (const long sidx);

    /// @return the slot index
    virtual long getsidx (void) const;

    /// @return true if the slot matches in time and duration
    /// @param slot the slot to match
    virtual bool match (const Slot& slot) const;

  public:
    /// create a object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
