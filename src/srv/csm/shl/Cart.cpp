// ---------------------------------------------------------------------------
// - Cart.cpp                                                                -
// - afnix:csm service - cloud object cart class implementation              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cart.hpp"
#include "System.hpp"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "Integer.hpp"
#include "Runnable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a nil cart

  Cart::Cart (void) {
    reset ();
  }

  // create a cart with a rid

  Cart::Cart (const String& rid) {
    d_rid = rid;
    reset ();
  }

  // return the cart class name

  String Cart::repr (void) const {
    return "Cart";
  }

  // reset this cart
  
  void Cart::reset (void) {
    wrlock ();
    try {
      d_cas.reset ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // validate a rid by cart rid

  bool Cart::isrid (const String& rid) const {
    rdlock ();
    try {
      bool result = (d_rid == rid);
      unlock ();
      return result;
    } catch  (...) {
      unlock ();
      throw;
    }
  }

  // set the registration id

  void Cart::setrid (const String& rid) {
    wrlock ();
    try {
      d_rid = rid;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the registration id

  String Cart::getrid (void) const {
    rdlock ();
    try {
      String result = d_rid;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the number of agent in the cart

  long Cart::length (void) const {
    rdlock ();
    try {
      long result = d_cas.length ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
	
  // return true if the cart is empty

  bool Cart::empty (void) const {
    rdlock ();
    try {
      bool result = d_cas.empty ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // check if an agent exists in this cart

  bool Cart::exists (const t_long kid) const {
    rdlock ();
    try {
      // check the key id
      if (kid <= 0LL) {
	throw Exception ("cart-error", "invalid agent key id to validate");
      }
      // get the set length
      long slen = d_cas.length ();
      // loop in the set
      for (long i = 0; i < slen; i++) {
	Object* obj = d_cas.get (i);
	Agent*  agt = dynamic_cast <Agent*> (obj);
	if (agt == nilp) {
	  throw Exception ("internal-error", "invalid object in cart",
			   Object::repr (obj));
	}
	// check for valid key id
	if (agt->iskid (kid) == true) {
	  unlock ();
	  return true;
	}
      }
      unlock ();
      return false;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get an agent  at position

  Agent* Cart::getat (const long idx) const {
    rdlock ();
    try {
      Agent* agt = dynamic_cast <Agent*> (d_cas.get (idx));
      if (agt == nilp) {
	throw Exception ("internal-error", "invalid object in cart");
      }
      unlock ();
      return agt;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add a agent in this cart

  t_long Cart::addagt (Agent* agt) {
    wrlock ();
    try {
      if (agt == nilp) {
	throw Exception ("cart-error", "invalid nil agent toadd");
      }
      // get the agent rid and validate
      String rid = agt->getrid ();
      if ((d_rid.isnil () == false) && (d_rid != rid)) {
	throw Exception ("cart-error", 
			 "invalid rid for registered cart", rid);
      }
      // get the agent key id and check
      t_long kid = agt->getkid ();
      if (kid <= 0LL) {
	// generate a unique id
	kid = System::uniqid ();
	// make sure the id is unique
	if (exists (kid) == true) {
	  throw Exception ("internal-error", "duplicate unique id found");
	}
	// create a copy of this agent with a key id
	Agent* ao = dynamic_cast<Agent*>(agt->clone()); ao->setkid (kid);
	// add the object in the set
	d_cas.add (ao);
      } else {
	// check if the agent exists
	if (exists (kid) == false) {
	  // create a copy of this agent with a key id
	  Agent* ao = dynamic_cast<Agent*>(agt->clone());
	  // add the object in the set
	  d_cas.add (ao);
	}
      }
      unlock ();
      return kid;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a agent object by key id

  Agent* Cart::getagt (const t_long kid) const {
    rdlock ();
    try {
      // check the key id
      if (kid <= 0LL) {
	throw Exception ("cart-error", "invalid agent key id to validate");
      }
      // get the set length
      long slen = d_cas.length ();
      // loop in the set
      for (long i = 0; i < slen; i++) {
	Object* obj = d_cas.get (i);
	Agent*  agt = dynamic_cast <Agent*> (obj);
	if (agt == nilp) {
	  throw Exception ("internal-error", "invalid object in cart",
			   Object::repr (obj));
	}
	// check for valid key id
	if (agt->iskid (kid) == true) {
	  unlock ();
	  return agt;
	}
      }
      // not found
      throw Exception ("cart-error", "cannot find cart by id");
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 10;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_RESET   = zone.intern ("reset");
  static const long QUARK_GETAT   = zone.intern ("get-at");
  static const long QUARK_LENGTH  = zone.intern ("length");
  static const long QUARK_ISRIDP  = zone.intern ("rid-p");
  static const long QUARK_SETRID  = zone.intern ("set-rid");
  static const long QUARK_GETRID  = zone.intern ("get-rid");
  static const long QUARK_EMPTYP  = zone.intern ("empty-p");
  static const long QUARK_ADDAGT  = zone.intern ("add-agent");
  static const long QUARK_GETAGT  = zone.intern ("get-agent");
  static const long QUARK_EXISTSP = zone.intern ("exists-p");

  // create a new object in a generic way

  Object* Cart::mknew (Vector* argv) {
    long argc = (argv == nilp) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Cart;
    // check for 1 argument
    if (argc == 1) {
      String rid = argv->getstring (0);
      return new Cart (rid);
    }
    throw Exception ("argument-error",
                     "too many argument with cart constructor");
  }

  // return true if the given quark is defined

  bool Cart::isquark (const long quark, const bool hflg) const {
    rdlock ();
    if (zone.exists (quark) == true) {
      unlock ();
      return true;
    }
    bool result = hflg ? Object::isquark (quark, hflg) : false;
    unlock ();
    return result;
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Cart::apply (Runnable* robj, Nameset* nset, const long quark,
		       Vector* argv) {
    // get the number of arguments
    long argc = (argv == nilp) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_EMPTYP) return new Boolean (empty  ());
      if (quark == QUARK_LENGTH) return new Integer (length ());
      if (quark == QUARK_GETRID) return new String  (getrid ());
      if (quark == QUARK_RESET) {
	reset ();
	return nilp;
      }
    }
    // check for 1 argument
    if (argc == 1) {
      if (quark == QUARK_ADDAGT) {
	Object* obj = argv->get (0);
	Agent*  agt = dynamic_cast <Agent*> (obj);
	if (agt == nilp) {
	  throw Exception ("type-error", "invalid object with cart add",
			   Object::repr (obj));
	}
	t_long result = addagt (agt);
	return new Integer (result);
      }
      if (quark == QUARK_GETAT) {
	long idx = argv->getlong (0);
	rdlock ();
	try {
	  Object* result = getat (idx);
	  robj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_GETAGT) {
	t_long kid = argv->getlong (0);
	rdlock ();
	try {
	  Object* result = getagt (kid);
	  robj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_ISRIDP) {
	String rid = argv->getstring (0);
	return new Boolean (isrid (rid));
      }
      if (quark == QUARK_SETRID) {
	String rid = argv->getstring (0);
	setrid (rid);
	return nilp;
      }
      if (quark == QUARK_EXISTSP) {
	t_long kid = argv->getlong (0);
	return new Boolean (exists (kid));
      }
    }
    // call the object method
    return Object::apply (robj, nset, quark, argv);
  }
}
