Source: afnix
Maintainer: Nobuhiro Iwamatsu <iwamatsu@debian.org>
Section: interpreters
Priority: optional
Build-Depends: debhelper (>= 10), libncurses5-dev (>= 5.5),
	dpkg-dev (>= 1.16.1~)
Standards-Version: 4.1.1
Vcs-Browser: https://anonscm.debian.org/cgit/collab-maint/afnix.git
Vcs-Git: https://anonscm.debian.org/git/collab-maint/afnix.git
Homepage: http://www.afnix.org/

Package: afnix
Architecture: any
Section: interpreters
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: afnix-doc
Conflicts: aleph
Replaces: aleph
Description: Compiler and run-time for the AFNIX programming language
 AFNIX is a multi-threaded functional programming language with
 dynamic symbol bindings that support the object oriented paradigm.
 The language features a state of the art runtime engine. The
 distribution is available with several clients and a rich set
 of modules that are designed to be platform independent.

Package: afnix-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: afnix
Description: Compiler and run-time for the AFNIX programming language (documentation)
 AFNIX is a multi-threaded functional programming language with
 dynamic symbol bindings that support the object oriented paradigm.
 The language features a state of the art runtime engine. The
 distribution is available with several clients and a rich set
 of modules that are designed to be platform independent.
 .
 This package contains the documentation for AFNIX.
