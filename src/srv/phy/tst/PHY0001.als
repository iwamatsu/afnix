# ---------------------------------------------------------------------------
# - PHY0001.als                                                             -
# - afnix:phy service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   physics test unit
# @author amaury darsch

# get the module
interp:library "afnix-phy"

# create a physics object
const p (afnix:phy:Physics)

# check constant
assert true (p:exists-p "C")
assert true (p:exists-p "U0")
assert true (p:exists-p "E0")
assert true (p:exists-p "G")
assert true (p:exists-p "H")
