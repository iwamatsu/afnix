// ---------------------------------------------------------------------------
// - Libsec.cpp                                                              -
// - afnix:sec module - declaration & implementation                         -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Dsa.hpp"
#include "Des.hpp"
#include "Aes.hpp"
#include "Rc2.hpp"
#include "Rc4.hpp"
#include "Rc5.hpp"
#include "Rsa.hpp"
#include "Md2.hpp"
#include "Md4.hpp"
#include "Md5.hpp"
#include "Sha1.hpp"
#include "Kdf1.hpp"
#include "Kdf2.hpp"
#include "Hmac.hpp"
#include "Meta.hpp"
#include "Sha224.hpp"
#include "Sha256.hpp"
#include "Sha384.hpp"
#include "Sha512.hpp"
#include "Libsec.hpp"
#include "Predsec.hpp"
#include "Function.hpp"
#include "Signature.hpp"
#include "InputCipher.hpp"

namespace afnix {

  // initialize the afnix:sec module

  Object* init_afnix_sec (Interp* interp, Vector* argv) {
    // make sure we are not called from something crazy
    if (interp == nilp) return nilp;
    
    // create the afnix:sys nameset
    Nameset* aset = interp->mknset ("afnix");
    Nameset* nset = aset->mknset   ("sec");
    
    // bind all classes in the afnix:sec nameset 
    nset->symcst ("Key",             new Meta (Key::meval, Key::mknew));
    nset->symcst ("Dsa",             new Meta (Dsa::mknew));
    nset->symcst ("Des",             new Meta (Des::mknew));
    nset->symcst ("Aes",             new Meta (Aes::mknew));
    nset->symcst ("Rc2",             new Meta (Rc2::mknew));
    nset->symcst ("Rc4",             new Meta (Rc4::mknew));
    nset->symcst ("Rc5",             new Meta (Rc5::mknew));
    nset->symcst ("Rsa",             new Meta (Rsa::meval, Rsa::mknew));
    nset->symcst ("Md2",             new Meta (Md2::mknew));
    nset->symcst ("Md4",             new Meta (Md4::mknew));
    nset->symcst ("Md5",             new Meta (Md5::mknew));
    nset->symcst ("Sha1",            new Meta (Sha1::mknew));
    nset->symcst ("Kdf1",            new Meta (Kdf1::mknew));
    nset->symcst ("Kdf2",            new Meta (Kdf2::mknew));
    nset->symcst ("Hmac",            new Meta (Hmac::mknew));
    nset->symcst ("Sha224",          new Meta (Sha224::mknew));
    nset->symcst ("Sha256",          new Meta (Sha256::mknew));
    nset->symcst ("Sha384",          new Meta (Sha384::mknew));
    nset->symcst ("Sha512",          new Meta (Sha512::mknew));
    nset->symcst ("Signature",       new Meta (Signature::meval, 
					       Signature::mknew));
    nset->symcst ("BlockCipher",     new Meta (BlockCipher::meval));
    nset->symcst ("InputCipher",     new Meta (InputCipher::mknew));

    // bind all predicates in the afnix:sec nameset
    nset->symcst ("key-p",           new Function (sec_keyp));
    nset->symcst ("dsa-p",           new Function (sec_dsap));
    nset->symcst ("kdf-p",           new Function (sec_kdfp));
    nset->symcst ("des-p",           new Function (sec_desp));
    nset->symcst ("aes-p",           new Function (sec_aesp));
    nset->symcst ("rc2-p",           new Function (sec_rc2p));
    nset->symcst ("rc4-p",           new Function (sec_rc4p));
    nset->symcst ("rc5-p",           new Function (sec_rc5p));
    nset->symcst ("rsa-p",           new Function (sec_rsap));
    nset->symcst ("md2-p",           new Function (sec_md2p));
    nset->symcst ("md4-p",           new Function (sec_md4p));
    nset->symcst ("md5-p",           new Function (sec_md5p));
    nset->symcst ("sha1-p",          new Function (sec_sha1p));
    nset->symcst ("kdf1-p",          new Function (sec_kdf1p));
    nset->symcst ("kdf2-p",          new Function (sec_kdf2p));
    nset->symcst ("mac-p",           new Function (sec_macp));
    nset->symcst ("hmac-p",          new Function (sec_hmacp));
    nset->symcst ("sha224-p",        new Function (sec_sha224p));
    nset->symcst ("sha256-p",        new Function (sec_sha256p));
    nset->symcst ("sha384-p",        new Function (sec_sha384p));
    nset->symcst ("sha512-p",        new Function (sec_sha512p));
    nset->symcst ("hasher-p",        new Function (sec_hashp));
    nset->symcst ("cipher-p",        new Function (sec_cifrp));
    nset->symcst ("signature-p",     new Function (sec_sgnp));
    nset->symcst ("hashed-kdf-p",    new Function (sec_hkdfp));
    nset->symcst ("input-cipher-p",  new Function (sec_icfrp));
    nset->symcst ("block-cipher-p",  new Function (sec_bcfrp));
    nset->symcst ("public-cipher-p", new Function (sec_pcfrp));

    // not used but needed
    return nilp;
  }
}

extern "C" {
  afnix::Object* dli_afnix_sec (afnix::Interp* interp, afnix::Vector* argv) {
    return init_afnix_sec (interp, argv);
  }
}
