// ---------------------------------------------------------------------------
// - Point3.hpp                                                              -
// - afnix:geo service - point 3 class definition                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_POINT3_HPP
#define  AFNIX_POINT3_HPP

#ifndef  AFNIX_RPOINT_HPP
#include "Rpoint.hpp"
#endif

namespace afnix {

  /// The Point3 class is a 3 dimensional point class for the afnix geometry
  /// service. A 3 dimensional point is defined by its (x,y,z) coordinates.
  /// @author amaury darsch

  class Point3 : public Rpoint {
  public:
    /// create a default point
    Point3 (void);

    /// create a point by coordinates
    /// @param x the x coordinate
    /// @param y the y coordinate
    /// @param z the z coordinate
    Point3 (const t_real x, const t_real y, const t_real z);

    /// copy construct this point
    /// @param that the point to copy
    Point3 (const Point3& that);

    /// assign a point to this one
    /// @param that the point to assign
    Point3& operator = (const Point3& that);
    
    /// @return the class name
    String repr (void) const;
    
    /// @return a clone of this object
    Object* clone (void) const;

    /// @return the point x coordinate
    virtual t_real getx (void) const;
    
    /// @return the point y coordinate
    virtual t_real gety (void) const;
    
    /// @return the point z coordinate
    virtual t_real getz (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
