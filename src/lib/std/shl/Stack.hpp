// ---------------------------------------------------------------------------
// - Stack.hpp                                                               -
// - standard object library - object stack class definition                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_STACK_HPP
#define  AFNIX_STACK_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif

namespace afnix {

  /// The Stack class is a simple stack for the afnix standard object. The
  /// stack is dynamically resized as needed when objects are pushed onto it.
  /// The push and pop methods are the standard methods to manipulate the
  /// stack.
  /// @author amaury darsch

  class Stack : public virtual Object {
  private:
    /// the stack size in bytes
    long d_size;
    /// the base of the stack
    Object** p_base;
    /// the top of the stack
    Object** p_top;
    /// the current stack pointer
    Object** p_sp;

  public:
    /// create a default stack
    Stack (void);

    /// destroy this stack
    ~Stack (void);

    /// @return the class name
    String repr (void) const;

    /// push an object on this stack
    /// @param object the object to push
    virtual void push (Object* object);

    /// @return the object on top of the stack
    virtual Object* pop (void);

    /// @return true if the stack is empty
    virtual bool empty (void) const;

    /// unwind the stack
    virtual void unwind (void);

    /// resize the stack
    virtual void resize (const long size);

  private:
    // make the copy constructor private
    Stack (const Stack&);
    // make the assignement operator private
    Stack& operator = (const Stack&);

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
