# ---------------------------------------------------------------------------
# - GEO0001.als                                                             -
# - afnix:geo service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2017 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   vector 1 test unit
# @author amaury darsch

# get the service
interp:library "afnix-geo"

# create a vector 1 object
trans v1 (afnix:geo:Vector1)
assert true (afnix:geo:vector1-p v1)
assert "Vector1" (v1:repr)

# create a new vector and check
trans v1 (afnix:geo:Vector1 1.0)
assert 1.0 (v1:get-x)
