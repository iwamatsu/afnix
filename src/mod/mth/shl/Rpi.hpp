// ---------------------------------------------------------------------------
// - Rpi.hpp                                                                 -
// - afnix:mth module - real point interface definitions                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_RPI_HPP
#define  AFNIX_RPI_HPP

#ifndef  AFNIX_RVI_HPP
#include "Rvi.hpp"
#endif

namespace afnix {

  /// This Rpi class is an abstract class that models the behavior of a
  /// real based point. The class defines the basic operations associated
  /// with a point, such like translation by a vector.
  /// @author amaury darsch

  class Rpi : public virtual Serial {
  protected:
    /// the point size
    t_long d_size;

  public:
    /// create a null point
    Rpi (void);

    /// create a point by size
    /// @param size the point size
    Rpi (const t_long size);

    /// @return the object serial id
    t_byte serialid (void) const;

    /// serialize this object
    /// @param os the output stream
    void wrstream (OutputStream& os) const;

    /// deserialize this object
    /// @param is the input stream
    void rdstream (InputStream& os);

    /// compare two points
    /// @param  x the point argument
    /// @return true if they are equals
    virtual bool operator == (const Rpi& x) const;

    /// compare two points
    /// @param  x the point argument
    /// @return true if they are not equals
    virtual bool operator != (const Rpi& x) const;

    /// @return the point size
    virtual t_long getsize (void) const;

    /// reset this point
    virtual void reset (void);

    /// clear this point
    virtual void clear (void);

    /// resize this point
    /// @param size the new point size
    virtual void resize (const t_long size) =0;

    /// set a point by value
    /// @param val the value to set
    virtual void set (const t_real val);

    /// set a point by position
    /// @param pos the point position
    /// @param val the value to set
    virtual void set (const t_long pos, const t_real val);

    /// get a point value by position
    /// @param pos the point position
    virtual t_real get (const t_long pos) const;

  public:
    /// no lock - set a point by position
    /// @param pos the point position
    /// @param val the value to set
    virtual void nlset (const t_long pos, const t_real val) =0;

    /// no lock - get a point value by position
    /// @param pos the point position
    virtual t_real nlget (const t_long pos) const =0;

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;

    /// apply this object with a set of arguments and a quark
    /// @param robj  the current runnable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Runnable* robj, Nameset* nset, const long quark,
                   Vector* argv);
  };
}

#endif
