// ---------------------------------------------------------------------------
// - Builtin.hpp                                                             -
// - afnix engine - builtin functions definitions                            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_BUILTIN_HPP
#define  AFNIX_BUILTIN_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif

namespace afnix {
  // reserved keywords
  Object* builtin_if      (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_do      (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_for     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_try     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_eval    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_sync    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_loop    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_enum    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_const   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_trans   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_unref   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_class   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_block   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_while   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_gamma   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_throw   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_force   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_delay   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_launch  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_lambda  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_switch  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_return  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_protect (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_nameset (Runnable* robj, Nameset* nset, Cons* args);

  // standard operators
  Object* builtin_add     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_sub     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_mul     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_div     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_eql     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_neq     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_geq     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_gth     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_leq     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_lth     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_assert  (Runnable* robj, Nameset* nset, Cons* args);

  // logical operator
  Object* builtin_or      (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_not     (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_and     (Runnable* robj, Nameset* nset, Cons* args);

  // standard predicates
  Object* builtin_nilp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_objp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_evlp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_nblp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_cblp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_lexp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_symp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_setp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_clop    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_litp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_nump    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_intp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_rltp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_strp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_bufp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_stvp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_vecp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_cmbp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_nstp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_ashp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_clsp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_prmp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_thrp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_logp    (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_bbufp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_hashp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_fifop   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_heapp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_lbrnp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_qualp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_listp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_instp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_bytep   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_realp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_boolp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_charp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_consp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_formp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_enump   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_itemp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_bitsp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_condp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_atblp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_ptblp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_mesgp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_propp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_cntrp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_thrsp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_loadp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_rslvp   (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_localp  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_regexp  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_queuep  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_plistp  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_lexerp  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_stylep  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_formrp  (Runnable* robj, Nameset* nset, Cons* args);
  Object* builtin_readrp  (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
