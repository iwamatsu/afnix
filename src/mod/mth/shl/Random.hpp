// ---------------------------------------------------------------------------
// - Random.hpp                                                              -
// - afnix:mth module - random number generator definitions                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_RANDOM_HPP
#define  AFNIX_RANDOM_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// This file contains specific procedures that produce random numbers
  /// in different formats and objects.
  /// @author amaury darsch

  /// get a random byte
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_byternd (Runnable* robj, Nameset* nset, Cons* args);
  
  /// get a random integer value
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_longrnd (Runnable* robj, Nameset* nset, Cons* args);

  /// get a random real value
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_realrnd (Runnable* robj, Nameset* nset, Cons* args);

  /// get a large random relatif value
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_relnrnd (Runnable* robj, Nameset* nset, Cons* args);

  /// get a random prime number
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_primrnd (Runnable* robj, Nameset* nset, Cons* args);

  /// get a random bitset
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_bitsrnd (Runnable* robj, Nameset* nset, Cons* args);

  /// get a random vector
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_rvecrnd (Runnable* robj, Nameset* nset, Cons* args);

  /// get a random block matrix
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_rblkrnd (Runnable* robj, Nameset* nset, Cons* args);

  /// get a random sparse matrix
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_rmtxrnd (Runnable* robj, Nameset* nset, Cons* args);

  /// get a block matrix
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_rblksps (Runnable* robj, Nameset* nset, Cons* args);

  /// get a sparse matrix
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_rmtxsps (Runnable* robj, Nameset* nset, Cons* args);

  /// get a uniform deviate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_unidev (Runnable* robj, Nameset* nset, Cons* args);

  /// get a normal deviate
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* mth_nrmdev (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
