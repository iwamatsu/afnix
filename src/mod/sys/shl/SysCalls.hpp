// ---------------------------------------------------------------------------
// - SysCalls.hpp                                                            -
// - afnix:sys module - system call definitions                              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2017 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_SYSCALLS_HPP
#define  AFNIX_SYSCALLS_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains specific system procedures that require only
  /// arguments and that are not bounded to a class
  /// @author amaury darsch

  /// exit the process with an exit code
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sys_exit (Runnable* robj, Nameset* nset, Cons* args);

  /// pause for a certain time
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sys_sleep (Runnable* robj, Nameset* nset, Cons* args);

  /// format an option to the system convention
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sys_getopt (Runnable* robj, Nameset* nset, Cons* args);

  /// return the process id
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sys_getpid (Runnable* robj, Nameset* nset, Cons* args);

  /// get an environemnt variable
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sys_getenv (Runnable* robj, Nameset* nset, Cons* args);

  /// get a unique id from the system
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sys_uniqid (Runnable* robj, Nameset* nset, Cons* args);

  /// get the host fqdn
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sys_hostfqdn (Runnable* robj, Nameset* nset, Cons* args);

  /// get the host domain name
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sys_domainname (Runnable* robj, Nameset* nset, Cons* args);

  /// get the host name
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sys_hostname (Runnable* robj, Nameset* nset, Cons* args);

  /// get the user name
  /// @param robj the current runnable
  /// @param nset the current nameset
  /// @param args the argument list
  Object* sys_username (Runnable* robj, Nameset* nset, Cons* args);
}

#endif
